# Basic Redis ElastiCache Template

This CloudFormation template will deploy an AWS ElastiCache instance of Redis,
along with required supporting resources.

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
