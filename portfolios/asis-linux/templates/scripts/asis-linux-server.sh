#!/bin/bash

# ASIS Linux Server deployment script, used to configure the
# instance at deployment time.

# Import deployment time variables, including:
# ${HostedZoneName}
# ${ServerName}
# ${AWS::Region} as ${AWSRegion}
# ${GroupName}
# ${S3BucketName}
# ${EFSId}
# ${WORKINGPATH}
# ${SNS_NOTIFICATION_TOPIC}
# ${WebServer}

# Debug on
#set -x

# Check if params file passed as an argument exists, and if so import it, otherwise fail
if [ -f $1 ]; then
  . $1
else
  echo "Configuration parameters for instance deployment missing or not provided: $1"
  exit 1
fi

# Register with Route53
yum -y install jq
export AWSAZ=$(curl -s "http://169.254.169.254/latest/meta-data/placement/availability-zone")
export AWSHOSTIP=$(curl -s "http://169.254.169.254/latest/meta-data/local-ipv4")
export AWSZONEID=$(aws route53 list-hosted-zones-by-name --dns-name ${HostedZoneName} | \
          jq '.HostedZones[] | select(.Name|test("'${HostedZoneName}'.$")).Id' | \
          sed 's|[^A-Z0-9]||g')
aws route53 change-resource-record-sets --hosted-zone-id $AWSZONEID --cli-input-json '{
    "HostedZoneId": "'$AWSZONEID'",
    "ChangeBatch": {
        "Comment": "Update registration for this instance",
        "Changes": [
            {
                "Action": "UPSERT",
                "ResourceRecordSet": {
                    "Name": "'${ServerName}'-'${AWSAZ: -1}'.'${HostedZoneName}'",
                    "Type": "A",
                    "TTL": 200,
                    "ResourceRecords": [
                        {
                            "Value": "'$AWSHOSTIP'"
                        }
                    ]
                }
            }
        ]
    }
}'
# Set hostname
hostnamectl set-hostname "${ServerName}-${AWSAZ: -1}.${HostedZoneName}"
# INSTALL UPDATES
yum check-update
YUMCHECK=$?
# Outstanding patches available
if [ $YUMCHECK -eq 100 ] ; then
  # Apply all OS updates
  yum update -y
  # Reset cloud-init script so userdata runs again on next boot
  rm /var/lib/cloud/instance/sem/config_scripts_user
  # Reboot to make the updates active
  shutdown -r now
fi

if [ -f /var/lib/cloud/instance/done-initial.txt ] ; then
  echo "Already completed initial configuration. Exiting."
  exit
fi

# Set the Time Zone to MST (America/Phoenix)
sed -i 's|ZONE="UTC"|ZONE="America/Phoenix"|' /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/America/Phoenix /etc/localtime

# Add local group
groupadd -g 1010 localusers

# Fix root's search PATH
sed -i -r 's|^(PATH=.*)|\1:/usr/local/bin|' /root/.bash_profile

# Install AWS CloudWatch Agent
#
# Still unclear how to properly enable and start the agent with the desired config file. See:
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/install-CloudWatch-Agent-on-EC2-Instance.html
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch-Agent-Configuration-File-Details.html
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/install-CloudWatch-Agent-commandline-fleet.html
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch-Agent-procstat-process-metrics.html
yum -y install amazon-cloudwatch-agent
cp ${WORKINGPATH}/amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
systemctl enable amazon-cloudwatch-agent.service
systemctl start amazon-cloudwatch-agent.service

# INSTALL INSPECTOR AGENT
# only do this once OS is patched and updated
cd /tmp
curl -O https://inspector-agent.amazonaws.com/linux/latest/install
bash install
rm ./install

# INSTALL SOPHOS AGENT
mkdir -p /tmp/sophosinstaller
export AGENT_SOPHOS_URL=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/sophos/linux_url" --output text --query Parameter.Value)
/usr/bin/curl "$AGENT_SOPHOS_URL" -o /tmp/sophosinstaller/SophosInstall.sh
/bin/bash /tmp/sophosinstaller/SophosInstall.sh

# INSTALL TENABLE/NESSUS AGENT
#mkdir -p /tmp/nessusinstaller
#export AGENT_NESSUS_URL=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/linux_url" --output text --query Parameter.Value)
#export AGENT_NESSUS_HOST=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f1)
#export AGENT_NESSUS_PORT=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f2)
#export AGENT_NESSUS_KEY=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/key" --with-decryption --output text --query Parameter.Value)
#aws s3 cp "$AGENT_NESSUS_URL" /tmp/nessusinstaller/
#rpm -vhU /tmp/nessusinstaller/NessusAgent*.rpm
#/opt/nessus_agent/sbin/nessuscli agent link --key=$AGENT_NESSUS_KEY --host=$AGENT_NESSUS_HOST --port=$AGENT_NESSUS_PORT --groups="CloudOps, CloudOps-Linux"
#systemctl enable nessusagent.service
#systemctl start nessusagent.service

# INSTALL AZURE OMS LOG ANALYTICS AGENT
# Get the OMS Agent
yum -y install curl wget
mkdir -p /tmp/omsinstaller
/usr/bin/curl 'https://raw.githubusercontent.com/Microsoft/OMS-Agent-for-Linux/master/installer/scripts/onboard_agent.sh' -o /tmp/omsinstaller/onboard_agent.sh
# Install the OMS Agent
cd /tmp/omsinstaller
export CLOPS_OMS_WORKSPACE=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/workspaceid" --with-decryption --output text --query Parameter.Value)
export CLOPS_OMS_PRIMKEY=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/primarykey" --with-decryption --output text --query Parameter.Value)
/bin/bash /tmp/omsinstaller/onboard_agent.sh -w "$CLOPS_OMS_WORKSPACE" -s "$CLOPS_OMS_PRIMKEY" -d opinsights.azure.com

# INSTALL/CONFIGURE AUTHENTICATION
# Install and configure NLNet Labs Unbound DNS resolver
yum -y install unbound
# Get fresh root hints
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints
# Configure Unbound service
sed -i~ -e 's/harden-dnssec-stripped: yes/harden-dnssec-stripped: no/' \
        -e '/# root-hints: ""/a\
        root-hints: "/etc/unbound/root.hints"' \
        -e 's/module-config: "ipsecmod validator iterator"/#module-config: "ipsecmod validator iterator"/' \
        -e '/#module-config: "ipsecmod validator iterator"/a\
        module-config: "iterator"' \
        /etc/unbound/unbound.conf
# Forward zone configuration
cat <<EOFUNBOUND >/etc/unbound/conf.d/aws-uarizona.conf
forward-zone:
    name: "${AWSRegion}.compute.internal"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "amazonaws.com"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "."
    forward-addr: 128.196.11.233
    forward-addr: 128.196.11.234
EOFUNBOUND
# Enable and restart the unbound service
systemctl enable unbound.service
systemctl restart unbound.service

# Override DHCP assigned DNS servers used for lookups
# https://aws.amazon.com/premiumsupport/knowledge-center/ec2-static-dns-ubuntu-debian/
cat <<EOFDNS >/etc/dhcp/dhclient.conf
timeout 300;
append domain-search "uits.arizona.edu", "catnet.arizona.edu", "bluecat.arizona.edu";
supersede domain-name-servers 127.0.0.1;
EOFDNS
systemctl restart network.service

# Authentication configuration
# Install and configure sssd, kerberos PAM module,
# and oddjob packages to enable AD authentication
yum -y install oddjob-mkhomedir sssd nss_ldap pam_krb5 \
      oddjob openldap-clients realmd samba-common-tools \
      adcli krb5-workstation policycoreutils-python

mv /etc/nsswitch.conf /etc/nsswitch.conf.orig
cat <<EOF1 >/etc/nsswitch.conf
passwd:     files sss
shadow:     files
group:      files sss
sudoers:    files

hosts:      files dns

bootparams: nisplus [NOTFOUND=return] files

ethers:     files
netmasks:   files
networks:   files
protocols:  files
rpc:        files
services:   files

netgroup:   files

publickey:  nisplus

automount:  files
aliases:    files nisplus
EOF1

# Update kerberos logging destination
mkdir /var/log/krb5
chown sssd:sssd /var/log/krb5
chmod 750 /var/log/krb5
sed -i~ 's|/var/log/|/var/log/krb5/|g' /etc/krb5.conf

# Join AD using parameters pulled from SSM
export AD_JOIN_USER=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/user" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_PASSWD=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/password" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_DOMAIN=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/domain" --with-decryption --output text --query Parameter.Value)
echo $AD_JOIN_PASSWD | realm join --user=$AD_JOIN_USER --computer-name=${ServerName} $AD_JOIN_DOMAIN

# Change authentication and homedir designation so it's not fully qualified
sed -i~ 's|use_fully_qualified_names = True|use_fully_qualified_names = False|;s|fallback_homedir = /home/%u@%d|fallback_homedir = /home/%u|' /etc/sssd/sssd.conf

# Set local login permitted groups
mv /etc/security/local.allowed_groups /etc/security/local.allowed_groups.orig
cat <<EOF5 >/etc/security/local.allowed_groups
root
wheel
localusers
is-oc
${GroupName}
EOF5

# Reconfigure PAM
mv /etc/pam.d/system-auth /etc/pam.d/system-auth.orig
cat <<EOF6 >/etc/pam.d/system-auth
auth        required      pam_listfile.so onerr=fail item=group sense=allow file=/etc/security/local.allowed_groups
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        sufficient    pam_sss.so use_first_pass
auth        required      pam_deny.so

account     required      pam_unix.so broken_shadow
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account     [default=bad success=ok user_unknown=ignore] pam_sss.so
account     required      pam_permit.so

password    requisite     pam_cracklib.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha256 shadow nullok try_first_pass use_authtok
password    sufficient    pam_sss.so use_authtok
password    required      pam_deny.so

session     required      pam_oddjob_mkhomedir.so skel=/etc/skel/ umask=0022
session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session     optional      pam_sss.so
EOF6

mv /etc/pam.d/password-auth /etc/pam.d/password-auth.orig
cp /etc/pam.d/system-auth /etc/pam.d/password-auth

systemctl restart sssd.service
systemctl restart nslcd.service
systemctl restart oddjobd.service

# Configure sudoers for the service owning group
cat <<EOF7 > /etc/sudoers.d/99-service-owners
# The provisioning/service owning group has full access
%${GroupName} ALL=(root)   ALL
EOF7
chmod 440 /etc/sudoers.d/99-service-owners

# Configure sshd to permit login from authenticated users in the owning group
sed -i.orig \
    -e 's/^PasswordAuthentication .*$/PasswordAuthentication yes/' \
    -e 's/^GSSAPICleanupCredentials .*$/GSSAPICleanupCredentials yes/' \
    -e '/Subsystem sftp/a \' \
    -e '\nAllowGroups root wheel localusers is-oc '${GroupName} \
    /etc/ssh/sshd_config

systemctl restart sshd.service

# Import ssh public keys for admin users
mkdir -p /tmp/pubkeys
aws s3 sync s3://${S3BucketName}/pubkeys/ /tmp/pubkeys/
for u in ec2-user; do
  sudo -i -u $u sh -c \
  "if [ ! -d ~/.ssh ]; then \
    mkdir ~/.ssh ; \
    chmod 700 ~/.ssh ; \
  fi ; \
  cat /tmp/pubkeys/* >> ~/.ssh/authorized_keys ; \
  chmod 644 ~/.ssh/authorized_keys"
done
rm -fr /tmp/pubkeys

# Any final automation of copy media and installation of extra components
# Optionally install httpd web services
if [ "$WEBSERVER" == "true" ]; then
  yum -y install httpd
  # Maybe copy in a website and set up other stuff
  cp ${WORKINGPATH}/*.html /var/www/html/
  # Enable and start the web server
  systemctl enable httpd.service
  systemctl start httpd.service
fi

# Install additional dependencies
## git and GNU Debugger
yum -y install git gdb
## GNU Parallel
#(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - pi.dk/3) | bash

# Add file store connection information to config file
echo -en "S3BUCKET=${S3BucketName}\n" > /etc/asis-linux.files
echo -en "SNS_NOTIFICATION_TOPIC=${SNS_NOTIFICATION_TOPIC}\n" >> /etc/asis-linux.files
if [ "$EFSId" != "" ]; then
  echo -en "EFSDNS=${EFSId}.efs.${AWSRegion}.amazonaws.com\n" >> /etc/asis-linux.files
fi
if [ "$DRS3BucketName" != "" ]; then
  echo -en "DRS3BUCKET=s3://${DRS3BucketName}/\n" >> /etc/asis-linux.files
fi
chmod 644 /etc/asis-linux.files

# Placeholder for the EFS and ISO mounts
mkdir -p /mounts/efs
if [ "$EFSId" != "" ]; then
  yum -y install amazon-efs-utils
  . /etc/controlm.files ; echo "$EFSDNS:/     /controlm/efs    efs    defaults,_netdev          0   0" >> /etc/fstab
  mount /controlm/efs/
fi

# Last line
touch /var/lib/cloud/instance/done-initial.txt

# Debug off
#set +x
