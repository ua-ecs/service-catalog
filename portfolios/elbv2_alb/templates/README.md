# General Purpose ELBv2/ALB Redirector

## Template Description:

This template can be used as the basis for a standalone ALB deployment to handle redirection for URLs that should be redirected to new URLs, whether that includes a simple rewrite of parts of the full URL, or blind redirects, discarding the path and query portion of the URL.

This template also defaults to providing HTTP to HTTPS redirection before performing the final redirection to the specified destination.

**NOTE:** The template is not usable in this form. A number of changes need to be made to adapt it to a specific use case. The template also depends on the availability of the `fdn-cf-account-info` lambda.

## Template customization:

The major customization needed to the template are as follows:

1. In the `AlbRedirect1` and `AlbRedirect2` resource blocks, adjust the `RedirectConfig` and `HostHeaderConfig` values as needed. For example:

        AlbRedirect1:
          Type: AWS::ElasticLoadBalancingV2::ListenerRule
          Properties:
            Actions:
              # Permanent redirect preserving path and query strings
              - Type: redirect
                RedirectConfig:
                  Protocol: HTTPS
                  Port: "443"
                  Host: 'target-host-1'
                  Path: '/#{path}'
                  Query: '#{query}'
                  StatusCode: HTTP_301
            Conditions:
              - Field: host-header
                HostHeaderConfig:
                  Values:
                    - 'orig-host-1'
                    - 'orig-host-2'

  can become:

        AlbRedirect1:
          Type: AWS::ElasticLoadBalancingV2::ListenerRule
          Properties:
            Actions:
              # Blind permanent redirect
              - Type: redirect
                RedirectConfig:
                  Protocol: HTTPS
                  Port: "443"
                  Host: 'it.arizona.edu'
                  Path: '/get-support'
                  Query: ''
                  StatusCode: HTTP_301
            Conditions:
              - Field: host-header
                HostHeaderConfig:
                  Values:
                    - '247.arizona.edu'
                    - '*.247.arizona.edu'
                    - 'computerhelp.arizona.edu'
                    - '*.computerhelp.arizona.edu'

  **Note** that there are a maximum of 5 Values per ELBv2::ListenerRule. If you need more than 5 domain matches for the HostHeaderConfig values, split them into multiple rules with a maximum of 5 in each rule. This is a fixed maximum and cannot be altered.

1. If only one set of redirects is needed, comment out the `AlbRedirect2` resource block. If you need more than 2 distinct sets of redirects, add an `AlbRedirect3` or more as needed to define the additional types of redirects.

1. If a default redirect destination is needed or a default fixed-response is needed besides the included one, edit the `AlbListener443` resource to modify the `DefaultActions` section to something other than what's included. See the [AWS ELBv2 Listener](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-elasticloadbalancingv2-listener-action.html) documentation for more on how to configure this section.

## Deploying the template

Prior to deploying the customized template, some additional resources should be provisioned:

1. Ensure the `fdn-cf-account-info` lambda is already deployed in the target account. This template can be deployed without it, but will require modifying the parameters to allow direct input of the VPC, public subnets, and private subnets IDs.

1. Import or create and validate an SSL certificate in ACM that contains all the domain variations used in the template. For example, the above sample would require an SSL certificate with the following domain names included as Subject Alternative Names (SANs):

    - '247.arizona.edu'
    - '*.247.arizona.edu'
    - 'computerhelp.arizona.edu'
    - '*.computerhelp.arizona.edu'

  get the ARN of the new SSL certificate in ACM, as that will be used as one of the input parameters. **Note** that ACM by default has a limit of 10 domains per SSL certificate. This can be raised up to a max of 100 domains per SSL certificate by placing a request to increase it using the forms in AWS Service Quotas.

## Parameters

#### ELBv2/ALB Instance Configuration

  - **AlbSSLCertificateARN:** Full ARN of the SSL Certificate to use on the load balancer
  - **AlbCIDR:** The CIDR allowed to access the ALB. Default: **0.0.0.0/0**
  - **AlbAccessibility:** Should the Web Application Load Balancer be public facing or campus/internal facing only? Default: **public**

#### Tagging and Cost Management

  - **ServiceTag:** Exact name of the Service as defined in the service catalog.
  - **EnvironmentTag:** Used to distinguish between development, test, or production environment types. Default: tst
  - **ContactNetidTag:** Used to identify the netid of the person most familiar with the usage of the resource.
  - **AccountNumberTag:** Identifies the financial system account number.
  - **TicketNumberTag:** Used to identify the Jira, Cherwell, or other ticketing system ticket number to link to more information about the need for the resource.
