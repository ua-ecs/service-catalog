Basic CloufFormation example of the AppConfig Lambda Extension
==============================================================

References:
https://aws.amazon.com/blogs/mt/introducing-aws-appconfig-lambda-extension-deploying-application-configuration-serverless/
https://docs.aws.amazon.com/appconfig/latest/userguide/what-is-appconfig.html
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/AWS_AppConfig.html
https://docs.aws.amazon.com/appconfig/latest/userguide/appconfig-integration-lambda-extensions.html


For this demo I've implemented the basic Hello World example from the [AppConfig introductory blog post][1].

If you have npm installed, and current AWS CLI credentials, you should be able to deploy this template with `npm run deploy`.

[1]: https://aws.amazon.com/blogs/mt/introducing-aws-appconfig-lambda-extension-deploying-application-configuration-serverless/
