#!/bin/bash

SERVICE_NAME="helloworld-appconfig"

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source $SCRIPTPATH/env.sh


aws cloudformation deploy \
--stack-name $SERVICE_NAME \
--region "$AWS_REGION" \
--template-file /$SCRIPTPATH/../templates/appconfig.yaml \
--capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND \
--role-arn $CLOUDFORMATION_DEPLOY_ROLE \
--parameter-overrides \
        "ExclamationPoints=5" \
        "ServiceName=$SERVICE_NAME"
