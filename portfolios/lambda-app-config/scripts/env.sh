#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

export AWS_ACCESS_KEY_ID=`cat /tmp/awscli.json | jq -r '.Credentials.AccessKeyId'`
export AWS_SESSION_TOKEN=`cat /tmp/awscli.json | jq -r '.Credentials.SessionToken'`
export AWS_SECRET_ACCESS_KEY=`cat /tmp/awscli.json | jq -r '.Credentials.SecretAccessKey'`
export AWS_REGION=us-west-2

# If we have a set of valid AWS credentials loaded, this sts call should return the ARN of the active user
# ie: arn:aws:sts::XYZ123:assumed-role/fdn-SuperAdmin/fischerm@arizona.edu
AUTH_USER_ID=$(aws sts get-caller-identity --output text --query "Arn")
if [[ $AUTH_USER_ID != *"arn:aws"* ]]; then
    echo "AWS Authentication failed"
    echo "Be sure an environment file exists and you have a valid AWS CLI session"
    echo ""
    exit 0
fi

ACCOUNT_NAME=$(aws iam list-account-aliases --query 'AccountAliases[0]' --output text)
echo ""
echo "#####"
echo "#" $ACCOUNT_NAME
echo "#" $AUTH_USER_ID
echo "#####"
echo ""

get_stack_export () {
    EXPORTNAME=$1
    echo $(aws cloudformation list-exports --query "Exports[?Name==\`$EXPORTNAME\`].Value" --output text)
}

get_ssm_param () {
    PARAMNAME=$1
    echo $(aws ssm get-parameter --name $PARAMNAME --with-decryption --output text --query 'Parameter.[Value]')
}

ACCOUNT_NUMBER=$(aws sts get-caller-identity --output text --query "Account")
CLOUDFORMATION_DEPLOY_ROLE=$(get_stack_export fdn-iam-cloudformation-deployer-role-arn)
