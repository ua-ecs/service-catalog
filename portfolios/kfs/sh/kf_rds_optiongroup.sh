#!/bin/bash
#*******************************************************************************
#
# TITLE......: kf_rds_optiongroup.sh
# PARAMETER..:
#    /INPUTS.:
#              -m  Mode C=Create U=Update
#*******************************************************************************

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: kf_rds_optiongroup.sh"
echo "Options:"
echo "       -m (required) = Mode C=Create U=Update"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}


#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make MODE is either C or U
if [ "$MODE" != "C" ] && [ "$MODE" != "U" ]; then
  error_abort "The mode passed must be C for Create or U for update, value passed was $MODE"
fi

# ARN of the Foundational CloudFormation deployer role to use
ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
STACK_NAME="Kuali-Rds-Option-Group"
TEMPLATE_NAME="kfs_rds_optiongroup.yaml"

########
# Tags #
########
# See the overall current tagging strategy here: https://confluence.arizona.edu/display/ECS/Cloud+Tagging+Policy
TAG_SERVICE="UAccess Financials"
TAG_NAME="kfs-rds-option-group"
TAG_ENVIRONMENT="prd"
TAG_CREATEDBY="met"
TAG_CONTACT_NETID="fischerm"
TAG_ACCOUNT_NUMBER="UAccess Financials"
TAG_SUBACCOUNT="UAccess Financials"
TAG_RESOURCE_FUNCTION="RDS Option Group"
TAG_TICKET_NUMBER="UAF-3905"
}

#*******************************************************************************
#
# function: build_environment
#
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

# Current region
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | python -c "import json,sys; print json.loads(sys.stdin.read())['region']")

read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "TagService",     "ParameterValue": "$TAG_SERVICE" },
  { "ParameterKey": "TagName",     "ParameterValue": "$TAG_NAME" },
  { "ParameterKey": "TagEnvironment",     "ParameterValue": "$TAG_ENVIRONMENT" },
  { "ParameterKey": "TagCreatedBy",     "ParameterValue": "$TAG_CREATEDBY" },
  { "ParameterKey": "TagContactNetId",     "ParameterValue": "$TAG_CONTACT_NETID" },
  { "ParameterKey": "TagAccountNumber",     "ParameterValue": "$TAG_ACCOUNT_NUMBER" },
  { "ParameterKey": "TagSubAccount",     "ParameterValue": "$TAG_SUBACCOUNT" },
  { "ParameterKey": "TagResourceFunction",     "ParameterValue": "$TAG_RESOURCE_FUNCTION" },
  { "ParameterKey": "TagTicketNumber",     "ParameterValue": "$TAG_TICKET_NUMBER" }
]
ENDJSON

# -- Create Stack if MODE = C
if [ $MODE = 'C' ]; then
  aws cloudformation create-stack \
    --region "$REGION" \
    --stack-name "$STACK_NAME"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/$TEMPLATE_NAME" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$TAG_NAME" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATEDBY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUBACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION" 

 aws cloudformation wait stack-create-complete \
    --region "$REGION" \
    --stack-name "$STACK_NAME"
fi

# -- Update Stack if MODE = U
if [ $MODE = 'U' ]; then
  aws cloudformation update-stack \
    --region "$REGION" \
    --stack-name "$STACK_NAME"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/$TEMPLATE_NAME" \
    --role-arn $ROLE_ARN \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$TAG_NAME" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATEDBY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUBACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION"

 aws cloudformation wait stack-update-complete \
    --region "$REGION" \
    --stack-name "$STACK_NAME"
fi

}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`

# -- parse command-line options
while getopts :m: arguments
do
  case $arguments in
    m)
       MODE=${OPTARG^^}
       ;;
   \?)
       print_usage

       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments

# -- if we reach this point we can build the environment
build_environment

