#!/bin/bash
 
#TODO Populate with production account values
# ARN of the Foundational CloudFormation deployer role to use
ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
 
#TODO Enter deployer's NetID here
TAG_CREATED_BY="kellehs"
 
#TODO Modify region if necessary
REGION="us-west-2"
 
# These values do not need to be modified
STACK_NAME="kfs-cloudwatch-logs-role"
SERVICE_NAME="UAccess Financials"
SERVICE_SLUG=$STACK_NAME
TAG_SERVICE=$SERVICE_NAME
TAG_NAME=$STACK_NAME
TAG_ENVIRONMENT="prd"
TAG_CONTACT_NETID="quikkian"
TAG_ACCOUNT_NUMBER=$SERVICE_NAME
TAG_SUB_ACCOUNT=$SERVICE_NAME
TAG_TICKET_NUMBER="UAFAWS-238"
TAG_RESOURCE_FUNCTION="KFS CloudWatch Logs Read-only Role"
 
#This stack has no parameters -- all information is found from
#  outputs of other stacks -- so no need to build/read a JSON blob.
aws cloudformation create-stack \
    --region $REGION \
    --stack-name $STACK_NAME  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/kfs_cloudwatchlogs_role.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$STACK_NAME-CloudFormation-stack" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATED_BY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUB_ACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION CloudFormation Stack"
                   
aws cloudformation wait stack-create-complete \
    --region $REGION \
    --stack-name $STACK_NAME
