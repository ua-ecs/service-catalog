#!/bin/bash

# local variables
# for refrence:
#  ua-uits-kuali-nonprod AWS account: 397167497055
#  ua-uits-kuali-prod AWS account: 740525297805
#  DR AWS account: 507937877461, region=us-east-1
source_ecr_registry_id="397167497055"
target_ecr_registry_id="740525297805"
ecr_region="us-west-2"
target_ecr_region="us-west-2"
kfs_repository_name="kuali/kfs7"
kfs_image_prefix="7.20191031-ua-release"
rice_repository_name="kuali/rice"
rice_image_prefix="2.7.0.1-ua-release"
rhubarb_repository_name="kuali/rhubarb"
sup_rhubarb_image_prefix="sup7-release"
prd_rhubarb_image_prefix="prd7-release"
maintenance_repository_name="kuali/maintenance"
maintenance_image_prefix="maint-"

# Prompt user for values
echo -n "Source ECR region (default: $ecr_region): "
read input_ecr_region
if [[ $input_ecr_region ]]; then
    echo "Using ECR region: $input_ecr_region"
    ecr_region=$input_ecr_region
fi

echo -n "Target ECR region (default: $target_ecr_region): "
read input_target_ecr_region
if [[ $input_target_ecr_region ]]; then
    echo "Using Target ECR region: $input_target_ecr_region"
    target_ecr_region=$input_target_ecr_region
fi

echo -n "Input ECR registry ID (aka AWS account ID) to pull images from (default: $source_ecr_registry_id): "
read input_source_ecr_registry_id
if [[ $input_source_ecr_registry_id ]]; then
    echo "Using source ECR registry ID: $input_source_ecr_registry_id"
    source_ecr_registry_id=$input_source_ecr_registry_id
fi

echo -n "Input production ECR registry ID (aka AWS account ID) to push to (default: $target_ecr_registry_id): "
read input_target_ecr_registry_id
if [[ $input_target_ecr_registry_id ]]; then
    echo "Using source ECR registry ID: $input_target_ecr_registry_id"
    target_ecr_registry_id=$input_target_ecr_registry_id
fi

echo -n "Input KFS repository name (default: $kfs_repository_name): "
read input_kfs_repository_name
if [[ $input_kfs_repository_name ]]; then
    echo "Using KFS repository: $input_kfs_repository_name"
    kfs_repository_name=$input_kfs_repository_name
fi

echo -n "Input Rice repository name (default: $rice_repository_name): "
read input_rice_repository_name
if [[ $input_rice_repository_name ]]; then
    echo "Using Rice repository: $input_rice_repository_name"
    rice_repository_name=$input_rice_repository_name
fi

echo -n "Input Rhubarb repository name (default: $rhubarb_repository_name): "
read input_rhubarb_repository_name
if [[ $input_rhubarb_repository_name ]]; then
    echo "Using Rhubarb repository: $input_rhubarb_repository_name"
    rhubarb_repository_name=$input_rhubarb_repository_name
fi

echo -n "Input KFS app version for Docker image prefix (default: $kfs_image_prefix): "
read input_kfs_image_prefix
if [[ $input_kfs_image_prefix ]]; then
    echo "Using KFS app version: $input_kfs_image_prefix"
    kfs_image_prefix=$input_kfs_image_prefix
fi

echo -n "Input Rice app version for Docker image prefix (default: $rice_image_prefix): "
read input_rice_image_prefix
if [[ $input_rice_image_prefix ]]; then
    echo "Using Rice app version: $input_rice_image_prefix"
    rice_image_prefix=$input_rice_image_prefix
fi

echo -n "Input release version number (just the number): "
read release_number
echo "Will copy release $release_number Docker images."

echo -n "Input maintenance tag date: "
read maint_date
echo "Will copy $maint_date maintenance Docker image."

# Create ECR repository URIs
source_ecr_registry="$source_ecr_registry_id.dkr.ecr.$ecr_region.amazonaws.com"
target_ecr_registry="$target_ecr_registry_id.dkr.ecr.$target_ecr_region.amazonaws.com"

# Create Docker image names
kfs_image="$kfs_image_prefix$release_number"
rice_image="$rice_image_prefix$release_number"
sup_rhubarb_image="$sup_rhubarb_image_prefix$release_number"
prd_rhubarb_image="$prd_rhubarb_image_prefix$release_number"
maint_image="$maintenance_image_prefix$maint_date"

# print all values for sanity check
echo ""
echo "Source ECR registry: $source_ecr_registry"
echo "Target ECR registry: $target_ecr_registry"
echo "KFS repository name: $kfs_repository_name"
echo "Rice repository name: $rice_repository_name"
echo "Rhubarb repository name: $rhubarb_repository_name"
echo "Maintenance repository name: $maintenance_repository_name"
echo "KFS app version: $kfs_image_prefix"
echo "Rice app version: $rice_image_prefix"
echo "Release version: $release_number"
echo "KFS image: $kfs_image"
echo "Rice image: $rice_image"
echo "Rhubarb images: $sup_rhubarb_image and $prd_rhubarb_image"
echo "Maintenance image: $maint_image"
echo ""

# Have user verify values are OK before continuing
read -p "Press Enter to continue... "

# Log into source and target AWS ECR registries. Use login profile in production account.
#sudo $(aws --profile nonprod ecr get-login --region $ecr_region --no-include-email --registry-ids $source_ecr_registry_id)
sudo $(aws ecr get-login --region $ecr_region --no-include-email --registry-ids $source_ecr_registry_id)
sudo $(aws ecr get-login --region $target_ecr_region --no-include-email --registry-ids $target_ecr_registry_id)

# pull production images from source ECR repository and tag with target repository server
sudo docker pull $source_ecr_registry/$kfs_repository_name:$kfs_image
sudo docker tag $source_ecr_registry/$kfs_repository_name:$kfs_image $target_ecr_registry/$kfs_repository_name:$kfs_image
sudo docker pull $source_ecr_registry/$rice_repository_name:$rice_image
sudo docker tag $source_ecr_registry/$rice_repository_name:$rice_image $target_ecr_registry/$rice_repository_name:$rice_image
sudo docker pull $source_ecr_registry/$rhubarb_repository_name:$sup_rhubarb_image
sudo docker tag $source_ecr_registry/$rhubarb_repository_name:$sup_rhubarb_image $target_ecr_registry/$rhubarb_repository_name:$sup_rhubarb_image
sudo docker pull $source_ecr_registry/$rhubarb_repository_name:$prd_rhubarb_image
sudo docker tag $source_ecr_registry/$rhubarb_repository_name:$prd_rhubarb_image $target_ecr_registry/$rhubarb_repository_name:$prd_rhubarb_image
sudo docker pull $source_ecr_registry/$maintenance_repository_name:$maint_image
sudo docker tag $source_ecr_registry/$maintenance_repository_name:$maint_image $target_ecr_registry/$maintenance_repository_name:$maint_image

# push image to ECS repo
sudo docker push $target_ecr_registry/$kfs_repository_name:$kfs_image
sudo docker push $target_ecr_registry/$rice_repository_name:$rice_image
sudo docker push $target_ecr_registry/$rhubarb_repository_name:$sup_rhubarb_image
sudo docker push $target_ecr_registry/$rhubarb_repository_name:$prd_rhubarb_image
sudo docker push $target_ecr_registry/$maintenance_repository_name:$maint_image

# clean up local copies of images
sudo docker rmi -f $(sudo docker images | awk '{print $3}')
