#!/bin/bash

# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases --output text|awk '{print $2}'`
 
#TODO Populate with production account values
# Enter the CloudFormation stack name the pre-existing VPC designated for KFS
VPC_NAME="kuali-vpc"

# Current region
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | python -c "import json,sys; print json.loads(sys.stdin.read())['region']")

if [ $AWS_ACCOUNT = "ua-uits-kuali-nonprod" ]; then
  # Enter the CloudFormation stack name of the Route 53 hosted zone for KFS
  ROUTE_53_STACK_NAME="kfs-route53"
  # Enter the name of the EC2 keypair created for the SFTP Hub EC2 instance
  EC2_KEYPAIR_NAME="kuali-nonprod-keypair"
  # Enter the HTTP URL to the cookbooks uploaded in the 'Deployment Artifacts' section
  CUSTOM_COOKBOOKS_SOURCE="https://s3-${REGION}.amazonaws.com/kfs-cloudformation-deployment/kfs3-cookbooks.tar.gz"
  # Enter the name of the S3 bucket created in the 'Deployment Artifacts' section
  S3_RESOURCE_BUCKET="kfs-cloudformation-deployment"
  # Name of the CloudFormation Stack created in the 'EFS' section
  EFS_STACK_NAME="kfs3-efs"
  EFS_STACK_NAME2="kfs7-nonprod-efs"
  # ARN of the Foundational CloudFormation deployer role to use
  ROLE_ARN="arn:aws:iam::397167497055:role/fdn-iam-CloudFormationAdminDeployerRole-12AJX3KHD35CR"
  #TODO Change environment slug if necessary
  ENV_SLUG=nonprod
else
  # Enter the CloudFormation stack name of the Route 53 hosted zone for KFS
  ROUTE_53_STACK_NAME="kuali-prod-route53"
  # Enter the name of the EC2 keypair created for the SFTP Hub EC2 instance
  EC2_KEYPAIR_NAME="kuali-prod-keypair"
  # Enter the HTTP URL to the cookbooks uploaded in the 'Deployment Artifacts' section
  CUSTOM_COOKBOOKS_SOURCE="https://s3-${REGION}.amazonaws.com/kfs-prod-cloudformation-deployment/kfs3-cookbooks.tar.gz"
  # Enter the name of the S3 bucket created in the 'Deployment Artifacts' section
  S3_RESOURCE_BUCKET="kfs-prod-cloudformation-deployment"
  # Name of the CloudFormation Stack created in the 'EFS' section
  EFS_STACK_NAME="kfs-efs"
  EFS_STACK_NAME2="kfs-efs"
  # ARN of the Foundational CloudFormation deployer role to use
  ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
  #TODO Change environment slug if necessary
  ENV_SLUG=prd
fi

 
# IAM Access credentials for Docker repository access. See the Stache secret 'aws/ua-erp/docker-registry-access'
DOCKER_REPO_ACCESS_KEY=`aws ssm get-parameter --name AppDockerAccessKey --with-decryption --output text --query 'Parameter.[Value]'`
DOCKER_REPO_SECRET_KEY=`aws ssm get-parameter --name AppDockerSecretKey --with-decryption --output text --query 'Parameter.[Value]'`
 
 
#TODO Enter deployer's NetID here
TAG_CREATED_BY="kellehs"
 
# These values do not need to be modified
STACK_NAME="kfs-sftp-$ENV_SLUG"
SERVICE_NAME="Kuali SFTP Hub"
SERVICE_SLUG=$STACK_NAME
APP_DOCKER_IMAGE="760232551367.dkr.ecr.${REGION}.amazonaws.com/eas/sftphub:2017-10-04"
TAG_SERVICE=$SERVICE_NAME
TAG_NAME=$STACK_NAME
TAG_ENVIRONMENT="$ENV_SLUG"
TAG_CONTACT_NETID="quikkian"
TAG_ACCOUNT_NUMBER=$SERVICE_NAME
TAG_SUB_ACCOUNT=$SERVICE_NAME
TAG_TICKET_NUMBER="UAFAWS-238"
TAG_RESOURCE_FUNCTION="KFS SFTP Hub"
 
 
read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "EnvAppName", "ParameterValue": "$SERVICE_NAME" },
  { "ParameterKey": "AppSlug", "ParameterValue": "$SERVICE_SLUG" },
  { "ParameterKey": "VPCName", "ParameterValue": "$VPC_NAME" },
  { "ParameterKey": "Route53StackName", "ParameterValue": "$ROUTE_53_STACK_NAME" },
  { "ParameterKey": "KeyName", "ParameterValue": "$EC2_KEYPAIR_NAME" },
  { "ParameterKey": "CustomCookbooksSource",  "ParameterValue": "$CUSTOM_COOKBOOKS_SOURCE" },
  { "ParameterKey": "CustomCookbooksBucket",  "ParameterValue": "$S3_RESOURCE_BUCKET" },
  { "ParameterKey": "AppDockerImage",  "ParameterValue": "$APP_DOCKER_IMAGE" },
  { "ParameterKey": "DockerRepoAccessKey",  "ParameterValue": "$DOCKER_REPO_ACCESS_KEY" },
  { "ParameterKey": "DockerRepoSecretKey",  "ParameterValue": "$DOCKER_REPO_SECRET_KEY" },
  { "ParameterKey": "EFSStackName",  "ParameterValue": "$EFS_STACK_NAME" },
  { "ParameterKey": "EFSStackName2",  "ParameterValue": "$EFS_STACK_NAME2" },
  { "ParameterKey": "TagService", "ParameterValue": "$TAG_SERVICE" },
  { "ParameterKey": "TagName", "ParameterValue": "$TAG_NAME" },
  { "ParameterKey": "TagEnvironment", "ParameterValue": "$TAG_ENVIRONMENT" },
  { "ParameterKey": "TagCreatedBy", "ParameterValue": "$TAG_CREATED_BY" },
  { "ParameterKey": "TagContactNetId", "ParameterValue": "$TAG_CONTACT_NETID" },
  { "ParameterKey": "TagAccountNumber", "ParameterValue": "$TAG_ACCOUNT_NUMBER" },
  { "ParameterKey": "TagSubAccount", "ParameterValue": "$TAG_SUB_ACCOUNT" },
  { "ParameterKey": "TagTicketNumber", "ParameterValue": "$TAG_TICKET_NUMBER" },
  { "ParameterKey": "TagResourceFunction", "ParameterValue": "$TAG_RESOURCE_FUNCTION" }
]
ENDJSON
 
aws cloudformation create-stack \
    --region $REGION \
    --stack-name $STACK_NAME  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/kfs_sftp_hub.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$STACK_NAME-CloudFormation-stack" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATED_BY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUB_ACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION CloudFormation Stack"
                   
aws cloudformation wait stack-create-complete \
    --region $REGION \
    --stack-name $STACK_NAME
