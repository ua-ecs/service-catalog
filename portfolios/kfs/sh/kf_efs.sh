#!/bin/bash
 
#TODO Populate with production values before running
# VPC ID of the VPC to create the application stack in
VPC_ID="vpc-c6c639a0"
# Subnet ID of Private Subnet A
PRIVATE_SUBNET_A="subnet-f31b38ba"
# Subnet ID of Private Subnet B
PRIVATE_SUBNET_B="subnet-910901f6"
# ARN of the Foundational CloudFormation deployer role to use
ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
 
#TODO Enter deployer's NetID here
TAG_CREATED_BY="kellehs"
 
# Current region
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | python -c "import json,sys; print json.loads(sys.stdin.read())['region']")
 
# These values do not need to be modified
STACK_NAME="kfs-efs"
SERVICE_NAME="UAccess Financials"
SERVICE_SLUG=$STACK_NAME
TAG_SERVICE=$SERVICE_NAME
TAG_NAME=$STACK_NAME
TAG_ENVIRONMENT="prd"
TAG_CONTACT_NETID="kellehs"
TAG_ACCOUNT_NUMBER=$SERVICE_NAME
TAG_SUB_ACCOUNT=$SERVICE_NAME
TAG_TICKET_NUMBER="UAFAWS-238"
TAG_RESOURCE_FUNCTION="KFS EFS Space"
 
read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "VPCID", "ParameterValue": "$VPC_ID" },
  { "ParameterKey": "PrivSubnetA",   "ParameterValue": "$PRIVATE_SUBNET_A" },
  { "ParameterKey": "PrivSubnetB",   "ParameterValue": "$PRIVATE_SUBNET_B" },
  { "ParameterKey": "EnvAppName", "ParameterValue": "$SERVICE_NAME" },
  { "ParameterKey": "AppSlug", "ParameterValue": "$SERVICE_SLUG" },
  { "ParameterKey": "TagService", "ParameterValue": "$TAG_SERVICE" },
  { "ParameterKey": "TagName", "ParameterValue": "$TAG_NAME" },
  { "ParameterKey": "TagEnvironment", "ParameterValue": "$TAG_ENVIRONMENT" },
  { "ParameterKey": "TagCreatedBy", "ParameterValue": "$TAG_CREATED_BY" },
  { "ParameterKey": "TagContactNetId", "ParameterValue": "$TAG_CONTACT_NETID" },
  { "ParameterKey": "TagAccountNumber", "ParameterValue": "$TAG_ACCOUNT_NUMBER" },
  { "ParameterKey": "TagSubAccount", "ParameterValue": "$TAG_SUB_ACCOUNT" },
  { "ParameterKey": "TagTicketNumber", "ParameterValue": "$TAG_TICKET_NUMBER" },
  { "ParameterKey": "TagResourceFunction", "ParameterValue": "$TAG_RESOURCE_FUNCTION" }
]
ENDJSON
 
aws cloudformation create-stack \
    --region $REGION \
    --stack-name $STACK_NAME  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/efs_volume/templates/efs_volume.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$STACK_NAME-CloudFormation-stack" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATED_BY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUB_ACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION CloudFormation Stack"
                  
aws cloudformation wait stack-create-complete \
    --region $REGION \
    --stack-name $STACK_NAME
