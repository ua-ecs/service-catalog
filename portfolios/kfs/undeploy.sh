#!/bin/bash

STACK_NAME=$1

aws cloudformation delete-stack \
  --profile uapilotskfs \
  --stack-name $STACK_NAME \
  --role-arn "arn:aws:iam::998687558142:role/kfs_deployer_role"

