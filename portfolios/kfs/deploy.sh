#!/bin/bash

STACK_NAME=$1

aws cloudformation create-stack \
  --profile uapilotskfs \
  --stack-name $STACK_NAME \
  --capabilities CAPABILITY_IAM \
  --on-failure DO_NOTHING \
  --role-arn "arn:aws:iam::998687558142:role/kfs_deployer_role" \
  --template-body "file://$PWD/templates/kfs_opsworks.yaml" \
  --parameters "file://$PWD/../../../misc/kfs-parameters.json"

