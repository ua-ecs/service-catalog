---
# Basic EC2 Template
# -----------------------------------------
#
# This CloudFormation template creates an ALB, an EC2 instance that mounts an EFS volume,
# and related resources for developers to build and run the KFS application.
#

AWSTemplateFormatVersion: '2010-09-09'
Description:
  ALB and basic EC2 instance that allows for SSH access and mounts an EFS volume.

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Application Information
      Parameters:
      - AppSlug
      - EnvSlug
      - EFSStackName
      - MountPath
      - S3Bucket
    - Label:
        default: Instance Settings
      Parameters:
      - InstanceType
      - AmazonLinuxAmi
      - KeyName
    - Label:
        default: Network Settings
      Parameters:
      - EC2Subnet
      - HostedZoneName
      - SSLCertARN
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagEnvironment
      - TagCreatedBy
      - TagContactNetId
      - TagAccountNumber
      - TagSubAccount
      - TagTicketNumber
      - TagResourceFunction
    ParameterLabels:
      InstanceType:
        default: 'EC2 Instance Type:'
      AmazonLinuxAmi:
        default: 'Amazon Linux 2023 AMI:'
      KeyName:
        default: 'SSH Key Pair Name'

# ### Parameters
# 
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  AppSlug:
    MinLength: '3'
    Type: String
    Description: "Short application slug, ie 'kfs'. Lowercase letters, numbers and dashes only"
    AllowedPattern: "[a-z0-9-]*"
    Default: "kfs"
  EnvSlug:
    MinLength: '2'
    Type: String
    Description: "Short environment slug, ie 'dev', or 'tst'. Lowercase letters, numbers and dashes only"
    AllowedPattern: "[a-z0-9]*"
    Default: "dev"
  EFSStackName:
    Description: "Name of the EFS CloudFormation Stack for the EFS volume we are mounting"
    Type: String
    MinLength: '2'
    Default: "kfs7-nonprod-efs"
  MountPath:
    Description: "Path the user will see as the root directory of the mounted EFS volume."
    Type: String
    Default: "/efs/kfs"
  S3FileBucket:
    Description: 'The name of the S3 bucket with developer resources'
    Type: String
    MinLength: 1
    Default: "kfs-cloudformation-deployment"
  InstanceType:
    Description: "EC2 instance type"
    Type: String
    Default: t3.xlarge
  AmazonLinuxAmi:
    Description: "Amazon Linux 2023 Latest AMI ID"
    Type : 'AWS::SSM::Parameter::Value<String>'
    Default: /aws/service/ecs/optimized-ami/amazon-linux-2023/recommended/image_id
  KeyName:
    Description: "Amazon EC2 Key Pair for SSH access"
    Type: AWS::EC2::KeyPair::KeyName
    Default: "kfs-development-environments-keypair-2021"
  EC2Subnet:
    Description: "Select the private subnet to use for this instance"
    Type: AWS::EC2::Subnet::Id
    MinLength: 1
  HostedZoneName:
    MinLength: '3'
    Type: String
    Description: 'Name of Route53 Hosted Zone: ie ''aws.arizona.edu'''
    Default: "ua-uits-kuali-nonprod.arizona.edu"
  SSLCertARN:
    Description: Application SSL Certificate ARN
    Type: String
    Default: "arn:aws:acm:us-west-2:397167497055:certificate/ad8f66b6-b85e-4715-9df1-1543ebd9702c"
  TagService:
    Description: "Service name (from the service catalog) that is utilizing this resource"
    Type: String
    Default: "UAccess Financials"
  TagEnvironment:
    Description: "Type of environment that is using this resource, such as 'dev', 'tst', 'prd'."
    Type: String
  TagCreatedBy:
    Description: "NetID of the user that created this resource"
    Type: String
  TagContactNetId:
    Description: "NetID of the person to contact for information about this resource"
    Type: String
  TagAccountNumber:
    Description: "Financial system account number for the service utilizing this resource"
    Type: String
    Default: "1192620"
  TagSubAccount:
    Description: "Financial system subaccount number for the service utilizing this resource"
    Type: String
    Default: "12AWS"
  TagTicketNumber:
    Description: "Ticket number that this resource is for"
    Type: String
  TagResourceFunction:
    Description: "Human-readable description of what function this resource is providing"
    Type: String
    Default: "ALB and instance for developers"

# Resources
# ---------
#
# These are the ALB and EC2 instance resources deployed by the template.
#
Resources:

  # #### AWS Account Information
  #
  # Lambda function to introspect VPCs, subnets, and select most available
  # Requested from cloud services team; see https://bitbucket.org/ua-ecs/service-catalog/src/master/portfolios/account-foundation-cf-account-info/
  AccountInfo:
    Type: Custom::AccountInfo
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:fdn-cf-account-info"
      VPCInfo:
      - vpcid
      - public-subnet-a
      - public-subnet-b
      - private-subnet-a
      - private-subnet-b

  # Private App Load Balancer to sit in front of the EC2 instance
  EC2LoadBalancerV2Priv:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: !Sub "${AppSlug}-${EnvSlug}-${TagContactNetId}-ec2-elbv2"
      Scheme: "internal"
      Subnets:
      - !GetAtt AccountInfo.private-subnet-a
      - !GetAtt AccountInfo.private-subnet-b
      LoadBalancerAttributes:
      - Key: idle_timeout.timeout_seconds
        Value: '1800'
      SecurityGroups:
      - !Ref LoadBalancerSecurityGroup
      Tags:
        - Key: service
          Value: !Ref TagService
        - Key: environment
          Value: !Ref TagEnvironment
        - Key: createdby
          Value: !Ref TagCreatedBy
        - Key: contactnetid
          Value: !Ref TagContactNetId
        - Key: accountnumber
          Value: !Ref TagAccountNumber
        - Key: subaccount
          Value: !Ref TagSubAccount
        - Key: ticketnumber
          Value: !Ref TagTicketNumber
        - Key: resourcefunction
          Value: !Ref TagResourceFunction

  # #### Load Balancer Security Group
  #
  # Copied from the MET ECS template used for the KFS application stack.
  #
  # This is the Security Group that wraps the Load Balancer. This controls what network
  # traffic is allowed into the ALB. Just web traffic is allowed from anywhere.
  LoadBalancerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow web traffic to the Load Balancer
      VpcId: !GetAtt AccountInfo.vpcid
      SecurityGroupIngress:
      -
        IpProtocol: tcp
        FromPort: '80'
        ToPort: '80'
        CidrIp: 0.0.0.0/0
        Description: "HTTP to HTTPS Redirection"
      -
        IpProtocol: tcp
        FromPort: '443'
        ToPort: '443'
        CidrIp: 0.0.0.0/0
        Description: "Normal Application Flow"
      Tags:
        - Key: service
          Value: !Ref TagService
        - Key: environment
          Value: !Ref TagEnvironment
        - Key: createdby
          Value: !Ref TagCreatedBy
        - Key: contactnetid
          Value: !Ref TagContactNetId
        - Key: accountnumber
          Value: !Ref TagAccountNumber
        - Key: subaccount
          Value: !Ref TagSubAccount
        - Key: ticketnumber
          Value: !Ref TagTicketNumber
        - Key: resourcefunction
          Value: !Ref TagResourceFunction

  # ALB Target group for EC2 instance
  InstanceALBV2Tg:
    Type: "AWS::ElasticLoadBalancingV2::TargetGroup"
    Properties:
      Name: !Sub "${AppSlug}-${EnvSlug}-${TagContactNetId}-tg"
      HealthCheckIntervalSeconds: 15
      HealthCheckPath: "/"
      HealthCheckTimeoutSeconds: 10
      HealthyThresholdCount: 2
      Matcher:
        HttpCode: "200-399"
      UnhealthyThresholdCount: 3
      Port: 80
      Protocol: HTTP
      TargetGroupAttributes:
      - Key: "deregistration_delay.timeout_seconds"
        Value: "300"
      Targets:
        - Id: !Ref Ec2Instance
          Port: 80
      VpcId: !GetAtt AccountInfo.vpcid

  # Listener for KFS EC2 ALB
  InstanceELBListener:
    Type: "AWS::ElasticLoadBalancingV2::Listener"
    Properties:
      Certificates:
      - CertificateArn: !Ref SSLCertARN
      DefaultActions:
      - Type: "forward"
        TargetGroupArn: !Ref "InstanceALBV2Tg"
      LoadBalancerArn: !Ref EC2LoadBalancerV2Priv
      Port: "443"
      Protocol: "HTTPS"
      SslPolicy: "ELBSecurityPolicy-TLS-1-1-2017-01"

  # #### Route53 DNS Record
  #
  # Create a DNS entry in Route53 for this environment. This creates a CNAME pointing at
  # the DNS name of the Load Balancer.
  AppDnsRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      # Append a period after the hosted zone DNS name
      HostedZoneName: !Sub "${HostedZoneName}."
      Name: !Sub "${AppSlug}-${EnvSlug}-${TagContactNetId}.${HostedZoneName}."
      Type: CNAME
      TTL: '900'
      ResourceRecords:
      - !GetAtt EC2LoadBalancerV2Priv.DNSName

  # #### EC2 Instance
  #
  # Deploys the EC2 instance with some package installations and our resource tags.
  Ec2Instance:
    Type: AWS::EC2::Instance
    Properties: 
      ImageId: !Ref AmazonLinuxAmi
      KeyName: !Ref KeyName
      InstanceType: !Ref InstanceType
      IamInstanceProfile: !Ref EnvInstanceProfile
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            VolumeType: gp3
            VolumeSize: '100'
            DeleteOnTermination: 'true'
            Encrypted: 'false'
      SecurityGroupIds:
      - !Ref InstanceSecurityGroup
      - Fn::ImportValue: !Sub "${EFSStackName}-target-sg"
      SubnetId: !Ref EC2Subnet
      Tags:
        - Key: Name
          Value: !Sub "${AppSlug}-${EnvSlug}-${TagContactNetId} EC2"
        - Key: service
          Value: !Ref TagService
        - Key: environment
          Value: !Ref TagEnvironment
        - Key: createdby
          Value: !Ref TagCreatedBy
        - Key: contactnetid
          Value: !Ref TagContactNetId
        - Key: accountnumber
          Value: !Ref TagAccountNumber
        - Key: subaccount
          Value: !Ref TagSubAccount
        - Key: ticketnumber
          Value: !Ref TagTicketNumber
        - Key: resourcefunction
          Value: !Ref TagResourceFunction
      UserData:
        Fn::Base64: !Sub 
          - |
            #!/bin/bash
            
            # Basic Updates
            yum update -y

            # Install developer tools
            yum install -y git vim

            # Install Corretto 11 (https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/amazon-linux-install.html)
            yum install -y java-11-amazon-corretto-devel

            # Install Maven 3.9.2 (downloaded from https://archive.apache.org/dist/maven/maven-3/3.3.9/binaries/)
            cd /opt
            aws s3 cp s3://${S3FileBucket}/maven/apache-maven-3.3.9-bin.tar.gz .
            tar -xzf apache-maven-3.3.9-bin.tar.gz
            ln -s /opt/apache-maven-3.3.9 /opt/maven
            aws s3 cp s3://${S3FileBucket}/maven/set-maven-env-vars.sh /etc/profile.d
            chmod 744 /etc/profile.d/set-maven-env-vars.sh
            source /etc/profile.d/set-maven-env-vars.sh

            # Install ojdbc6.jar file into maven repo
            cd /opt
            aws s3 cp s3://${S3FileBucket}/ojdbc6.jar .
            # https://stackoverflow.com/questions/9898499/oracle-jdbc-ojdbc6-jar-as-a-maven-dependency
            mvn install:install-file -Dfile=./ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3 -Dpackaging=jar -DgeneratePom=true

            # Set the Time Zone
            sed -i 's/ZONE="UTC"/ZONE="America\/Phoenix"/' /etc/sysconfig/clock
            ln -sf /usr/share/zoneinfo/America/Phoenix /etc/localtime

            # Create directory and mount EFS volume
            mkdir ${MountPath}
            mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 "${efsid}.efs.${AWS::Region}.amazonaws.com:/" ${MountPath}
          - efsid:
              Fn::ImportValue: !Sub "${EFSStackName}-fs-id"

  # #### Instance Security Group
  #
  # Security group for the EC2 instance, that allows you to SSH into the instance
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: !Sub "${AWS::StackName} EC2 Security Group"
      VpcId: !GetAtt AccountInfo.vpcid
      SecurityGroupIngress:
      - IpProtocol: "tcp"
        FromPort: "22"
        ToPort: "22"
        CidrIp: "10.138.0.0/17"
        Description: "Allow SSH when logged into Mosaic VPN"
      # Web Ingress Rules from MET ECS template used for the KFS application layer.
      - IpProtocol: "tcp"
        FromPort: "0"
        ToPort: "65535"
        SourceSecurityGroupId: !Ref LoadBalancerSecurityGroup
        Description:  "Allow all ports inbound from ALB SG"
      Tags:
        - Key: Name
          Value: !Sub "${AppSlug}-${EnvSlug}-${TagContactNetId} SG"
        - Key: service
          Value: !Ref TagService
        - Key: environment
          Value: !Ref TagEnvironment
        - Key: createdby
          Value: !Ref TagCreatedBy
        - Key: contactnetid
          Value: !Ref TagContactNetId
        - Key: accountnumber
          Value: !Ref TagAccountNumber
        - Key: subaccount
          Value: !Ref TagSubAccount
        - Key: ticketnumber
          Value: !Ref TagTicketNumber
        - Key: resourcefunction
          Value: !Ref TagResourceFunction

  AppInstToDB1521:
    Type: "AWS::EC2::SecurityGroupIngress"
    Properties:
      GroupId: !ImportValue "Kuali-DbSg-DbSecurityGroup"
      IpProtocol: "tcp"
      FromPort: "1521"
      ToPort: "1521"
      SourceSecurityGroupId: !Ref InstanceSecurityGroup
      Description: "Instance to DB Security Group"

  # #### Instance Role
  #
  # This is the IAM role that will be applied to the EC2 Instance. Any AWS specific
  # permissions that the node might need should be defined here.
  #
  EnvInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
            - ssm.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
      Policies:
        -
          PolicyName: "S3Read"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              -
                Effect: "Allow"
                Action:
                - "s3:GetObject"
                Resource: !Join ["", ["arn:aws:s3:::", !Ref S3FileBucket, "/*"]]

  # #### Instance Profile
  #
  # This is just a little construct to connect a set of roles together into a profile. The profile
  # is referenced by the EC2 Instance.
  EnvInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - !Ref EnvInstanceRole

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  InstanceID:
    Description: "The Instance ID"
    Value: !Ref "Ec2Instance"

  InstanceSecurityGroup:
    Description: "This instances's security group, which was added as a new inbound rule for the shared mounted EFS volume."
    Value: !Ref "InstanceSecurityGroup"
    Export:
      Name: !Sub "${AWS::StackName}-instance-sg"

  InstancePrivateIP:
    Description: "The private IP address of the instance"
    Value: !GetAtt Ec2Instance.PrivateIp

  InstancePrivateDNSName:
    Description: "The private DNS name of the instance"
    Value: !GetAtt Ec2Instance.PrivateDnsName

  EC2EndpointDNS:
    Value: !Ref AppDnsRecord
    Export:
      Name: !Sub "${AWS::StackName}-dns"