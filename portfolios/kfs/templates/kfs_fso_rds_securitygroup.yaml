---
# KFS FSO RDS Security Group CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will create the security group required for an FSO EC2
# instance to access a non-production KFS database.
# 

AWSTemplateFormatVersion: '2010-09-09'
Description: KFS FSO RDS Access Security Group

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Security Group Metadata
      Parameters:
      - VPCID
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagName
      - TagEnvironment
      - TagCreatedBy
      - TagContactNetId
      - TagAccountNumber
      - TagSubAccount
      - TagTicketNumber
      - TagResourceFunction

# ### Parameters
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  VPCID:
    Description: Target VPC
    Type: AWS::EC2::VPC::Id
    Default: "vpc-2b1ea94c"
  TagService:
    Description: Service name (from the service catalog) that is utilizing this resource
    Type: String
    Default: "UAccess Financials"
  TagName:
    Description: Descriptive identifier of resource.
    Type: String
    Default: "DB security group for FSO EC2 instances."
  TagEnvironment:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'.
    Type: String
    Default: "dev"
  TagCreatedBy:
    Description: NetID of the user that created this resource
    Type: String
    Default: "hlo"
  TagContactNetId:
    Description: NetID of the person to contact for information about this resource
    Type: String
    Default: "hlo"
  TagAccountNumber:
    Description: Financial system account number for the service utilizing this resource
    Type: String
    Default: "1192620"
  TagSubAccount:
    Description: Financial system subaccount number for the service utilizing this resource
    Type: String
    Default: "12AWS"
  TagTicketNumber:
    Description: Ticket number that this resource is for
    Type: String
    Default: "FIN-1901"
  TagResourceFunction:
    Description: Human-readable description of what function this resource is providing
    Type: String
    Default: "Security group for non-production DB access by FSO EC2 instances."

# Resources
# ---------
#
# These are all of the actual AWS resources created for this application.
#
Resources:

  # #### DB Security Group
  #
  # Defines the Security Group for the RDS Database. This restricts DB access to only 
  # the devices in the InstanceSecurityGroup, so our App nodes.
  DBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow DB traffic from Application Instances
      VpcId: !Ref VPCID
      # Allow inbound traffic on port 1521 from IP addresses of specific FSO EC2 instances.
      SecurityGroupIngress:
      - 
        IpProtocol: tcp
        FromPort: '1521'
        ToPort: '1521'
        CidrIp: "10.220.131.185/32" # katalontest.fso.arizona.edu
      - 
        IpProtocol: "tcp"
        FromPort: "1521"
        ToPort: "1521"
        CidrIp: "10.220.131.196/32" # katalontest2.fso.arizona.edu
      - 
        IpProtocol: "tcp"
        FromPort: "1521"
        ToPort: "1521"
        CidrIp: "10.220.131.186/32" # katalontest3.fso.arizona.edu
      - 
        IpProtocol: "tcp"
        FromPort: "1521"
        ToPort: "1521"
        CidrIp: "10.220.131.151/32" # katalontest4.fso.arizona.edu
      - 
        IpProtocol: "tcp"
        FromPort: "1521"
        ToPort: "1521"
        CidrIp: "10.220.131.144/32" # katalontest5.fso.arizona.edu
      #Allow all outbound traffic
      SecurityGroupEgress:
      - 
        IpProtocol: "-1"
        CidrIp: "0.0.0.0/0"
      Tags:
      - 
        Key: service
        Value: !Ref TagService
      - 
        Key: Name
        Value: !Sub "kfs-fso-rds-sg"
      - 
        Key: environment
        Value: !Ref TagEnvironment
      - 
        Key: createdby
        Value: !Ref TagCreatedBy
      - 
        Key: contactnetid
        Value: !Ref TagContactNetId
      - 
        Key: accountnumber
        Value: !Ref TagAccountNumber
      - 
        Key: subaccount
        Value: !Ref TagSubAccount
      - 
        Key: ticketnumber
        Value: !Ref TagTicketNumber
      - 
        Key: resourcefunction
        Value: !Ref TagResourceFunction

# Outputs
# -------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  DBSecurityGroup:
    Value: !Ref DBSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-DbSecurityGroup"