---
# EC2 Basic CloudFormation Deployment
# -----------------------------------------
#
# This CloudFormation template will deploy a single EC2 instance with
# its own security group.

AWSTemplateFormatVersion: "2010-09-09"

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # HostName to be used in tagging the EC2 instance.
  HostName:
    Description: "Enter the name of the host or service, ie 'Civil Engineering Structures App', or 'UITS Cloud Services Testing', etc."
    Type: String
    Default: kfs-nonprod-log-viewer

  HostedZoneName:
    Description: "The Route53 Hosted Zone Name"
    Type: String
    Default: ua-uits-kuali-nonprod.arizona.edu

  # SSH Key Pair to be used on the application EC2 instances for administrative access.
  KeyName:
    Description: "Amazon EC2 Key Pair"
    Type: AWS::EC2::KeyPair::KeyName

  # VPCID is the ID of the VPC where this template will be deployed.
  VPCID:
    Description: "Target VPC"
    Type: AWS::EC2::VPC::Id
  InstanceSubnet:
    Description: "Private Subnet"
    Type: AWS::EC2::Subnet::Id

  # Default EC2 Instance Type for Application instances.
  InstanceType:
    Description: "EC2 Instance Type"
    Type: String
    Default: "t3.micro"
    AllowedValues:
      - "t3.micro"
      - "t3.medium"
      - "m5.large"

  EFSStackName:
    Description: "Name of the EFS CloudFormation Stack"
    Type: String
    MinLength: '2'
    Default: kfs7-nonprod-efs

  AmazonLinuxAmi:
    Description: "Amazon Linux 2023 Latest AMI ID"
    Type : AWS::SSM::Parameter::Value<String>
    Default: /aws/service/ami-amazon-linux-latest/al2023-ami-kernel-default-x86_64

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  ServiceTag:
    Description: "Exact name of the Service as defined in the service catalog."
    Type: String
    Default: UAccess Financials
  NameTag:
    Description: "Name of EC2"
    Type: String
    Default: kfs-nonprod-log-viewer
  EnvironmentTag:
    Description: "Used to distinguish between development, test, production, etc. environment types."
    Type: String
    AllowedValues: [dev, tst, prd, trn, stg, cfg, sup, rpt]
    Default: dev
  ContactNetidTag:
    Description: "Used to identify the netid of the person most familiar with the usage of the resource."
    Type: String
  AccountNumberTag:
    Description: "Financial system account number for the service utilizing this resource"
    Type: String
    Default: "1192620"
  TicketNumberTag:
    Description: "Ticket number that this resource is for"
    Type: String

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Instance Settings
      Parameters:
      - HostName
      - InstanceType
      - EFSStackName
      - KeyName
      - VPCID
      - InstanceSubnet      
    - Label:
        default: Tags
      Parameters:
      - ServiceTag
      - EnvironmentTag
      - ContactNetidTag
      - AccountNumberTag
      - TicketNumberTag

    ParameterLabels:
      ServiceTag:
        default: "Service Name:"
      EnvironmentTag:
        default: 'Environment Type:'
      ContactNetidTag:
        default: 'Contact NetID:'
      AccountNumberTag:
        default: 'Financial Account Number:'
      TicketNumberTag:
        default: 'Ticket Number:'

# Resources
# ---------
#
# This is the EC2 instance deployed by the template.
#
Resources:

  # #### EC2 Instance
  #
  # Deploys the EC2 instance with some tags.
  Ec2Instance:
    Type: "AWS::EC2::Instance"
    Properties:
      ImageId: !Ref AmazonLinuxAmi
      # ImageId: ami-08e9fac54dbae58b4
      KeyName: !Ref KeyName
      InstanceType: !Ref InstanceType
      IamInstanceProfile: !Ref EnvInstanceProfile
      NetworkInterfaces:
        - DeviceIndex: "0"
          GroupSet:
            - !Ref InstanceSecurityGroup
            - Fn::ImportValue: !Sub "${EFSStackName}-target-sg"
          SubnetId: !Ref InstanceSubnet
          AssociatePublicIpAddress: false
      Tags:
        - Key: "Name"
          Value: !Ref "HostName"
        - Key: service
          Value: !Ref ServiceTag
        - Key: environment
          Value: !Ref EnvironmentTag
        - Key: contactnetid
          Value: !Ref ContactNetidTag
        - Key: accountnumber
          Value: !Ref AccountNumberTag
        - Key: ticketnumber
          Value: !Ref TicketNumberTag
      UserData:
        Fn::Base64: !Sub 
          - |
            #!/bin/bash -e
            #
            # Basic Updates
            sudo yum update -y
            sudo yum install -y git vim telnet
            
            sudo mkdir -p /efs
            sudo echo "${efsid}.efs.${AWS::Region}.amazonaws.com:/ /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,ro 0 0" >> /etc/fstab            

            sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${efsid}.efs.${AWS::Region}.amazonaws.com:/ /efs
            
          - efsid:
               Fn::ImportValue:
                  !Sub "${EFSStackName}-fs-id"

  # #### Instance Security Group
  #
  # Security group for the EC2 instance, that allows you to SSH into the instance
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: "Allow ssh to client host"
      VpcId: !Ref "VPCID"
      SecurityGroupIngress:
        - IpProtocol: "tcp"
          FromPort: 22
          ToPort: 22
          CidrIp: "10.138.2.0/24"
      Tags:
        - Key: "Name"
          Value: !Sub "${HostName} Security Group"
        - Key: service
          Value: !Ref ServiceTag
        - Key: environment
          Value: !Ref EnvironmentTag
        - Key: contactnetid
          Value: !Ref ContactNetidTag
        - Key: accountnumber
          Value: !Ref AccountNumberTag
        - Key: ticketnumber
          Value: !Ref TicketNumberTag


  # #### Instance Role
  #
  # This is the IAM role that will be applied to the EC2 Instance. Any AWS specific
  # permissions that the node might need should be defined here.
  #
  EnvInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
            - ssm.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
      - arn:aws:iam::aws:policy/AmazonS3FullAccess
      - arn:aws:iam::aws:policy/AmazonEC2FullAccess


  # #### Instance Profile
  #
  # This is just a little construct to connect a set of roles together into a profile. The profile
  # is referenced by the EC2 Instance.
  EnvInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - !Ref EnvInstanceRole

  # #### Route53 DNS Record
  #
  # Create a friendlier DNS entry pointing to the EC2 instance
  EnvDnsRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: !Sub "${HostedZoneName}."
      Name: !Sub "${HostName}.${HostedZoneName}."
      Type: "A"
      TTL: "900"
      ResourceRecords:
      - !GetAtt Ec2Instance.PrivateIp

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  InstancePrivateIP:
    Description: "The Private IP address of the instance"
    Value: !GetAtt Ec2Instance.PrivateIp

  InstanceID:
    Description: "The Instance ID"
    Value: !Ref "Ec2Instance"
  
  DNSName:
    Description: "DNS record name"
    Value: !Ref "EnvDnsRecord"