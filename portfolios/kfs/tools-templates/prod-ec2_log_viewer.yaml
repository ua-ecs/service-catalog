---
# EC2 Basic CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will deploy a single EC2 instance to 
# view logs from the KFS EFS volume.

AWSTemplateFormatVersion: "2010-09-09"

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # HostName to be used in tagging the EC2 instance.
  HostName:
    Type: String
    Description: "Enter the name of the host or service."
    Default: KFS Production Log Viewer

  # SSH Key Pair to be used on the application EC2 instances for emergency administrative access.
  KeyName:
    Description: Amazon EC2 Key Pair
    Type: AWS::EC2::KeyPair::KeyName

  # VPCID is the ID of the VPC where this template will be deployed.
  VPCID:
    Description: Target VPC
    Type: AWS::EC2::VPC::Id

  InstanceSubnet:
    Description: Private Subnet
    Type: AWS::EC2::Subnet::Id

  HostedZoneName:
    Type: String
    Description: The Route53 Hosted Zone Name
    Default: kuali-prod-aws.arizona.edu

  # Default EC2 Instance Type for Application instances.
  InstanceType:
    Description: "EC2 Instance Type"
    Type: String
    Default: "t3.micro"
    AllowedValues:
      - "t3.micro"
      - "t3.medium"
      - "m5.large"

  AmazonLinuxAmi:
    Description: "Amazon Linux 2023 Latest AMI ID"
    Type : AWS::SSM::Parameter::Value<String>
    Default: /aws/service/ami-amazon-linux-latest/al2023-ami-kernel-default-x86_64

  EFSStackName:
    MinLength: '2'
    Type: String
    Description: Name of the EFS CloudFormation Stack
    Default: kfs-prod-efs

  MountDirList:
    Type: String
    Description: Comma separated list of EFS directories to mount logs from. No Spaces.
    Default: "prd,sup"

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  ServiceTag:
    Description: "Exact name of the Service as defined in the service catalog."
    Type: String
    Default: UAccess Financials
  NameTag:
    Description: "Name of EC2"
    Type: String
    Default: kfs-prod-log-viewer
  EnvironmentTag:
    Description: "Used to distinguish between development, test, production, etc. environment types."
    Type: String
    AllowedValues: [dev, prd ]
    Default: dev
  ContactNetidTag:
    Description: "Used to identify the netid of the person most familiar with the usage of the resource."
    Type: String
  AccountNumberTag:
    Description: "Financial system account number for the service utilizing this resource"
    Type: String
    Default: "1192620"
  TicketNumberTag:
    Description: "Ticket number that this resource is for"
    Type: String


# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Instance Settings
      Parameters:
      - HostName
      - KeyName
      - VPCID
      - InstanceSubnet
      - HostedZoneName
      - InstanceType
      - AmazonLinuxAmi
    - Label:
        default: Application Settings
      Parameters:
      - EFSStackName
      - MountDirList
    - Label:
        default: Tags
      Parameters:
      - ServiceTag
      - NameTag
      - EnvironmentTag
      - ContactNetidTag
      - AccountNumberTag
      - TicketNumberTag

    ParameterLabels:
      ServiceTag:
        default: "Service Name:"
      NameTag:
        default: "EC2 Name"
      EnvironmentTag:
        default: 'Environment Type:'
      ContactNetidTag:
        default: 'Contact NetID:'
      AccountNumberTag:
        default: 'Financial Account Number:'
      TicketNumberTag:
        default: 'Ticket Number:'

# Resources
# ---------
#
# This is the EC2 instance deployed by the template.
#
Resources:

  # #### EC2 Instance
  #
  # Deploys the EC2 instance with some tags.
  Ec2Instance:
    Type: "AWS::EC2::Instance"
    Metadata:            
      AWS::CloudFormation::Init: 
        configSets:
          bootStrap:
            - "configureUser"
        configureUser:
          groups:
            logviewgroup: {}
          users:
            logview:
              groups:
                - logviewgroup
              homeDir: /home/logview
    CreationPolicy:
      ResourceSignal:
        Timeout: PT20M
    Properties:
      ImageId: !Ref AmazonLinuxAmi
      KeyName: !Ref KeyName
      InstanceType: !Ref InstanceType
      IamInstanceProfile: !Ref EnvInstanceProfile
      NetworkInterfaces:
        - AssociatePublicIpAddress: false
          DeviceIndex: "0"
          GroupSet:
            - !Ref InstanceSecurityGroup
            - Fn::ImportValue: !Sub "${EFSStackName}-target-sg"
          SubnetId: !Ref InstanceSubnet
      Tags:
        - Key: "Name"
          Value: !Ref "HostName"
        - Key: service
          Value: !Ref ServiceTag
        - Key: environment
          Value: !Ref EnvironmentTag
        - Key: contactnetid
          Value: !Ref ContactNetidTag
        - Key: accountnumber
          Value: !Ref AccountNumberTag
        - Key: ticketnumber
          Value: !Ref TicketNumberTag
      UserData:
        Fn::Base64: !Sub 
            - |
              #!/bin/bash -e
              #
              # Basic Updates
              sudo yum update -y
              sudo yum install -y git vim telnet

              # https://stackoverflow.com/questions/17126051/how-to-change-shell-on-amazon-ec2-linux-instance
              # fixed something preventing authorized_keys file set-up
              sudo yum install -y util-linux-user

              mount_dirs=${MountDirList}
              for d in $(echo $mount_dirs | sed "s/,/ /g")
              do
                  mkdir -p /efs/kfs/$d
                  echo "${efsid}.efs.${AWS::Region}.amazonaws.com:/$d/transactional /efs/kfs/$d nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,ro 0 0" >> /etc/fstab 
              done

              mount -a -t nfs4
              
              #Ask CFN-INIT to install the configs listed in the bootStrap configset
              /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --configsets bootStrap --resource Ec2Instance --region ${AWS::Region}
             
              #Signal the result to the CF service
              /opt/aws/bin/cfn-signal -e $?  --stack ${AWS::StackName} --resource Ec2Instance --region ${AWS::Region}
              
              HOME=/home/logview
              AUTH_KEYS=$HOME/.ssh/authorized_keys
              mkdir -p $HOME/.ssh/
              touch $AUTH_KEYS
              chown -R logview:logviewgroup $HOME
              chown -R logview:logviewgroup $HOME/.ssh
              chmod 700 $HOME/.ssh
              chmod 644 $AUTH_KEYS
              chsh -s /bin/bash logview
              ln -s /efs/kfs $HOME/logs
              
              echo "# https://github.com/heather-lo.keys"   >> $AUTH_KEYS
              curl https://github.com/heather-lo.keys       >> $AUTH_KEYS

              echo "# https://github.com/mhirstua.keys"     >> $AUTH_KEYS
              curl https://github.com/mhirstua.keys         >> $AUTH_KEYS

              echo "# https://github.com/nataliac06.keys"   >> $AUTH_KEYS
              curl https://github.com/nataliac06.keys       >> $AUTH_KEYS

              echo "# https://github.com/sskinner.keys"     >> $AUTH_KEYS
              curl https://github.com/sskinner.keys         >> $AUTH_KEYS

              echo "# https://github.com/fimbresrc.keys"    >> $AUTH_KEYS
              curl https://github.com/fimbresrc.keys        >> $AUTH_KEYS

            - efsid:
                 Fn::ImportValue:
                    !Sub "${EFSStackName}-fs-id"


  # #### Instance Security Group
  # 
  # Security group for the EC2 instance, that allows you to SSH into the instance
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: "Allow ssh to client host"
      VpcId: !Ref "VPCID"
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 22
        ToPort: 22
        CidrIp: 10.138.2.0/24
        Description: Mosaic VPN
      Tags:
        - Key: "Name"
          Value: !Sub "${HostName} Security Group"
        - Key: service
          Value: !Ref ServiceTag
        - Key: environment
          Value: !Ref EnvironmentTag
        - Key: contactnetid
          Value: !Ref ContactNetidTag
        - Key: accountnumber
          Value: !Ref AccountNumberTag
        - Key: ticketnumber
          Value: !Ref TicketNumberTag


  # #### Instance Role
  #
  # This is the IAM role that will be applied to the EC2 Instance. Any AWS specific
  # permissions that the node might need should be defined here.
  #
  EnvInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
            - ssm.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM


  # #### Instance Profile
  #
  # This is just a little construct to connect a set of roles together into a profile. The profile
  # is referenced by the EC2 Instance.
  EnvInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles: 
      - !Ref EnvInstanceRole

  # #### Route53 DNS Record
  #
  # Create a friendlier DNS entry pointing to the EC2 instance
  EnvDnsRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: !Sub "${HostedZoneName}."
      Name: !Sub "kfslogs.${HostedZoneName}."
      Type: "A"
      TTL: "900"
      ResourceRecords:
      - !GetAtt Ec2Instance.PrivateIp

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#  
Outputs:

  InstancePrivateIP:
    Description: "The Private IP address of the instance"
    Value: !GetAtt Ec2Instance.PrivateIp

  InstanceID:
    Description: "The Instance ID"
    Value: !Ref "Ec2Instance"

  DNSName:
    Description: "DNS record name"
    Value: !Ref "EnvDnsRecord"