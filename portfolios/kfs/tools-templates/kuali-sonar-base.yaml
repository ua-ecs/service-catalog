---
# Generic ECS Cluster Template
# -----------------------------------------
#
# This CloudFormation template will build an ECS base cluster that will be used for SonarQube infrastructure.
# The design uses a shared ECS cluster and load balancer via a base stack.  Additional applications can then 
# be deployed on top of the base stack using a different CloudFormation stack.  The benefit will be in having 
# other related applications use the base stack and could also help with upgrading and testing.
# Complete details can be found at the link below:
#   https://confluence.arizona.edu/display/KAST/SonarQube+Environment+Setup
#

AWSTemplateFormatVersion: '2010-09-09'
Description: ECS Kuali Cluster Base

# ### Parameters
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:

  EFSStackName:
    MinLength: '2'
    Type: String
    Description: Name of the EFS CloudFormation Stack
    Default: kuali-sonartst-efs

  AppSlug:
    MinLength: '3'
    Type: String
    Description: Short application slug, ie 'toolshed'. Lowercase letters, numbers and dashes only
    AllowedPattern: "[a-z0-9-]*"

  EnvSlug:
    MinLength: '2'
    Type: String
    Description: Short environment slug, ie 'dev', or 'markdev'. Lowercase letters, numbers and dashes only
    AllowedPattern: "[a-z0-9]*"

  HostedZoneName:
    Type: String
    Description: Name of the Route53 Hosted Zone
    Default: "ua-uits-kuali-nonprod.arizona.edu"

  # Launch Template Version
  LaunchTemplateVersion:
    Description: Launch Template Version
    Type: String
    Default: 1

  KeyName:
    Description: Amazon EC2 Key Pair
    Type: AWS::EC2::KeyPair::KeyName
    Default: "kfs-development-environments-keypair"

  EcsImageId:
    Description: Latest AMI Amazon built specifically for ECS
    Type : AWS::SSM::Parameter::Value<String>
    Default: /aws/service/ecs/optimized-ami/amazon-linux/recommended/image_id
    AllowedValues:
    - /aws/service/ecs/optimized-ami/amazon-linux/recommended/image_id

  SSLCertificateARN:
    Type: String
    Description: Full ARN of the SSL Certificate to use on the load balancer
    Default: "arn:aws:acm:us-west-2:397167497055:certificate/9a4ee0ac-1031-41c5-9457-0181eab28f7b"
    
  ServiceTag:
    Description: Refers to the application (Uaccess Learning, Uaccess Employee, Uaccess Student)
    Type: String
    Default: Uaccess Financials

  ResourceNameTag:
    Description: Descriptive identifier of resource.
    Type: String
    Default: "Kuali-SonarQube"

  EnvironmentTag:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'.
    Type: String
    Default: poc

  ContactNetidTag:
    Description: NetID of person most familiar with resource
    Type: String
    Default: kuali

  AccountNumberTag:
    Description: Identifies the financial system account number
    Type: String
    Default: 1192620

  SubAccountTag:
    Description: Identifies the financial system subaccount number
    Type: String
    Default: 12AWS

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Application Information
      Parameters:
      - AppSlug
      - EnvSlug
      - EFSStackName
    - Label:
        default: Instance Settings
      Parameters:
      - LaunchTemplateVersion
      - KeyName
      - EcsImageId
    - Label:
        default: Load Balancer Settings
      Parameters:
      - HostedZoneName
    - Label:
        default: Tags
      Parameters:
      - ServiceTag
      - ResourceNameTag
      - SubAccountTag
      - EnvironmentTag
      - AccountNumberTag
      - ContactNetidTag

# Resources
# ---------
#
# These are all of the actual AWS resources created for this application.
#
Resources:

  ############## ECS Hosts and Configurations 
  #
  #
  # #### Instance Role
  #
  # This is the IAM role that will be applied to the ECS Instances. Any AWS specific
  # permissions that the node might need should be defined here.
  #
  EnvInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      ManagedPolicyArns:
      - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role
      - arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM  # TODO may not need this role

  # #### Instance Profile
  #
  # This is just a little construct to connect a set of roles together into a profile. The profile
  # is referenced by ec2 instances.
  EnvInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - !Ref EnvInstanceRole

  # #### Instance Security Group
  #
  # Security group for the host nodes themselves.
  # Needs to permit incoming traffice from the ELB, and any other authorized incoming
  # sources.
  InstanceSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow Load Balancer and SSH to host
      VpcId: !ImportValue kuali-vpc-vpcid
      SecurityGroupIngress:
      - Description: Mosaic VPN-1
        IpProtocol: tcp
        FromPort: '22'
        ToPort: '22'
        CidrIp: 10.138.2.0/24
      - Description: Mosaic VPN-2
        IpProtocol: tcp
        FromPort: '22'
        ToPort: '22'
        CidrIp: 150.135.241.0/24
      - Description: InfraDev VPN
        IpProtocol: tcp
        FromPort: '22'
        ToPort: '22'
        CidrIp: 150.135.112.0/24
      - Description: ben.uits bastion
        IpProtocol: tcp
        FromPort: '22'
        ToPort: '22'
        CidrIp: 128.196.130.211/32
      - Description: "Incoming Traffic from Load Balancer"
        IpProtocol: tcp
        FromPort: '31000'
        ToPort: '61000'
        SourceSecurityGroupId: !Ref AlbSecurityGroup
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}-Instance-sg"
        - Key: service
          Value: !Ref ServiceTag
        - Key: environment
          Value: !Ref EnvironmentTag
        - Key: contactnetid
          Value: !Ref ContactNetidTag
        - Key: accountnumber
          Value: !Ref AccountNumberTag
        - Key: subaccount
          Value: !Ref SubAccountTag

  # #### Launch Template
  #
  # Create an EC2 Launch Template for kuali hosts
  InstanceLaunchTemplate:
    Type: "AWS::EC2::LaunchTemplate"
    Properties:
      LaunchTemplateName: !Sub "${AWS::StackName}-launchtemplate"
      LaunchTemplateData:
        ImageId: !Ref EcsImageId
        KeyName: !Ref KeyName
        InstanceType: t3.medium
        IamInstanceProfile:
          Name: !Ref EnvInstanceProfile
        NetworkInterfaces:
          - AssociatePublicIpAddress: "false"
            DeviceIndex: "0"
            Groups:
              - !Ref InstanceSecurityGroup
              - Fn::ImportValue: !Sub "${EFSStackName}-target-sg"
            SubnetId: !ImportValue kuali-vpc-private-subnet-b 
        TagSpecifications:
          - ResourceType: volume
            Tags:
            - Key: service
              Value: !Ref ServiceTag
            - Key: environment
              Value: !Ref EnvironmentTag
            - Key: contactnetid
              Value: !Ref ContactNetidTag
            - Key: accountnumber
              Value: !Ref AccountNumberTag
        UserData:
          Fn::Base64: !Sub 
            - |
                Content-Type: multipart/mixed; boundary="==BOUNDARY=="
                MIME-Version: 1.0

                --==BOUNDARY==
                Content-Type: text/cloud-boothook; charset="us-ascii"

                # Install nfs-utils
                cloud-init-per once yum_update yum update -y
                cloud-init-per once install_nfs_utils yum install -y nfs-utils

                # SonarQube requirements:
                # the user running SonarQube can open at least 65536 file descriptors
                # the user running SonarQube can open at least 4096 threads
                # More info: https://docs.sonarqube.org/latest/requirements/requirements/
                cloud-init-per once sysctl_vm_max sysctl -w vm.max_map_count=262144
                cloud-init-per once sysctl_fs_file sysctl -w fs.file-max=65536

                # Create /efs folder
                cloud-init-per once mkdir_efs mkdir -p "/efs"

                # Mount /efs
                cloud-init-per once mount_efs echo -e "${efsid}.efs.us-west-2.amazonaws.com:/ /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab
                mount -a
          
                --==BOUNDARY==
                Content-Type: text/x-shellscript; charset="us-ascii"

                #!/bin/bash
                # Set any ECS agent configuration options
                echo ECS_CLUSTER=kuali-sonar-ECS >> /etc/ecs/ecs.config

                --==BOUNDARY==--
            - efsid:
                Fn::ImportValue: !Sub "${EFSStackName}-fs-id"      
        

  EcsInstanceAsg:
    Type: AWS::AutoScaling::AutoScalingGroup
    DependsOn: EcsCluster
    Properties:
      AutoScalingGroupName: !Sub "${AWS::StackName}-asg"
      VPCZoneIdentifier: 
      - !ImportValue kuali-vpc-private-subnet-a 
      - !ImportValue kuali-vpc-private-subnet-b
      LaunchTemplate:
        LaunchTemplateId: !Ref InstanceLaunchTemplate
        Version: !Ref LaunchTemplateVersion
      MinSize: 0
      MaxSize: 2
      DesiredCapacity: 1
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName} Auto Scaling Group"
          PropagateAtLaunch: 'true'
        - Key: Description
          Value: "This instance is the part of the Auto Scaling group which was created through CloudFormation"
          PropagateAtLaunch: 'true'
        - Key: service
          Value: !Ref ServiceTag
          PropagateAtLaunch: 'true'
        - Key: environment
          Value: !Ref EnvironmentTag
          PropagateAtLaunch: 'true'
        - Key: contactnetid
          Value: !Ref ContactNetidTag
          PropagateAtLaunch: 'true'
        - Key: accountnumber
          Value: !Ref AccountNumberTag
          PropagateAtLaunch: 'true'
        - Key: subaccount
          Value: !Ref SubAccountTag
          PropagateAtLaunch: 'true'


  ############## Shared Load Balancer Resources
  #
  #
  # #### Load Balancer
  #
  # The load balancer (ALB) constructor along with the Security Group
  # that allows client traffic to the ALB on ports 80 & 443
  LoadBalancer:
      Type: AWS::ElasticLoadBalancingV2::LoadBalancer
      Properties:
        Name: !Sub "${ResourceNameTag}-lb"
        Scheme: internal
        Subnets:
        - !ImportValue kuali-vpc-private-subnet-a
        - !ImportValue kuali-vpc-private-subnet-b
        LoadBalancerAttributes:
        - Key: idle_timeout.timeout_seconds
          Value: '120'
        SecurityGroups:
        - !Ref AlbSecurityGroup


  # #### ALB Listeners definitions
  AlbListener80:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
      - Type: redirect
        RedirectConfig:
          Host: '#{host}'
          Path: '/#{path}'
          Port: 443
          Protocol: HTTPS
          Query: '#{query}'
          StatusCode: HTTP_301
      LoadBalancerArn: !Ref LoadBalancer
      Port: 80
      Protocol: HTTP
  
  AlbListener443:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
      - Type: forward
        TargetGroupArn:
          Ref: AlbTargetGroup
      LoadBalancerArn: !Ref LoadBalancer
      Port: 443
      Protocol: HTTPS
      SslPolicy: 'ELBSecurityPolicy-TLS-1-2-2017-01'
      Certificates: 
      - CertificateArn: !Ref SSLCertificateARN

 # #### Add to Load Balancer
  #
  # #### Target Group
  #
  # Define the Target Group for adding Instances to the ALB
  # as well as the health checks for those Instances
  AlbTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      Name: !Sub "${AWS::StackName}-alb"
      HealthCheckIntervalSeconds: 60
      UnhealthyThresholdCount: 10
      HealthCheckPath: /
      Matcher:
        HttpCode: "200-399"
      Port: 80
      Protocol: HTTP
      VpcId: !ImportValue kuali-vpc-vpcid


  # #### ALB Security Group
  #
  # Create the Security Group for the ALB here in the base template so it can be referenced here.
  # Exported for use in the host template.
  AlbSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: 'Allow external traffic to load balancer'
      VpcId: !ImportValue kuali-vpc-vpcid
      SecurityGroupIngress:
        - CidrIp: 0.0.0.0/0
          IpProtocol: "tcp"
          FromPort: "80"
          ToPort: "80"  
        - CidrIp: 0.0.0.0/0
          IpProtocol: "tcp"
          FromPort: "443"
          ToPort: "443"
      Tags:
        - Key: Name
          Value: !Sub "${ResourceNameTag}-asg"

 
   # #### Route53 DNS Record
  
  #
  # DNS name to point at the load balancer
  DnsRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: !Sub "${HostedZoneName}."
      Name: !Sub "${AppSlug}.${HostedZoneName}."
      Type: "CNAME"
      TTL: "200"
      ResourceRecords:
      - !GetAtt LoadBalancer.DNSName


  ############## Shared ECS Cluster
  #
  #
  #ECS Cluster
  EcsCluster:
    Type: "AWS::ECS::Cluster"
    #Need to make sure the LB is created before the ECS cluster is created
    Properties:
      ClusterName: !Sub "kuali-sonar-ECS"
      Tags:
        - Key: Name
          Value: !Sub "${ResourceNameTag}-Cluster"


  

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#  
Outputs:

  VPCID:
    Description: Target VPC
    Value: !ImportValue kuali-vpc-vpcid
    Export:
      Name: !Sub "${AWS::StackName}-vpcid"
  
  InstanceSecurityGroup:
    Description: The Instance Security Group
    Value: !Ref InstanceSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-instance-sg"

  LoadBalancer:
    Description: The ALB arn
    Value: !Ref LoadBalancer
    Export:
      Name: !Sub "${AWS::StackName}-alb-arn"

  AlbSecurityGroup:
    Description: The ALB Security Group
    Value: !Ref AlbSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-alb-sg"

  AlbTargetGroup:
    Description: The ALB Target Group
    Value: !Ref AlbTargetGroup
    Export:
      Name: !Sub "${AWS::StackName}-alb-tg"

  AlbDns:
    Description: The ALB DNS Entry
    Value: !GetAtt LoadBalancer.DNSName
    Export:
      Name: !Sub "${AWS::StackName}-alb-dns"

  AlbListener:
    Description: SSL Listener
    Value: !Ref AlbListener443
    Export:
      Name: !Sub "${AWS::StackName}-alb-listener"

  EcsCluster:
    Description: The ECS Cluster
    Value: !Ref EcsCluster
    Export:
      Name: !Sub "${AWS::StackName}-ecs-cluster"
