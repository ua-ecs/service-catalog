# Ansible Deployer EC2 Instance

This CloudFormation template will deploy a single EC2 node and install Ansible on it.
It will also create an Admin Role and assign that role as an instance profile. This will
give this EC2 instance Admin access to the AWS Account. Use caution when deploying this,
and delete this stack as soon as you are finished.

## Goals
* Provide a standard way of using Ansible to drive CloudFormation deployments

## Variants

This portfolio has 3 templates in it

#### admin-deployer
This template simply creates an EC2 instance and a full access IAM role for it. No
user-data script is run.

#### admin-ansible-ec2.yaml
This template does the same as the one above, but has a user-data section which installes
Ansible, and checks out the UITS Service Catalog project from bitbucket. So its ready to
do whatever sort of deployment you want, but you have to log in and do it manually.

#### uits-foundation-deployer.yaml
This template builds on the first two, and actually kicks off the Ansible playbook to 
deploy the account-foundation portfolio. This makes it easy to initially deploy the foundation
templates to a new account, as well as update them as changes are made. No interactive login
should be required on the EC2 host.

## Usage


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
