# Basic MySQL Instance

The Basic MySQL Instance blueprint (mysql-rds) is intended as a quick and simple RDS instance for a MySQL RDS instance.  There is an option for Multi-AZ support.

## Goals
* Provide a quick way to spin up a MySQL RDS Instance
* Allow operations staff to deploy MySQL RDS instances with minimal configuration

## Assumptions

1. A VPC Exists, with appropriate subnets



## Usage



## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
