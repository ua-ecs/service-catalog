# Basic EC2 Instance

The Basic EC2 Instance blueprint (ec2basic) is intended as a quick and simple test machine for
anyone to use to prototype things in AWS.  It creates a single stand alone EC2 instance of
small to moderate size with a public IP address.

This will be made available via the AWS Service Catalog.

## Goals
* Provide a quick way to spin up a simple EC2 instance
* Allow operations staff to deploy EC2 instances with minimal configuration

## Foundation/Installation

To deploy the foundation for this blueprint:

1. Update JSON template with VPC and Subnet IDs

The target VPC that these EC2 instances will deploy into must be coded into the template before being
added to the Service Catalog.  Also the available subnets need to be defined in the Parameters section
of the CloudFormation template. These should be public subnets in the target VPC.

2. Make sure IAM Policy is correct for users to deploy this from the Service Catalog. 

There are two policies that need to be configured for allowing IAM users who might not have permissions
to create EC2 resources to do so through the Service Catalog. 
* ServiceCatalogBasicPolicy.json
* DeployEC2Policy.json

Add these policies to a role, and set that role as the launch role for this Service Catalog Product.


## Usage

Users that have access to Service Catalog and are assigned to the correct group will be able to search for
this product and launch it via the Service Catalog in the AWS Console. At the time when this product was
created, there is a limitation where CloudFormation does not propagate tags to nested stacks. For this reason, the
owner, netid, and projectname must be entered in the Parameters screen and manually added to the tags screen. These tags
are required and used for tracking and cost management.

Here is a detailed step by step to launch this product:

_Important_
Launching Linux EC2 Instances requires SSH Key Pairs. A Key Pair must be created before launching
an EC2 instance so it can be selected in the configuration screen.  Create a Key Pair and name it
appropriately for this EC2 instance.  Keys can be re-used across multiple instances, so the customer
may already have one. Verify before proceeding.

1. Log into the AWS Console and go to Service Catalog
2. Search for 'EC2: Basic', select it, and click launch.
3. Add a stack name that will allow you to identify it and click next.
4. Add a host name.
5. Manually add the tags owner, netid, and projectname on the tags screen and populate the same values as the Parameters
screen.
6. Review the information and click Launch. 
7. Once the stack if finished launching, you will be presented with the IP address.


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
