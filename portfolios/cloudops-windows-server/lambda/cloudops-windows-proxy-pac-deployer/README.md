# CloudOps - Windows Server - proxy.pac deployer

This Lambda function takes a passed in LDAP group description value (GroupName) and checks whether or not a proxy.pac file exists in an S3 bucket for that group. If it does not exist, a copy is made of the master proxy.pac file for the group.

* This is only intended to be used as a CloudFormation custom resource. It is coded specifically for the UArizona CloudOps AWS accounts and will not work in other accounts without modification.

## Deployment

Your environment will need to be configured with AWS credentials for the account you want to deploy to.

It is deployed using the Serverless Framework. If you don't have NPM installed it can be downloaded [here](https://nodejs.org/en). To install the Serverless Framework, run:

    npm install -g serverless

For a non-prod deployment:

```
git clone https://bitbucket.org/ua-ecs/service-catalog
cd service-catalog/portfolios/cloudops-windows-server/lambda/cloudops-windows-proxy-pac-deployer
sls plugin install -n serverless-python-requirements
sls deploy
```

For a prod deployment:

```
git clone https://bitbucket.org/ua-ecs/service-catalog
cd service-catalog/portfolios/cloudops-windows-server/lambda/cloudops-windows-proxy-pac-deployer
sls plugin install -n serverless-python-requirements
sls deploy --stage prod
```

## Usage

To use the custom resource in your CloudFormation template, in the Resources section:

```
  ProxyPacDeployer:
    Type: Custom::ProxyPacDeployer
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:cloudops-windows-proxy-pac-deployer"
      GroupName: "group_name_goes_here"
```