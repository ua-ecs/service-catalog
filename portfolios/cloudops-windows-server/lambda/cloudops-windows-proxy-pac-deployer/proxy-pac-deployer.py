# UArizona CloudOps Windows Server proxy.pac deployer 
#
# If a group doesn't already have a proxy.pac, this deploys a copy of the master proxy.pac to a
# new key with a prefix of the group name. 

import boto3
import os
from crhelper import CfnResource

crhelper = CfnResource()
s3 = boto3.client('s3')

@crhelper.create
def create(event, context):

    try:

        # Get the group name from the CFN resource properties
        group_name = event['ResourceProperties']['GroupName']

        # Get the bucket name from the environment variable
        s3_bucket = os.environ['BUCKETNAME']

        # Build the object keys
        proxy_key_master = "master/CLOPS-Template-proxy.pac"
        proxy_key_group = group_name + "/proxy.pac"

        # Check if a key for group's proxy.pac does not already exist
        if not 'Contents' in s3.list_objects( Bucket = s3_bucket, Prefix = proxy_key_group ):
            # If it doesn't already exist, copy the master key to the group's key
            s3.copy_object( Bucket = s3_bucket, CopySource = s3_bucket + "/" + proxy_key_master, Key = proxy_key_group )
            print("Created object (" + proxy_key_group + ") from master proxy.pac as it did not already exist.")
        else:
            print("Group (" + group_name + ") already has a proxy.pac, no action taken.")
    except Exception as error:
        error_message = str(error)
        print("ERROR: " + error_message)
        raise ValueError(error_message)

@crhelper.delete
@crhelper.update
def do_nothing(event, context):
    pass

def handler(event, context):
    crhelper(event, context)