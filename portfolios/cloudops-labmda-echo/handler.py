import os
import logging
import json
import ptvsd


rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)


if os.getenv("AWS_SAM_LOCAL") or os.getenv("IS_LOCAL"):
    # Local Only Config
    rootlogger.setLevel(logging.DEBUG)
    if os.getenv("REMOTE_DEBUGGING") == 'true':
        logging.info("Activating Debugger")
        ptvsd.enable_attach(address=('0.0.0.0', 5678), redirect_output=True)
        ptvsd.wait_for_attach()
        logging.info("Debugger Connected")
else:
    # Regular AWS Config
    pass


def handler(event, context):
    logging.info(json.dumps(event, indent=2))
    return(event)
