# CloudOps Lambda Echo

This is a very basic Lambda function which just echos back the event object passed to it.

You will need active IAM credentials in your environment for the target account you want to deploy to.

Deployed with the serverless framework:

    npm run setup
    npm run local <AWS_ACCOUNT_ID>
    npm run package <AWS_ACCOUNT_ID>
    npm run deploy <AWS_ACCOUNT_ID>


When you're finished, remove unneeded stuff:

    npm run clean
