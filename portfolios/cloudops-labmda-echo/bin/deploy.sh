#!/bin/bash -e

ACCOUNT_NUMBER=$1

if [ "$ACCOUNT_NUMBER" == "" ]
then
    echo "Deployment Account Number required"
    echo "Syntax: npm run deploy 123456789012"
    echo ""
    exit 0
fi

sls deploy --verbose --aws-account-id $ACCOUNT_NUMBER

