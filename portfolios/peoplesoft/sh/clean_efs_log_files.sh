base_folder="${1,,}"

for f in $(ls -d "/$base_folder/uaz"* 2>/dev/null) ; do
    for g in app batch web ; do
        for h in $(ls -d "$f/logs/$g/"* 2>/dev/null) ; do
            #Find a count of files that are younger than 10 days old
            match_count=$(find "$h" -xdev -mtime -10 -exec ls -ld {} \; | wc -l)
            #If there are no files in the directory 10 days or younger then they
            #can be removed
            if [ $match_count -eq 0 ] ; then
                #Only remove the directory if there is not a file named AAA_SAVE
                #This provides a means to save a directory from being removed
                if ! [ -e "$h/AAA_SAVE" ]; then
                  rm -rf "$h"
                fi
            fi
        done
    done
done
