#!/bin/bash
#*******************************************************************************
#
# TITLE......: build_ps_env.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA) (req)
#              -e  Environment (DEV, TST, STG, etc) (req)
#              -s  Source Environment 
#              -d  Destroy the CloudFormation Template first, if it exists and rebuild
#              -D  Destroy the CloudFormation Template first, if it exists and stop
#              -z  Set up like production
#              -Z  Beef up non production environment (a bigger env for major testing)
#              -o  Obscure data if this is a refresh
#              -P  Point in time refresh (if left blank the --use-latest-restorable-time will be used)
#                  Example format: 2017-11-26T18:56:40MST (YYYY-MM-DDTHH:MI:SSZONE)
#                  Bash date format date +"%Y-%m-%dT%H:%M:%S%Z" - 2017-11-26T18:56:40MST
#              -m  Mode - Use "a" to just apply this to the app/web layer and leave the DB alone
#              -n  No Snapshot - If this is present then a final snapshot will not be taken when 
#                  the RDS instnace is dropped.  Useful for TRN environments, DMO, and also SUP.
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="EL HR SA"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values EL, HR, or SA (${UPPER_PILLAR})" 
fi 
LOWER_PILLAR=${UPPER_PILLAR,,}

#Make sure SET_UP_LIKE_PRD is set
if [ "${UPPER_ENV}" = "PRD" ]; then
  SET_UP_LIKE_PRD="Y"
fi

# -- Set the Slack Channel name to send refresh messages
REFRESH_SLACK_CHANNEL="${LOWER_PILLAR}-refresh"

#  -- create Service Tag based on pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_SERVICE="Uaccess Learning"
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Learning"
    ;;
  HR)
    TAG_SERVICE="Uaccess Employee"
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Employee"
    ;;
  SA)
    TAG_SERVICE="Uaccess Student"
    TAG_ACCOUNT_NUMBER="1192630"
    TAG_SUBACCOUNT="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Service Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- make sure an Environment was passed
if [ "${UPPER_ENV}" = "" ]; then
  error_abort "An environmnet was not passed, use -e and include the enviornment name i.e. DEV, TST, STG etc."
fi 
LOWER_ENV=${UPPER_ENV,,}

# -- if the source environment was not passed then default to the target environment
if [ -z ${UPPER_SOURCE_ENV} ]; then
  UPPER_SOURCE_ENV=${UPPER_ENV}
fi
LOWER_SOURCE_ENV=${UPPER_SOURCE_ENV,,}

# -- Validate MODE, if one was not passed then default to B for Both
if [ "$MODE" = "" ]; then
  MODE="B"
fi

if [ "$MODE" != "A" ] && [ "$MODE" != "B" ]; then
  error_abort "Mode passed it not valid.  Must be A for just the Appliation or B for Both App and DB ($MODE)"
fi

# -- Default PSWEB_REQUIRE_DUO to Y meaning that the env should be shib enabled
PSWEB_REQUIRE_DUO="Y"

# -- if the Env and Source Env do not match then we need to set the RUN_POST_REFRESH flag to Y
# -- Do not run post_refresh scripts for PRD environments
# -- otherwise it will be set to N
if [ "${UPPER_ENV}" == "${UPPER_SOURCE_ENV}" ] || [ "${UPPER_ENV}" == "PRD" ]; then
  RUN_POST_REFRESH="N"
  #Also set the Point in Time Refresh flag to N as well.  Point in Time refreshes only make sense
  #when the source and target are different
  PIT_REFRESH="N"
else
  RUN_POST_REFRESH="Y"
fi

# -- Set SOURCE and TARGET DB Instance Variables
SOURCE_DB_INSTANCE="peoplesoft-${LOWER_PILLAR}${LOWER_SOURCE_ENV}"
TARGET_DB_INSTANCE="peoplesoft-${LOWER_PILLAR}${LOWER_ENV}"

# -- default DESTROY_IF_EXISTS to N if it was not passed via the -D argument
# -- this parameter will be used to determine if we will destroy the environmnet
# -- before we build it.  This will be the case if we are refreshing an environment.
if [ -z ${DESTROY_IF_EXISTS} ]; then
  DESTROY_IF_EXISTS="N"
fi

# -- CloudFormation Stack Name will be PeopleSoft+UPPER_PILLAR+UPPER_ENV
CF_STACK_NAME="PeopleSoft${UPPER_PILLAR}${UPPER_ENV}-ECS"
CF_STACK_NAME_DB="PeopleSoft${UPPER_PILLAR}${UPPER_ENV}-DB"


# -- Before checking on the environment check to see if this is a Point in Time refresh
# -- At this point we need to check to ensure if a date was passed that it follows a proper date format
if [ "$PIT_REFRESH" = "Y" ]; then
  #If the PIT_DATE is not blank then validate the date format
  if ! [ -z $PIT_DATE ]; then
    date +"$PIT_FORMAT" -d "$PIT_DATE" 2>&1 > /dev/null
    if ! [ "$?" = "0" ]; then
      error_abort "Point in Time refresh date format invalid should look like 2017-11-26T18:56:40MST ($PIT_DATE)"
    fi
  fi
fi

# -- HostedZoneName will be ps-nonprod-aws.arizona.edu for non PRD and non SUP environments
# -- HostedZoneName will be ps-prod-aws.arizona.edu for PRD and SUP
# -- May want to see if we can describe this from a CLI command
# -- First default the Fully Qualified Domain Name prefix to pillar-env (el-dev)
FQDN_PREFIX="${LOWER_PILLAR}-${LOWER_ENV}"
PUB_FQDN_PREFIX="${LOWER_PILLAR}-${LOWER_ENV}"
case ${AWS_ACCOUNT} in
  415418166582) #Non Prod
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'

    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  242275737326) #Production
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    TEMPLATE_URL='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'

    HOSTED_ZONE_NAME="ps-prod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-prod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="uaccess.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="schedule.arizona.edu"

    if [ "${UPPER_ENV}" = 'SUP' ]; then
      if [ "${UPPER_PILLAR}" == "HR" ]; then
        FQDN_PREFIX="employeesup851"
      elif [ "${UPPER_PILLAR}" == "SA" ]; then
        FQDN_PREFIX="studentsup851"
        PUB_FQDN_PREFIX="uaccesssup"
      fi
    fi

    if [ "${UPPER_ENV}" = 'RPT' ]; then
      FQDN_PREFIX="studentrpt"
      PUB_FQDN_PREFIX="uaccesssup"
    fi

    if [ "${UPPER_ENV}" = 'PRD' ]; then
      if [ "${UPPER_PILLAR}" == "HR" ]; then
        FQDN_PREFIX="employee851"
      elif [ "${UPPER_PILLAR}" == "SA" ]; then
        FQDN_PREFIX="student851"
        PUB_FQDN_PREFIX='uaccess'
      else
        FQDN_PREFIX="learning"
      fi
    fi
    ;;
  507937877461) #DR
    ROLE_ARN="arn:aws:iam::507937877461:role/fdn-iam-CloudFormationAdminDeployerRole-1EL4BVAXEQ9BM"
    TEMPLATE_URL='http://peoplesoft-dr-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-dr-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'

    HOSTED_ZONE_NAME="dr-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-dr-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="dr-aws.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-dr-aws.arizona.edu"

    if [ "${UPPER_ENV}" = 'SDR' ]; then
      if [ "${UPPER_PILLAR}" == "HR" ]; then
        FQDN_PREFIX="employeesup851"
      elif [ "${UPPER_PILLAR}" == "SA" ]; then
        FQDN_PREFIX="studentsup851"
        PUB_FQDN_PREFIX="uaccesssup"
      fi
    fi

    if [ "${UPPER_ENV}" = 'PDR' ]; then
      if [ "${UPPER_PILLAR}" == "HR" ]; then
        FQDN_PREFIX="employee851"
      elif [ "${UPPER_PILLAR}" == "SA" ]; then
        FQDN_PREFIX="student851"
        PUB_FQDN_PREFIX='uaccess'
      else
        FQDN_PREFIX="learning"
      fi
    fi
    ;;
  *)
    error_abort "Could not determine App Bit Bucket Branch based on the AWS Account (${AWS_ACCOUNT})"
    ;;
esac

# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

#######################
#######################
# -- make sure the Pillar/Environment exists
# -- find the most recent RDS snapshot for the Pillar/Source Env passed
# -- find latest snapshot, this will return a snapshot name, we will test to see if that snapshot exists
# -- in the next step
#DB_LATEST_SNAPSHOT=`aws rds describe-db-snapshots --db-instance-identifier "${SOURCE_DB_INSTANCE}" --output text 2>&1 | sort -k10 -r | awk "NR==1{print}" | awk '{print $6}'`
DB_LATEST_SNAPSHOT=$(aws rds describe-db-snapshots --db-instance-identifier "${SOURCE_DB_INSTANCE}" --output text --query 'DBSnapshots[?Status==`available`].[DBInstanceIdentifier,DBSnapshotIdentifier,SnapshotCreateTime]' 2>&1 | sort -k3 -r | awk "NR==1{print}" | awk '{print $2}')

# -- if the variable was not populated then default it to something
if [ -z ${DB_LATEST_SNAPSHOT} ]; then
  DB_LATEST_SNAPSHOT="~~none~~"
fi

# -- now check to see that the DB_LATEST_SNAPSHOT actually exists
# -- PSAWS-47 grabbing the EngineVersion of the current snapshot
#aws rds describe-db-snapshots --db-instance-identifier "${SOURCE_DB_INSTANCE}" --output text 2>&1 | grep ${DB_LATEST_SNAPSHOT} &> /dev/null
CURRENT_DB_VERSION=$(aws rds describe-db-snapshots --db-instance-identifier ${SOURCE_DB_INSTANCE} --db-snapshot-identifier ${DB_LATEST_SNAPSHOT} --output text --query 'DBSnapshots[*].[EngineVersion]' 2>/dev/null)
if [ -z "${CURRENT_DB_VERSION}" ]; then
  error_abort "An RDS Snapshot does not exist for the DB Instance ${SOURCE_DB_INSTANCE} - ${DB_LATEST_SNAPSHOT}"
fi


# -- Populate the variable TLS12, which will be passed via a Docker Variable into the Web and App container
# -- if set to Y then TLS1.2 parameters will be set on the Web and App otherwise they will not be set
# -- TLS is a ciper for SSL and 1.0 has to be removed, we are going to 1.2 because we are able with the 
# -- version of java we have on the web and app servers
# -- As of 09/17/2017 all non prod environments will be set to Y, production will be set to N
#11/29/2017 changing to Y for all environments
TLS12="Y"

# -- Web Listener Policy Name will be ELBSecurityPolicy-2016-08 as of 01/03/2017
# -- May want to see if we can describe this from a CLI command
# -- Changed to ELBSecurityPolicy-TLS-1-2-2017-01 to we elimiate TLS1.0
# -- If TLS12 = Y then set to ELBSecurityPolicy-TLS-1-2-2017-01, otherwise set to ELBSecurityPolicy-2016-08
if [ "$TLS12" = "Y" ]; then
  WEB_ELB_LISTENER_POLICY_NAME="ELBSecurityPolicy-TLS-1-2-2017-01"
else
  WEB_ELB_LISTENER_POLICY_NAME="ELBSecurityPolicy-2016-08"
fi

# -- SSL Cert ARN will be arn:aws:iam::415418166582:server-certificate/ps-nonprod-aws.arizona.edu_2016 for nonprod
# -- and ? for prod
if [ "${HOSTED_ZONE_NAME}" == "ps-nonprod-aws.arizona.edu" ]; then
  #mosaic.arizona.edu
  SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/28c01581-cf7a-45cb-98eb-0d7e4678a1ca"

  if [ "${UPPER_PILLAR}" == "SA" ]; then
    #ps-pub-nonprod-aws.arizona.edu - Need to 
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/a36477f3-b5b4-45ea-8238-ea0b19b8c104"
  else
    #Public cert will match the private cert for PILLAR <> SA
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/28c01581-cf7a-45cb-98eb-0d7e4678a1ca"
  fi
elif [ "${HOSTED_ZONE_NAME}" == "ps-prod-aws.arizona.edu" ]; then
  if [ "${UPPER_PILLAR}" == "SA" ]; then
    #uaccess.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/eeeeacdb-dc78-4191-9d36-a114013c50e3"
    #There are two certs for the PROD account
    #uaccess.schedule.arizona.edu - is for prd
    #uaccesssup.schedule.arizona.edu -  is for sup
    if [ "${UPPER_ENV}" = "SUP" ]; then
      #uaccesssup.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/3cee344c-d799-457c-9ecd-7d8db7cfb0aa"
    elif [ "${UPPER_ENV}" = "PRD" ]; then
      #uaccess.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/f5466aea-49d0-49a1-8118-0f4dd3a9b96b"
    fi
  else
    #uaccess.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/eeeeacdb-dc78-4191-9d36-a114013c50e3"
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/eeeeacdb-dc78-4191-9d36-a114013c50e3"
  fi
elif [ "${HOSTED_ZONE_NAME}" == "dr-aws.arizona.edu" ]; then
  if [ "${UPPER_PILLAR}" == "SA" ]; then
    #dr-aws.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-east-1:507937877461:certificate/b3dea8c7-c750-4708-9769-78192cd313fe"
    #There are two certs for the PROD account
    #uaccess.schedule.arizona.edu - is for prd
    #uaccesssup.schedule.arizona.edu -  is for sup
    ##Need to figure this out for DR when we do a DR test for SA
    if [ "${UPPER_ENV}" = "SUP" ]; then
      #uaccesssup.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/3cee344c-d799-457c-9ecd-7d8db7cfb0aa"
    elif [ "${UPPER_ENV}" = "PRD" ]; then
      #uaccess.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/f5466aea-49d0-49a1-8118-0f4dd3a9b96b"
    fi
  else
    #dr-aws.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-east-1:507937877461:certificate/b3dea8c7-c750-4708-9769-78192cd313fe"
    ##Need to figure this out for DR when we do a DR test for SA
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/eeeeacdb-dc78-4191-9d36-a114013c50e3"
  fi
else
  error_abort "Could not determine the SSL Cert ARN based on the Hosted Zone Name (${HOSTED_ZONE_NAME})"
fi

# -- Docker Images
# -- This will change frequently and may be different from Environment to Environment
# -- Will hard code for now and revisit in the future
case ${UPPER_ENV} in
  DMO)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    PSWEB_REQUIRE_DUO="N"
    ;;
  DMA)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    PSWEB_REQUIRE_DUO="N"
    ;;
  DMB)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85705-Oct2019CPUv1"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85705-Oct2019CPU"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85620-Oct2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85620-Oct2019CPUv1"
    fi
    PSWEB_REQUIRE_DUO="N"
    ;;
  DMC)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    PSWEB_REQUIRE_DUO="N"
    ;;
  DEV)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TRN)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85618-Oct2019CPU-v2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85618-Oct2019CPU-v2"
    fi
    ;;
  TST)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEA)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85705-Oct2019CPUv1"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85705-Oct2019CPU"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEB)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85705-Oct2019CPUv1"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85705-Oct2019CPU"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEC)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos7.6.1810-PT85705-Oct2019CPUv1"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos7.6.1810-PT85705-Oct2019CPU"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TED)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEE)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEF)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEG)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEH)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  TEI)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85524-Apr2019CPUv2"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85524-Apr2019CPUv2"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos6-PT85525-Apr2019CPUv4"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos6-PT85525-Apr2019CPUv4"
    fi
    ;;
  STG)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  SUP)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  RPT)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
 PRD)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  PDR)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  SDR)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/app-batch:centos-7.7.1908-PT8.57.12-Apr2020CPU-v2-inflight"
      WEB_DOCKER_IMAGE="507937877461.dkr.ecr.us-west-2.amazonaws.com/web:centos-7.7.1908-PT8.57.12-Apr2020CPU-v7"
    fi
    ;;
  *)
    error_abort "Could not determine a Docker Images based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- Web Profile Name 
# -- Will be DEV for all non-prod environments and PROD for prod environrments
# -- Will hard code for now and revisit in the future
if [ "${UPPER_ENV}" == "PRD" ] || [ "${UPPER_ENV}" == "SUP" ] || [ "${UPPER_ENV}" == "PDR" ] || [ "${UPPER_ENV}" == "SDR" ]; then
  WEB_PROFILE="PROD"
else
  WEB_PROFILE="DEV"
fi

#Add this just for testing will remove after
if [ "${UPPER_ENV}" == "TEC" ] && [ "${UPPER_PILLAR}" == "HR" ]; then
  WEB_PROFILE="PROD"
fi

# -- PS Reports Directory
# -- Will be /[pillar]/reports/[env]
PSREPORTS_DIR="/${LOWER_PILLAR}/reports/uaz${LOWER_PILLAR}${LOWER_ENV}"

# -- App or Batch with be BOTH for all non-production environments and BATCH for production environments
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then
  APP_OR_BATCH="APP"
else
  APP_OR_BATCH="BOTH"
fi

# -- App Template will be small for all non-production environments and large for production
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then
  # -- MEM-18377 will change template to small from large to small
  APP_TEMPLATE="small"
else
  APP_TEMPLATE="small"
fi

# -- App OPRID will be UAZPRCS and the password as well, need to find a better way to handle creds
if [ "${UPPER_ENV}" == "DMO" ] || [ "${UPPER_ENV}" == "DMA" ] || [ "${UPPER_ENV}" == "DMB" ] || [ "${UPPER_ENV}" == "DMC" ]; then 
  APP_OPR_ID="PS"
  APP_OPR_ID_PW="PS"
else
  APP_OPR_ID=$(aws ssm get-parameter --name "PeopleSoftUser" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
  APP_OPR_ID_PW=$(aws ssm get-parameter --name "PeopleSoftUserPW" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
fi

# -- App Bit Bucket Branch will vary based environment
# -- MEM-21853 app_bit_bucket_branch will be the same of lower_env
APP_BIT_BUCKET_BRANCH=${LOWER_ENV}

#case ${UPPER_ENV} in
#  DMO)
#    APP_BIT_BUCKET_BRANCH="dmo"
#    ;;
#  DMA)
#    APP_BIT_BUCKET_BRANCH="dma"
#    ;;
#  DMB)
#    APP_BIT_BUCKET_BRANCH="dmb"
#    ;;
#  DMC)
#    APP_BIT_BUCKET_BRANCH="dmc"
#    ;;
#  DEV)
#    APP_BIT_BUCKET_BRANCH="dev"
#    ;;
#  TST)
#    APP_BIT_BUCKET_BRANCH="tst"
#    ;;
#  TEA)
#    APP_BIT_BUCKET_BRANCH="tea"
#    ;;
#  TEB)
#    APP_BIT_BUCKET_BRANCH="teb"
#    ;;
#  TEC)
#    APP_BIT_BUCKET_BRANCH="tec"
#    ;;
#  TED)
#    APP_BIT_BUCKET_BRANCH="ted"
#    ;;
#  TEE)
#    APP_BIT_BUCKET_BRANCH="tee"
#    ;;
#  TEF)
#    APP_BIT_BUCKET_BRANCH="tef"
#    ;;
#  TEG)
#    APP_BIT_BUCKET_BRANCH="teg"
#    ;;
#  TEH)
#    APP_BIT_BUCKET_BRANCH="teh"
#    ;;
#  TEI)
#    APP_BIT_BUCKET_BRANCH="tei"
#    ;;
#  STG)
#    APP_BIT_BUCKET_BRANCH="stg"
#    ;;
#  TRN)
#    APP_BIT_BUCKET_BRANCH="trn"
#    ;;
#  SUP)
#    APP_BIT_BUCKET_BRANCH="sup"
#    ;;
#  SDR)
#    APP_BIT_BUCKET_BRANCH="sdr"
#    ;;
#  RPT)
#    APP_BIT_BUCKET_BRANCH="rpt"
#    ;;
#  PRD)
#    APP_BIT_BUCKET_BRANCH="prd"
#    ;;
#  PDR)
#    APP_BIT_BUCKET_BRANCH="pdr"
#    ;;
#  *)
#    error_abort "Could not determine App Bit Bucket Branch based on the target environment passed (${UPPER_ENV})"
#    ;;
#esac

# -- SES Server will be sesdev.ps-nonprod-aws.arizona.edu for non prod and sesprd.ps-nonprod-aws.arizona.edu for
# -- SUP and PRD
case ${UPPER_ENV} in
  DMO)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DMA)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DMB)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DMC)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DEV)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TRN)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TST)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEA)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEB)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEC)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TED)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEE)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEF)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEG)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEH)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEI)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  STG)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  SUP)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  RPT)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  PRD)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  PDR)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  SDR)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  *)
    error_abort "Could not determine SES_SERVER based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- SES Definitions will vary passed on pillar
case ${UPPER_PILLAR} in
  EL)
    SES_DEFNS="LS_LM_ACT_CI,LS_LM_LEARNING,LS_LM_OBJV,LS_LM_PRG,PTPORTALREGISTRY"
    ;;
  HR)
    SES_DEFNS="HC_BEN_HEALTH_BENEFIT,PTPORTALREGISTRY"
    ;;
  SA)
    SES_DEFNS="PTPORTALREGISTRY"
    ;;
  *)
    error_abort "Could not determine Search Definitions based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- DB Multi Zone will be true for production and false for everything else
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
  DB_MULTI_ZONE="true"
else
  DB_MULTI_ZONE="false"
fi

# -- DB Inst Class must be at least db.t2.large for encryption to work
# -- MEM-20427 added parameters for WAP, IB, and Batch instance class and WAP Count
case ${UPPER_PILLAR} in
  EL)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m5.large"
      WAP_INST_CLASS="r5.large"
      WAP_INST_COUNT="2"
      IB_INST_CLASS="r5.large"
      BATCH_INST_CLASS="m5.large"
    elif [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
      DB_INST_CLASS="db.m5.large"
      WAP_INST_CLASS="r5.large"
      WAP_INST_COUNT="1"
      IB_INST_CLASS="r5.large"
      BATCH_INST_CLASS="t3.large"
    else
      DB_INST_CLASS="db.m5.large"
      WAP_INST_CLASS="r5.large"
      WAP_INST_COUNT="1"
      IB_INST_CLASS="r5.large"
      BATCH_INST_CLASS="m5.large"
    fi
    ;;
  HR)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m5.2xlarge"
      WAP_INST_CLASS="r5.xlarge"
      WAP_INST_COUNT="4"
      IB_INST_CLASS="r5.xlarge"
      BATCH_INST_CLASS="m5.large"
    else
      #Make SUP and m5.xlarge, other nonprods just use m5.large
      if [ "${UPPER_ENV}" = "SUP" ]; then
        DB_INST_CLASS="db.m5.xlarge"
        WAP_INST_CLASS="r5.large"
        WAP_INST_COUNT="1"
        IB_INST_CLASS="r5.large"
        BATCH_INST_CLASS="m5.large"
      else
        if [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
          DB_INST_CLASS="db.m5.xlarge"
          WAP_INST_CLASS="r5.xlarge"
          WAP_INST_COUNT="1"
          IB_INST_CLASS="r5.large"
          BATCH_INST_CLASS="m5.large"
        else
          DB_INST_CLASS="db.m5.large"
          WAP_INST_CLASS="r5.large"
          WAP_INST_COUNT="1"
          IB_INST_CLASS="r5.large"
          BATCH_INST_CLASS="m5.large"
        fi
      fi
    fi
    ;;
  SA)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m5.2xlarge"
      WAP_INST_CLASS="r5.xlarge"
      WAP_INST_COUNT="6"
      IB_INST_CLASS="r5.xlarge"
      BATCH_INST_CLASS="m5.xlarge"
    else
      #Make SUP and m5.xlarge, other nonprods just use m5.large
      if [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ]; then
        DB_INST_CLASS="db.m5.xlarge"
        WAP_INST_CLASS="r5.large"
        WAP_INST_COUNT="1"
        IB_INST_CLASS="r5.large"
        BATCH_INST_CLASS="m5.large"
      else
        if [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
          DB_INST_CLASS="db.m5.xlarge"
          WAP_INST_CLASS="r5.xlarge"
          WAP_INST_COUNT="1"
          IB_INST_CLASS="r5.large"
          BATCH_INST_CLASS="m5.large"
        else
          DB_INST_CLASS="db.m5.large"
          WAP_INST_CLASS="r5.large"
          WAP_INST_COUNT="1"
          IB_INST_CLASS="r5.large"
          BATCH_INST_CLASS="m5.large"
        fi
      fi
    fi
    ;;
  *)
    error_abort "Could not determine DB Inst Class based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- DB Storage Type will be gp2 (general purpose) for all DBs for now
DB_STORAGE_TYPE="gp2"

# -- For now HR environments will be behing the VPN so we will not make the ELB public facing
# -- all other environments will be pubic facing
if [ "$UPPER_PILLAR" == "HR" ]; then
  PUBLIC_FACING_ELB="Y"
else
  PUBLIC_FACING_ELB="Y"
fi


# -- MEM-18660 Sender Email Address for app server
APP_SENDER_EMAIL="peoplesoft+${LOWER_PILLAR}${LOWER_ENV}@email.arizona.edu"
# -- For HR Production the sender needs to be UAccessEmployee
if [ "${UPPER_PILLAR}" = "HR" ] && [ "${UPPER_ENV}" = "PRD" ]; then
  APP_SENDER_EMAIL="UAccessEmployee@email.arizona.edu"
fi

# -- This if the AMI for the ECS Instance running in the ECS cluster 
# -- it will change from time to time
# -- Image last updated on 09/17/2017 
# -- Go here to find latest image 
# -- http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
# -- Updated to ami-decc7fa6 (amzn-ami-2017.09.g-amazon-ecs-optimized) 01/22/2018
ECS_IMAGE_ID=ami-decc7fa6

# -- SAAWS-1 Add logic for the Public Sites
# -- For now we will create a public site for DEV, TST, STG, SUP, PRD
# -- MEM-19784 08/06/2018 - Added TEA, TEB, and TEC to the list
# -- MEM-20361 01/30/2019 - Added TED, TEE, and TEF to the list
CREATE_PUBLIC_SITE="N"
ENV_LIST="DEV TST STG TEA TEB TEC TED TEE TEF TEG TEH SUP PRD"
if [ "$UPPER_PILLAR" = "SA" ]; then
  for env in $ENV_LIST
  do
    if [ "$UPPER_ENV" = $env ]; then
      CREATE_PUBLIC_SITE="Y"
    fi
  done
fi

# -- PSAWS-69 find perferred AZ, need to place all nonprod environments in the same
# -- AZ in order to avoid some batch processes from suffering from latency
PREFERRED_AZ=$(aws ec2 describe-availability-zones \
               --output text \
               --region ${REGION} \
               --filters "Name=state,Values=available" \
               --query 'AvailabilityZones[*].ZoneName' | awk '{print $1}')

# -- PSAWS-69 find perferred AZ, need to place all nonprod environments in the same
# -- Not that we have the Preferred AZ, get the Private Subnet that is in that AZ
PREFERRED_PRIVATE_SUBNET=$(aws ec2 describe-subnets --output text --region ${REGION} --filters "Name=availabilityZone,Values=${PREFERRED_AZ}" "Name=tag-key,Values=Name" "Name=tag-value,Values=*Private*" --query 'Subnets[*].[SubnetId]' | awk '{print $1}')

# -- SAAWS-350 list of INAS mak files that need to be run when COBOLs are compiled
#INAS_MAK_FILES="inasbl16.mak inasbl17.mak inasbl18.mak"
#MEM-20047 - new financial aid patch 10/02/2018
INAS_MAK_FILES="inasbl17.mak inasbl18.mak inasbl19.mak"

# -- MEM-19942 introduced the ability to not take a snapshot when a RDS instance is deleted
# If the TAKE_SNAPSHOT variable does not = Y or N then just make it Y
if [ "${TAKE_SNAPSHOT}" != "Y" ] && [ "${TAKE_SNAPSHOT}" != "N" ]; then
  TAKE_SNAPSHOT="Y"
fi

# -- PSAWS-74 - Add DB Engine Parameter to pass to DB Build
# -- If this is PRD or SUP just use the DBEngineVersion of the snapshot the RDS instance will be built, otherwise use the default version
# -- when a new version is released this will automatically pick up the new version and apply it to the env on the next build
# -- grab the EngineVersion of the current snapshot

# -- This expression will return everything before the 2nd period in the full version
# -- For example, 12.1.0.2.v17 is the value of CURRENT_DB_VERSION, this will pull out the 12.1
# -- Get the first number before the first period.  If this is > then 12 we just need that, if this is 12 or less we need the next number after the first period

DB_ENGINE_MAJOR_VERSION=$(echo ${CURRENT_DB_VERSION} | awk -F. '{print $1}')

#--Determine the DB parameter group and option group to be used based on the major version
case ${DB_ENGINE_MAJOR_VERSION} in
  12)
    DB_PARAMETER_GROUP="peoplesoft-oracle-ee-12-1"
    DB_OPTION_GROUP="timezone"
    DB_OPTION_GROUP_OEM="oem-agent-ee-12-1"
    ;;
  19)
    DB_PARAMETER_GROUP="peoplesoft-oracle-ee-19"
    DB_OPTION_GROUP="timezone19"
    DB_OPTION_GROUP_OEM="oem-agent-ee-19"
    ;;
  \?)
    error_abort "Could not determine DB Parameter Group or Option Group based on the current DB Major Version  (${DB_ENGINE_MAJOR_VERSION})"
    ;;
esac

# -- If the major version is less than 13 then we need the next number
if (( ${DB_ENGINE_MAJOR_VERSION} < 13 )); then
  DB_ENGINE_MAJOR_VERSION=$(echo ${CURRENT_DB_VERSION} | awk -F. '{print $1"."$2}')
fi

if [ "${UPPER_ENV}" = "PRD" ]|| [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ] || [ "${UPPER_ENV}" = "PDR" ]|| [ "${UPPER_ENV}" = "SDR" ]; then
  DB_ENGINE_VERSION=${CURRENT_DB_VERSION}
else
  #Grab the default version, which is the most recent version of the DB 
  DB_ENGINE_VERSION=$(aws rds describe-db-engine-versions --engine oracle-ee --engine-version $DB_ENGINE_MAJOR_VERSION --query 'DBEngineVersions[*].[EngineVersion]' --default-only)
fi

}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: build_ps_env.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
echo "       -e (required) = Environment including but not limited to DEV, TST, STG, SUP"
echo "       -s (optional) = Source Environment, populate this if refreshing from a different environment (ie PRD)"
echo "       -d (optional) = Destroy the Environment first if it is already running, then build it"
echo "       -D (optional) = Destroy the Environment if it is already running, and then stop"
echo "       -z (optional) = Set up as though it were production (useful when testing in nonprod)"
echo "       -Z  Beef up non production environment (a bigger env for major testing)"
echo "       -o (optional) = Run Obscure Post Refresh SQL if this is a refresh"
echo "       -P (optional) = Point in Time refresh specify date/time or leave blank for latest refresh date"
echo "                       Example format: 2017-11-26T18:56:40MST"
echo "       -m  Mode - Use "a" to just apply this to the app/web layer and leave the DB alone"
echo "       -n  No Snapshot - If this is present then a final snapshot will not be taken when"
echo "           the RDS instnace is dropped.  Useful for TRN environments, DMO, and also SUP."
echo "       -f  Use this flag to specify that this is a fresh build, this will require that a source env be passed"
echo "           otherwise it will be ignored"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- If this is an error for a RPT, SUP, or PRD then send a message to the VictorOps channel
if [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ] || [ "${UPPER_ENV}" = "PRD" ]; then
  USE_THIS_SLACK_URL=${SLACK_URL_VO}
  SLACK_CHANNEL="ua-errors"
else
  USE_THIS_SLACK_URL=${SLACK_URL}
  SLACK_CHANNEL="met-errors"
fi

# -- Send a message to the #met-errors channel
if ! [ -z "${USE_THIS_SLACK_URL}" ]; then
  SLACK_MSG="Error running build_ps_env_ecs.sh for the environment ${UPPER_PILLAR}${UPPER_ENV} -- (${MSG})"
  JSON="{ \"channel\": \"${SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${USE_THIS_SLACK_URL}" 2>&1)
fi

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: destroy_environment
#
# This function will destroy the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function destroy_environment
{

#Toggle the DB Ready flag to N(o)
toggle_db_ready N

# -- run pre-refresh process on targert DB before deleting CloudFormation Delete
# -- this will retain important info for environment refreshes
PLSQL_OUTPUT=$(sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
begin
  aws_refresh.pre_refresh('${UPPER_PILLAR}','${UPPER_ENV}');
end;
/
EOF-SQL
)

# -- if the there was a syntax error then error_abort
if [ "$?" -ne "0" ]; then
  error_abort "Error running aws_refesh.post_refresh-${PLSQL_OUTPUT}"
fi

# -- if the there was a ORA- error then error_abort
echo $PLSQL_OUTPUT|egrep "ORA-" 2>&1
if [ "$?" = "0" ]; then
  error_abort "Error (ORA-) running aws_refesh.pre_refresh-${PLSQL_OUTPUT}"
fi

# -- MEM-19186 put the DB into blackout in OEM before removing the environment
OUTPUT=$(../../common/sh/blackout.sh -t ${TARGET_DB_INSTANCE} 2>&1) 

if ! [ "$?" = "0" ]; then
  echo "WARNING: Issues placing DB into blackout mode ($OUTPUT)."
fi

# -- Get the PointInTimeRefresh parameter from the stack, it it's set to Y then the DB was not created
# -- as part of the stack and will need to be deleted separately
CF_PIT_REFRESH=$(aws cloudformation describe-stacks \
                 --region ${REGION} \
                 --stack-name ${CF_STACK_NAME} \
                 --output text \
                 --query 'Stacks[*].Parameters[?ParameterKey==`PointInTimeRefresh`].ParameterValue' 2>&1)
#--Check to see if the command failed
if ! [ "$?" = "0" ]; then
  error_abort "Error find PointInTimeRefresh parameter in existing CF Stack ($CF_PIT_REFRESH)"
fi
#--If the parameter was not set then default to N
if [ -z $CF_PIT_REFRESH ]; then
  CF_PIT_REFRESH="N"
fi

# -- issue the delete stack command
OUTPUT=$(aws cloudformation delete-stack \
         --region ${REGION} \
         --stack-name ${CF_STACK_NAME}  \
         --role-arn ${ROLE_ARN} 2>&1)

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  error_abort "Problems deleting CloudFormation Stack ${CF_STACK_NAME} ($OUTPUT)"
fi

# -- If MODE = B then we need to delete the DB as well
if [ "$MODE" = "B" ]; then
  # -- While stack is deleting check to see if we need to remove the RDS instance because it was a 
  # -- Point in Time refresh
  if [ "$CF_PIT_REFRESH" = "Y" ]; then
    OUTPUT=$(../../common/sh/deldb.sh -t ${TARGET_DB_INSTANCE} -s 2>&1)
    if ! [ "$?" = "0" ]; then
      error_abort "Encountered issues deleting RDS Instance built based on Point In Time refresh ($OUTPUT)"
    fi
  else
    # -- issue the delete stack command
    OUTPUT=$(aws cloudformation delete-stack \
             --region ${REGION} \
             --stack-name ${CF_STACK_NAME_DB}  \
             --role-arn ${ROLE_ARN} 2>&1)
    # -- wait for delete to be complete, it will check every 30 seconds for an hour
    # -- if delete does not complete by then it will throw an error
    aws cloudformation wait stack-delete-complete \
       --region ${REGION} \
       --stack-name "${CF_STACK_NAME_DB}" 
    if [ "$?" -ne "0" ]; then
      error_abort "Problems waiting for the deleting of Database CloudFormation Stack ${CF_STACK_NAME_DB}."
    fi
  fi
fi

# -- wait for delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
aws cloudformation wait stack-delete-complete \
   --region ${REGION} \
   --stack-name "${CF_STACK_NAME}" 

# -- if there was an error then try to delete again
# -- put this in place to attempt to fix the issue with the ECS Cluster
# -- not deleting becuase the EC2 instance has not been terminated yet
if [ "$?" -ne "0" ]; then
  # -- issue the delete stack command again
  aws cloudformation delete-stack \
     --region ${REGION} \
     --stack-name "${CF_STACK_NAME}"  \
     --role-arn "${ROLE_ARN}"

  # -- Wait for stack delete cammand to finish
  aws cloudformation wait stack-delete-complete\
     --region ${REGION} \
     --stack-name "${CF_STACK_NAME}" 

  #Now we can abort if the second delete did not work
  if [ "$?" -ne "0" ]; then
    error_abort "Problems waiting for the deleting of CloudFormation Stack ${CF_STACK_NAME}."
  fi
fi
}

#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation 
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
# -- describe the CloudFormation Template stored in CF_STACK_NAME
# -- if the snapshot file was not populated with data then we have an issue and have to abort
# -- look for the string arn:aws:cloudformation in the output, if it's there the environment exists
aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} --output text 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then 
  ENV_EXISTS="Y"
else  
  ENV_EXISTS="N"
fi

# -- if the env exists and -D (destroy env) was passed then destroy the environment
if [ "${ENV_EXISTS}" == "Y" ]; then
  if [ "${DESTROY_IF_EXISTS}" == "Y" ]; then

    # -- Send a message to the to the refresh channel
    if ! [ -z "${SLACK_URL}" ]; then
      SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} is going offline"
      JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
      OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
    fi
    destroy_environment
    # -- Send a message to the to the refresh channel
    if ! [ -z "${SLACK_URL}" ]; then
      SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} has been successfully destroyed"
      JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
      OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
    fi

  elif [ "${DESTROY_IF_EXISTS}" == "N" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} already exists and a \"-D\" or \"-d\" was not passed."
  fi
fi

# --if -D was passed then we only want to destroy the environment and be done
if [ "${DESTROY_ONLY}" == "Y" ]; then
  if [ "${ENV_EXISTS}" == "Y" ]; then
    echo "CloudFormation Stack ${CF_STACK_NAME} has been successfully destroyed.  All done."
  else
    echo "CloudFormation Stack ${CF_STACK_NAME} did not exist, so there is nothing to destroy."
  fi
  exit
fi

}

#*******************************************************************************
#
# function: build_environment
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

# -- Send a message to the to the refresh channel
if ! [ -z "${SLACK_URL}" ]; then
  SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} is starting the build process"
  JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
fi

# -- if MODE = B then we need to build the DB, if not then this is only for the App Layer so don't touch the DB
if [ "$MODE" = "B" ]; then 

  #Toggle the DB Ready flag to N(o)
  toggle_db_ready N

  # -- if this is a point in time refresh create the RDS instance here
  # -- will pass a parameter into the CF template that will prevent the DB from begin
  # -- created in the template
  if [ "$PIT_REFRESH" = "Y" ]; then
    TARGET_DB_NAME="${UPPER_PILLAR}${UPPER_ENV}"
    if ! [ -z $PIT_DATE ]; then
      OUTPUT=$(../../common/sh/resdbpit.sh -s $SOURCE_DB_INSTANCE -t $TARGET_DB_INSTANCE -d $TARGET_DB_NAME -P $PIT_DATE 2>&1)
    else
      OUTPUT=$(../../common/sh/resdbpit.sh -s $SOURCE_DB_INSTANCE -t $TARGET_DB_INSTANCE -d $TARGET_DB_NAME 2>&1)
    fi
    #If there was an error stop and report
    if ! [ "$?" = "0" ]; then
      error_abort "Issues creating Point in Time Refresh database DB Inst=$TARGET_DB_INSTANCE ($OUTPUT)"
    fi
  else
    #Kick off DB build
    build_db

    #Now kick off App Layer build while DB is creating
    build_app_layer

    #Make sure the CF Stack wait command finished without any issues
    #sometimes the DB takes longer than an hour to build so we need to wait longer
    cnt=0
    while [ $cnt -lt 6 ]
    do
      OUTPUT=`aws cloudformation wait stack-create-complete --stack-name "${CF_STACK_NAME_DB}" 2>&1`

      if [ $? = 0 ]; then
        break
      fi

      #Increment counter
      cnt=$(( cnt+1 ))

      if [ $cnt -gt 5 ]; then
        error_abort "Error encountered waiting for Database CF Stack to create ($OUTPUT)"
      fi
    done

  fi #End of if this is a PIT Refresh

  # -- At this point the DB is done being built.  Will modify the instance and apply the OEM option group
  # -- in Oracle 19 the OEM option is taking 15-20 minutes to apply in addition to the build.  This is something
  # -- that can be done in parallel with other activities because the database is up.  This will cut down 
  # -- on the time it takes the environment to come online.
  OUTPUT=$(aws rds modify-db-instance --db-instance-identifier ${TARGET_DB_INSTANCE} --option-group-name ${DB_OPTION_GROUP_OEM} --apply-immediately 2>&1)

  # -- If the CLI modify database command fails then send a warning message to the warnings channel
  if [ "$?" != "0" ]; then
    MSG="Encountered a problem trying apply the Option Group ${DB_OPTION_GROUP_OEM} to the instance ${TARGET_DB_INSTANCE} - (${OUTPUT})"

    USE_THIS_SLACK_URL=${SLACK_URL}
    SLACK_CHANNEL="met-errors"

    # -- Send a message to the #met-errors channel
    if ! [ -z "${USE_THIS_SLACK_URL}" ]; then
      SLACK_MSG="Error running build_ps_env_ecs.sh for the environment ${UPPER_PILLAR}${UPPER_ENV} -- (${MSG})"
      JSON="{ \"channel\": \"${SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
      OUTPUT=$(curl -s -d "payload=${JSON}" "${USE_THIS_SLACK_URL}" 2>&1)
    fi
  fi

  # -- If this is a refresh (source is different from target) then we need to reset the 
  # -- bitbucket branch to reflect the state production-copy if the branch is the dev repo
  # -- or production if the branch is in the prd repo
  if [ "${RUN_POST_REFRESH}" = "Y" ] && [ "${UPPER_ENV}" != "PRD" ]; then

    TARGET_BRANCH=${APP_BIT_BUCKET_BRANCH}
    SOURCE_BRANCH=${LOWER_SOURCE_ENV}

    if [ ${TARGET_BRANCH} = "prd" ] || [ ${TARGET_BRANCH} = "stg" ]; then
        REPO="${LOWER_PILLAR}-prd"
        SOURCE_BRANCH="prd"
    else
        REPO="${LOWER_PILLAR}-dev"
        if [ "${SOURCE_BRANCH}" = "prd" ]; then
          SOURCE_BRANCH="prd-copy"
        fi
    fi

    GIT_HOME=~/git/${REPO}

    # -- First make sure source is up to date
    OUTPUT=$(git -C ${GIT_HOME} checkout ${SOURCE_BRANCH} 2>&1)
    OUTPUT=$(git -C ${GIT_HOME} fetch origin 2>&1)
    OUTPUT=$(git -C ${GIT_HOME} reset --hard origin/${SOURCE_BRANCH} 2>&1)

    # -- Next, check out target butbucket branch
    OUTPUT=$(git -C ${GIT_HOME} checkout ${TARGET_BRANCH} 2>&1)

    # -- Reset to SOURCE_BRANCH
    OUTPUT=$(git -C ${GIT_HOME} reset --hard  ${SOURCE_BRANCH} 2>&1)

    # -- Push changes to bitbucket forced
    OUTPUT=$(git -C ${GIT_HOME} push --force origin ${TARGET_BRANCH} 2>&1)


    # -- Now sync the App Home
    SOURCE_BRANCH=${LOWER_SOURCE_ENV}
    REPO=peoplesoft-app-home-${LOWER_PILLAR}
    GIT_HOME=~/git/${REPO}

    # -- First make sure master is up to date
    OUTPUT=$(git -C ${GIT_HOME} checkout ${SOURCE_BRANCH} 2>&1)
    OUTPUT=$(git -C ${GIT_HOME} fetch origin 2>&1)
    OUTPUT=$(git -C ${GIT_HOME} reset --hard origin/${SOURCE_BRANCH} 2>&1)

    # -- Next, check out environment butbucket branch
    OUTPUT=$(git -C ${GIT_HOME} checkout ${TARGET_BRANCH} 2>&1)

    # -- Reset to SOURCE_BRANCH
    OUTPUT=$(git -C ${GIT_HOME} reset --hard ${SOURCE_BRANCH} 2>&1)

    # -- Push changes to bitbucket forced
    OUTPUT=$(git -C ${GIT_HOME} push --force origin ${TARGET_BRANCH} 2>&1)


    # -- SAAWS-68 - create SASUP environment
    # -- Now run the Post Refresh process because the DB is up
    # -- moved this here and out of the app-batch docker image
    PLSQL_OUTPUT=$(sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
begin
 aws_refresh.post_refresh('${UPPER_PILLAR}','${UPPER_ENV}','${OBSCURE_ON_REFRESH}',in_seed_env=>'${SEED_ENV}',in_source_env=>'${LOWER_SOURCE_ENV}');
end;
/
EOF-SQL
)

    # -- if the there was a syntax error then error_abort
    if [ "$?" -ne "0" ]; then
      error_abort "Error running aws_refesh.post_refresh-${PLSQL_OUTPUT}"
    fi

    # -- if the there was a ORA- error then error_abort
    echo $PLSQL_OUTPUT|egrep "ORA-" 2>&1
    if [ "$?" = "0" ]; then
      error_abort "Error (ORA-) running aws_refesh.post_refresh-${PLSQL_OUTPUT}"
    fi
  fi

  #Toggle the DB Ready flag to Y(es)
  #Because the DB is up and all Post Refresh processes have been run if needed
  toggle_db_ready Y

  # -- MEM-19186 take DB out of blackout in OEM after environment is built
  OUTPUT=$(../../common/sh/blackout.sh -t ${TARGET_DB_INSTANCE} -d 2>&1) 

  if ! [ "$?" = "0" ]; then
    echo "WARNING: Issues taking DB out of blackout mode ($OUTPUT)."
  fi

else #This is just an App Layer build

  #Toggle the DB Ready flag to Y(es)
  #Make sure it's set to Y as this is only an app layer build
  toggle_db_ready Y

  #Now kick off App Layer build while DB is creating
  build_app_layer

fi #End of if MODE = B

OUTPUT=`aws cloudformation wait stack-create-complete --stack-name "${CF_STACK_NAME}" 2>&1`

#Make sure the CF Stack wait command finished without any issues
if ! [ $? = "0" ]; then
  error_abort "Error encountered waiting for CF Stack to create ($OUTPUT)"
fi

#Now that the stack is created attach EC2 instance(s) to the Excel to CI Target Group
register_excel_to_ci

# -- Sleep for 5 minutes to give the web/app/servers to come online
sleep 5m

# -- Send a message to the to the refresh channel
if ! [ -z "${SLACK_URL}" ]; then
  SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} build process is complete"
  JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
fi

sleep 15m

bash ~/git/envy/envy.sh -O ua -p ${LOWER_PILLAR} -e ${LOWER_ENV} -a preload_cache -v 2>&1 | tee ~/logs/envy/${LOWER_PILLAR}-${LOWER_ENV}-preload_cache.log

}

#*******************************************************************************
#
# function: toggle_db_ready
#
# This function with toggle the DB_READY flag with the value passed in the first parameter
#
# INPUTS:  $1 - Y/N (Y = DB is ready, N = DB is not ready)
#
# RETURNS: none
#
#*******************************************************************************
function toggle_db_ready
{

DB_READY=$1

#Before we build the DB set the DB_READY flag to N
#This will be set to Y after the DB is up and any post refresh processes have been run

# -- run pre-refresh process on targert DB before deleting CloudFormation Delete
# -- this will retain important info for environment refreshes
PLSQL_OUTPUT=$(sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
var v_db_ready varchar2;
begin
  :v_db_ready := aws_refresh.db_ready('${UPPER_PILLAR}','${UPPER_ENV}','${DB_READY}');
end;
/
EOF-SQL
)

# -- if the there was a syntax error then error_abort
if [ "$?" -ne "0" ]; then
  error_abort "Error running aws_refesh.db_ready-${PLSQL_OUTPUT}"
fi

# -- if the there was a ORA- error then error_abort
echo $PLSQL_OUTPUT|egrep "ORA-" 2>&1
if [ "$?" = "0" ]; then
  error_abort "Error (ORA-) running aws_refesh.db_ready-${PLSQL_OUTPUT}"
fi

}

#*******************************************************************************
#
# function: build_app_layer
#
# This function with build the app layter and return to the calling program after the 
# CloudFormation stack has been initiated.  It will not wait for the stack to be 
# built so other processes can be kicked off.  Waiting for the stack to be complete
# will be done outside of this function.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_app_layer
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "PillarLowerCase","ParameterValue": "${LOWER_PILLAR}"}, 
 { "ParameterKey": "EnvironmentLowerCase","ParameterValue": "${LOWER_ENV}" }, 
 { "ParameterKey": "PillarUpperCase","ParameterValue": "${UPPER_PILLAR}" }, 
 { "ParameterKey": "EnvironmentUpperCase","ParameterValue": "${UPPER_ENV}" }, 
 { "ParameterKey": "RunPostRefresh","ParameterValue": "${RUN_POST_REFRESH}" },
 { "ParameterKey": "HostedZoneName","ParameterValue": "${HOSTED_ZONE_NAME}" }, 
 { "ParameterKey": "AuthTokenDomain","ParameterValue": "${AUTH_TOKEN_DOMAIN}" }, 
 { "ParameterKey": "FQDNPrefix","ParameterValue": "${FQDN_PREFIX}" }, 
 { "ParameterKey": "WebELBListenerPolicyNames","ParameterValue": "${WEB_ELB_LISTENER_POLICY_NAME}" }, 
 { "ParameterKey": "WebELBListenerSSLCertID","ParameterValue": "${SSL_CERT_ARN}" }, 
 { "ParameterKey": "WebDockerImage","ParameterValue": "${WEB_DOCKER_IMAGE}" },
 { "ParameterKey": "WebProfileName","ParameterValue": "${WEB_PROFILE}" }, 
 { "ParameterKey": "PsReportsDirecory","ParameterValue": "${PSREPORTS_DIR}" },
 { "ParameterKey": "AppDockerImage","ParameterValue": "${APP_DOCKER_IMAGE}" },
 { "ParameterKey": "AppOrBatch","ParameterValue": "${APP_OR_BATCH}" },
 { "ParameterKey": "AppTemplate","ParameterValue": "${APP_TEMPLATE}" },
 { "ParameterKey": "AppOprId","ParameterValue": "${APP_OPR_ID}" },
 { "ParameterKey": "PSAppOpridPw", "ParameterValue": "${APP_OPR_ID_PW}" },
 { "ParameterKey": "AppBitBucketBranch","ParameterValue": "${APP_BIT_BUCKET_BRANCH}" },
 { "ParameterKey": "AppSesServer","ParameterValue": "${SES_SERVER}" }, 
 { "ParameterKey": "AppSesDefns","ParameterValue": "${SES_DEFNS}" }, 
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
 { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
 { "ParameterKey": "PublicFacingELB", "ParameterValue": "${PUBLIC_FACING_ELB}" },
 { "ParameterKey": "Tls12", "ParameterValue": "${TLS12}" },
 { "ParameterKey": "ObscureOnRefresh", "ParameterValue": "${OBSCURE_ON_REFRESH}" }, 
 { "ParameterKey": "PsWebRequireDuo", "ParameterValue": "${PSWEB_REQUIRE_DUO}" },
 { "ParameterKey": "PointInTimeRefresh", "ParameterValue": "${PIT_REFRESH}" },
 { "ParameterKey": "AppSenderEmail", "ParameterValue": "${APP_SENDER_EMAIL}" },
 { "ParameterKey": "CreatePublicSite", "ParameterValue": "${CREATE_PUBLIC_SITE}" },
 { "ParameterKey": "PreferredPrivateSubnet", "ParameterValue": "${PREFERRED_PRIVATE_SUBNET}" },
 { "ParameterKey": "PubFQDNPrefix", "ParameterValue": "${PUB_FQDN_PREFIX}" },
 { "ParameterKey": "PubAuthTokenDomain", "ParameterValue": "${PUB_AUTH_TOKEN_DOMAIN}" },
 { "ParameterKey": "PubHostedZoneName", "ParameterValue": "${PUB_HOSTED_ZONE_NAME}" },
 { "ParameterKey": "PubWebELBListenerSSLCertID", "ParameterValue": "${PUB_SSL_CERT_ARN}" },
 { "ParameterKey": "InasMakFiles", "ParameterValue": "${INAS_MAK_FILES}" },
 { "ParameterKey": "WAPInstClass", "ParameterValue": "${WAP_INST_CLASS}" },
 { "ParameterKey": "WAPInstCount", "ParameterValue": "${WAP_INST_COUNT}" },
 { "ParameterKey": "IBInstClass", "ParameterValue": "${IB_INST_CLASS}" },
 { "ParameterKey": "BATCHInstClass", "ParameterValue": "${BATCH_INST_CLASS}" }
]
ENDJSON

# Removed from parameter list as this will be retrieved via a SSM call in the CF template 
# { "ParameterKey": "EcsImageId", "ParameterValue": "${ECS_IMAGE_ID}" },

OUTPUT=$(aws cloudformation create-stack \
   --region ${REGION} \
   --stack-name "${CF_STACK_NAME}"  \
   --capabilities "CAPABILITY_IAM" \
   --template-url "${TEMPLATE_URL}" \
   --parameters "${JSON_STRING}" \
   --role-arn "${ROLE_ARN}" \
   --disable-rollback 2>&1)

#Make sure the CF Stack create command was kicked off without any issues
if ! [ "$?" = "0" ]; then
  error_abort "Error encountered creating CF Stack ($OUTPUT)"
fi

}

#*******************************************************************************
#
# function: build_db
#
# This function with build the DB and return to the calling program after the 
# CloudFormation stack has been initiated.  It will not wait for the DB to be 
# built so other processes can be kicked off.  Waiting for the DB to be complete
# will be done outside of this function.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_db
{

  # -- PSAWS-69 If this is not a PIT refresh then we need to build the DB via a CF template
  # -- the DB has been removed from the main CF template and moved into it's own.
  read -d '' JSON_STRING <<ENDJSON
  [
   { "ParameterKey": "PillarLowerCase","ParameterValue": "${LOWER_PILLAR}"}, 
   { "ParameterKey": "EnvironmentLowerCase","ParameterValue": "${LOWER_ENV}" }, 
   { "ParameterKey": "PillarUpperCase","ParameterValue": "${UPPER_PILLAR}" }, 
   { "ParameterKey": "EnvironmentUpperCase","ParameterValue": "${UPPER_ENV}" }, 
   { "ParameterKey": "DBSnapshotID","ParameterValue": "${DB_LATEST_SNAPSHOT}" }, 
   { "ParameterKey": "DBMultiAz","ParameterValue": "${DB_MULTI_ZONE}" }, 
   { "ParameterKey": "DBInstanceClass","ParameterValue": "${DB_INST_CLASS}" }, 
   { "ParameterKey": "DBStorageType","ParameterValue": "${DB_STORAGE_TYPE}" },
   { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
   { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
   { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
   { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
   { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
   { "ParameterKey": "PreferredAz", "ParameterValue": "${PREFERRED_AZ}" },
   { "ParameterKey": "TakeSnapshot", "ParameterValue": "${TAKE_SNAPSHOT}"},
   { "ParameterKey": "DBEngineVersion", "ParameterValue": "${DB_ENGINE_VERSION}"},
   { "ParameterKey": "DBParameterGroupName", "ParameterValue": "${DB_PARAMETER_GROUP}"},
   { "ParameterKey": "OptionGroupName", "ParameterValue": "${DB_OPTION_GROUP}"}
  ]
ENDJSON

  OUTPUT=$(aws cloudformation create-stack \
     --region ${REGION} \
     --stack-name "${CF_STACK_NAME_DB}"  \
     --capabilities "CAPABILITY_IAM" \
     --template-url "${TEMPLATE_URL_DB}" \
     --parameters "${JSON_STRING}" \
     --role-arn "${ROLE_ARN}" \
     --disable-rollback 2>&1)

  #Make sure the CF Stack create command was kicked off without any issues
  if ! [ "$?" = "0" ]; then
    error_abort "Error encountered creating CF Stack ($OUTPUT)"
  fi

}

#*******************************************************************************
#
# function: register_excel_to_ci
#
# This function will check to see if an elbv2 Target Group exists with the
# lower PillarEnv name along with a ExCI in the name.  If one does exist then
# the EC2 instances that have the upper PillarEnv-ECS at the end of the Tag=Name
# (i.e. HRPRD-ECS) will be registered to that Target Group.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function register_excel_to_ci
{

#This will find the Excel to CI Target Group and attach 
#instances that include the PillarEnv-ECS (i.e. HRPRD-ECS) at the end
#First find the target group that includes the Lower PillarEnv and ExCI
TARGETGROUP_ARN=`aws elbv2 describe-target-groups --query 'TargetGroups[*].[TargetGroupArn,TargetGroupName]' --output text | grep ${LOWER_PILLAR}${LOWER_ENV} | grep "ExCI" | awk '{print $1}' 2>&1`

#Make sure the describe statement ran properly
if ! [ $? = "0" ]; then
  error_abort "Error encountered describing Target Group ($OUTPUT)"
fi

#If a Target Group ARN was found then find instances to register to it
if ! [ -z $TARGETGROUP_ARN ]; then

  #Find instances that are running (instance-state-code=16) and have Upper PillarEnv-ECS in the Tag Name
  INSTANCES=`aws ec2 describe-instances --filter "Name=tag:Name,Values=*${UPPER_PILLAR}${UPPER_ENV}-ECS" "Name=instance-state-code,Values=16" --query 'Reservations[*].Instances[*].[InstanceId]' --output=text`

  for inst_id in `echo $INSTANCES`; do
    OUTPUT=`aws elbv2 register-targets --target-group-arn $TARGETGROUP_ARN --targets Id=$inst_id 2>&1`
    #Check for errors
    if ! [ $? = "0" ]; then
      error_abort "Error encountered registering EC2 instance to Target Group ($OUTPUT)"
    fi
  done

fi

}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- Make sure to be located in the directory the script is run
cd $(dirname $0)

# -- set environment
START_TS=`${DATE}`
SET_UP_LIKE_PRD="N"
OBSCURE_ON_REFRESH="N"
PIT_REFRESH="N"
PIT_FORMAT="%Y-%m-%dT%H:%M:%S%Z"
# -- Get current account, added for DR
# -- 507937877461 is the DR acct
AWS_ACCOUNT=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .accountId)
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
TAKE_SNAPSHOT="Y"
SEED_ENV="N"

# -- Find the SLACK_URL for slack messages
SLACK_URL=$(aws ssm get-parameter --name "SLACK_ALERT_URL" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL
fi

# -- Find the SLACK_URL for slack messages that will go to VictorOps
SLACK_URL_VO=$(aws ssm get-parameter --name "SLACK_ALERT_URL_VO" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL_VO
fi

# -- parse command-line options
while getopts :p:e:s:DdzZoP:m:nf arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    e)
       UPPER_ENV=${OPTARG^^}
       ;;
    s)
       UPPER_SOURCE_ENV=${OPTARG^^}
       ;;
    d) 
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="N"
       ;;
    D)
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="Y"
       ;;
    #If -z is passed that means this will be set up like a PRD environment
    z) 
       SET_UP_LIKE_PRD="Y"
       ;;
    #MEM-19089
    #If -Z is passed that means this will be set up like a beefed up Test environment
    Z) 
       SET_UP_LIKE_PRD="T"
       ;;
    o)
       OBSCURE_ON_REFRESH="Y"
       ;;
    m)
       MODE=${OPTARG^^}
       ;;
    n)
       #If -n is passed then a final snapshot will not be taken when the RDS Instance is deleted
       TAKE_SNAPSHOT="N"
       ;;
    #If -P is passed then we need to do a Point in Time refresh
    P)
       PIT_REFRESH="Y"
       #If the value passed is now blank out the PIT_DATE variable, that will 
       #make the point in time look at the Latest Restoreable Time of the database
       if [ "${OPTARG,,}" = "now" ]; then
         unset PIT_DATE
       else
         PIT_DATE=${OPTARG}
       fi
       ;;
    :)
       error_abort "-P requires a value, either a date in the format \"`date +"%Y-%m-%dT%H:%M:%S%Z"`\"  or the word \"now\" for current date time"
       ;;
    #If -f is passed then we want to run the Seed Env process on the post refresh
    f)
       SEED_ENV="Y"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
  esac
done

# validate input parameters
validate_arguments

# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

# -- if we reach this point we can build the environment
build_environment
