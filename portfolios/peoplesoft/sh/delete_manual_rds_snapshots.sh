#Cut date will be used to make sure we keep at least snapshots younger than 7 days old
cut_date=$(date "+%Y%m%d" -d "7 days ago")

#Initilize the prev_db_instance variable
prev_db_instance="NEW"

#List the db instance ($4), snapshot name ($6), and snapshot create date ($11) for all database instances
#Loop through and only keep the snapshots that are less than 8 days old and have a count < 8
aws rds describe-db-snapshots --snapshot-type manual 2>&1 | sort -k4,4 -k11,11 -r | awk '{print $4 " " $6 " " $11}' | while read line
do

  #Grab the first field from the rds snapshot output, which is the db instance name
  db_instance_name=`echo $line|awk '{print $1}'`

  #Grab the second field from the rds snapshot output, which is the snapshot name
  snapshot_name=`echo $line|awk '{print $2}'`

  #Grab the thrid field from the rds snapshot output which is the create date of the snapshot
  #grab the first 10 characters which will be YYYY-MM-DD and format it to YYYMMDD
  snapshot_date=`echo $line|awk '{print $3}'`
  snapshot_date=${snapshot_date:0:10}
  snapshot_date=$(date -d $snapshot_date +%Y%m%d)

  #If this is the first row or the db instance name has changed then reset the counter
  if [ ! "$db_instance_name" == "$prev_db_instance" ]; then
    snapshot_cnt=0
  fi

  #Increment the snapshot counter
  let "snapshot_cnt++"

  #If the snapshot count is greater than 7 
  #and the snapshot is older than the cut date, remove it
  if [ $snapshot_cnt -gt 7 ] && [ "$snapshot_date" -lt "$cut_date" ]; then 
    echo "will delete $snapshot_name"
    delete_output=`aws rds delete-db-snapshot --db-snapshot-identifier $snapshot_name 2>&1`
    if [ $? -ne 0 ]; then
      echo "Error deleting old manual rds snapshot--$delete_output"
    fi
  fi

  prev_db_instance=$db_instance_name

done 
