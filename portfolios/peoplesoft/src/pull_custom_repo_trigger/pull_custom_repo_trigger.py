"""
pull_custom_repo_trigger

This is a Lambda function which receives a trigger from Bitbucket when a repository is updated.
In turn it makes ssm calls to PeopleSoft hosts to refresh code on the running hosts. This 
abstracts the hosts from needing to be reached directly from a Bitbucket event.

This python code file must be zipped up by iteself (not the enclosing folder) and put in an S3
bucket which it will be pulled from in the 'pull_custom_repo_trigger.yaml' template.

"""

import json
import logging
import boto3
import os

print('Loading function')

aws_session = boto3.Session()
ssm = aws_session.client('ssm', region_name='us-west-2')

target_tag_name = os.environ['TagName']
target_tag_value = os.environ['TagValue']

def lambda_handler(event, context):
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.INFO)
    logging.info("-" * 100)

    if "body" not in event:
        return handle_response(f"No body element found in event", logging.ERROR, event) 
    
    # The body element is still JSON encoded, so decode that first.
    body = json.loads( event["body"] )

    if "repository" not in body:
        return handle_response(f"No Repository data found in event", logging.ERROR, body)      

    if "full_name" not in body['repository']:
        return handle_response(f"Missing full_name from repository data", logging.ERROR, bodyv['repository'])      

    repository_name = body['repository']['full_name']

    allowed_repositories = ['uapeoplesoft/hr-prd', 
                            'uapeoplesoft/hr-dev',
                            'uapeoplesoft/el-prd',
                            'uapeoplesoft/el-dev',
                            'uapeoplesoft/sa-prd',
                            'uapeoplesoft/sa-dev',
                            'uapeoplesoft/peoplesoft-app-home-hr',
                            'uapeoplesoft/peoplesoft-app-home-el',
                            'uapeoplesoft/peoplesoft-app-home-sa'
                            ]

    if repository_name not in allowed_repositories:
        return handle_response(f"Unsupported Repository: {repository_name}", logging.ERROR)      

    if "push" not in body:
        return handle_response(f"Not a Push Event", logging.ERROR)      

    if "changes" not in body['push']:
        return handle_response(f"No Push Changes Found", logging.ERROR, body['push'])      

    new_change = None
    for change_event in body['push']['changes']:
        if "new" in change_event:
            new_change = change_event['new']
        else:
            continue

    if new_change == None:
        return handle_response(f"No New Change found in Push event", logging.ERROR, new_change)      

    if new_change['type'] != "branch":

        return handle_response(f"Only branch changes supported", logging.ERROR)      

    branch_name = new_change['name']

    response = ssm.send_command(
              Targets = [
                  { 'Key': f"tag:{target_tag_name}", 'Values': [ target_tag_value ] },
              ],
              DocumentName = 'AWS-RunShellScript',
              Comment = f"PeopleSoft Refresh: Triggering pull_custom_repos.sh from branch {branch_name}",
              Parameters = {
                  "commands": [
                      f"/bin/bash /home/ec2-user/git/service-catalog/portfolios/peoplesoft/sh/pull_custom_repos.sh -b {branch_name}"
                  ],
                  "executionTimeout": ["300"]
              },
              TimeoutSeconds = 300
    )

    logging.info(response)

    return_data = {}
    return_data['repository'] = repository_name
    return_data['branch'] = branch_name

    return handle_response(f"Triggered refresh for branch {branch_name} triggered", logging.INFO, return_data) 

err_level_strings = {
  logging.CRITICAL: "critical",
  logging.ERROR:    "error",
  logging.WARNING:  "warning",
  logging.INFO:     "info",
  logging.DEBUG:    "debug",
  logging.NOTSET:   "notset"
}

def handle_response(msg, log_level, data = None):
    if data is not None:
        msg = msg + "\n" + json.dumps(data, indent=2)
    logging.log(log_level, msg)

    return_obj = {}
    return_obj['message'] = msg
    return_obj['status'] = err_level_strings[log_level]
    return_obj['data'] = data

    return {
      "statusCode": 200,
      "headers": {"Content-Type": "application/json"},
      "body": json.dumps(return_obj, indent=2)
    }

