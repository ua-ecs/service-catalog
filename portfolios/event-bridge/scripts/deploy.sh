#!/bin/bash

SERVICE_NAME=$1

if [[ $SERVICE_NAME == "" ]]; then
    echo "Service Name not set"
    echo "usage:  npm run deploy <my-service-name>"
    echo ""
    exit 0
fi

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source $SCRIPTPATH/env.sh

aws cloudformation create-stack \
--stack-name $SERVICE_NAME \
--template-body file:///$SCRIPTPATH/../templates/event-bridge.yaml \
--parameters ParameterKey=ServiceName,ParameterValue=$SERVICE_NAME \
--capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND \
--role-arn $CLOUDFORMATION_DEPLOY_ROLE
