AWS EventBridge Bus and API Gateway
===================================

This tempalte sets up a new AWS EventBridge Bus, and configures an API Gateway to deliver events to it.

This is not intended to be used as-is in production, but more of a starting place for your specific 
use case.  You can deploy this template as-is to see the various pieces in action, and to test things out.

The following resources are created:

| Resource | Notes |
| --- | --- |
| EventBridge Bus | This is the custom bus that the API Gateway will deliver requests to as events |
| CloudWatch Log Group | A log group with a retention period of 3 days to catch all events from the bus |
| EventBridge Rule | A catch-all rule to deliver all events to the log group |
| API Gateway | An HTTP REST endpoint which will accept any application/json POST messages and deliver them to the event bus in the appropriate wrapper |


There are several hard-coded values which you will likey need to customize for your particular use case when you copy this project.

| Resource | Notes |
| --- | --- |
| ApiGateway/requestTemplates | The DetailType and Source values should be updated to better fit your incoming messages |
| CatchallRule | You may not want to log all events to cloudwatch logs. You will like want to add additional rules. |
| Authenticaton | There is no authentication on the API Gateway, anything can POST to it. |
