import boto3
import cfnresponse
import time


def generate_response(event, context, response_code, response_data, error_message=None):
    return cfnresponse.send(event, context, response_code, response_data, error_message)


def copy_default_site(event, context):
    if event["RequestType"] == "Create":

        new_bucket_name = event['ResourceProperties']['TargetBucketName']
        bucket_to_copy = event['ResourceProperties']['SourceBucketName']

        try:
            copy_s3_bucket(bucket_to_copy, new_bucket_name)
            msg = f"Copied template from {bucket_to_copy} to new S3 bucket: {new_bucket_name}"
            return generate_response(event, context, cfnresponse.SUCCESS, None, msg)
        except Exception as ex:
            msg = f"Failed to copy contents of {bucket_to_copy} to {new_bucket_name}: {ex}"
            return generate_response(event, context, cfnresponse.FAILED, None, msg)
    elif event["RequestType"] == "Delete":
        bucket_name = event['ResourceProperties']['TargetBucketName']
        try:
            empty_s3_bucket(bucket_name)
            msg = f'Emptied bucket {bucket_name}'
            return generate_response(event, context, cfnresponse.SUCCESS, None, msg)
        except Exception as ex:
            msg = f'Failed to delete contents of {bucket_name}: {ex}'
            return generate_response(event, context, cfnresponse.FAILED, None, msg)
    else:
        msg = "Nothing to do for " + event["RequestType"]
        return generate_response(event, context, cfnresponse.SUCCESS, None, msg)


def empty_s3_bucket(bucket_name):
    s3_resource = boto3.resource('s3')

    res = []
    bucket = s3_resource.Bucket(bucket_name)
    for obj_version in bucket.object_versions.all():
        res.append({'Key': obj_version.object_key,
                    'VersionId': obj_version.id})

    bucket.delete_objects(Delete={'Objects': res})

    while sum(1 for _ in bucket.object_versions.all()) > 0:
        time.sleep(5)


def copy_s3_bucket(bucket_to_copy, new_bucket_name):
    """Copy all files from one s3 bucket to another"""
    s3 = boto3.client('s3')

    for key in s3.list_objects_v2(Bucket=bucket_to_copy)['Contents']:
        files = key['Key']
        copy_source = {'Bucket': bucket_to_copy, 'Key': files}
        s3.copy(copy_source, new_bucket_name, files)


