import os
import boto3
from datetime import date, datetime
import decimal
import json
import logging
import ptvsd
import requests

region = "us-west-2"
ssm = boto3.client('ssm', region_name=region)
cfn = boto3.client('cloudformation', region_name=region)
iam = boto3.client('iam', region_name=region)
ssm_prefix = "/cloudops/serverless-website/"
stack_response = None

rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)

if os.getenv("AWS_SAM_LOCAL") or os.getenv("IS_LOCAL"):
    # Local Only Config
    rootlogger.setLevel(logging.INFO)
    if os.getenv("REMOTE_DEBUGGING") == 'true':
        logging.info("Activating Debugger")
        ptvsd.enable_attach(address=('0.0.0.0', 5678), redirect_output=True)
        ptvsd.wait_for_attach()
        logging.info("Debugger Connected")
else:
    # Regular AWS Config
    pass


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)

    raise TypeError("Type %s not serializable" % type(obj))


def loginfo(obj):
    logging.info(json.dumps(obj, indent=2, default=json_serial))


def get_secure_parameter(p_name):
    try:
        ssm_parameter_key = f"{ssm_prefix}{p_name}"
        response = ssm.get_parameter(Name=ssm_parameter_key, WithDecryption=True)
        v = response['Parameter']['Value']
        return v
    except Exception as ex:
        msg = str(ex)
        if "ParameterNotFound" in msg:
            loginfo(f"SSM Parameter '{ssm_parameter_key}' not found in current account.")
            return None
        else:
            raise ex


def get_stack_output(stack_name, output_key):
    global stack_response
    if stack_response is None:
        stack_response = cfn.describe_stacks(StackName=stack_name)
    outputs = stack_response["Stacks"][0]["Outputs"]

    for o in outputs:
        if o["OutputKey"] == output_key:
            return o["OutputValue"]

    loginfo(f"Output '{output_key}' not found in stack 'f{stack_name}'")
    return None


def get_stack_tag(stack_name, tag_key):
    global stack_response
    if stack_response is None:
        stack_response = cfn.describe_stacks(StackName=stack_name)
    tags = stack_response["Stacks"][0]["Tags"]

    for t in tags:
        if t["Key"] == tag_key:
            return t["Value"]

    raise KeyError(f"Tag '{tag_key}' not found in stack 'f{stack_name}'")


def get_portal_group_members(group_id):
    groups_api_url = get_secure_parameter("groups_api_url")
    groups_api_key = get_secure_parameter("groups_api_key")

    headers = {
        "X-API-KEY": groups_api_key,
        "content-type": "application/json"
    }

    rest_call_url = f"{groups_api_url}/groups/{group_id}/members"

    stache_response = requests.get(rest_call_url, headers=headers)

    try:
        response_json = stache_response.json()
    except Exception as ex:
        msg = str(ex)
        loginfo(f"get_portal_group_members error: {msg}")
        return None

    if isinstance(response_json, dict) and 'error' in response_json:
        loginfo(response_json)
        return None

    return response_json


def delete_access_keys(username):
    key_list_response = iam.list_access_keys(UserName=username)
    keys = key_list_response['AccessKeyMetadata']
    if len(keys) > 0:
        for k in keys:
            iam.delete_access_key(
                UserName=username,
                AccessKeyId=k['AccessKeyId']
            )


def create_access_key(username):
    response = iam.create_access_key(UserName=username)
    return response


def save_keys_to_stache(key_response, stack_name, group_members):
    stache_url = get_secure_parameter("stache_api_url")
    stache_key = get_secure_parameter("stache_api_key")
    stache_folder = get_secure_parameter("stache_folder")

    k = key_response['AccessKey']
    access_key_id = k['AccessKeyId']
    secret_access_key = k['SecretAccessKey']

    stache_headers = {
        "X-STACHE-KEY": stache_key,
        "content-type": "application/json"
    }

    stache_body = {
        "nickname": f"uits/cloudops/serverlesswebsites/{stack_name}",
        "purpose": f"AWS IAM Access Keys for CloudOps Serverless Website {stack_name}.",
        "secret": f"Access Key ID: {access_key_id}\nSecret Access Key: {secret_access_key}",
        "memo": "",
        "permissions": {
            stache_folder: "folder"
        }
    }

    for netid in group_members:
        stache_body['permissions'][netid] = "owner"

    try:
        stache_response = requests.post(stache_url, headers=stache_headers, data=json.dumps(stache_body))
    except Exception as ex:
        msg = str(ex)
        loginfo(f"save_keys_to_stache error with stache call: {msg}")
        loginfo(stache_body)
        loginfo(group_members)
        raise ex

    try:
        response_json = stache_response.json()
    except Exception as ex:
        msg = str(ex)
        loginfo(f"save_keys_to_stache error with stache response: {msg}")
        loginfo(stache_response)
        raise ex

    return response_json


def handler(event, context):

    status = "ok"
    message = ""

    # Get Stack Name from event
    if 'stack_name' in event:
        stack_name = event['stack_name']
    else:
        raise KeyError(f"Key: 'stack_name' not found in event")

    iam_user = get_stack_output(stack_name, 'BucketUser')

    # Delete any existing IAM keys
    delete_access_keys(iam_user)

    # Make some keys
    key_response = create_access_key(iam_user)

    # Get CloudOps Portal Group Membership
    group_id = get_stack_output(stack_name, 'CloudOpsGroupId')
    if group_id is None:
        status = "warning"
        message = ""

    # group_id = "e6c9caf7-5186-4a51-9c2f-677080de78b9"
    group_members = get_portal_group_members(group_id)
    if group_members is None:
        status = "warning"
        message = f"Group not found: {group_id}"
        group_members = []

    # Store them in stache
    stache_response = save_keys_to_stache(key_response, stack_name, group_members)
    stache_entry_id = stache_response['id']

    # Return some sort of success, probably the stache entry name
    return {
        "status": status,
        "access_key_id": key_response['AccessKey']['AccessKeyId'],
        "stache_entry_url": f"https://stache.arizona.edu/new/entry/{stache_entry_id}",
        "stache_entry_name": stache_response['nickname'],
        "website_dns_name": get_stack_output(stack_name, 'WebsiteDnsRecord'),
        "cloudfront_dns_name": get_stack_output(stack_name, 'CloudFrontDNS'),
        "site_url": get_stack_output(stack_name, 'WebsiteURL'),
        "message": message
    }
