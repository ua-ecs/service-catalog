boto3==1.9.182
botocore==1.12.182
docutils==0.14
jmespath==0.9.4
python-dateutil==2.8.0
s3transfer==0.2.1
six==1.12.0
urllib3==1.25.3
requests==2.22.0

# Python Tools for Visual Studio - Remote Debugging
ptvsd==4.2.10
