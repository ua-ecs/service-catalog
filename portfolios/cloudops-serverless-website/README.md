UA CloudOPS Serverless Website
==============================

This is a product in the CloudOPS catalog. It provides a service to host static web files in to host a serverless website (ie no back-end code).

See https://confluence.arizona.edu/display/OaaS/Serverless+Website+Technical+Overview for more details.


ServiceNow Details
==================

Catalog Item Description:
The Serverless Website service provisions an AWS S3 bucket to hold HTML, CSS, JavaScript, and other static resources to host a website.

Serverless websites are an extremely cost effective solution if you maintain web content by creating HTML pages by hand, or with tools such as Dreamweaver, or if you use a type of static site generator like Jekyl or Hugo.

https://cloudops.arizona.edu/serverless-website



### Website specific Parameters

* SiteName: (read-write) Free-text. The DNS name of the website (no spaces). This will be the bucket name also.
    * Label Text: Site Name
    * Description: Website Domain Name i.e. 'myproject.cloudsites.arizona.edu'. If your site name is something other than
                    *.cloudsites.arizona.edu you will need to coordinate with campus DNS.
* HTTPSWebsite: (drop down) Yes or No. Defaults to Yes
    * Label: Enable SSL
    * Description: Should SSL/TLS be enabled for this site? SSL should be used for all 'production' websites. You will need
                    to choose an SSL certificate to use also. This certificate must be valid for the Site Name you specified.
                    There is a wildcard certificate for *.cloudsites.arizona.edu for everyone to use. If you need a custom
                    certificate, it must be added to CloudOps first. See the [import certificate] service catalog item.
* CertificateARN: (drop down) This should be a list of the currently available certificates for the group selected on the
                    general tab. The certificate names should be certificate names, ie mysite.arizona.edu or *.cloudsites.arizona.edu
                    The ARN should be available to be passed to the CLoudFormation template.
                    Bonus: Could this field be hidden if the user selects "No" for the HTTPSWebsite field.
    * Label: SSL Certificate
    * Description: Choose the appropriate certificate from the available list. This certificate must be valid for the Site Name you specified.
                    There is a wildcard certificate for *.cloudsites.arizona.edu for everyone to use. If you need a custom
                    certificate, it must be added to CloudOps first. See the [import certificate] service catalog item.

### Tagging and billing Parameters

* TagService: (hidden) Name of the service. This is probably the "StackName".
* TagContactNetid: (hidden) NetID (user ID) of the user that deployed the instance.
* TagAccountNumber: (hidden) Billing KFS account number (From group).
* TagTicketNumber: (hidden) Ticket number tracking deployment of this instance.
* TagCloudOpsGroupName: (hidden) CloudOps group name. (description attribute in LDAP)
* TagCloudOpsGroupId: (hidden) CloudOps group ID. (UUID of cn attribute in LDAP)


Development Setup
=================
To work on the Lambda functions locally, you will need [nodejs, npm][1] and [serverless][2] installed.

[1]: https://nodejs.org
[2]: https://serverless.com/framework/docs/getting-started/

You will also need an environment shell script named `environment-dev.sh` or `environment-prd.sh` containing environment variables for AWS account access and role information:

    #!/bin/bash

    echo "Sourcing DEV Environment"

    export AWS_ACCESS_KEY_ID=`cat /tmp/awscli.json | jq -r '.Credentials.AccessKeyId'`
    export AWS_SESSION_TOKEN=`cat /tmp/awscli.json | jq -r '.Credentials.SessionToken'`
    export AWS_SECRET_ACCESS_KEY=`cat /tmp/awscli.json | jq -r '.Credentials.SecretAccessKey'`
    export AWS_REGION=us-west-2

    # cloudops-nonprod AWS Account
    export ACCOUNT_NUMBER=XXXXXXXXXXXX
    export AWS_CLOUD_FORMATION_DEPLOYER_ROLE="arn:aws:iam::${ACCOUNT_NUMBER}:role/fdn-CloudFormationAdminDeployer"

These environment scripts should not be checked in to version control.


AWS Account Setup
=================
## SSM Parameters
    /cloudops/serverless-website/template-bucket - String - the name of the bucket that holds the default template
    /cloudops/serverless-website/groups_api_key - SecureString - the group API key. Located in stache under cloudops-groups-api-serverlesswebsites
    /cloudops/serverless-website/groups_api_url - SecureString - Group API URL
    /cloudops/serverless-website/stache_api_key - SecureString - Password key for making new S3 key entries in stache
    /cloudops/serverless-website/stache_api_url - String - URL for Stache API to create bucket key entries
    /cloudops/serverless-website/stache_folder - String - Stache Folder ID to put bucket keys into

## S3 Bucket
An S3 bucket must be created to hold the sample files for the serverless website. These files are copied into each new site as a placeholder.

This can be created with:

    sls deploy
    sls deploy client

## Lambda Functions
There are a few Lambda functions required for this service. These Lambda functions are deployed as a Serverless project. With appropriate credentials being set in an `environment-dev.sh` or `environment-prd.sh` environment script, run the deployment command.

    npm run deploy

The Serverless deployment only has to happen once per account, or upon updates to the Lambda functions.

### Copy Default Files (util_functions.py)
This Lambda function implements a CloudFormation custom resource. This resource is referened after the S3 bucket is created, and copies a set of sample website files over to the bucket. Upon deletion of the stack, this function also empties the S3 bucket prior to deletion.

### Generate IAM Keys (lambda_generate_iam_keys.py)
This Lambda function is called after a serverless-website stack has been deployed. A CloudFormation stack name is passed to it, and the lambda function will create IAM access keys for the user of the stack, and store those credentials in Stache. It will delete all existing IAM keys on the user before making new ones, so it can be called multiple times in case credentials are lost.
