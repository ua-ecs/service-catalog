#!/bin/bash -e

ENVIRONMENT=$1
SITENAME=$2
NETID=$3

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

if [ "$ENVIRONMENT" == "prd" ]
then
    source $SCRIPTPATH/../environment-prd.sh
else
    source $SCRIPTPATH/../environment-dev.sh
fi

if [ "$SITENAME" == "" ]
then
    echo "Site Name and NetID arguments required:"
    echo "npm run deploy-site <dev|prd> <sitename> <netid>"
    echo ""
    exit 0
fi

python3 $SCRIPTPATH/deploy-serverless-website.py $SITENAME $NETID
