import time
import boto3

"""
Monitor a CloudFormation stack and print out the status of the individual resources.
"""
def monitor_cfn_stack(stack_name, region):
    stack_complete_conditions = ['CREATE_COMPLETE', 'ROLLBACK_COMPLETE', 'DELETE_COMPLETE', 'ROLLBACK_FAILED']
    cfn = boto3.client('cloudformation', region_name=region)

    print("")
    print(f"{'Resource Name':^40} {'Status':^30} {'Resource Type':^35} {'Reason':^35}")
    print(f"{'='*40} {'='*30} {'='*35} {'='*35}")
    wait_for_stack = True
    count = 0
    max_count = 500
    printed_statuses = []
    while wait_for_stack:
        stack_status = cfn.describe_stack_events(StackName=stack_name)
        events = stack_status['StackEvents']

        for e in events:
            print_resource = False
            resourceId = e['LogicalResourceId']
            resourceStatus = e['ResourceStatus']
            if 'ResourceStatusReason' in e:
                statusReason = e['ResourceStatusReason']
            else:
                statusReason = ""

            # If we've seen this resource/status before ignore it
            resource_tuple = resourceId + resourceStatus
            if resource_tuple not in printed_statuses:
                printed_statuses.append(resource_tuple)
                print_resource = True

            if print_resource:
                print(f"{resourceId:<40} {resourceStatus:<30} {e['ResourceType']:<35} {statusReason}")

            # See if this is the stack itself, and stop if it is finished
            if e['ResourceType'] == "AWS::CloudFormation::Stack":
                if resourceStatus in stack_complete_conditions:
                    wait_for_stack = False
                    continue

        count = count + 1
        if count > max_count:
            print(f"Gave Up after {max_count} ticks")
            wait_for_stack = False

        time.sleep(5)
