#
# $ python3 deploy-serverless-website.py
import sys
import boto3
import hashlib
import time
from uitsappdev_utils import monitor_cfn_stack

region = "us-west-2"
cfn = boto3.client('cloudformation', region_name=region)
ssm = boto3.client('ssm', region_name=region)
acm = boto3.client('acm', region_name="us-east-1")

if len(sys.argv) < 3:
    print("Usage: python3 deploy-serverless-website.py <sitename> <netid>")
    sys.exit()

site_name = sys.argv[1]
netid = sys.argv[2]

with open("templates/serverless-website.yaml", 'r') as f:
    template_data = f.read()


def get_temp_hash():
    t = time.time()
    st = str(t).encode("utf8")
    h = hashlib.sha1(st).hexdigest()
    h_short = h[:12]
    return h_short


def get_stack_output(stack_name, output_key):
    stack_response = cfn.describe_stacks(StackName=stack_name)
    outputs = stack_response["Stacks"][0]["Outputs"]

    for o in outputs:
        if o["OutputKey"] == output_key:
            return o["OutputValue"]

    raise KeyError(f"Key: '{output_key}' not found in stack 'f{stack_name}'")


def get_secure_parameter(p_name):
    response = ssm.get_parameter(Name=p_name, WithDecryption=True)
    v = response['Parameter']['Value']
    return v


def get_certificate_arn(name):
    acm_result = acm.list_certificates(CertificateStatuses=['ISSUED'])
    certificate_list = acm_result['CertificateSummaryList']
    for cert in certificate_list:
        if cert['DomainName'] == name:
            return cert['CertificateArn']
    return None


role_arn = get_stack_output("fdn-iam", "CloudFormationAdminDeployerRoleArn")

params_input = {
    "SiteName": site_name,
    # "CertificateARN": get_certificate_arn('*.cloudsites-nonprod.arizona.edu'),
    "CertificateARN": get_certificate_arn('fischerm.uits.arizona.edu'),
    'TagContactNetid': netid,
    'TagCloudOpsGroup': 'appdev',
    'TagAccountNumber': '1192660',
    'TagTicketNumber': '',
    'TagCloudOpsGroupId': 'e6c9caf7-5186-4a51-9c2f-677080de78b9',
}

params = []
for key, value in params_input.items():
    params.append({
        'ParameterKey': key,
        'ParameterValue': value
    })

tags_input = {
    "accountnumber": "1192660",      # Data Center Operations for testing
    "createdby": netid,
}

tags = []
for key, value in tags_input.items():
    tags.append({
        'Key': key,
        'Value': value
    })

# A stack name can contain only alphanumeric characters (case sensitive) and hyphens. 
# It must start with an alphabetic character and cannot be longer than 128 characters.
stack_name = site_name.replace(".", "-")
temp_hash = get_temp_hash()
stack_name = f"clops-sws-{stack_name}"

response = cfn.create_stack(
    StackName=stack_name,
    TemplateBody=template_data,
    RoleARN=role_arn,
    DisableRollback=False,
    Capabilities=['CAPABILITY_IAM'],
    Parameters=params,
    Tags=tags,
)

print(response)

monitor_cfn_stack(stack_name, region)
