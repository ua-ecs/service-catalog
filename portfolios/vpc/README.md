# The VPC template has been moved

The VPC CloudFormation template is now maintained at [https://bitbucket.org/ua-uits-cloud/aws-vpc/src/master](https://bitbucket.org/ua-uits-cloud/aws-vpc/src/master)
