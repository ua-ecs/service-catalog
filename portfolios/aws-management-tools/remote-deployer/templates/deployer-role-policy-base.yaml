#
# The TrustED account.
# http://docs.aws.amazon.com/IAM/latest/UserGuide/troubleshoot_roles.html
# http://docs.aws.amazon.com/lambda/latest/dg/with-sns-create-x-account-permissions.html
# 
AWSTemplateFormatVersion: "2010-09-09"
Description: "UITS Account Foundation: Remote Deployer IAM Role (Base)"

Metadata:

  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Account profile DynamoDB table
      Parameters:
      - DynamoDbName
    - Label:
        default: Remote deployer Lambda function
      Parameters:
      - LambdaCodeS3Bucket
      - LambdaCodeS3Path
      - LambdaRuntime
      - LambdaTimeout
    ParameterLabels: {}

Parameters:

  DynamoDbName:
    Type: "String"
    Default: "deployer_targets"
    Description: The name of the DynamoDB table containing account profiles.

  LambdaCodeS3Bucket:
    Type: "String"
    Default: "ua-uits-ecs-public"
    Description: The S3 Bucket in which the Lambda code can be found.

  LambdaCodeS3Path:
    Type: "String"
    Default: "lambda"
    Description: The path within the S3 Bucket in which the Lambda code can be found.

  LambdaRuntime:
    Type: "String"
    Default: "python3.6"
    AllowedValues: [ "python3.6" ]
    Description: The runtime environment required for the Lambda functions.

  LambdaTimeout:
    Type: "String"
    Default: "60"
    Description: Lambda function timeout (in seconds).

Resources:

  RemoteDeployerRoleBase:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: "fdn-iam-RemoteDeployerRoleBase"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement: 
          - Effect: "Allow"
            Principal:
              Service: "lambda.amazonaws.com"
            Action: "sts:AssumeRole"
      Path: "/"
      Policies:
      - PolicyName: "RemoteDeployerPolicy"
        PolicyDocument:
          Version: "2012-10-17"
          Statement:
          - Effect: "Allow"
            Action: 
            - "dynamodb:GetItem"
            - "dynamodb:UpdateItem"
            Resource: !Sub arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${DynamoDbName}
          - Effect: "Allow"
            Action: "ssm:GetParameters"
            Resource: !Sub arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:*
          - Effect: "Allow"
            Action: "s3:GetObject"
            Resource: arn:aws:s3:::ua-uits-ecs-public/service-catalog/master/*
          - Effect: "Allow"
            Action: "sts:AssumeRole"
            Resource: arn:aws:iam::*:role/fdn-iam-RemoteDeployerRoleTarget
          - Effect: "Allow"
            Action: "kms:Decrypt"
            Resource: !Sub arn:aws:kms:${AWS::Region}:${AWS::AccountId}:key/*
          - Effect: "Allow"
            Action:
            - "cloudformation:CreateStack"
            - "cloudformation:UpdateStack"
            Resource: !Sub "arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/fdn-*/*"
          - Effect: "Allow"
            Action:
            - "cloudformation:ListStacks"
            - "cloudformation:DescribeStacks"
            - "logs:CreateLogGroup"
            - "logs:CreateLogStream"
            - "logs:PutLogEvents"
            Resource: "*"

  AccountProfileDynamoDB:
    Type: "AWS::DynamoDB::Table"
    DeletionPolicy: Retain
    Properties:
      TableName: !Ref DynamoDbName
      AttributeDefinitions:
        - AttributeName: "account_id"
          AttributeType: "N"
      KeySchema:
        - AttributeName: "account_id"
          KeyType: "HASH"
      ProvisionedThroughput:
        ReadCapacityUnits: "1"
        WriteCapacityUnits: "1"

  RemoteDeployerLambda:
    Type: "AWS::Lambda::Function"
    Properties:
      FunctionName: "fdn-remote-deployer-lambda"
      Handler: "lambda_remote_deployer.lambda_handler"
      Description: "Remote Deployer Lambda function"
      Role: !GetAtt RemoteDeployerRoleBase.Arn
      Runtime: !Ref LambdaRuntime
      Timeout: !Ref LambdaTimeout
      Code:
        S3Bucket: !Ref LambdaCodeS3Bucket
        S3Key: !Sub "${LambdaCodeS3Path}/lambda-remote-deployer.zip"
        S3ObjectVersion: "XlrwSNOjiehM8Cwnvtk7.82AMzWq5n0x"

Outputs:

  RdRoleBaseArn:
    Value: !GetAtt RemoteDeployerRoleBase.Arn
  RdDynamoDbArn:
    Value: !GetAtt AccountProfileDynamoDB.Arn
  RdLambdaFunctionArn:
    Value: !GetAtt RemoteDeployerLambda.Arn
