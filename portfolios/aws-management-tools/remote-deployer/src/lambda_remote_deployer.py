#
# This program allows CloudFormation templates to be in nominated accounts from
# a central account.
#
# Version 1.1
# Added last_run to DynamoDB. Updates to Now() UTC when the desired operation 
# has been successfully performed in the specified account.
#
# Version 1.0
# Initial implementation combining DynamoDB, Secure Parameter store, Lambda,
# CloudFormation, and IAM roles and policies to create and update CloudFormation
# stacks in remote accounts.
#

# Load dependencies.
import boto3
import json
import logging
import sys
import os
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

# Data class definitions.
class profile(object):

    AccountId = int(0)
    Name = str('')
    Deployment = str('')
    Enabled = False
    FoundationParams = dict({})
    FoundationUrl = str('')
    FoundationStack = str('')
    DeployerRoleArn = str('')
    CloudFormationRoleArn = str('')
    LastRunDateTimeUtc = str('')

    def __init__(self, account_id, name, deployment, enabled, fdn_stack, fdn_url, fdn_params, last_run):
        self.AccountId = str(account_id).strip()
        self.DeployerRoleArn = "arn:aws:iam::" + str(account_id) + ":role/fdn-iam-RemoteDeployerRoleTarget"
        self.Name = str(name).strip()
        self.Deployment = str(deployment).strip()
        self.Enabled = bool(enabled)
        self.FoundationStack = str(fdn_stack).strip()
        self.FoundationUrl = str(fdn_url).strip()
        self.FoundationParams = eval(str(fdn_params).strip())
        self.LastRunDateTimeUtc = str(last_run).strip()

def lambda_handler(event, context):

    # Set up logging for later use.
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.INFO)

    # Use environment variables to pass in parameters.
    #this_account_id = os.environ['TargetAccountId'] 
    #check_fdn_version = bool(str(os.environ['PerformVersionCheck']).strip().lower() == 'true')

    # Use the event parameter as the source of all other parameters.
    this_account_id = event['TargetAccountId'] 
    check_fdn_version = bool(str(event['PerformVersionCheck']).strip().lower() == 'true')

    logging.info("Processing request for account: " + str(this_account_id))
    logging.info("Perform a version check on the foundation? " + str(check_fdn_version))

    # Get the details from the database about this account.
    this_account_profile = None
    accounts_table = None
    try:
        # Get a reference to the account profile table.
        dynamodb_resource = boto3.resource('dynamodb')
        accounts_table = dynamodb_resource.Table('deployer_targets')
        # Look for this account's profile.
        account_record = accounts_table.get_item(Key = {'account_id': int(this_account_id)})
        # Look for secure parameters.
        these_parameters = None
        try:
            # Connect to the secure paramater store.
            ssm_client = boto3.client('ssm')
            # Form the secure parameter key.
            ssm_key = "fdn_param_" + str(this_account_id) 
            logging.info("Seeking foundation parameters in SSM with key: " + str(ssm_key))
            # Request the decrypted secure parameter.
            response = ssm_client.get_parameters(Names = [ssm_key], WithDecryption = True)
            if (response != None): these_parameters = response["Parameters"][0]["Value"]
        except ClientError as ex:
            # An exception occurred during account profile collection.
            logging.error("Exception while seeking CloudFormation parameters: " + ex.response['Error']['Message'])
        except Exception as ex:
            # An exception occurred.
            logging.error("Exception while seeking CloudFormation parameters: " + str(ex))
        # Did we find what we're looking for?
        if ('Item' in account_record):
            # Display the details of the account profile.
            this_account_profile = profile(
                str(this_account_id),
                str(account_record['Item']['name']),
                str(account_record['Item']['deployment']),
                bool(account_record['Item']['enabled']),
                str(account_record['Item']['fdn_stack']),
                str(account_record['Item']['fdn_url']),
                str(these_parameters), 
                str(account_record['Item']['last_run'])
                )
    except ClientError as ex:
        # An exception occurred during account profile collection.
        logging.error("Exception while seeking account profile record: " + ex.response['Error']['Message'])
    except Exception as ex:
        # An exception occurred.
        logging.error("Exception while seeking account profile record: " + str(ex))
                
    if (this_account_profile == None):
        logging.error("Unable to get account profile for account: " + str(this_account_id))
    else:
        if (not this_account_profile.Enabled):
            # This account profile is marked as disabled, skip it.
            logging.info("Account " + str(this_account_profile.AccountId) + " has a profile marked as disabled. Skipped.")
        else:

            # Get the assume role object for this connection.
            logging.info("Getting temporary credentials for " + this_account_profile.DeployerRoleArn)
            sts_client = boto3.client('sts')
            assumedRoleObject = sts_client.assume_role(
                RoleArn = this_account_profile.DeployerRoleArn,
                RoleSessionName = "RemoteDeployerSession")
            credentials = assumedRoleObject['Credentials']
                
            # Create a CloudFormation client.
            logging.info("Connecting to the CloudFormation service of the target account (" + str(this_account_profile.AccountId) + "), with an assumed role (" + this_account_profile.DeployerRoleArn + "), and with temporary credentials.")
            cf_resource = boto3.resource('cloudformation',
                                         aws_access_key_id = credentials['AccessKeyId'], 
                                         aws_secret_access_key = credentials['SecretAccessKey'],
                                         aws_session_token = credentials['SessionToken'])
            cf_client = cf_resource.meta.client

            # If required, perform a version check on the target account's foundation.
            is_version_valid = (not check_fdn_version)
            if (check_fdn_version):
                try:
                    # Look for a stack named "fdn-version" as it contains the current version of the foundation.
                    foundtation_version = 0
                    these_stacks = cf_client.describe_stacks( StackName = 'fdn-version' )
                    for this_stack in these_stacks['Stacks']:
                        for this_output in this_stack['Outputs']:
                            if (this_output['OutputKey'] == 'FoundationVersion'):
                                foundtation_version = int(this_output['OutputValue'])
                                logging.info("Found 'FoundationVersion' output in stack 'fdn-version' with value " + str(foundtation_version))
                                break
                        break
                    # The foundation version is 'valid' if it is > 8.
                    is_version_valid = (foundtation_version > 8)
                    logging.info("Foundation version was checked. Current version: " + str(foundtation_version) + ", Required version: > 8, Result: " + str(is_version_valid))
                except Exception as ex:
                    # An exception occurred.
                    logging.error("Exception while seeking the FoundationVersion output from CloudFormation stack 'fdn-version': " + str(ex))
                    is_version_valid = False

            if (not is_version_valid):
                # We were instructed to check the version of the foundation and we've determined
                # that the current version is below what is required.
                logging.error("Foundation version is below required version, operation abandoned.")
            else:

                try:
                    # Look for a stack named "fdn-iam" as it contains the ARN for CloudFormationAdminDeployerRole.
                    these_stacks = cf_client.describe_stacks( StackName = 'fdn-iam' )
                    for this_stack in these_stacks['Stacks']:
                        for this_output in this_stack['Outputs']:
                            if (this_output['OutputKey'] == 'CloudFormationAdminDeployerRole'):
                                output_value = str(this_output['OutputValue'])
                                this_account_profile.CloudFormationRoleArn = "arn:aws:iam::" + str(this_account_profile.AccountId) + ":role/" + output_value
                                logging.info("Found 'CloudFormationAdminDeployerRole' output in stack 'fdn-iam' with value '" + output_value + "' fully qualified as '" + this_account_profile.CloudFormationRoleArn + "'")
                                break
                        break
                except Exception as ex:
                    # An exception occurred.
                    logging.error("Exception while seeking the CloudFormationAdminDeployerRole output from CloudFormation stack 'fdn-iam': " + str(ex))

                stack_op_ok = False
                try:

                    stack_list = None
                    stack_exists = False
                    abandon_op = False

                    # Get the list of stacks present in the account and see if what we're trying to deploy already exists.
                    try:
                        stack_list = cf_client.list_stacks()
                    except Exception as ex:
                        # An exception occurred.
                        logging.error("Exception while enumerating CloudFormation stacks in account " + str(this_account_profile.AccountId))

                    if (stack_list == None):
                        logging.error("Unable to enumerate existing CloudFormation stacks in account " + str(this_account_profile.AccountId) + ", abandoning operation.")
                        abandon_op = True
                    else:
                        for this_stack in stack_list['StackSummaries']:
                            if (str(this_stack['StackName']) == this_account_profile.FoundationStack):
                                # We found our stack.
                                if (this_stack['StackStatus'] in ['CREATE_COMPLETE', 'ROLLBACK_COMPLETE', 'UPDATE_ROLLBACK_COMPLETE', 'UPDATE_COMPLETE']):
                                    # This stack looks to be in a state we can work with.
                                    logging.info("The CloudFormation stack (" + this_account_profile.FoundationStack + ") already exists in the account and is in a manageable state (status = '" + this_stack['StackStatus'] + "').")
                                    stack_exists = True
                                    abandon_op = False
                                elif (this_stack['StackStatus'] == 'DELETE_COMPLETE'):
                                    # When stacks are deleted, they're reported as still existing. UpdateStack cannot be called, but CreateStack can.
                                    logging.info("The CloudFormation stack (" + this_account_profile.FoundationStack + ") already exists in the account and has been deleted. (status = 'DELETE_COMPLETE').")
                                    stack_exists = False
                                    abandon_op = False
                                else: # Any other stack status, abandon the operation.
                                    logging.error("The CloudFormation stack already exists and is in an unmanageable state (status = '" + this_stack['StackStatus'] + "'). The operation will be abanonded.")
                                    stack_exists = True
                                    abandon_op = True
                                break

                    if (not abandon_op):

                        # Convert parameters from key:value pairs to the API's [{'ParameterKey': 'key', 'ParameterValue': 'value'}, ...] notation.
                        stack_params = []
                        if (this_account_profile.FoundationParams == None):
                            logging.info("Foundation parameters for this account appear to be missing or unobtainable.")
                        else:
                            for this_item in this_account_profile.FoundationParams.items():
                                new_tup = { "ParameterKey": this_item[0], "ParameterValue": this_item[1] }
                                stack_params.append(new_tup.copy())

                        if (not stack_exists):
                            # Create the foundation stack.
                            logging.info("Calling create_stack with StackName: " + this_account_profile.FoundationStack + ", TemplateURL: " + this_account_profile.FoundationUrl + " and RoleARN: " + this_account_profile.CloudFormationRoleArn)
                            cf_client.create_stack(StackName = this_account_profile.FoundationStack, 
                                                   TemplateURL = this_account_profile.FoundationUrl,
                                                   Parameters = stack_params,
                                                   DisableRollback = True,
                                                   Capabilities = ['CAPABILITY_IAM'],
                                                   RoleARN = this_account_profile.CloudFormationRoleArn)
                            logging.info("Call completed successfully. CloudFormation stack creation in progress.")
                        else:
                            # Update the foundation stack.
                            logging.info("Calling update_stack with StackName: " + this_account_profile.FoundationStack + ", TemplateURL: " + this_account_profile.FoundationUrl + " and RoleARN: " + this_account_profile.CloudFormationRoleArn)
                            cf_client.update_stack(StackName = this_account_profile.FoundationStack, 
                                                   TemplateURL = this_account_profile.FoundationUrl,
                                                   Parameters = stack_params,
                                                   Capabilities = ['CAPABILITY_IAM'],
                                                   RoleARN = this_account_profile.CloudFormationRoleArn)
                            logging.info("Call completed successfully. CloudFormation stack update in progress.")

                        # Signal that we were able to invoke the CloudFormation operation successfully.
                        stack_op_ok = True

                except ClientError as ex:
                    # An exception occurred during account profile collection.
                    logging.error("ClientError exception while invoking CloudFormation template: " + ex.response['Error']['Message'])
                except Exception as ex:
                    # An exception occurred.
                    logging.error("General exception while invoking CloudFormation template: " + str(ex))

                # Was the selected CloudFormation operation completed successfully?
                if (stack_op_ok):
                    
                    # Update the DynamoDB table with SET last_run = Now() UTC
                    accounts_table.update_item(Key={'account_id': this_account_profile.AccountId},
                                               UpdateExpression="SET last_run = :updated",                   
                                               ExpressionAttributeValues={':updated': datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")})

    return None
