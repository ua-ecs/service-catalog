---
# D2L Course Site Request - CloudFormation Template (3 of 4)
# -----------------------------------------
#
# Deploys a CloudFront Distribution for the maintenance site.
#
# 1) This assumes a publicly accessible S3 bucket (S3 ACL granting 'Everyone' List) exists.
#    - At this time CloudFront does not support creating an Origin Access Identity (OAI).
#      Rather than adding a manual step to create an OAI we are not using one, instead
#      leaving the whole S3 bucket pubic.
#    - Template #1 creates the bucket and modifies the ACL.
# 2) This is in its own template because CloudFormation waits until the CloudFront Distribution
#    is fully deployed (status=Deployed) before completing the stack build.
#    - A CloudFront Distribution takes anywhere from 20 minutes to an hour to fully deploy.
#

AWSTemplateFormatVersion: 2010-09-09
Description: D2L Course Site Request (CloudFront)

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
#

Parameters:

  # S3StackName is the name of the stack that created the S3 buckets
  S3StackName:
    Description: Name of the stack that created the S3 buckets (template 1 of 4)
    Type: String
    Default: d2lcsr-prd-s3
  # AcmSSLCertARN is ARN of the *.d2l.arizona.edu wildcard cert in ACM (Must be imported in US-East-1)
  # - sls-nonprod: arn:aws:acm:us-east-1:776095071695:certificate/a2c2bc9c-0fc3-479b-b9a0-7e6f10b875de
  # - sls-prod: arn:aws:acm:us-east-1:918461542486:certificate/90775a98-14a1-42ac-94a8-b4d56b193a89
  AcmSSLCertARN:
    Description: ARN of the *.d2l.arizona.edu wildcard cert in ACM (US-East-1 region)
    Type: String
    Default: arn:aws:acm:us-east-1:918461542486:certificate/90775a98-14a1-42ac-94a8-b4d56b193a89

  ### Tags
  TagService:
    Description: Name of the service associated with this resource (as listed in the service catalog)
    Type: String
    Default: D2L Course Site Request
  TagEnvironment:
    Description: Environment type of this resource (dev, tst, rpt, trn, prd)
    Type: String
    Default: prd
    AllowedValues:
      - dev
      - tst
      - rpt
      - trn
      - prd
  TagContactNetID:
    Description: NetID of the primary technical resource
    Type: String
    Default: dbaty
  TagTicketNumber:
    Description: Ticket number of the associated
    Type: String
    Default: CLOUD-76
  TagAccountNumber:
    Description: Account number associated with the service
    Type: String
    Default: 2433643 # Learning Management Systems
  TagSubAccount:
    Description: Sub account associated with the service
    Type: String
    Default: D2L

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Console UI.

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Settings
      Parameters:
      - S3StackName
      - AcmSSLCertARN
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagEnvironment
      - TagContactNetID
      - TagTicketNumber
      - TagAccountNumber
      - TagSubAccount
    ParameterLabels: {}

# Conditions
# ----------
# Establishes conditions based on input parameters.
#

Conditions:
  IsPRD: !Equals [ !Ref TagEnvironment, prd ]
  IsTST: !Equals [ !Ref TagEnvironment, tst ]

# Resources
# ---------
#
# Wow. This is a lot of resources to deploy. Do you really need them all?
#

Resources:

  # #### CloudFront
  #
  # CloudFront Distribution for maintenance site
  #

  CloudFrontDistributionMaintSite:
    Type: AWS::CloudFront::Distribution
    Properties:
      DistributionConfig:
        Aliases:
        - !If [ IsTST, test-maintenance.d2l.arizona.edu, !If [ IsPRD, maintenance.d2l.arizona.edu, !Sub "${TagEnvironment}-maintenance.d2l.arizona.edu" ] ]
        - !If [ IsTST, aws-test-csr.d2l.arizona.edu, !If [ IsPRD, csr.d2l.arizona.edu, !Sub "aws-${TagEnvironment}-csr.d2l.arizona.edu" ] ]
        - Fn::ImportValue:
            !Sub ${S3StackName}-fqdn-for-csr-no-trailing-dot
        Origins:
        - DomainName:
            Fn::ImportValue:
              !Sub ${S3StackName}-bucket-maint-fqdn
          Id: !Sub S3-d2lcsr-${TagEnvironment}-maintenance
          S3OriginConfig:
            OriginAccessIdentity: ""
        Enabled: true
        Comment: !Sub D2L Course Site Request (${TagEnvironment}) maintenance site
        CustomErrorResponses:
        - ErrorCode: 404
          ResponseCode: 200
          ResponsePagePath: "/"
        DefaultRootObject: index.html
        DefaultCacheBehavior:
          TargetOriginId: !Sub S3-d2lcsr-${TagEnvironment}-maintenance
          ForwardedValues:
            QueryString: false
            Cookies:
              Forward: none
          ViewerProtocolPolicy: redirect-to-https
        HttpVersion: http2
        PriceClass: PriceClass_100
        ViewerCertificate:
          AcmCertificateArn: !Ref AcmSSLCertARN
          SslSupportMethod: sni-only

# Outputs
# -------
#
# Outputs are values resulting from the CloudFormation stack that can be:
# 1) Viewed in the AWS cosole under the CloudFormation service.
# 2) Marked as export to be imported into another stack allowing cross-stack references.
#

Outputs:

  MaintCloudFrontFQDN:
    Description: FQDN of CloudFront Distribution for maintenance site.
    Value: !GetAtt CloudFrontDistributionMaintSite.DomainName
    Export:
      Name: !Sub ${AWS::StackName}-cloudfront-maint-fqdn

  MaintCloudFrontAliasMaintenanceArizona:
    Description: FQDN of CloudFront Distribution alias (maintenance.d2l.arizona.edu).
    Value: !If [ IsTST, test-maintenance.d2l.arizona.edu, !If [ IsPRD, maintenance.d2l.arizona.edu, !Sub "${TagEnvironment}-maintenance.d2l.arizona.edu" ] ]
    Export:
      Name: !Sub ${AWS::StackName}-cloudfront-maint-alias-maintenance-arizona

  MaintCloudFrontAliasCSRArizona:
    Description: FQDN of CloudFront Distribution alias (d2l.arizona.edu).
    Value: !If [ IsTST, aws-test-csr.d2l.arizona.edu, !If [ IsPRD, csr.d2l.arizona.edu, !Sub "aws-${TagEnvironment}-csr.d2l.arizona.edu" ] ]
    Export:
      Name: !Sub ${AWS::StackName}-cloudfront-maint-alias-csr-arizona

  MaintCloudFrontAliasCSRHostedZone:
    Description: FQDN of CloudFront Distribution alias (Route53 hosted zone).
    Value:
      Fn::ImportValue:
        !Sub ${S3StackName}-fqdn-for-csr-no-trailing-dot
    Export:
      Name: !Sub ${AWS::StackName}-cloudfront-maint-alias-csr-hostedzone
