---
# S3 Bucket for Disaster Recovery CloudFormation Deployment
# -----------------------------------------
#
# This CloudFormation template will deploy an S3 bucket designed to be used
# as a disaster recovery target from another account. Versioning is enabled,
# and a cross-account bucket policy is attached.

AWSTemplateFormatVersion: '2010-09-09'
Description: S3 Disaster Recovery Bucket

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # The name of the bucket.
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.

  RemoteAccountsList:
    Type: CommaDelimitedList
    Description: Comma delimited list of full root account ARNs

  EnableVersioningParameter:
    Type: String
    Description: Enable S3 Bucket Versioning
    Default: "Yes"
    AllowedValues: ["Yes", "No"]

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  TagService:
    Description: Service name (from the service catalog) that is utilizing this resource
    Type: String
  TagEnvironment:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'.
    Type: String
  TagContactNetId:
    Description: NetID of the person to contact for information about this resource
    Type: String
  TagAccountNumber:
    Description: Financial system account number for the service utilizing this resource
    Type: String
  TagSubAccount:
    Description: Financial system subaccount number for the service utilizing this resource
    Type: String
  TagTicketNumber:
    Description: Ticket number that this resource is for
    Type: String

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
      - RemoteAccountsList
      - EnableVersioningParameter
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagSubAccount
      - TagEnvironment
      - TagAccountNumber
      - TagCreatedBy
      - TagContactNetId
      - TagTicketNumber
    ParameterLabels:
      EnableVersioningParameter:
        default: 'Enable Versioning:'

# Conditions
# ----------
Conditions:
  EnableVersioning: !Equals [!Ref EnableVersioningParameter, "Yes"]

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### S3 Bucket
  #
  # This deploys the S3 bucket with some tags.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref "BucketName"
      AccessControl: Private
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: "AES256"
      VersioningConfiguration:
        Fn::If:
          - EnableVersioning
          - Status: Enabled
          - !Ref AWS::NoValue
      Tags:
      - Key: Name
        Value: !Sub "${BucketName}-s3"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetId
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: subaccount
        Value: !Ref TagSubAccount
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  BucketPolicy:
    Type: "AWS::S3::BucketPolicy"
    Properties:
      Bucket: !Ref S3Bucket
      PolicyDocument:
        Statement:
          - Sid: AllowRemoteAccountAccess
            Action:
              - "s3:GetBucketLocation"
              - "s3:ListBucket"
              - "s3:PutObject"
              - "s3:DeleteObject"
              - "s3:ListBucketVersions"
            Effect: "Allow"
            Principal:
              AWS: !Ref RemoteAccountsList
            Resource:
              - !Sub "arn:aws:s3:::${S3Bucket}"
              - !Sub "arn:aws:s3:::${S3Bucket}/*"
          - Sid: RequireFullAccessPut
            Action:
              - "s3:PutObject"
            Effect: "Deny"
            Principal:
              AWS: !Ref RemoteAccountsList
            Resource:
              - !Sub "arn:aws:s3:::${S3Bucket}/*"
            Condition:
              StringNotEquals:
                s3:x-amz-acl: bucket-owner-full-control

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Value: !Ref S3Bucket
