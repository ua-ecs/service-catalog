#!/bin/bash -ex

# Mount this directory in the container, then run the in-container-setup.sh script from within the container.
docker run -it --name pythontest -v `pwd`:/local --entrypoint /local/bin/in-container-setup.sh lambci/lambda:python3.7

# Clean up after ourselves
docker rm pythontest
