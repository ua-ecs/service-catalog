#!/bin/bash -ex

# This script should only be run from within the lambci/lambda container 
# to insure the correct architecture binaries are installed.

# Install python modules from requirements.txt
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd $SCRIPTPATH/..
pip install -r requirements.txt -t .
