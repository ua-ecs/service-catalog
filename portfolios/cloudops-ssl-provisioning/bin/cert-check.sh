#!/bin/bash -ex

sam local invoke CheckCertificateReady \
    -e event.json \
    -d 5678 \
    --env-vars environment_vars.json
