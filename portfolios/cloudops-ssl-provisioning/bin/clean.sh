#!/bin/bash -ex

rm -rf .libs_c
rm -rf ptvsd*
rm -rf asn1crypto*
rm -rf cffi*
rm -rf _cffi*
rm -rf .libs_cffi*
rm -rf cryptography*
rm -rf OpenSSL
rm -rf pycparser*
rm -rf pyOpenSSL*
rm -rf six*
rm -rf __pycache__

rm cloudops-ssl-provisioning.yaml
