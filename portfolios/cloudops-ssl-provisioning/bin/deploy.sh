aws cloudformation deploy \
    --template-file cloudops-ssl-provisioning.yaml \
    --stack-name cloudops-ssl-provisioning-functions \
    --capabilities CAPABILITY_IAM
