#!/bin/bash -ex

sam package \
    --template-file template.yaml \
    --output-template-file cloudops-ssl-provisioning.yaml \
    --s3-bucket edu-arizona-uitsnonprod-cloudops-deployments
