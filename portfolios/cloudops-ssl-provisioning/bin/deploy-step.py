#
# $ python3 deploy-step.py
#
# This program will deploy the CloudFormation step-function template.
import sys
import boto3

region = "us-west-2"
cfn = boto3.client('cloudformation', region_name=region)
ssm = boto3.client('ssm', region_name=region)

# if len(sys.argv) == 1:
#    print("Usage: python3 deploy-serverless-website.py <sitename>")
#    sys.exit()

with open("step-function.yaml", 'r') as f:
    template_data = f.read()


def get_stack_output(stack_name, output_key):
    stack_response = cfn.describe_stacks(
        StackName = stack_name
    )
    outputs = stack_response["Stacks"][0]["Outputs"]

    for o in outputs:
        if o["OutputKey"] == output_key:
            return o["OutputValue"]

    raise KeyError(f"Key: '{output_key}' not found in stack 'f{stack_name}'")


def get_secure_parameter(p_name):
    response = ssm.get_parameter(Name=p_name, WithDecryption=True)
    v = response['Parameter']['Value']
    return v


role_arn            = get_stack_output("fdn-iam", "CloudFormationAdminDeployerRoleArn")
request_lambda      = get_stack_output("cloudops-ssl-provisioning-functions", "RequestCertificateLambda")
check_lambda        = get_stack_output("cloudops-ssl-provisioning-functions", "CheckCertificateReadyLambda")
install_lambda      = get_stack_output("cloudops-ssl-provisioning-functions", "InstallCertificateLambda")
error_topic         = get_stack_output("fischerm-sns-error-testing", "ErrorReportingTopic")

params_input = {
    "RequestLambda"         : request_lambda,
    "StatusLambda"          : check_lambda,
    "InstallLambda"         : install_lambda,
    "ErrorReportingTopic"   : error_topic,
}

params = []
for key, value in params_input.items():
    p = {}
    p['ParameterKey'] = key
    p['ParameterValue'] = value
    params.append(p)

tags_input = {
    "accountnumber"         : "1192660",      # Data Center Operations for testing
    "createdby"             : "fischerm",
    "service"               : "CloudOps",
}

tags = []
for key, value in tags_input.items():
    t = {}
    t['Key'] = key
    t['Value'] = value
    tags.append(t)


# A stack name can contain only alphanumeric characters (case sensitive) and hyphens.
# It must start with an alphabetic character and cannot be longer than 128 characters.
stack_name = f"cloudops-ssl-provisioning-step-fn"

response = cfn.create_stack(
    StackName         = stack_name,
    TemplateBody      = template_data,
    RoleARN           = role_arn,
    DisableRollback   = False,
    Capabilities      = ['CAPABILITY_IAM'],
    Parameters        = params,
    Tags              = tags,
)

print(response)
