import os
import json
import ptvsd
import logging
import xml.dom.minidom
import boto3
from botocore.vendored import requests
from datetime import date, datetime
import decimal

rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)

if os.getenv("AWS_SAM_LOCAL"):
    # Local Only Config
    rootlogger.setLevel(logging.DEBUG)
    if os.getenv("REMOTE_DEBUGGING") == 'true':
        ptvsd.enable_attach(address=('0.0.0.0', 5678), redirect_output=True)
        ptvsd.wait_for_attach()
else:
    # Regular AWS Config
    True

region = 'us-west-2'

dynamodb = boto3.resource('dynamodb', region_name=region)
key_db = dynamodb.Table(os.environ['KEY_TABLE'])
ssm = boto3.client('ssm', region_name=region)


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)

    raise TypeError("Type %s not serializable" % type(obj))


def get_secure_parameter(p_name):
    response = ssm.get_parameter(Name=p_name, WithDecryption=True)
    v = response['Parameter']['Value']
    return v


username = get_secure_parameter("/cloudops/ssl_req/username")
password = get_secure_parameter("/cloudops/ssl_req/password")
url = get_secure_parameter("/cloudops/ssl_req/url")


def retrieveCertificateReq(site):
    response = key_db.get_item(
        Key={
            'site': site.lower()
        }
    )
    logging.debug(json.dumps(response, default=json_serial))
    cert_req = response['Item']
    return cert_req


def parse_cert_response(xmlString):
    """
    Parse the resulting XML from our SOAP call. Should look like this:
    <?xml version='1.0' encoding='UTF-8'?>
    <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body>
            <ns2:collectResponse xmlns:ns2="http://ssl.ws.epki.comodo.com/">
                <return>
                    <statusCode>2</statusCode>
                    <SSL>
                        <certificate>*********</certificate>
                        <renewID>*********</renewID>
                    </SSL>
                </return>
            </ns2:collectResponse>
        </S:Body>
    </S:Envelope>
    """
    DOMTree = xml.dom.minidom.parseString(xmlString)
    envelope = DOMTree.documentElement
    body = envelope.getElementsByTagName("S:Body")[0]
    collectResponse = body.getElementsByTagName("ns2:collectResponse")[0]
    returnElement = collectResponse.getElementsByTagName("return")[0]
    SSLElement = returnElement.getElementsByTagName("SSL")[0]
    certificateElement = SSLElement.getElementsByTagName("certificate")[0]
    certificate = str(certificateElement.childNodes[0].data)
    return certificate


def retrieveCertificate(site_req, type):
    """
    type can be either 1 or 2:
        1 = X509 PEM Certificate only
        2 = X509 PEM Intermidiate certificate only
    """
    certificate_id = site_req['certificate_identifier']
    requestXML = f"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ssl="http://ssl.ws.epki.comodo.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <ssl:collect>
         <!--Optional:-->
         <authData>
            <login>{username}</login>
            <password><![CDATA[{password}]]></password>
            <customerLoginUri>InCommon</customerLoginUri>
         </authData>
         <id>{certificate_id}</id>
         <formatType>{type}</formatType>
      </ssl:collect>
   </soapenv:Body>
</soapenv:Envelope>
"""
    headers = {'content-type': 'text/xml'}
    response = requests.post(url, data=requestXML, headers=headers)
    logging.debug(requestXML)
    logging.debug(response.content.decode("utf-8"))
    return parse_cert_response(response.content.decode("utf-8"))


def importCertificate(site_req, cert, chain):
    site = site_req['site']
    key = site_req['key']
    acm_region = boto3.client('acm', region_name=region)
    acm_east = boto3.client('acm', region_name='us-east-1')

    # Check for existing cert to replace first
    # blah blah checks

    # Install the cert in both the region we're dealing with and us-east-1
    response = acm_region.import_certificate(
        Certificate=cert.encode("utf-8"),
        PrivateKey=key.encode("utf-8"),
        CertificateChain=chain.encode("utf-8")
    )
    logging.info(f"Imported certificate for {site} into {region} region.")
    response = acm_east.import_certificate(
        Certificate=cert.encode("utf-8"),
        PrivateKey=key.encode("utf-8"),
        CertificateChain=chain.encode("utf-8")
    )
    logging.info(f"Imported certificate for {site} into us-east-1 region.")


def lambda_handler(event, context):
    logging.debug(json.dumps(event, indent=2))
    if "site" in event:
        site = event['site']
    else:
        logging.error("Required key 'site' not found in event.")
        return {
            "status": "error",
            "msg": f"Required key 'site' not found in event.",
        }

    site_req = retrieveCertificateReq(site)

    # Retrieve the certificate (Type 1)
    certificate = retrieveCertificate(site_req, 1)
    # Retrieve the intermediary chain (Type 2)
    chain = retrieveCertificate(site_req, 2)

    importCertificate(site_req, certificate, chain)

    return {
        "status": "installed",
        "msg": f"Certificat installed for {site}",
    }
