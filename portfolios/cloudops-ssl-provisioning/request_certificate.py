import os
import traceback
import json
import ptvsd
import time
import logging
from OpenSSL import crypto
import xml.dom.minidom
import boto3
from boto3.dynamodb.conditions import Key
from botocore.vendored import requests


rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)

region = 'us-west-2'

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
key_db = dynamodb.Table(os.environ['KEY_TABLE'])
ssm = boto3.client('ssm', region_name=region)


if os.getenv("AWS_SAM_LOCAL"):
    # Local Only Config
    rootlogger.setLevel(logging.DEBUG)
    if os.getenv("REMOTE_DEBUGGING") == 'true':
        ptvsd.enable_attach(address=('0.0.0.0', 5678), redirect_output=True)
        ptvsd.wait_for_attach()
else:
    # Regular AWS Config
    True


def get_secure_parameter(p_name):
    response = ssm.get_parameter(Name=p_name, WithDecryption=True)
    v = response['Parameter']['Value']
    return v


def storeKey(site, key):
    """
    Store a key in a DynamoDB table
    """
    key_db.put_item(
        Item = {
            'site': site.lower(),
            'key': key,
            'created_at': str(time.time()),
            'last_updated': str(time.time())
        }
    )
    logging.info(f"Storing new key for {site}")


def storeCertificateIdentifier(site, identifier):
    response = key_db.update_item(
        Key={
            'site': site.lower()
        },
        UpdateExpression="set certificate_identifier = :id, last_updated = :ts",
        ExpressionAttributeValues={
            ':id': identifier,
            ':ts': str(time.time())
        },
        ReturnValues="UPDATED_NEW"
    )
    logging.info(f"Storing certificate identifier for {site}")
    return response


def getKey(site):
    """
    Retrieve a private key from DynamoDB, or create it if it doesn't exist
    """
    query_args = {
        'KeyConditionExpression': Key('site').eq(site.lower()),
    }
    response = key_db.query(**query_args)

    keytext = ""

    if response['Count'] == 0:
        # Make a new key
        type = crypto.TYPE_RSA
        bits = 2048
        key = crypto.PKey()
        key.generate_key(type, bits)
        keytext = crypto.dump_privatekey(crypto.FILETYPE_PEM, key)
        keytext = keytext.decode("utf-8")
        storeKey(site, keytext)
    else:
        keytext = response['Items'][0]['key']

    return keytext


def generateCSR(nodename, sans = []):

    # Add 'DNS:' prefix to each SAN
    ss = []
    for altName in sans:
        ss.append(f"DNS: {altName}")
    ss = ", ".join(ss)

    req = crypto.X509Req()
    req.get_subject().CN = nodename
    req.get_subject().countryName                   = 'US'
    req.get_subject().stateOrProvinceName           = 'Arizona'
    req.get_subject().localityName                  = 'Tucson'
    req.get_subject().organizationName              = 'The University of Arizona'
    req.get_subject().organizationalUnitName        = 'UITS'
    # Add in extensions
    base_constraints = ([
        crypto.X509Extension(b"keyUsage", False, b"Digital Signature, Non Repudiation, Key Encipherment"),
        crypto.X509Extension(b"basicConstraints", False, b"CA:FALSE"),
    ])
    x509_extensions = base_constraints

    # If there are SAN entries, append the base_constraints to include them.
    if ss:
        san_constraint = crypto.X509Extension("subjectAltName", False, ss)
        x509_extensions.append(san_constraint)
    req.add_extensions(x509_extensions)

    # Utilizes generateKey function to kick off key generation.
    keytext = getKey(nodename)
    key = crypto.load_privatekey(crypto.FILETYPE_PEM, keytext)
    req.set_pubkey(key)
    req.sign(key, "sha1")
    return req


def parse_cert_response(xmlString):
    """
    Parse the resulting XML from our SOAP call. Should look like this:
    <?xml version='1.0' encoding='UTF-8'?>
    <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body>
            <ns2:enrollResponse xmlns:ns2="http://ssl.ws.epki.comodo.com/">
                <return>1273082</return>
            </ns2:enrollResponse>
        </S:Body>
    </S:Envelope>
    """
    DOMTree = xml.dom.minidom.parseString(xmlString)
    envelope = DOMTree.documentElement
    body = envelope.getElementsByTagName("S:Body")[0]
    enrollResponse = body.getElementsByTagName("ns2:enrollResponse")[0]
    returnElement = enrollResponse.getElementsByTagName("return")[0]
    certificate_id = str(returnElement.childNodes[0].data)
    return certificate_id


def requestCertificate(csr, site):
    """
    Make a raw SOAP call to the cert-manager.com service to request a certificate.
    """
    username = get_secure_parameter("/cloudops/ssl_req/username")
    password = get_secure_parameter("/cloudops/ssl_req/password")
    secretkey = get_secure_parameter("/cloudops/ssl_req/secretkey")
    url = get_secure_parameter("/cloudops/ssl_req/url")
    requestXML = f"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ssl="http://ssl.ws.epki.comodo.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <ssl:enroll>
         <!--Optional:-->
         <authData>
            <login>{username}</login>
            <password><![CDATA[{password}]]></password>
            <customerLoginUri>InCommon</customerLoginUri>
         </authData>
         <!--Optional:-->
         <orgId>10113</orgId>
         <!--Optional:-->
         <csr>{csr}</csr>
         <!--Optional:-->
         <certType>
             <id>224</id>
             <!--Optional:-->
             <name>InCommon SSL (SHA-2)</name>
             <!--Zero or more repetitions:-->
             <terms>2</terms>
         </certType>
         <serverType>-1</serverType>
         <!--Optional:-->
         <secretKey>{secretkey}</secretKey>
         <term>2</term>
         <phrase>no-cert-for-you</phrase>
         <!--Optional:-->
         <comments>Requesting new certificate for {site}</comments>
      </ssl:enroll>
   </soapenv:Body>
</soapenv:Envelope>
"""
    headers = {'content-type': 'text/xml'}
    response = requests.post(url, data=requestXML, headers=headers)
    certificate_id = parse_cert_response(response.content)
    return certificate_id


def decriment_retry(event):
    if event is not None:
        # If we have a retry count set, decriment it and see if we need
        # to keep going
        if "retry_count" in event:
            c = event["retry_count"]
            if c > 0:
                c = c - 1
            if c == 0:
                event["retry"] = False
            else:
                event["retry"] = True

        # If its not set, then we'll retry a few times
        else:
            c = 3
            event["retry"] = True

        event["retry_count"] = c

    return event


def generate_response(status=None, site=None, certificate_id=None, msg=None, retry_count=None, event=None, errorInfo=None):
    # We always need an explicitly set status to return
    if status is None:
        logging.error("Required response value 'status' cannot be None.")

    # If we're not explicitly given a site, see if there's one in the event.
    # Unless this response is an error, we should have a site to return.
    if site is None:
        if event is not None and "site" in event:
            site = event["site"]
        else:
            if status != "error":
                logging.error("Required response value 'site' cannot be None.")

    # certificate_id is not required
    if certificate_id is None:
        if event is not None and "certificate_id" in event:
            certificate_id = event["certificate_id"]

    # msg is not required, return an empty string if its not set
    if msg is None:
        msg = ""

    # We'll need to pass through the value of dryrun in case we end up retrying
    # this function
    if event is not None and "dryrun" in event:
        dryrun = event["dryrun"]
    else:
        dryrun = False

    # If retry_count is not explicitly set, there should always be a retry count
    # value available in event. See the decriment_retry() call.
    if retry_count is None:
        if event is not None and "retry_count" in event:
            retry_count = int(event["retry_count"])

    if event is not None and "retry" in event:
        retry = event["retry"]

    # If retry count is zero, don't retry
    if retry_count == 0:
        retry = False

    # If the status is not error, then we don't need to retry
    if status != "error":
        retry_count = 0
        retry = False

    return {
        "status": status,
        "site": site,
        "certificate_id": certificate_id,
        "msg": msg,
        "retry_count": retry_count,
        "retry": retry,
        "dryrun": dryrun,
        "errorInfo": errorInfo,
    }


def lambda_handler(event, context):
    logging.info(json.dumps(event, indent=2))
    event = decriment_retry(event)
    if "site" in event:
        site = event['site']
    else:
        logging.error("Required key 'site' not found in event.")
        return generate_response(
            event=event,
            status = "error",
            msg = f"Required key 'site' not found in event.",
            retry_count=0,
        )

    # If this is a dry run event, don't actually request a new certificate, just pass on
    # a provided certificate_id
    if "dryrun" in event and event['dryrun'] == "true":
        dryrun = True
        try:
            if "certificate_id" in event:
                certificate_id = int(event['certificate_id'])
            if "raiseException" in event:
                raise Exception("Exception triggered from event")

        except Exception:
            return generate_response(
                event=event,
                status = "error",
                msg = f"Certificate request error for {site}",
                errorInfo=traceback.format_exc(3),
            )

    else:
        dryrun = False

        try:
            req = generateCSR(site)
            csrtext = crypto.dump_certificate_request(crypto.FILETYPE_PEM, req)
            csrtext = csrtext.decode("utf-8")
            certificate_id = requestCertificate(csrtext, site)
            certificate_id = int(certificate_id)

        except Exception:
            return generate_response(
                event=event,
                status = "error",
                msg = f"Certificate request error for {site}",
                errorInfo=traceback.format_exc(3),
            )

    if certificate_id > 0:
        logging.info(f"Certificate requested for {site}: {certificate_id}")
        if not dryrun:
            try:
                storeCertificateIdentifier(site, certificate_id)

            except Exception:
                return generate_response(
                    event=event,
                    status = "error",
                    certificate_id = certificate_id,
                    msg = f"Certificate request error for {site}",
                    errorInfo=traceback.format_exc(3),
                )

    else:
        logging.error(f"Error code from certificate enroll call: {certificate_id}")
        return generate_response(
            event=event,
            status = "error",
            certificate_id = certificate_id,
            msg = f"Certificate request error for {site}",
        )

    return generate_response(
        status="requested",
        certificate_id=certificate_id,
        msg=f"Certificat requested for {site}",
        event=event,
    )
