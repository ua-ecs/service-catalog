import os
import json
import ptvsd
import logging
import xml.dom.minidom
import boto3
from botocore.vendored import requests
from datetime import date, datetime
import decimal

rootlogger = logging.getLogger()
rootlogger.setLevel(logging.INFO)

if os.getenv("AWS_SAM_LOCAL"):
    # Local Only Config
    rootlogger.setLevel(logging.DEBUG)
    if os.getenv("REMOTE_DEBUGGING") == 'true':
        ptvsd.enable_attach(address=('0.0.0.0', 5678), redirect_output=True)
        ptvsd.wait_for_attach()
else:
    # Regular AWS Config
    True

region = 'us-west-2'

dynamodb = boto3.resource('dynamodb', region_name=region)
key_db = dynamodb.Table(os.environ['KEY_TABLE'])
ssm = boto3.client('ssm', region_name=region)


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)

    raise TypeError("Type %s not serializable" % type(obj))


def get_secure_parameter(p_name):
    response = ssm.get_parameter(Name=p_name, WithDecryption=True)
    v = response['Parameter']['Value']
    return v


username = get_secure_parameter("/cloudops/ssl_req/username")
password = get_secure_parameter("/cloudops/ssl_req/password")
url = get_secure_parameter("/cloudops/ssl_req/url")


def retrieveCertificateReq(site):
    response = key_db.get_item(
        Key={
            'site': site.lower()
        }
    )
    logging.debug(json.dumps(response, default=json_serial))
    cert_req = response['Item']
    return cert_req


def parse_status_response(xmlString):
    """
    Parse the resulting XML from our SOAP call. Should look like this:
    <?xml version='1.0' encoding='UTF-8'?>
    <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body>
            <ns2:enrollResponse xmlns:ns2="http://ssl.ws.epki.comodo.com/">
                <return>1273082</return>
            </ns2:enrollResponse>
        </S:Body>
    </S:Envelope>
    """
    DOMTree = xml.dom.minidom.parseString(xmlString)
    envelope = DOMTree.documentElement
    body = envelope.getElementsByTagName("S:Body")[0]
    statusResponse = body.getElementsByTagName("ns2:getCollectStatusResponse")[0]
    returnElement = statusResponse.getElementsByTagName("return")[0]
    status = str(returnElement.childNodes[0].data)
    return status


def retrieveCertificateStatus(site_req):
    certificate_id = site_req['certificate_identifier']
    requestXML = f"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ssl="http://ssl.ws.epki.comodo.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <ssl:getCollectStatus>
         <!--Optional:-->
         <authData>
            <login>{username}</login>
            <password><![CDATA[{password}]]></password>
            <customerLoginUri>InCommon</customerLoginUri>
         </authData>
         <id>{certificate_id}</id>
      </ssl:getCollectStatus>
   </soapenv:Body>
</soapenv:Envelope>
"""
    headers = {'content-type': 'text/xml'}
    response = requests.post(url, data=requestXML, headers=headers)
    logging.debug(requestXML)
    return parse_status_response(response.content.decode("utf-8"))


def lambda_handler(event, context):
    logging.debug(json.dumps(event, indent=2))
    if "site" in event:
        site = event['site']
    else:
        logging.error("Required key 'site' not found in event.")
        return {
            "status": "failed",
            "msg": "Required key 'site' not found in event."
        }

    site_req = retrieveCertificateReq(site)
    cert_request_status = retrieveCertificateStatus(site_req)

    response_map = {
        "1"     : "certificate is ready",
        "0"     : "certificate not ready",
        "-14"   : "unknown error",
        "-16"   : "permission denied",
        "-23"   : "waitinf for approval",
        "-40"   : "invalid ID",
    }

    status_map = {
        "1"     : "ready",
        "0"     : "not ready",
        "-23"   : "not ready",
    }
    status = status_map.get(cert_request_status, "error")
    response_message = response_map.get(cert_request_status, "unknown response status")

    if status == "error":
        error_msg = f"Bad status code response from retrieve certificate SOAP call: {response_message} ({cert_request_status})"
        logging.error(error_msg)
    else:
        msg = f"Status code response from retrieve certificate SOAP call: {response_message} ({cert_request_status})"
        logging.info(msg)

    return {
        "status": status,
        "site": site,
        "msg": response_message,
    }
