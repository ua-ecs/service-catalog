SSL/TLS Certificate Provisioning
================================

This project allows for the programatic request and provisioning of SSL/TLS certificates through the UA subscription to cert-manager.com. It leverages the SOAP API to provision the certificates, and then installes them into the AWS Certificate Manager in the CloudOPS account.

The project consists of two major parts.

First is a set up Lambda functions developed with the SAM CLI framework. These lambda functions do the main work of requesting, checking, and installing into AWS the SSL/TLS certificates.

Second is an AWS step function. This orchestrates the Lambda functions in a way that allows a robust workflow, and eliminates a lot of the loop/wait/check requirements from the Lambda functions themselves.


## Prerequisites

* This project assumes you have an AWS account ready for deployment or testing in. 
* Valid AWS credentials for the target AWS account and region present as environment variables.
* Python 3.7 installed
* AWS CLI installed
* AWS SAM CLI installed
* npm installed


## npm script commands

Even though this project is all python and cloudformation, npm is a commonly installed and has the ability to run local scripts. This makes it a good candidate for discovering what local utility scripts a project has.

    npm run

This will list the available commands.

### setup

    npm run setup

This project uses the OpenSSL python module, which requires platform specific extensions (different for mac, linux, windows). Therefore the setup process must take place on the intended architecture.

Since we're using docker to supply our lambda environment, we can use the same docker container to perform the setup. This will get us to a bash shell inside the container. from there we can cd to /local and run setup.

Because we mapped in the working directory of this projet to /local in the container, when we run the setup script, everything gets installed to the local directory.

See the bin/local-setup.sh and bin/in-container-setup.sh for details.

### clean

Its always useful to be able to clean up all the temporary stuff which doesn't need to be checked into version control. This script should match the contents of the .gitignore file, and remove anything that needs to be present for packaging, but does not need to be stored in version control.

See bin/clean.sh for details.

### cert-request

This script will use the SAM CLI to locally invoke the request_certificate lambda function. It uses the event data stored in event.json for its source. Update this file as needed. The certificate request lambda function supports a dryrun = "true" property, which will prevent the certificate request from actually being issued to InCommon, but does still generate the CSR. This is useful for repeated testing where you don't want to fill up our InCommon portal with needless certificates.

### cert-check

Check the status of a certificate. This issues a SOAP call to InCommon to check if the certificate for a given site is ready to go.

### cert-install

This will retrieve all the certificate bits from InCommon and DynamoDB, and install them into the AWS Certificate Manager.

### package

This calls the SAM CLI package command, which zips everything up and stages it in an S3 bucket. It also generates a final CloudFormation template with hard-coded values to the uploaded package in S3. This template is then deployed with the deploy script.

### deploy-lambda

This calls the AWS CLI cloudformation deploy command to deploy the generated cloudformation template from the package script. Once this runs the lambda functions should be available to run in the AWS account.

### deploy-step

This calls the AWS CLI cloudformation deploy command to deploy the step-function.yaml template. Values for the lambda function ARNs are fetched from the deployed stack from the deploy-lambda script.
