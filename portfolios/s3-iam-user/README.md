# IAM User for S3 Bucket Access

This blueprint creates a single IAM user with Access and Secret Keys. A policy is attached to the user to allow full access to a single S3 bucket and optional path.

Useful for when you need to grant specific access to an already existing S3 bucket.

## Parameters

* Username
* S3 Bucket Path

## Outputs

* BucketName
* AccessKey For New IAM User
* SecretKey For New IAM User


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
