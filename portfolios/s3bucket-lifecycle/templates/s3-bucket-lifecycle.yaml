---
# S3 Bucket Archive CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will deploy an S3 bucket with it's own IAM user.
# This S3 bucket will be transferred from a standard S3 bucket to an S3 bucket
# with Infrequent Access and then be transferred to Glacier.

AWSTemplateFormatVersion: '2010-09-09'
Description: S3 Bucket with Lifecycle

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # Name of the bucket.
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.
  # Indicates the number of days the S3 bucket will stay in standard mode,
  # then it will be moved to Infrequent Access mode.
  DaysToInfrequentAccess:
    Type: Number
    Description: Migrate to Infrequent Access After This Many Days. (Minimum 30 days, use 0 to skip this class.)
    Default: 30
  # Indicates the number of the days the S3 bucket will stay in infrequent access mode,
  # then it will be moved to Glacier.
  DaysToGlacier:
    Type: Number
    Description: Migrate to Glacier After This Many Days. (use 0 to skip this class)
    Default: 60
  # Indicates the number of days the S3 will bucket will stay in Glacier,
  # then it will be completely deleted.
  # It must stay in Glacier at a minimim of 150 days.
  DaysToDeletion:
    Type: Number
    Description: Delete From Glacier After This Many Days. (use 0 to not delete)
    Default: 0
  EnableEncryptionParameter:
    Type: String
    Description: Enable S3 Bucket Encryption
    Default: "Yes"
    AllowedValues: ["Yes", "No"]
  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  TagService:
    Description: Service name (from the service catalog) that is utilizing this resource
    Type: String
  TagEnvironment:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'. 
    Type: String
  TagContactNetId:
    Description: NetID of the person to contact for information about this resource
    Type: String
  TagAccountNumber:
    Description: Financial system account number for the service utilizing this resource
    Type: String
  TagSubAccount:
    Description: Financial system subaccount number for the service utilizing this resource
    Type: String
  TagTicketNumber:
    Description: Ticket number that this resource is for
    Type: String

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
      - EnableEncryptionParameter
      - DaysToInfrequentAccess
      - DaysToGlacier
      - DaysToDeletion
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagSubAccount
      - TagEnvironment
      - TagAccountNumber
      - TagCreatedBy
      - TagContactNetId
      - TagTicketNumber
    ParameterLabels:
      BucketName:
        default: 'Bucket Name:'
      DaysToInfrequentAccess:
        default: 'Days to Infq Access:'
      DaysToGlacier:
        default: 'Days to Glacier:'
      DaysToDeletion:
        default: 'Days to Deletion:'
      EnableEncryptionParameter:
        default: 'Enable Encryption?'

# Conditions
# ----------
Conditions:
  # Enable Infrequent Access Storage Class if its not set to Zero.
  UseInfrequentAccess: !Not [!Equals [!Ref DaysToInfrequentAccess, 0]]
  # Use Glacier Storage Class if its not set to Zero.
  UseGlacierAccess: !Not [!Equals [!Ref DaysToGlacier, 0]]
  # Enable Object Deletion if its not set to Zero.
  EnableDeletion: !Not [!Equals [!Ref DaysToDeletion, 0]]
  # Enable Encryption or not
  EnableEncryption: !Equals [!Ref EnableEncryptionParameter, 'Yes']

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### S3 Bucket
  #
  # This deploys the S3 bucket with some tags.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref BucketName
      AccessControl: Private
      BucketEncryption:
        Fn::If:
        - EnableEncryption
        - ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault: 
              SSEAlgorithm: "AES256"
        - !Ref AWS::NoValue
      LifecycleConfiguration:
        Rules:
        - Id: Archive
          Status: Enabled
          # We can only specify ExpirationInDays _or_ AbortIncompleteMultipartUpload
          ExpirationInDays: 
            Fn::If:
            - EnableDeletion
            - !Ref DaysToDeletion
            - !Ref AWS::NoValue
          AbortIncompleteMultipartUpload:
            Fn::If:
            - EnableDeletion
            - !Ref AWS::NoValue
            - DaysAfterInitiation: 7
          Transitions:
          # Only add Infrequent Access transition if set
          - Fn::If:
            - UseInfrequentAccess
            - TransitionInDays: !Ref DaysToInfrequentAccess
              StorageClass: STANDARD_IA
            - !Ref AWS::NoValue
          # Only add Glacier transition if set
          - Fn::If:
            - UseGlacierAccess
            - TransitionInDays: !Ref DaysToGlacier
              StorageClass: GLACIER
            - !Ref AWS::NoValue
      Tags:
      - Key: Name
        Value: !Sub "${BucketName}-s3"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetId
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: subaccount
        Value: !Ref TagSubAccount
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### S3 Bucket User
  #
  # Creates an IAM user that can only connect to the S3 bucket specified.     
  S3BucketUser:
    Type: AWS::IAM::User
    Properties:
      Path: "/"
      Policies:
      - PolicyName: bucketaccess
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - s3:ListAllMyBuckets
            Resource:
            - "*"
          - Effect: Allow
            Action:
            - s3:*
            Resource: 
            - !Sub "arn:aws:s3:::${S3Bucket}"
            - !Sub "arn:aws:s3:::${S3Bucket}/*"

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Value: !Ref S3Bucket

