# Archive S3 Lifecycle Bucket

This blueprint creates an S3 Bucket and an IAM account that has full access to it. 
The S3 bucket has lifecycle policies attached to it which will move data down to
cheaper but less accessible storage over time, and optionally eventually deleted.

## Template Features

### Encryption

The contents of this S3 bucket can be automatically encrypted on upload using the 
default S3 AES256 method.

Available Values: Yes, No
Default: Yes

## Storage Classes

The AWS Storage Class of objects can be changed to various values upon object age.

### Infrequent Access

Objects can be transitioned to S3 Infrequent Access only after 30 days. In addition
objects must remain in Infrequent Access for at least 30 days before they can be 
deleted or transitioned to glacier.

Allowed Values: 0 to not use Infrequent Access, or a value of 30 or greater.
Default: 30

### Glacier

Objects can be transitioned to S3 Glacier after either 7 days of initial creation, or
after 30 days in Infrequent Access. So if you transition an object to IA after 30 days,
you can transition to Glacier after 30 _more_ days, for 60 total days after creation.

Object must stay in the S3 Glacier storage class for at least 1 day before they can 
be deleted.

Allowed Values: 0 to not use Glacier, otherwise a value greater than IA Access transition + 30.
Default: 60

## Delete Objects

Objects can be automatically deleted after some time. Objects can be deleted after 1 day
if the objects don't lifecycle down to either IA or Glacier storage classes. Otherwise
they must remain in the appropriate storage class the required amount of time. If you 
transition to IA and then to Glacier, the minimum values for when you can delete an 
object would be 30 + 30 + 1 = 61 days.

Allowed Values: 0 to never delete objects, otherwise a value greater than the required
time in previous storage class.

Default: 0 (don't delete objects)

## Examples

### Delete objects after 10 days

Days to Infq Access: 0
Days to Glacier: 0
Days to Deletion: 10

### Transition to Glacier in a week, then Delete after another week

Days to Infq Access: 0
Days to Glacier: 7
Days to Deletion: 14

### Transition to IA, then Glacier, don't ever Delete

Days to Infq Access: 30
Days to Glacier: 60
Days to Deletion: 0


## IAM User

An IAM user is created with full control of this bucket.

IAM User Access Keys are not automatically created, as they cannot be secured from
a CloudFormation template. Access Keys can be created manually in the IAM console after 
the stack is deployed successfully. 

## Parameters

* BucketName
* DaysToInfrequentAccess
* DaysToGlacier
* DaysToDeletion
* EnableEncryptionParameter
* TagService
* TagEnvironment
* TagCreatedBy
* TagContactNetId
* TagAccountNumber
* TagSubAccount
* TagTicketNumber


## Outputs

* BucketName

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
