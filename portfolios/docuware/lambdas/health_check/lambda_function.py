import json
import requests
import boto3
import os
from botocore.exceptions import ClientError

SENDER = "docuware_admin@list.arizona.edu"
RECIPIENTS = ["docuware_admin@list.arizona.edu "]
AWS_REGION = os.getenv('AWS_REGION', 'us-west-2')  # Default to 'us-west-2' if not set
CHARSET = "UTF-8"  # for email

# slack channel: docuware-tech-team
# SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

# slack channel: testing
SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

# URL's to test
URL = "https://docuware.arizona.edu"
# URL = "https://dwtest.docuware-aws.arizona.edu/"
TIMEOUT_SECONDS = 5  # for the response check on the URL

# Email defaults
EMAIL_SUBJECT = "Docuware PROD URL is not reachable"
EMAIL_BODY_HTML = "The Docuware production URL is unresponsive, please check if the DW system is up and running!"
EMAIL_BODY_TEXT = "The Docuware production URL is unresponsive, please check if the DW system is up and running!"

# Initialize clients
ses = boto3.client('ses', region_name=AWS_REGION)


def slack_notification(message):
    # Prepare the payload
    payload = {"text": message}
    data = json.dumps(payload).encode("utf-8")
    req = requests.Request(
        method="POST", url=SLACK_WEBHOOK_URL, data=data, headers={"Content-Type": "application/json"}
    )
    try:
        response = requests.send(req)
        response.raise_for_status()  # Raise exception for non-2xx status codes
        return {"statusCode": 200, "body": json.dumps("Message sent to Slack successfully!")}
    except requests.exceptions.RequestException as e:
        print(f"Error sending Slack notification: {str(e)}")
        return {
            "statusCode": 500,
            "body": json.dumps(f"Error sending Slack notification: {str(e)}"),
        }


def send_email(email_body_html, email_body_text, subject):
    for recipient in RECIPIENTS:
        try:
            response = ses.send_email(
                Destination={'ToAddresses': [recipient]},
                Message={
                    'Body': {
                        'Html': {'Charset': CHARSET, 'Data': email_body_html},
                        'Text': {'Charset': CHARSET, 'Data': email_body_text},
                    },
                    'Subject': {'Charset': CHARSET, 'Data': subject},
                },
                Source=SENDER,
            )
            print(f"Email sent! Meassage ID: {response['MessageId']}")
        except ClientError as e:
            print(e.response['Error']['Message'])


def lambda_handler(event, context):
    try:
        response = requests.get(URL, timeout=TIMEOUT_SECONDS)
        response.raise_for_status()  # Raise exception for non-2xx status codes
        print("Web application is healthy.")
    except requests.exceptions.RequestException as e:
        print(f"Web application is down. Error: {str(e)}")
        # slack_notification("Docuware PROD IS DOWN - YES WE CAN PANIC NOW")
        # send_email(
        #     EMAIL_BODY_HTML, EMAIL_BODY_TEXT, EMAIL_SUBJECT
        # )  # Send notifications on error
    return {
        "statusCode": 200 if response.status_code == 200 else 500,
        "body": f"URL {URL} is {(response.status_code if response else 'not')} healthy.",
    }


# test locally
if __name__ == "__main__":
    lambda_handler(None, None)
