---
# Docuware EBS automated snapshots
# -----------------------------------------
# Docuware EBS snapshot/backup handled by  Amazon Data Lifecycle Manager
# to automate the creation, retention, and deletion of snapshots

AWSTemplateFormatVersion: 2010-09-09
Description: Docuware EBS snapshots

# ### Parameters
# No real parameters needed at this time, only tags.
Parameters:

  # Tagging information
  TagService:
    Type: String
    Description: Exact name of the Service as defined in the service catalog.
    Default: Docuware EBS backup
  TagEnvironment:
    Type: String
    Description: Used to distinguish between development, test, production,etc. environment types.
    AllowedValues: [dev, tst, prd]
    Default: prd
  TagContactNetid:
    Type: String
    Description: Used to identify the netid of the person most familiar with the usage of the resource.
    Default: mhirst
  TagAccountNumber:
    Type: String
    Description: Identifies the financial system account number.
    Default: '1192623'
  TagSubAccount:
    Type: String
    Description: Identifies the financial system's sub account, which contains a name that identifies a collection of services.
    Default: 11AWS
  TagName:
    Type: String
    Description: Used by Amazon to display a default name in the AWS console
    Default: Docuware EBS automated snapshots
  TagCreatedBy:
    Type: String
    Description: Who created this resource
  TagTicketNumber:
    Type: String
    Description: Jira ticket number that references this resource
    Default: DW-258

#Metadata:
Resources:
  dlmRole:
    Type: AWS::IAM::Role
    Properties:
      Path: /service-role/dlm/
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
        -
          Effect: "Allow"
          Action:
          - sts:AssumeRole
          Principal:
            Service:
            - dlm.amazonaws.com
      Policies:
      - PolicyName: "dlmPolicy"
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - ec2:CreateSnapshot
            - ec2:CreateSnapshots
            - ec2:DeleteSnapshot
            - ec2:DescribeVolumes
            - ec2:DescribeInstances
            - ec2:DescribeSnapshots
            Resource: "*"
          - Effect: Allow
            Action:
            - ec2:CreateTags
            Resource: arn:aws:ec2:*::snapshot/*

  EBSRootLifecyclePolicy:
    Type: "AWS::DLM::LifecyclePolicy"
    Properties:
      Description: "DW EBS Lifecycle Policy for Root drive"
      State: "ENABLED"
      ExecutionRoleArn: !GetAtt dlmRole.Arn
      PolicyDetails:
        ResourceTypes:
          - "VOLUME"
        TargetTags:
          -
            Key: "Name"
            Value: "DWPRD Root"
        Schedules:
          -
            Name: "DW EBS Root"
            TagsToAdd:
              -
                Key: "type"
                Value: "DailySnapshot"
            CreateRule:
              Interval: 24
              IntervalUnit: "HOURS"
              # UTC The time at which the policy runs are scheduled to start. 
              # The first policy run starts within an hour after the scheduled time.
              Times:
                - "09:00"
            RetainRule:
              Count: 1
            CopyTags: true

  EBSDatabaseLifecyclePolicy:
    Type: "AWS::DLM::LifecyclePolicy"
    Properties:
      Description: "Database EBS Lifecycle Policy"
      State: "ENABLED"
      ExecutionRoleArn: !GetAtt dlmRole.Arn
      PolicyDetails:
        ResourceTypes:
          - "VOLUME"
        TargetTags:
          -
            Key: "Name"
            Value: "DWPRD SQL"
        Schedules:
          -
            Name: "Database EBS Daily Snapshots"
            TagsToAdd:
              -
                Key: "type"
                Value: "DailySnapshot"
            CreateRule:
              Interval: 24
              IntervalUnit: "HOURS"
              # UTC The time at which the policy runs are scheduled to start. 
              # The first policy run starts within an hour after the scheduled time.
              Times:
                - "09:00"
            RetainRule:
              Count: 1
            CopyTags: true

  FileServerLifecyclePolicy:
    Type: "AWS::DLM::LifecyclePolicy"
    Properties:
      Description: "File server EBS Lifecycle Policy"
      State: "ENABLED"
      ExecutionRoleArn: !GetAtt dlmRole.Arn
      PolicyDetails:
        ResourceTypes:
          - "VOLUME"
        TargetTags:
          -
            Key: "Name"
            Value: "DWPRD File Store"
        Schedules:
          -
            Name: "File Server EBS Daily Snapshots"
            TagsToAdd:
              -
                Key: "type"
                Value: "DailySnapshot"
            CreateRule:
              Interval: 24
              IntervalUnit: "HOURS"
              # UTC The time at which the policy runs are scheduled to start. 
              # The first policy run starts within an hour after the scheduled time.
              Times:
                - "09:00"
            RetainRule:
              Count: 1
            CopyTags: true

  
