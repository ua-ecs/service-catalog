---
# CloudFormation template for UACBT (Computer-Based Training) authentication server (1 of 2)
# -----------------------------------------
#
# - Deploys an S3 bucket used to store code, software & configuration files
#
AWSTemplateFormatVersion: 2010-09-09
Description: UACBT (S3)

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
#

Parameters:
  
  # BucketNamePrefix is the prefix of the name of the S3 bucket
  # - ua-uits-general-nonprod: edu-arizona-uits-general-nonprod-uacbt
  # - ua-erp: edu-arizona-uits-general-prod-uacbt
  BucketNamePrefix:
    Description: Prefix of the name of the S3 bucket (environment type will be appended)
    Type: String
    Default: edu-arizona-uits-general-prod-uacbt
  # Tags
  TagService:
    Description: Name of the service associated with this resource (as listed in the service catalog)
    Type: String
    Default: UACBT
  TagEnvironment:
    Description: Environment type of this resource (dev, tst, rpt, trn, prd)
    Type: String
    Default: prd
    AllowedValues:
      - dev
      - tst
      - rpt
      - trn
      - prd
  TagContactNetID:
    Description: NetID of the primary technical resource
    Type: String
    Default: dbaty
  TagTicketNumber:
    Description: Ticket number for the CLOUD Jira project
    Type: String
    Default: CLOUD-219
  TagAccountNumber:
    Description: Account number associated with the service
    Type: String
    Default: 1192661 # Campus Software
  TagSubAccount:
    Description: Sub account associated with the service
    Type: String
    Default: Software Licensing Services


# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Settings
      Parameters:
      - BucketNamePrefix
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagEnvironment
      - TagContactNetID
      - TagTicketNumber
      - TagAccountNumber
      - TagSubAccount
    ParameterLabels: {}

# Resources
# ---------
#

Resources:

  # #### S3
  #
  # S3 Bucket
  #

  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub ${BucketNamePrefix}-${TagEnvironment}
      AccessControl: Private
      Tags:
      - Key: Name
        Value: !Sub ${BucketNamePrefix}-${TagEnvironment}
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: ticketnumber
        Value: !Ref TagTicketNumber
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: service
        Value: !Ref TagService
      - Key: subaccount
        Value: !Ref TagSubAccount

# Outputs
# -------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  
  BucketName:
    Description: Name of the S3 bucket
    Value: !Sub ${BucketNamePrefix}-${TagEnvironment}
    Export:
      Name: !Sub "${AWS::StackName}-BucketName"