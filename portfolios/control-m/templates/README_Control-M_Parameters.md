UA Control-M Linux Instance
===========================

This template provides a basic EC2 instance running Linux (AWS Amazon Linux2) with custom components added for Control-M support.


### AWS Account and Network Parameters:
* All of the Account and Network parameters have been automated using Lambda resources, including the ALB public subnets.

### Host specific Parameters:
* ServerName: (read-write) Free-text. Final name should be checked against bluecat to ensure no name collisions in AD before the instance is deployed. Total text, including prefix, should be limited to 15 alphanumeric characters, including dash (-) and underscore (\_).
    * Label Text: Server Name
    * Description: Server instance name, will be prefixed with department code (eg 'myserver' becomes 'UITS-myserver'), with a maximum combined length of 15 characters (a-z, 0-9, dash, and underscore).
    * RegEx: **(^((?![-\_])(?=.\*[a-z].\*)[a-z0-9-\_]{1,15}(?<![-\_]))$)**
* OSType: Auto-selected by CFT at deployment time to pick the newest available AMI for Amazon Linux 2. The parameter and the default value for it are necessary for the auto-selection to work.
* InstanceType: (drop-down select) Permitted instance types and sizes, currently [t3.medium, t3.large, m5.large, m5.xlarge]
    * Label Text: Instance Type
    * Description: Instance size to be deployed. See additional information link for details on specific instance type and size features and costs.
* RootVolumeSize: (read-write) Numeric GB of root volume disk size between 20 and 2048. Default is 60 per Control-M recommendations.
    * Label Text: Root Volume Size
    * Description: Size of the root (OS) volume in GB. Permitted range is 20GB to 2048GB.
* FirstOptionalDataVolumeDesired: (drop-down: true or false) Whether to add an optional first data disk. CFT input is "true" or "false" with the default of "false".
    * Label Text: First Optional Data Volume Desired
    * Description: Select if an optional first data volume is desired (true/false)
* FirstOptionalDataVolumeSize: (read-write) Numeric GB of first optional data disk size between 10 and 2048. Default is 10. Unused by CFT if DataDisk1 is false.
    * Label Text: First Optional Data Volume Size
    * Description: Size of the first optional data volume in GB. Permitted range is 10GB to 2048GB.
* SecondOptionalDataVolumeDesired: (drop-down: true or false) Whether to add a second optional data disk. CFT input is "true" or "false" with the default of "false".
    * Label Text: Second Optional Data Volume Desired
    * Description: Select if an optional second data volume is desired (true/false)
* SecondOptionalDataVolumeSize: (read-write) Numeric GB of second optional data disk size between 10 and 2048. Default is 10. Unused by CFT if DataDisk2 is false.
    * Label Text: Second Optional Data Volume Size
    * Description: Size of the second optional data volume in GB. Permitted range is 10GB to 2048GB.
* KeyName: EC2 key pair name of a key already uploaded into the AWS account. Intended for Operations access to instances for assistance or emergency access in case of authentication failure.
    * NonProd: **control-m-shared-keypair**
    * Prod: **control-m-shared-keypair**
* GroupName: LDAP group name of the owning group. Used for setting permissions inside the instance once joined to AD.

### Tagging and Billing Parameters:
* TagService: Name of the service. Default is "Control-M EnterpriseManager/Server".
* TagEnvironment: Select from "tst" or "prd". Default is "tst".
* TagContactNetID: NetID (user ID) of the user that deployed the instance.
* TagAccountNumber: Billing KFS account number.
* TagTicketNumber: Ticket number tracking deployment of this instance.
* TagBackupSLA: (drop-down list) Allow specifying the Backup SLA to be used for this server instance.
    * Label Text: Instance Backup SLA
    * Description: Select Backup SLA from the following  
         **UITS Standard** - Backup policy includes daily backups retained for one month and monthly backups retained for one year  
         **None** - No backups performed
    * Allowed Values: "UITS Standard" and "None", default is "UITS Standard"

### NOTES:
* CFT currently permits connections to TCP port 22 (ssh) via the instance security group from a limited set of campus subnets.

### Deployment Procedure:

Deploy the resources in the following order:
1. Ensure that the AWS account in question has the following SSM Parameters defined:
    * /oracle/oem/agent-registration-pw
    * /agents/sophos/linux_url
    * /agents/omsagent/workspaceid
    * /agents/omsagent/primarykey
    * /ad_join/user
    * /ad_join/password
    * /ad_join/domain
    * As well as an EC2 key pair called **controlm-keypair**
1. Deploy the `control-m-foundation.yaml`, to establish the shared security groups, and deploy the S3 bucket, and EFS share for the environment.
1. Deploy the `control-m-rds.yaml` to build the database for the environment.
1. Populate the S3 bucket with the shell scripts used to provision the instance in `s3://<bucket>/scripts/`, and the ssh keys in `s3://<bucket>/pubkeys/`.
1. Deploy the `control-m-server.yaml` to build the CTM/EM Server instances.
1. Populate the installation media folders in `s3://<bucket>/Control-M_9019X00/`:
        Control-M_9019100/
                AFT82300/
                        DRAFT.8.2.00_Linux-x86_64.tar.z
                        PAAFT.8.2.00.300_Linux-x86_64_INSTALL.BIN
                Agent/
                        DRKAI.9.0.19.100.iso
                        DRKAI.9.0.19.100_Linux-x86_64.z
                BIM/
                        DRCBM_BIM_9019100_Linux-x86_64.tar.z
                Client/
                        DRNFT.9.0.19.100_windows_x86_64.zip
                EM/
                        DROST.9.0.19.100_Linux-x86_64.iso
                Forecast/
                        DRFOR_9019100_Linux-x86_64.tar.z
                PEOPLESOFT/
                        DRPMC.9.0.00_Linux-x86_64.z
                SelfService/
                        DRCAG_SLS_9019100_Linux-x86_64.tar.z
                Server/
                        DRCTV.9.0.19.100_Linux-x86_64.z
                        PACTV.9.0.19.101.tar.gz
                WCM/
                        DRWCM_9019100_Linux-x86_64.tar.z
        Control-M_9019200/
                Client/
                        DRNFT.9.0.19.200_windows_x86_64.exe
                EM/
                        DROST.9.0.19.200_Linux-x86_64.iso

1. Create and add the tomcat certificate in `s3://<bucket>/certificates/`. The certificate must be in p12 format and password protected. See the instructions in the Control-M SSL Guide.
    * ```
      openssl pkcs12 -in <certificate pem file name> \
          -inkey <private key file name> \
          -export -passout pass:<keystore password> \
          -passin pass:<private key password> \
          -CAfile <certificate chain pem file name> \
          -chain -out tomcat.p12 \
          -name <keystore friendly name> \
          -caname <ca friendly name>
      ```
    * eg:
      ```
      openssl pkcs12 -in __uits-prod-aws_arizona_edu_cert.cer \
          -inkey __uits-prod-aws_arizona_edu.key \
          -export -passout pass:<keystore password> \
          -CAfile __uits-prod-aws_arizona_edu_interm.cer \
          -chain -out tomcat.p12 \
          -name uits-prod-aws
          -caname incommon
      ```
1. Populate the CTM Agent/AFT/PS8 silent-install XML templates in `/controlm/efs/cm-templates/`.
1. Deploy the `control-m-agent.yaml` to build the local CTM Agent instances.

### Standalone Agent Deployment Procedure:

Deploy the resources in the following order:
1. Ensure that the AWS account in question has the following SSM Parameters defined:
    * /agents/sophos/linux_url
    * /agents/omsagent/workspaceid
    * /agents/omsagent/primarykey
    * /ad_join/user
    * /ad_join/password
    * /ad_join/domain
    * As well as an EC2 key pair called **controlm-keypair**
1. Deploy the `control-m-foundation.yaml`, to establish the shared security groups, and deploy the S3 bucket, and EFS share for the environment.
1. Sync the installation media from one of the production agents to the new S3 bucket with:

        . /etc/controlm.files
        aws s3 sync --acl bucket-owner-full-control s3://$S3BUCKET/ \
          s3://<target-bucket>/

1. Populate the S3 bucket with the shell scripts used to provision the instance in `s3://<bucket>/scripts/`, and the ssh keys in `s3://<bucket>/pubkeys/`.
1. Populate the CTM Agent/AFT/PS8 silent-install XML templates in `/controlm/efs/cm-templates/`.
1. Deploy the `control-m-agent.yaml` to build the local CTM Agent instances.
