---
# Control-M Foundation CloudFormation Deployment
# -----------------------------------------
#
# This CloudFormation template will deploy pieces set up for Control-M

AWSTemplateFormatVersion: '2010-09-09'
Description: Control-M Foundation (S3, EFS, SecurityGroup)

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
    - Label:
        default: EFS Export Configuration
      Parameters:
      - EFSName
    - Label:
        default: Tagging and Cost Management
      Parameters:
      - TagService
      - TagEnvironment
      - TagContactNetID
      - TagAccountNumber
      - TagTicketNumber
    ParameterLabels:
      BucketName:
        default: 'Bucket Name:'
      TagEnvironment:
        default: 'Environment Type:'
      TagTicketNumber:
        default: 'Ticket Number:'
      TagContactNetID:
        default: 'Contact NetID:'

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # #### Object names
  #
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.
  EFSName:
    Type: String
    Description: Name of the EFS Volume

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  TagService:
    Type: String
    Description: Exact name of the Service as defined in the service catalog.
    Default: Control-M EnterpriseManager/Server
  TagEnvironment:
    Type: String
    Description: >-
      Used to distinguish between development, test, production,etc. environment
      types.
    AllowedValues: [dev, tst, prd]
    Default: tst
  TagContactNetID:
    Type: String
    Description: >-
      Used to identify the netid of the person most familiar with the usage of
      the resource.
  TagAccountNumber:
    Type: String
    Description: Identifies the financial system account number.
  TagTicketNumber:
    Type: String
    Description: >-
      Used to identify the Jira, Cherwell, or other ticketing system ticket
      number to link to more information about the need for the resource.

# Conditions
# ----------
#
Conditions:
  NotMasterProd:
    !Not [!Equals ["760232551367", !Ref AWS::AccountId]]

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### AWS Account Information
  #
  # Lambda function to introspect VPCs, subnets, and select most available
  AccountInfo:
    Type: Custom::AccountInfo
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:fdn-cf-account-info"
      VPCInfo:
      - vpcid
      - private-subnet-a
      - private-subnet-b

  # #### S3 Bucket
  #
  # This deploys the S3 bucket with some tags. The bucket has versioning enabled
  # because this is where backups will go for now.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref "BucketName"
      AccessControl: Private
      VersioningConfiguration:
        Status: Enabled
      Tags:
      - Key: Name
        Value: !Ref BucketName
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### S3 BucketPolicy
  #
  # Permits remote access to bucket from indicated accounts
  RemoteBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Condition: NotMasterProd
    Properties:
      Bucket: !Ref S3Bucket
      PolicyDocument:
        Statement:
          - Sid: AllowRemoteAccountAccess
            Action:
              - "s3:GetBucketLocation"
              - "s3:ListBucket"
              - "s3:GetObject"
              - "s3:PutObject"
              - "s3:DeleteObject"
              - "s3:ListBucketVersions"
            Effect: "Allow"
            Principal:
              AWS: "arn:aws:iam::760232551367:root"
            Resource:
              - !Sub "${S3Bucket.Arn}"
              - !Sub "${S3Bucket.Arn}/*"
          - Sid: RequireFullAccessPut
            Action:
              - "s3:PutObject"
            Effect: "Deny"
            Principal:
              AWS: "arn:aws:iam::760232551367:root"
            Resource:
              - !Sub "${S3Bucket.Arn}/*"
            Condition:
              StringNotEquals:
                s3:x-amz-acl: bucket-owner-full-control

  # #### EFS Shared File System
  #
  # Create an EFS entity to be used as a shared filesystem for the application
  # instances.
  FileSystem:
    Type: AWS::EFS::FileSystem
    Properties:
      FileSystemTags:
      - Key: Name
        Value: !Ref EFSName
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### EFS Mount Points
  #
  # EFS Mountpoints must be created for each Availability Zone in the VPC. This is
  # also where you define access controls, as access to EFS is controlled by
  # these security groups.
  #
  # A pair of mount points must be created for each EFS volume.
  EFSMountTargetZoneA:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId: !Ref FileSystem
      SubnetId: !GetAtt AccountInfo.private-subnet-a
      SecurityGroups:
        - Ref: EFSSecurityGroup
  EFSMountTargetZoneB:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId: !Ref FileSystem
      SubnetId: !GetAtt AccountInfo.private-subnet-b
      SecurityGroups:
        - Ref: EFSSecurityGroup

  # #### EFS Security Group
  #
  # This security group defines what resources are able to access the EFS shared filesystem.
  EFSSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow EFS Ports to EFS Volume
      VpcId: !GetAtt AccountInfo.vpcid
      Tags:
      - Key: Name
        Value: !Sub "${EFSName}-efs-sg"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### SSH Security Group
  #
  # Allow inbound traffic on port 22 from Various IP Ranges
  SshSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow SSH from specific origins
      VpcId: !GetAtt AccountInfo.vpcid
      SecurityGroupIngress:
      -
        IpProtocol: "tcp"
        FromPort: 22
        ToPort: 22
        CidrIp: "128.196.130.211/32" # ben.uits
        Description: "ben.uits bastion"
      -
        IpProtocol: "tcp"
        FromPort: 22
        ToPort: 22
        CidrIp: "10.222.82.0/25"
        Description: "AWS bastion"
      -
        IpProtocol: "tcp"
        FromPort: 22
        ToPort: 22
        CidrIp: "150.135.112.0/24" # Infradev VPN
        Description: "Infradev VPN"
      -
        IpProtocol: "tcp"
        FromPort: 22
        ToPort: 22
        CidrIp: "10.138.2.0/17"
        Description: "Mosaic VPN"
      -
        IpProtocol: "tcp"
        FromPort: 22
        ToPort: 22
        CidrIp: "128.196.130.0/24"
        Description: "Control-M on-prem"
      #Allow all outbound traffic
      SecurityGroupEgress:
      -
        IpProtocol: "-1"
        CidrIp: "0.0.0.0/0"
      Tags:
      - Key: "Name"
        Value: !Sub "${AWS::StackName}-ssh-sg"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### Control-M Agent Security Group
  #
  # Allow inbound traffic on Control-M Agent ports from Various IP Ranges
  CMAgentSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow Control-M Agent ports from specific origins
      VpcId: !GetAtt AccountInfo.vpcid
      SecurityGroupIngress:
      -
        IpProtocol: "tcp"
        FromPort: 7017
        ToPort: 7017
        SourceSecurityGroupId: !Ref CMServerSecurityGroup
        Description: "Control-M Servers"
      #Allow all outbound traffic
      SecurityGroupEgress:
      -
        IpProtocol: "-1"
        CidrIp: "0.0.0.0/0"
      Tags:
      - Key: "Name"
        Value: !Sub "${AWS::StackName}-cmagent-sg"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### Control-M Enterprise Manager Security Group
  #
  # Allow inbound traffic on Control-M EM ports from Various IP Ranges
  CMEMSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow Control-M Enterprise Manager ports from specific origins
      VpcId: !GetAtt AccountInfo.vpcid
      #Ingress ports will be defined in the instance CFT
      #Allow all outbound traffic
      SecurityGroupEgress:
      -
        IpProtocol: "-1"
        CidrIp: "0.0.0.0/0"
      Tags:
      - Key: "Name"
        Value: !Sub "${AWS::StackName}-cmem-sg"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### Control-M Server Security Group
  #
  # Allow inbound traffic on Control-M Server ports from Various IP Ranges
  CMServerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow Control-M Server ports from specific origins
      VpcId: !GetAtt AccountInfo.vpcid
      SecurityGroupIngress:
      -
        IpProtocol: "tcp"
        FromPort: 2368
        ToPort: 2370
        CidrIp: "0.0.0.0/0"
        Description: "Routable campus"
      -
        IpProtocol: "tcp"
        FromPort: 7016
        ToPort: 7017
        CidrIp: "0.0.0.0/0"
        Description: "Routable campus"
      -
        IpProtocol: "tcp"
        FromPort: 17016
        ToPort: 17016
        CidrIp: "0.0.0.0/0"
        Description: "Routable campus"
      #Allow all outbound traffic
      SecurityGroupEgress:
      -
        IpProtocol: "-1"
        CidrIp: "0.0.0.0/0"
      Tags:
      - Key: "Name"
        Value: !Sub "${AWS::StackName}-cmserver-sg"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetID
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Description: "S3 bucket"
    Value: !Ref S3Bucket
    Export:
      Name: !Sub "${AWS::StackName}-bucket"

  FileSystem:
    Description: "EFS export"
    Value: !Ref FileSystem
    Export:
      Name: !Sub "${AWS::StackName}-fs-id"

  EFSSecurityGroup:
    Description: "EFS Security Group"
    Value: !Ref EFSSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-efs-sg"

  SshSecurityGroup:
    Description: "SSH Security Group"
    Value: !Ref SshSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-ssh-sg"

  CMAgentSecurityGroup:
    Description: "Control-M Agent Security Group"
    Value: !Ref CMAgentSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-cmagent-sg"

  CMEMSecurityGroup:
    Description: "Control-M Enterprise Manager Security Group"
    Value: !Ref CMEMSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-cmem-sg"

  CMServerSecurityGroup:
    Description: "Control-M Server Security Group"
    Value: !Ref CMServerSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-cmserver-sg"
