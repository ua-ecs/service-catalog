#!/bin/bash

# Run this to sync deployment assets from the primary Prod Control-M agent
# deployment to the other buckets. Must supply one argument, either the target
# bucket name or "all" meaning resync assets to all the known remote buckets.

. /etc/controlm.files

declare -A CMBUCKETS
CMBUCKETS=( [507937877461a]='edu-arizona-dr-aws-kuali-dr-control-m-tst'
            [397167497055]='edu-arizona-ua-uits-kuali-nonprod-control-m-tst'
            [740525297805]='edu-arizona-kuali-prod-aws-control-m-prd'
            [415418166582]='edu-arizona-ps-nonprod-control-m-tst'
            [242275737326]='edu-arizona-ps-prod-control-m-prd'
            [507937877461b]='edu-arizona-dr-aws-ps-dr-control-m-tst'
            [722748364533a]='edu-arizona-uits-general-nonprod-control-m-dev'
            [722748364533b]='edu-arizona-uits-general-nonprod-control-m-tst'
)

if [ $# -ne 1 ]; then
  echo ""
  echo "  Please provide 1 argument when calling $0."
  echo "  Either provide the bare S3 bucket name or the switch '-all' to indicate"
  echo "  a resync of all the known remote buckets. Use -list to list all known buckets."
  echo ""
  exit 1
fi

if [[ $1 =~ ^-(l|list)$ ]]; then
  echo "List of known S3 buckets for non-primary Control-M deployments:"
  for key in "${!CMBUCKETS[@]}"; do
    echo "  $key: ${CMBUCKETS[$key]}"
  done
  exit 0
fi

if [[ $1 =~ ^-(a|all)$ ]]; then
  echo -ne "####################\nSyncing to all known remote S3 buckets\n####################\n"
  for key in "${!CMBUCKETS[@]}"; do
    echo "  $key: ${CMBUCKETS[$key]}"
    aws s3 ls s3://${CMBUCKETS[$key]} >/dev/null
    if [ $? -eq 0 ]; then
      for PATH in Control-M_9019100 Control-M_9019200 Log4J-remediation certificates; do
        #echo "aws s3 sync --acl bucket-owner-full-control s3://${S3BUCKET}/$PATH s3://${CMBUCKETS[$key]}/$PATH"
        aws s3 sync --acl bucket-owner-full-control s3://${S3BUCKET}/$PATH s3://${CMBUCKETS[$key]}/$PATH
      done
    else
      echo -ne "####################\n#  Error reaching ${CMBUCKETS[$key]}\n####################\n"
    fi
  done
  exit 0
fi

if [[ $1 =~ ^[a-z0-9-]+$ ]]; then
  echo -ne "####################\nSyncing to remote S3 bucket $1\n####################\n"
  aws s3 ls s3://$1 >/dev/null
  if [ $? -eq 0 ]; then
    for PATH in Control-M_9019100 Control-M_9019200 Log4J-remediation certificates; do
      #echo "aws s3 sync --acl bucket-owner-full-control s3://${S3BUCKET}/$PATH s3://$1/$PATH"
      aws s3 sync --acl bucket-owner-full-control s3://${S3BUCKET}/$PATH s3://$1/$PATH
    done
  else
    echo -ne "####################\n#  Error reaching $1\n####################\n"
  fi
  exit 0
fi
