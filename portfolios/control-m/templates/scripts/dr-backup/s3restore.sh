#!/bin/bash

# Trace mode on
#set -x

# Import environment variables
. /etc/controlm.files

SOURCE=$1
DEST=$2
LOCK=$3

# Add '--exclude * --include folder1/* --include folder2/* --delete' to OPTIONS
#OPTIONS='--delete --dryrun'
#OPTIONS='--exclude * --include prd/* --delete'
OPTIONS='--delete'

# Assume SNS_NOTIFICATION_TOPIC exists in the environment
TOPIC=$SNS_NOTIFICATION_TOPIC
LOCKFILE=$LOCK/s3restoresync.lock
LOGFILE=/var/log/s3restoresync.log

EXEDIR=$(dirname $(readlink -f $0))

# If this script is running in Availability Zone 'a' wait for 5 minutes before proceeding.
# This lets the node in the other AZ go first, so that both nodes don't create dueling PID files.
# We wait on 'a' because not every region has a 'b', but they all have 'a'.
availability_zone=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
az_last_initial="${availability_zone: -1}"
if [[ $az_last_initial == 'a' ]]; then
  sleep 300
fi

# If the lock file is present...
if [ -f $LOCKFILE ]; then

  # If the lock file is less than 15m old, assume this must be running in zone A so exit
  if test `find "$LOCKFILE" -mmin -15`
  then
      exit 0
  fi

  # If the file is more than 15m old, try and figure out what's going on.
  pid=`cat $LOCKFILE`

  # See if an aws s3 command is still running on that pid
  r=`ps -fp $pid | grep "aws s3"`
  if [ -n "$r" ]; then
    # SNS Message, still going on me
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Restore Sync Failure on $HOSTNAME" --message "S3 Restore Sync Still running on pid $pid ($SOURCE)"
    exit 1
  else
    # SNS Message, pid file found, process not running
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Restore Sync Failure on $HOSTNAME" --message "PID Lock file found, process not running on this node. $pid ($SOURCE)"
    exit 1
  fi
fi

# Create lockfile parent folder if it's missing
if [ ! -d $LOCK ]; then
  mkdir -p $LOCK
  if [ $? -ne 0 ]; then
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Restore Failure on $HOSTNAME" --message "Lock file folder $LOCK missing and could not be created. ($SOURCE)"
    exit 1
  fi
fi

# Record start time for sync job
echo -en "=============\nStart S3 Restore Sync at $(date)\n=============\n" >> $LOGFILE

## Disable path expansion and wildcard globbing
#set -f
#
## Run our S3 Sync in the background
#aws s3 sync "$SOURCE" "$DEST" ${OPTIONS} 2>&1 >> $LOGFILE &

# Check that GNU Parallel is installed
if ! hash parallel 2>/dev/null; then
  echo "*** GNU Parallel is missing ***" >> $LOGFILE
  aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Restore Failure on $HOSTNAME" --message "GNU Parallel cannot be found. ($SOURCE)"
else
  # Launch concurrent S3 Sync processes using Gnu Parallel
  cd "$DEST"
  parallel --line-buffer -j300% --halt never --joblog /var/log/parallelrestore.log --arg-file $EXEDIR/folderlist.txt "aws s3 sync $SOURCE/{} {} $OPTIONS" 2>&1 >> $LOGFILE &

  # Capture sync process ID
  PID=$!

  ## Re-enable path expansion
  #set +f

  echo $PID > "$LOCKFILE"
  wait "$PID"
fi

# Record end time for sync job
echo -en "=============\nEnd S3 Restore Sync at $(date)\n=============\n" >> $LOGFILE

logrotate -f /etc/logrotate.d/s3backup.conf

rm "$LOCKFILE"
