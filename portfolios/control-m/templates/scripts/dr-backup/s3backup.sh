#!/bin/bash

# Trace mode on
#set -x

# Import environment variables
. /etc/controlm.files

SOURCE=$1
DEST=$2
LOCK=$3

# Add '--exclude * --include folder1/* --include folder2/* --delete' to OPTIONS
#OPTIONS='--delete --dryrun'
#OPTIONS='--exclude * --include prd/* --delete'
OPTIONS='--delete --acl bucket-owner-full-control'

# Assume SNS_NOTIFICATION_TOPIC exists in the environment
TOPIC=$SNS_NOTIFICATION_TOPIC
LOCKFILE=$LOCK/s3sync.lock
LOGFILE=/var/log/s3sync.log

EXEDIR=$(dirname $(readlink -f $0))

# If this script is running in Availability Zone 'a' wait for 5 minutes before proceeding.
# This lets the node in the other AZ go first, so that both nodes don't create dueling PID files.
# We wait on 'a' because not every region has a 'b', but they all have 'a'.
availability_zone=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
az_last_initial="${availability_zone: -1}"
if [[ $az_last_initial == 'a' ]]; then
  sleep 300
fi

# If the lock file is present...
if [ -f $LOCKFILE ]; then

  # If the lock file is less than 15m old, assume this must be running in zone A so exit
  if test `find "$LOCKFILE" -mmin -15`
  then
      exit 0
  fi

  # If the file is more than 15m old, try and figure out what's going on.
  pid=`cat $LOCKFILE`

  # See if an aws s3 command is still running on that pid
  r=`ps -fp $pid | grep "aws s3"`
  if [ -n "$r" ]; then
    # SNS Message, still going on me
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Sync Failure on $HOSTNAME" --message "S3 Sync Still running on pid $pid ($SOURCE)"
    exit 1
  else
    # SNS Message, pid file found, process not running
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Sync Failure on $HOSTNAME" --message "PID Lock file found, process not running on this node. $pid ($SOURCE)"
    exit 1
  fi
fi

# Create lockfile parent folder if it's missing
if [ ! -d $LOCK ]; then
  mkdir -p $LOCK
  if [ $? -ne 0 ]; then
    aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Sync Failure on $HOSTNAME" --message "Lock file folder $LOCK missing and could not be created. ($SOURCE)"
    exit 1
  fi
fi

# Record start time for sync job
echo -en "=============\nStart S3 Sync at $(date)\n=============\n" >> $LOGFILE

## Disable path expansion and wildcard globbing
#set -f
#
## Run our S3 Sync in the background
#aws s3 sync "$SOURCE" "$DEST" ${OPTIONS} 2>&1 >> $LOGFILE &

# Check that GNU Parallel is installed
if ! hash parallel 2>/dev/null; then
  echo "*** GNU Parallel is missing ***" >> $LOGFILE
  aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "DR Sync Failure on $HOSTNAME" --message "GNU Parallel cannot be found. ($SOURCE)"
else
  # Launch concurrent S3 Sync processes using Gnu Parallel
  cd "$SOURCE"
  parallel --line-buffer -j300% --halt never --joblog /var/log/parallel.log --arg-file $EXEDIR/folderlist.txt "aws s3 sync {} $DEST/{} $OPTIONS" 2>&1 >> $LOGFILE &

  # Capture sync process ID
  PID=$!

  ## Re-enable path expansion
  #set +f

  echo $PID > "$LOCKFILE"
  wait "$PID"
fi

# Record end time for sync job
echo -en "=============\nEnd S3 Sync at $(date)\n=============\n" >> $LOGFILE

logrotate -f /etc/logrotate.d/s3backup.conf

rm "$LOCKFILE"

# Check for untracked folders and send notification if any are found

# Make a list of all likely folders in the production subfolder
find $SOURCE -maxdepth 2 -type d -print > /tmp/allfolders.log

# Check for subfolders in the production tree that are not tracked
grep -v -f $EXEDIR/folderlist.txt -f $EXEDIR/ignorefolderlist.txt /tmp/allfolders.log > /tmp/untrackedfolders.log

# Check for tracked folders that are absent from the production tree
grep -v -x -f /tmp/allfolders.log $EXEDIR/folderlist.txt > /tmp/overtrackedfolders.log

SNSMSG=/tmp/snsmessage.txt

# Delete any old message file
if [ -f $SNSMSG ]; then
  rm $SNSMSG
fi

# Add untracked folders discovered to the message file
if [ -s /tmp/untrackedfolders.log ]; then
  # SNS Message, untracked folders
  echo -e "Untracked folders in EFS:\n" >> $SNSMSG
  cat /tmp/untrackedfolders.log >> $SNSMSG
  echo -e "\n========\n" >> $SNSMSG
fi

# Add overtracked folders discovered to the message file
if [ -s /tmp/overtrackedfolders.log ]; then
  # SNS Message, overtracked folders
  echo -e "Overtracked folders in EFS:\n" >> $SNSMSG
  cat /tmp/overtrackedfolders.log >> $SNSMSG
  echo -e "\n========\n" >> $SNSMSG
fi

# Send the message file if it has any contents
if [ -s $SNSMSG ]; then
  aws sns publish --region us-west-2 --topic-arn $TOPIC --subject "Folder Checks Alert on $HOSTNAME" --message file://$SNSMSG
  rm $SNSMSG
fi
