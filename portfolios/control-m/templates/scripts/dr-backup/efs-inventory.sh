#!/bin/bash

# This script will produce an inventory of the current state of Prod EFS

# Ensure environment variables are imported
. /root/.env

# Go to the working EFS mount path
cd $BackupSourcePath

WORKFOLDER=/tmp/inventory-$(date +%Y%m%d-%H%M%S)

mkdir $WORKFOLDER

# Collect the full EFS file list in parallel across the tracked folders
parallel  -Izz --line-buffer -j300% --halt never \
          --joblog $WORKFOLDER/parallel-EFS.log \
          --arg-file /root/folderlist.txt \
          "find zz -type f -exec ls -l {} \; | awk '{print \$9\",\"\$5}'" 2>&1 >> $WORKFOLDER/allEFSfiles-prod.csv

# Compress the resulting inventory
gzip $WORKFOLDER/allEFSfiles-prod.csv

# Collect the full S3 file list
parallel  -Izz --line-buffer -j300% --halt never \
          --joblog $WORKFOLDER/parallel-S3.log \
          --arg-file /root/folderlist.txt \
          'PATHNAME=zz; LOGNAME=S3-${PATHNAME//\//_}.csv; aws s3 ls $S3DRBackupURI/$PATHNAME --page-size 10000 --recursive | awk '\''{ sub(/^\w+\//, "", $4); print $4","$3 }'\'' > '$WORKFOLDER'/$LOGNAME; gzip '$WORKFOLDER'/$LOGNAME'

# Sort and compress the results for simpler comparison
zcat $WORKFOLDER/allEFSfiles-prod.csv.gz | sort > $WORKFOLDER/allEFSfiles-PROD-sorted.csv
gzip $WORKFOLDER/allEFSfiles-PROD-sorted.csv

zcat $WORKFOLDER/S3-*.csv.gz | sort > $WORKFOLDER/allS3files-DR-sorted.csv
gzip $WORKFOLDER/allS3files-DR-sorted.csv

echo "Check $WORKFOLDER for output of this inventory:"
echo "  $WORKFOLDER/allEFSfiles-PROD-sorted.csv.gz"
echo "  $WORKFOLDER/allS3files-DR-sorted.csv.gz"

echo "Compare Prod EFS and S3 DR bucket contents:"
echo -e "  In EFS, not in S3\n    zdiff --changed-group-format='%<' --unchanged-group-format='' $WORKFOLDER/allEFSfiles-PROD-sorted.csv.gz $WORKFOLDER/allS3files-DR-sorted.csv.gz"
echo -e "  In S3, not in EFS\n    zdiff --changed-group-format='%>' --unchanged-group-format='' $WORKFOLDER/allEFSfiles-PROD-sorted.csv.gz $WORKFOLDER/allS3files-DR-sorted.csv.gz"
