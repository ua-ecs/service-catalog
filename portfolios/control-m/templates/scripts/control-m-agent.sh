#!/bin/bash

# Control-M agent deployment script, used to configure the
# agent instance at deployment time.

# Import deployment time variables, including:
# ${HostedZoneName}
# ${ServerName}
# ${AWS::Region} as ${AWSRegion}
# ${GroupName}
# ${S3BucketName}
# ${EFSId}
# ${CTMServerName}
# ${CTMDomainName}
# ${PeopleSoftAgentDesired}
# ${SysJava11}
# ${DRS3BucketName}
# ${WORKINGPATH}
# ${SNS_NOTIFICATION_TOPIC}

# Debug on
#set -x

# Check if params file passed as an argument exists, and if so import it, otherwise fail
if [ -f $1 ]; then
  . $1
else
  echo "Configuration parameters for instance deployment missing or not provided: $1"
  exit 1
fi

# Register with Route53
yum -y install jq
export AWSAZ=$(curl -s "http://169.254.169.254/latest/meta-data/placement/availability-zone")
export AWSHOSTIP=$(curl -s "http://169.254.169.254/latest/meta-data/local-ipv4")
export AWSZONEID=$(aws route53 list-hosted-zones-by-name --dns-name ${HostedZoneName} | \
          jq '.HostedZones[0] | select(.Name|test("'${HostedZoneName}'.$")).Id' | \
          sed 's|[^A-Z0-9]||g')
echo "Registering ${ServerName}-${AWSAZ: -1}.${HostedZoneName} in DNS"
aws route53 change-resource-record-sets --hosted-zone-id $AWSZONEID --cli-input-json '{
    "HostedZoneId": "'$AWSZONEID'",
    "ChangeBatch": {
        "Comment": "Update registration for this instance",
        "Changes": [
            {
                "Action": "UPSERT",
                "ResourceRecordSet": {
                    "Name": "'${ServerName}'-'${AWSAZ: -1}'.'${HostedZoneName}'",
                    "Type": "A",
                    "TTL": 200,
                    "ResourceRecords": [
                        {
                            "Value": "'$AWSHOSTIP'"
                        }
                    ]
                }
            }
        ]
    }
}'
# Set hostname
hostnamectl set-hostname "${ServerName}-${AWSAZ: -1}.${HostedZoneName}"
# INSTALL UPDATES
yum check-update
YUMCHECK=$?
# Outstanding patches available
if [ $YUMCHECK -eq 100 ] ; then
  # Apply all OS updates
  yum update -y
  # Reset cloud-init script so userdata runs again on next boot
  rm /var/lib/cloud/instance/sem/config_scripts_user
  # Reboot to make the updates active
  shutdown -r now
fi

if [ -f /var/lib/cloud/instance/done-initial.txt ] ; then
  echo "Already completed initial configuration. Exiting."
  exit
fi

# Set the Time Zone to MST (America/Phoenix)
sed -i 's|ZONE="UTC"|ZONE="America/Phoenix"|' /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/America/Phoenix /etc/localtime

# CONTROL-M
# Create Control-M specific groups
groupadd -g 1005 controlm
groupadd -g 1008 ctmagent
groupadd -g 1010 localusers
sleep 1
# Create Control-M specific user accounts
useradd -u 1005 -g controlm -G localusers -d /home/controlm -s /bin/tcsh -c "Control-M" controlm
sleep 1
#useradd -u 1006 -g cmsrvr   -G localusers -d /home/cmsrvr   -s /bin/tcsh -c "Control-M Server" cmsrvr
#useradd -u 1007 -g controlm -G localusers -d /home/ctmem    -s /bin/tcsh -c "Control-M Enterprise Manager" ctmem
useradd -u 1008 -g ctmagent -G localusers -d /home/ctmagent -s /bin/bash -c "Control-M Agent" ctmagent
# Set homedir permissions
chmod 755 /home/controlm
chmod 755 /home/ctmagent
# Set custom security limits for Control-M
cat <<EOFCMLIMIT >/etc/security/limits.d/99-controlm.conf
# Override some limits for the Control-M accounts
*   soft  nofile  4096    # descriptors
*   hard  nofile  4096    # descriptors
# Don't need to set the following for CM because the default
# limits are high enough already
# *   soft/hard   data          unlimited   # datasize
# *   soft/hard   stack         8192        # stacksize (8MB)
# *   soft/hard   core          unlimited   # coredumpsize
# *   soft/hard   memlock       unlimited   # memoryuse
# *   soft/hard   nproc         4096        # maxproc
EOFCMLIMIT
chmod 644 /etc/security/limits.d/99-controlm.conf

# Fix root's search PATH
sed -i -r 's|^(PATH=.*)|\1:/usr/local/bin|' /root/.bash_profile

# INSTALL INSPECTOR AGENT
# only do this once OS is patched and updated
cd /tmp
curl -O https://inspector-agent.amazonaws.com/linux/latest/install
bash install
rm ./install

# INSTALL/CONFIGURE AUTHENTICATION
# Install and configure NLNet Labs Unbound DNS resolver
yum -y install unbound
# Get fresh root hints
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints
# Configure Unbound service
sed -i~ -e 's/harden-dnssec-stripped: yes/harden-dnssec-stripped: no/' \
        -e '/# root-hints: ""/a\
        root-hints: "/etc/unbound/root.hints"' \
        -e 's/module-config: "ipsecmod validator iterator"/#module-config: "ipsecmod validator iterator"/' \
        -e '/#module-config: "ipsecmod validator iterator"/a\
        module-config: "iterator"' \
        /etc/unbound/unbound.conf
# Forward zone configuration
cat <<EOFUNBOUND >/etc/unbound/conf.d/aws-uarizona.conf
forward-zone:
    name: "${AWSRegion}.compute.internal"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "amazonaws.com"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "."
    forward-addr: 128.196.11.233
    forward-addr: 128.196.11.234
EOFUNBOUND
# Enable and restart the unbound service
# Set custom service dependencies
mkdir -p /etc/systemd/system/unbound.service.d
cat <<EOF1DNS > /etc/systemd/system/unbound.service.d/override.conf
[Unit]
Before=controlm-efs.mount

EOF1DNS
systemctl daemon-reload
systemctl enable unbound.service
systemctl restart unbound.service

# Override DHCP assigned DNS servers used for lookups
# https://aws.amazon.com/premiumsupport/knowledge-center/ec2-static-dns-ubuntu-debian/
cat <<EOFDNS >/etc/dhcp/dhclient.conf
timeout 300;
append domain-search "mosaic.arizona.edu", "uits.arizona.edu", "catnet.arizona.edu", "bluecat.arizona.edu";
supersede domain-name-servers 127.0.0.1;
EOFDNS
systemctl restart network.service

# INSTALL SOPHOS AGENT
mkdir -p /tmp/sophosinstaller
export AGENT_SOPHOS_URL=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/sophos/linux_url" --output text --query Parameter.Value)
echo "Downloading Sophos installer"
# Retry file retrieval until successful
RC=1
SOPHOSSETUP='/tmp/sophosinstaller/SophosSetup.sh'
for i in $(seq 1 5); do
  if [ $RC -eq 0 ]; then
    break
  fi
  /usr/bin/curl "$AGENT_SOPHOS_URL" -o $SOPHOSSETUP
  if [ -s $SOPHOSSETUP ]; then
    /bin/bash $SOPHOSSETUP --version
    RC=$?
  fi
  sleep 1
done
if [ $RC -eq 0 ]; then
  if [ "$ENVIRONMENT" != "dev" ]; then
    SOPHOSLOG="/var/log/cloud-init-sophos.log"
    /bin/bash $SOPHOSSETUP --group=Enterprise >$SOPHOSLOG 2>&1 &
    SOPHOSPID=$!
    echo "Sophos installation proceeding in the background on PID $SOPHOSPID"
    echo "Output redirected to $SOPHOSLOG"
  else
    echo "Dev environment. Skipping Sophos install at build time."
  fi
else
  EXITCODE=$RC
  SNSMSG="${SNSMSG}Failed to download Sophos installer. "
fi

# INSTALL TENABLE/NESSUS AGENT
#mkdir -p /tmp/nessusinstaller
#export AGENT_NESSUS_URL=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/linux_url" --output text --query Parameter.Value)
#export AGENT_NESSUS_HOST=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f1)
#export AGENT_NESSUS_PORT=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f2)
#export AGENT_NESSUS_KEY=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/key" --with-decryption --output text --query Parameter.Value)
#aws s3 cp "$AGENT_NESSUS_URL" /tmp/nessusinstaller/
#rpm -vhU /tmp/nessusinstaller/NessusAgent*.rpm
#/opt/nessus_agent/sbin/nessuscli agent link --key=$AGENT_NESSUS_KEY --host=$AGENT_NESSUS_HOST --port=$AGENT_NESSUS_PORT --groups="CloudOps, CloudOps-Linux"
#systemctl enable nessusagent.service
#systemctl start nessusagent.service

# INSTALL AZURE OMS LOG ANALYTICS AGENT
# Get the OMS Agent
yum -y install curl wget
mkdir -p /tmp/omsinstaller
/usr/bin/curl 'https://raw.githubusercontent.com/Microsoft/OMS-Agent-for-Linux/master/installer/scripts/onboard_agent.sh' -o /tmp/omsinstaller/onboard_agent.sh
# Install the OMS Agent
cd /tmp/omsinstaller
export CLOPS_OMS_WORKSPACE=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/workspaceid" --with-decryption --output text --query Parameter.Value)
export CLOPS_OMS_PRIMKEY=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/primarykey" --with-decryption --output text --query Parameter.Value)
OMSLOG="/var/log/cloud-init-oms.log"
/bin/bash /tmp/omsinstaller/onboard_agent.sh -w "$CLOPS_OMS_WORKSPACE" -s "$CLOPS_OMS_PRIMKEY" -d opinsights.azure.com  >$OMSLOG 2>&1 &
OMSPID=$!
echo "Azure Monitor/OMS installation proceeding in the background on PID $OMSPID"
echo "Output redirected to $OMSLOG"

# Authentication configuration
# Install and configure sssd, kerberos PAM module,
# and oddjob packages to enable AD authentication
yum -y install oddjob-mkhomedir sssd nss_ldap pam_krb5 \
      oddjob openldap-clients realmd samba-common-tools \
      adcli krb5-workstation policycoreutils-python

mv /etc/nsswitch.conf /etc/nsswitch.conf.orig
cat <<EOF1 >/etc/nsswitch.conf
passwd:     files sss
shadow:     files
group:      files sss
sudoers:    files

hosts:      files dns

bootparams: nisplus [NOTFOUND=return] files

ethers:     files
netmasks:   files
networks:   files
protocols:  files
rpc:        files
services:   files

netgroup:   files

publickey:  nisplus

automount:  files
aliases:    files nisplus
EOF1

# Update kerberos logging destination
mkdir /var/log/krb5
chown sssd:sssd /var/log/krb5
chmod 750 /var/log/krb5
sed -i~ 's|/var/log/|/var/log/krb5/|g' /etc/krb5.conf

# Join AD using parameters pulled from SSM
export AD_JOIN_USER=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/user" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_PASSWD=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/password" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_DOMAIN=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/domain" --with-decryption --output text --query Parameter.Value)
echo $AD_JOIN_PASSWD | realm join --user=$AD_JOIN_USER --computer-name=${ServerName} $AD_JOIN_DOMAIN

# Change authentication and homedir designation so it's not fully qualified
sed -i~ 's|use_fully_qualified_names = True|use_fully_qualified_names = False|;s|fallback_homedir = /home/%u@%d|fallback_homedir = /home/%u|' /etc/sssd/sssd.conf

# Set local login permitted groups
mv /etc/security/local.allowed_groups /etc/security/local.allowed_groups.orig
cat <<EOF5 >/etc/security/local.allowed_groups
root
wheel
localusers
is-oc
${GroupName}
EOF5

# Reconfigure PAM
mv /etc/pam.d/system-auth /etc/pam.d/system-auth.orig
cat <<EOF6 >/etc/pam.d/system-auth
auth        required      pam_listfile.so onerr=fail item=group sense=allow file=/etc/security/local.allowed_groups
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        sufficient    pam_sss.so use_first_pass
auth        required      pam_deny.so

account     required      pam_unix.so broken_shadow
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account     [default=bad success=ok user_unknown=ignore] pam_sss.so
account     required      pam_permit.so

password    requisite     pam_cracklib.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha256 shadow nullok try_first_pass use_authtok
password    sufficient    pam_sss.so use_authtok
password    required      pam_deny.so

session     required      pam_oddjob_mkhomedir.so skel=/etc/skel/ umask=0022
session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session     optional      pam_sss.so
EOF6

mv /etc/pam.d/password-auth /etc/pam.d/password-auth.orig
cp /etc/pam.d/system-auth /etc/pam.d/password-auth

systemctl restart sssd.service
systemctl restart nslcd.service
systemctl restart oddjobd.service

# Configure sudoers for the service owning group
cat <<EOF7 > /etc/sudoers.d/99-service-owners
# The provisioning/service owning group has full access
%${GroupName} ALL=(root)   ALL
EOF7
chmod 440 /etc/sudoers.d/99-service-owners

# Configure sudoers for the Control-M service accounts
cat <<EOF7CTM > /etc/sudoers.d/99-controlm
# The Control-M related accounts have access to some privileged commands
Cmnd_Alias CTMAGENT_CMDS = /home/ctmagent/ctm/scripts/start-ag *, \\
                           /home/ctmagent/ctm/scripts/shut-ag *, \\
                           /home/ctmagent/ctm/scripts/rc.agent_user *, \\
                           /bin/systemctl start ctmagent.service, \\
                           /bin/systemctl stop ctmagent.service, \\
                           /bin/systemctl status ctmagent.service
ctmagent ALL=(root) NOPASSWD: CTMAGENT_CMDS
EOF7CTM
chmod 440 /etc/sudoers.d/99-controlm

# Configure sshd to permit login from authenticated users in the owning group
sed -i.orig \
    -e 's/^PasswordAuthentication .*$/PasswordAuthentication yes/' \
    -e 's/^GSSAPICleanupCredentials .*$/GSSAPICleanupCredentials yes/' \
    -e '/Subsystem sftp/a \' \
    -e '\nAllowGroups root wheel localusers is-oc '${GroupName} \
    /etc/ssh/sshd_config

systemctl restart sshd.service

# Import ssh public keys for admin users
mkdir -p /tmp/pubkeys
aws s3 sync s3://${S3BucketName}/pubkeys/ /tmp/pubkeys/
for u in ec2-user ctmagent; do
  sudo -i -u $u sh -c \
  "if [ ! -d ~/.ssh ]; then \
    mkdir ~/.ssh ; \
    chmod 700 ~/.ssh ; \
  fi ; \
  cat /tmp/pubkeys/* >> ~/.ssh/authorized_keys ; \
  chmod 644 ~/.ssh/authorized_keys"
done
rm -fr /tmp/pubkeys

# Any final automation of copy media and installation of Control-M components

# Install additional dependencies
## git and GNU Debugger
yum -y install git gdb
## GNU Parallel
(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - pi.dk/3) | bash

# Add Control-M file store connection information to config file
echo -en "S3BUCKET=${S3BucketName}\nEFSDNS=${EFSId}.efs.${AWSRegion}.amazonaws.com\n" > /etc/controlm.files
echo -en "SNS_NOTIFICATION_TOPIC=${SNS_NOTIFICATION_TOPIC}\n" >> /etc/controlm.files
if [ "$DRS3BucketName" != "" ]; then
  echo -en "DRS3BUCKET=s3://${DRS3BucketName}/\n" >> /etc/controlm.files
fi
chmod 644 /etc/controlm.files

# Add Control-M server FQDN information to config file
> /etc/controlm.servers
if [ "${CTMDomainName}" != "" ]; then
  echo -en "CTMSERVER1=${CTMServerName}-a.${CTMDomainName}\nCTMSERVER2=${CTMServerName}-b.${CTMDomainName}\n" > /etc/controlm.servers
else
  i=1; for s in $(echo $CTMServerName); do
    echo "CTMSERVER$i=$s" >> /etc/controlm.servers
    ((i++))
  done
fi
chmod 644 /etc/controlm.servers

# Placeholder for the EFS and ISO mounts
mkdir -p /controlm/efs
mkdir -p /controlm/iso
yum -y install amazon-efs-utils
. /etc/controlm.files ; echo "$EFSDNS:/     /controlm/efs    efs    defaults,_netdev          0   0" >> /etc/fstab
mount /controlm/efs/

# Check that installation media is populated in EFS. If it is not, this must be a solo
# Installation of an agent, so the agent needs to populate the media folders
#####
# BEGIN: Populate the installation media in EFS based on the media in S3
#####

# Get environment variables and create target and temp folders
. /etc/controlm.files

# Set initial package path information
declare -A SRC
declare -A DST
SRC[CTM]='/Control-M_9019200/EM/'
SRC[AFT]='/Control-M_9019100/AFT82300/'
SRC[PS8]='/Control-M_9019100/PEOPLESOFT/'
SRC[BIM]='/Control-M_9019100/BIM/'
SRC[FOR]='/Control-M_9019100/Forecast/'
SRC[SLS]='/Control-M_9019100/SelfService/'
SRC[WCM]='/Control-M_9019100/WCM/'
SRC[L4J]='/Log4J-remediation'
DST[ALL]='/controlm/efs/Control-M_9019200'
DST[OST]='CTM'
DST[AFT]='AFT-V8.2'
DST[PMC]='PS8-V9'
DST[CBM]='BIM'
DST[FOR]='Forecast'
DST[CAG]='SLS'
DST[WCM]='WCM'
DST[L4J]='Log4J-remediation'
TMP_ALL='/tmp/media'

if [ ! -d ${DST[ALL]} ]; then
  mkdir ${DST[ALL]}

  if [ ! -d $TMP_ALL ]; then
    mkdir $TMP_ALL
  fi

  for f in CTM AFT PS8 BIM FOR SLS WCM L4J; do
    echo $S3BUCKET${SRC[$f]}
    aws s3 ls s3://$S3BUCKET${SRC[$f]}
    if [ "$f" == "L4J" ]; then
      aws s3 sync s3://$S3BUCKET${SRC[$f]} $TMP_ALL/${SRC[$f]}
    else
      aws s3 sync s3://$S3BUCKET${SRC[$f]} $TMP_ALL
    fi
  done

  for f in OST AFT PMC CBM FOR CAG WCM L4J; do
    if [ ! -d ${DST[ALL]}/${DST[$f]} ]; then
      mkdir -p ${DST[ALL]}/${DST[$f]}
    fi
    # Check for ISO media file
    FNAME=$(ls $TMP_ALL/DR${f}*.iso | tail -n 1)
    if [ "$FNAME" != "" ]; then
      mount -t iso9660 $FNAME /controlm/iso
      cp -ar /controlm/iso/* ${DST[ALL]}/${DST[$f]}/
      umount /controlm/iso
    fi
    # Check for tar/gzipped media file
    FNAME=$(ls $TMP_ALL/DR${f}*.z | tail -n 1)
    if [ "$FNAME" != "" ]; then
      pushd ${DST[ALL]}/${DST[$f]}/
      tar xzf $FNAME
      popd
    fi
    # Check for BIN patch files
    if [ -f $TMP_ALL/PA${f}*.BIN ]; then
      cp $TMP_ALL/PA${f}*.BIN ${DST[ALL]}/${DST[$f]}/
      chmod +x ${DST[ALL]}/${DST[$f]}/PA${f}*.BIN
    fi
    # Plain copy the following
    if [ "$f" == "L4J" ]; then
      cp -r $TMP_ALL/${DST[$f]} ${DST[ALL]}/
    fi
  done

  # Set folder ownership
  if [ -O ${DST[ALL]} ]; then
    chown -R controlm:controlm ${DST[ALL]}
  fi

  # Clean up temporary package space
  rm -rf ${TMP_ALL}

  # Symlink to current install media
  if [ ! -h /controlm/efs/Control-M_current ]; then
    ln -s ${DST[ALL]} /controlm/efs/Control-M_current
  fi
fi

# If the media folder already exists, just wait until the Symlink is created,
# as that will indicate the process is truly complete.
while [ ! -h /controlm/efs/Control-M_current ]; do
  sleep 10
  echo -n "."
done
echo -ne "Done extracting installation media\n"

# Pull current silent installation templates
if [ ! -d /controlm/efs/cm-templates/ ]; then
  mkdir -p /controlm/efs/cm-templates
fi
aws s3 sync s3://$S3BUCKET/ctmtemplates /controlm/efs/cm-templates/

#####
# END: Populate the installation media in EFS based on the media in S3
#####

# Install needed package dependencies
# libstdc++.so.5 needed by AFT
yum -y install compat-libstdc++-33.x86_64
# Java 11 if wanted/needed by PS8 for PeopleTools version 8.59+
if [ "${SysJava11}" == "true" ]; then
  yum -y install java-11-amazon-corretto-headless
fi

# Do the following as the ctmagent account
sudo -i -u ctmagent <<EOFAGENT1
# Install the basic CTM agent
. /etc/controlm.servers ; cat /controlm/efs/cm-templates/ctmagent.xml | \
  sed -e 's/\(field.Primary.Controlm.Server.Host" value="\).*\("\)/\1'\$CTMSERVER1'\2/g' \
      -e 's/\(field.Authorized.Controlm.Server.Host" value="\).*\("\)/\1'\$CTMSERVER1'|'\$CTMSERVER2'\2/g' \
      > /tmp/ctmagent.xml
/controlm/efs/Control-M_current/CTM/setup.sh -silent /tmp/ctmagent.xml
# rm /tmp/ctmagent.xml
EOFAGENT1
# Do the following as the ctmagent account
sudo -i -u ctmagent <<EOFAGENT2
# Check and shut down the agent
/home/ctmagent/ctm/scripts/shagent
/home/ctmagent/ctm/scripts/shut-ag -u ctmagent -p ALL
# Set the logical agent name to the FQDN
sed -i~ -e 's/^\(LOGICAL_AGENT_NAME\s\+\).*$/\1'\$HOSTNAME'/g' \$CONTROLM/data/CONFIG.dat
# Set the job output name, replacing MEMNAME with JOBNAME
sed -i~ -e 's/^\(OUTPUT_NAME\s\+\).*$/\1JOBNAME/g' \$CONTROLM/data/OS.dat
# Install the AFT module
/controlm/efs/Control-M_current/AFT-V8.2/setup.sh -silent /controlm/efs/cm-templates/ctmagent-aft.xml
# Install the AFT module patch
/controlm/efs/Control-M_current/AFT-V8.2/PAAFT.8.2.00.300_Linux-x86_64_INSTALL.BIN -s
# Install the PS8 module and fix java path
if [ "${PeopleSoftAgentDesired}" == "true" ]; then
  /controlm/efs/Control-M_current/PS8-V9/setup.sh -silent /controlm/efs/cm-templates/ctmagent-ps8.xml
  if [ "${SysJava11}" == "true" ]; then
    # PS8 module to use upgraded system Java version
    sed -i~ 's|^\(JAVA_PATH \).*ctm/cm/PS8/JRE/bin$|\1/usr/lib/jvm/java-11-amazon-corretto.x86_64/bin|' ctm/data/PS8.dat
  else
    # PS8 module to use as-shipped CM Agent Java version
    sed -i~ 's|^\(JAVA_PATH .*\)ctm/cm/PS8/JRE/bin$|\1ctm/JRE/bin|' ctm/data/PS8.dat
  fi
fi
EOFAGENT2
# Ensure that the shared content folder exists on EFS
if [ ! -d "/controlm/efs/shared/${ServerName}" ]; then
  mkdir -p "/controlm/efs/shared/${ServerName}"
  chown ctmagent:ctmagent "/controlm/efs/shared/${ServerName}"
  chmod 755 "/controlm/efs/shared/${ServerName}"
fi
# Do the following as the ctmagent account
sudo -i -u ctmagent <<EOFAGENT3
# Symlink the PS8 connection profiles to the shared one, creating it if it doesn't exist
if [[ "${PeopleSoftAgentDesired}" == "true" && ! -h ~/ctm/cm/PS8/java/data/profiles.dat ]]; then
  # Ensure that the shared PS8 folder exists on EFS
  if [ ! -d /controlm/efs/shared/${ServerName}/PS8 ]; then
    mkdir -p /controlm/efs/shared/${ServerName}/PS8
  fi
  # Symlink PS8 connection profiles
  if [ -f ~/ctm/cm/PS8/java/data/profiles.dat ]; then
    mv ~/ctm/cm/PS8/java/data/profiles.dat ~/ctm/cm/PS8/java/data/profiles.dat.orig
  fi
  if [ ! -f /controlm/efs/shared/${ServerName}/PS8/profiles.dat ]; then
    cp ~/ctm/cm/PS8/java/data/profiles.dat.orig /controlm/efs/shared/${ServerName}/PS8/profiles.dat
  fi
  ln -s /controlm/efs/shared/${ServerName}/PS8/profiles.dat ~/ctm/cm/PS8/java/data/profiles.dat
  # Symlink PS8 jar files for available PeopleTools versions
  pushd ~/ctm/cm/PS8/java
  find /controlm/efs/shared/${ServerName}/PS8/ -type d \
    -regextype posix-extended -regex ".*\/[0-9]+(\.[0-9]+){2}" -printf "%p %f\n" | \
    xargs -l bash -c 'ln -s \$0 \$1'
  popd
fi
# Ensure that the shared AFT folder exists on EFS
if [ ! -d /controlm/efs/shared/${ServerName}/AFT ]; then
  mkdir -p /controlm/efs/shared/${ServerName}/AFT
fi
# Symlink the AFT connection profiles, known hosts, and pgp profiles to the shared ones,
# creating them if they don't exist
for f in accounts.xml known_hosts Pgp_templates.dat; do
  # Symlink the AFT connection profiles to the shared one, creating it if it doesn't exist
  if [ ! -h ~/ctm/cm/AFT/data/\$f ]; then
    if [ -f ~/ctm/cm/AFT/data/\$f ]; then
      mv ~/ctm/cm/AFT/data/\$f ~/ctm/cm/AFT/data/\$f.orig
    fi
    if [ ! -f /controlm/efs/shared/${ServerName}/AFT/\$f ]; then
      cp ~/ctm/cm/AFT/data/\$f.orig /controlm/efs/shared/${ServerName}/AFT/\$f
    fi
    ln -s /controlm/efs/shared/${ServerName}/AFT/\$f ~/ctm/cm/AFT/data/\$f
  fi
done
# Symlink the AFT keys to the shared one, creating it if it doesn't exist
if [ ! -h ~/ctm/cm/AFT/data/Keys ]; then
  mv ~/ctm/cm/AFT/data/Keys ~/ctm/cm/AFT/data/Keys.orig
  if [ ! -d /controlm/efs/shared/${ServerName}/AFT/Keys ]; then
    cp -ar ~/ctm/cm/AFT/data/Keys.orig /controlm/efs/shared/${ServerName}/AFT/Keys
  fi
  ln -s /controlm/efs/shared/${ServerName}/AFT/Keys ~/ctm/cm/AFT/data/Keys
fi
# Ensure that the shared GPG Keyring folder exists on EFS
if [ ! -d /controlm/efs/shared/${ServerName}/GPG ]; then
  mkdir -p /controlm/efs/shared/${ServerName}/GPG
  chmod 700 /controlm/efs/shared/${ServerName}/GPG
fi
# Symlink the GPG Keyring to the shared one, creating it if it doesn't exist
if [ ! -h ~/.gnupg ]; then
  mv ~/.gnupg ~/.gnupg.orig
  if [ ! -f /controlm/efs/shared/${ServerName}/GPG/gpg.conf ]; then
    cp -a ~/.gnupg.orig/* /controlm/efs/shared/${ServerName}/GPG
  fi
  ln -s /controlm/efs/shared/${ServerName}/GPG ~/.gnupg
fi
# Symlink the sa-nelnet folders if they exist in EFS
if [ -d /controlm/efs/shared/${ServerName}/sa-nelnet ]; then
  if [ ! -h ~/sa-nelnet ]; then
    if [ -d ~/sa-nelnet ]; then
      mv ~/sa-nelnet ~/sa-nelnet.orig
    fi
    ln -s /controlm/efs/shared/${ServerName}/sa-nelnet ~/sa-nelnet
    # Import the current Nelnet GPG key file
    /usr/bin/gpg --import ~/sa-nelnet/.ssh/UA-NELNET-GPG-KEY.private
  fi
fi
# Symlink the boomi folders if they exist in EFS
if [ -d /controlm/efs/shared/${ServerName}/boomi ]; then
  if [ ! -h ~/boomi ]; then
    if [ -d ~/boomi ]; then
      mv ~/boomi ~/boomi.orig
    fi
    ln -s /controlm/efs/shared/${ServerName}/boomi ~/boomi
  fi
fi
# Symlink the AWS config folders if they exist in EFS
if [ -d /controlm/efs/shared/${ServerName}/AWS ]; then
  if [ ! -h ~/.aws ]; then
    if [ -d ~/.aws ]; then
      mv ~/.aws ~/.aws.orig
    fi
    ln -s /controlm/efs/shared/${ServerName}/AWS ~/.aws
  fi
fi
# Symlink the SA-HANDSHAKE folders if they exist in EFS
if [ -d /controlm/efs/shared/${ServerName}/SA-HANDSHAKE ]; then
  if [ ! -h ~/SA-HANDSHAKE ]; then
    if [ -d ~/SA-HANDSHAKE ]; then
      mv ~/SA-HANDSHAKE ~/SA-HANDSHAKE.orig
    fi
    ln -s /controlm/efs/shared/${ServerName}/SA-HANDSHAKE ~/SA-HANDSHAKE
  fi
fi

# Create temp folders for GPG staging files
mkdir -p ~/CHASEUA/HCM
mkdir -p ~/CHASEUA/AZPM
mkdir -p ~/CHASEUA/SA
mkdir -p ~/CHASEUA/UAF
mkdir -p ~/CHASEUA/UAF/ACH
mkdir -p ~/CHASEUA/UAF/POSPAY
# Set AFT module custom properties
sed -i~ -e 's/^\(com.bmc.aft.configurable.ftp.disableUnixPass=false\)$/#\1/' \
        -e '/#com.bmc.aft.configurable.ftp.disableUnixPass=false/a\
#allow root mode to impersonate and without password on LocalCM\ncom.bmc.aft.configurable.linux.impersonation.enabled=true\ncom.bmc.aft.configurable.ftp.disableUnixPass=true' \
\$CONTROLM/cm/AFT/data/aft_configurable.properties
EOFAGENT3
## Do the following as the ctmagent account
#sudo -i -u ctmagent <<EOFAGENT4
## Start the agent and check status
#start-ag -u ctmagent -p ALL
#shagent
#EOFAGENT4
# As root, set the agent mode to non-root, restarting the agent
/home/ctmagent/ctm/scripts/set_agent_mode -u ctmagent -o 2 -r Y
# Start the agent
/home/ctmagent/ctm/scripts/start-ag -s -u ctmagent -p ALL
# Stop the agent to allow systemd to control it
/home/ctmagent/ctm/scripts/rc.agent_user stop
# Configure agent auto-start service at reboot
cat <<EOFAGENT5 > /etc/systemd/system/ctmagent.service
[Unit]
Description=Control-M Agent
[Service]
Type=forking
RemainAfterExit=yes
ExecStart=/home/ctmagent/ctm/scripts/rc.agent_user start
ExecStop=/home/ctmagent/ctm/scripts/rc.agent_user stop
[Install]
WantedBy=multi-user.target
EOFAGENT5
chmod 644 /etc/systemd/system/ctmagent.service
/bin/systemctl daemon-reload
/bin/systemctl enable ctmagent.service
/bin/systemctl start ctmagent.service

# Patch Log4J vulnerability
echo "Remediating Log4J agent vulnerability"
sudo -i -u ctmagent <<EOFAGENT6
cp ${DST[ALL]}/${DST[L4J]}/rm_log4j.sh ~/.
chmod +x ~/rm_log4j.sh
~/rm_log4j.sh
EOFAGENT6

# Install media distribution/sync script
cp -r $WORKINGPATH/deploy-sync /root/.
chmod +x /root/deploy-sync/*.sh

# Install backup/DR services
cp -r $WORKINGPATH/dr-backup /root/.
if [ ! -d /root/.aws ]; then
  mkdir /root/.aws
fi
mv /root/dr-backup/awsconfig /root/.aws/config
mv /root/dr-backup/s3backup.conf /etc/logrotate.d/s3backup.conf
chmod +x /root/dr-backup/*.sh
ln -s /root/dr-backup/folderlist-agent.txt /root/dr-backup/folderlist.txt
ln -s /root/dr-backup/ignorefolderlist-agent.txt /root/dr-backup/ignorefolderlist.txt

# Schedule the backup job if this is Prod, to run at 2 min past every 4th hour;
# ie. 00:02, 04:02, 08:02, 12:02, 16:02, 20:02.
#
if [ "$DRS3BucketName" != "" ]; then
  echo "2 0,4,8,12,16,20 * * *   . /etc/controlm.files ; /root/dr-backup/s3backup.sh /home \${DRS3BUCKET} /controlm/efs/shared/${ServerName}" | crontab -
fi

echo "Completed Control-M Agent configuration at $(date)."
echo "Waiting for Sophos and OMS to complete installation."
wait $SOPHOSPID $OMSPID

# Last line
touch /var/lib/cloud/instance/done-initial.txt

# Debug off
#set +x

# End External UserData
