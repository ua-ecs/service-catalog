#!/bin/bash

# Control-M agent deployment script, used to configure the
# agent instance at deployment time.

# Import deployment time variables, including:
# ${AlbHostedZoneName}
# ${ServerName}
# ${AWS::Region} as ${AWSRegion}
# ${GroupName}
# ${S3BucketName}
# ${EFSId}
# ${DBEndpoint}
# ${DBPort}
# ${DBName}
# ${CMSecondaryInstallDesired}
# ${DRS3BucketName}
# ${WORKINGPATH}
# ${SNS_NOTIFICATION_TOPIC}

# Debug on
#set -x

# Check if params file passed as an argument exists, and if so import it, otherwise fail
if [ -f $1 ]; then
  . $1
else
  echo "Configuration parameters for instance deployment missing or not provided: $1"
  exit 1
fi

# Register with Route53
yum -y install jq
export AWSAZ=$(curl -s "http://169.254.169.254/latest/meta-data/placement/availability-zone")
export AWSHOSTIP=$(curl -s "http://169.254.169.254/latest/meta-data/local-ipv4")
export AWSZONEID=$(aws route53 list-hosted-zones-by-name --dns-name ${AlbHostedZoneName} | \
          jq '.HostedZones[0] | select(.Name|test("'${AlbHostedZoneName}'.$")).Id' | \
          sed 's|[^A-Z0-9]||g')
echo "Registering ${ServerName}-${AWSAZ: -1}.${HostedZoneName} in DNS"
aws route53 change-resource-record-sets --hosted-zone-id $AWSZONEID --cli-input-json '{
    "HostedZoneId": "'$AWSZONEID'",
    "ChangeBatch": {
        "Comment": "Update registration for this instance",
        "Changes": [
            {
                "Action": "UPSERT",
                "ResourceRecordSet": {
                    "Name": "'${ServerName}'-'${AWSAZ: -1}'.'${AlbHostedZoneName}'",
                    "Type": "A",
                    "TTL": 200,
                    "ResourceRecords": [
                        {
                            "Value": "'$AWSHOSTIP'"
                        }
                    ]
                }
            }
        ]
    }
}'
# Set hostname
hostnamectl set-hostname "${ServerName}-${AWSAZ: -1}.${AlbHostedZoneName}"
# INSTALL UPDATES
yum check-update
YUMCHECK=$?
# Outstanding patches available
if [ $YUMCHECK -eq 100 ] ; then
  # Apply all OS updates
  yum update -y
  # Reset cloud-init script so userdata runs again on next boot
  rm /var/lib/cloud/instance/sem/config_scripts_user
  # Reboot to make the updates active
  shutdown -r now
fi

if [ -f /var/lib/cloud/instance/done-initial.txt ] ; then
  echo "Already completed initial configuration. Exiting."
  exit
fi

# Set the Time Zone to MST (America/Phoenix)
sed -i 's|ZONE="UTC"|ZONE="America/Phoenix"|' /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/America/Phoenix /etc/localtime

# CONTROL-M
# Add swapfile
dd if=/dev/zero of=/swapfile bs=1G count=8
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile     swap    swap    defaults          0   0" >> /etc/fstab
swapon -a

# Create Control-M specific groups
groupadd -g 1005 controlm
groupadd -g 1006 cmsrvr
groupadd -g 1010 localusers
# Create Control-M specific user accounts
useradd -u 1005 -g controlm -G localusers -d /home/controlm -s /bin/tcsh -c "Control-M" controlm
useradd -u 1006 -g cmsrvr   -G localusers -d /home/cmsrvr   -s /bin/tcsh -c "Control-M Server" cmsrvr
useradd -u 1007 -g controlm -G localusers -d /home/ctmem    -s /bin/tcsh -c "Control-M Enterprise Manager" ctmem
# Set homedir permissions
chmod 755 /home/controlm
chmod 755 /home/ctmem
# Set custom security limits for Control-M
cat <<EOFCMLIMIT >/etc/security/limits.d/99-controlm.conf
# Override some limits for the Control-M accounts
*   soft  nofile  8192    # descriptors
*   hard  nofile  8192    # descriptors
*   soft  nproc   8192    # maxproc
*   hard  nproc   8192    # maxproc
*   soft  stack   16384   # stacksize (16MB)
*   hard  stack   16384   # stacksize (16MB)

# Don't need to set the following for CM because the default
# limits are high enough already
# *   soft/hard   data          unlimited   # datasize
# *   soft/hard   core          unlimited   # coredumpsize
# *   soft/hard   memlock       unlimited   # memoryuse
EOFCMLIMIT
chmod 644 /etc/security/limits.d/99-controlm.conf

# Fix root's search PATH
sed -i -r 's|^(PATH=.*)|\1:/usr/local/bin|' /root/.bash_profile

# INSTALL INSPECTOR AGENT
# only do this once OS is patched and updated
cd /tmp
curl -O https://inspector-agent.amazonaws.com/linux/latest/install
bash install
rm ./install

# INSTALL/CONFIGURE AUTHENTICATION
# Install and configure NLNet Labs Unbound DNS resolver
yum -y install unbound
# Get fresh root hints
wget -S -N https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints
# Configure Unbound service
sed -i~ -e 's/harden-dnssec-stripped: yes/harden-dnssec-stripped: no/' \
        -e '/# root-hints: ""/a\
        root-hints: "/etc/unbound/root.hints"' \
        -e 's/module-config: "ipsecmod validator iterator"/#module-config: "ipsecmod validator iterator"/' \
        -e '/#module-config: "ipsecmod validator iterator"/a\
        module-config: "iterator"' \
        /etc/unbound/unbound.conf
# Forward zone configuration
cat <<EOFUNBOUND >/etc/unbound/conf.d/aws-uarizona.conf
forward-zone:
    name: "${AWSRegion}.compute.internal"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "amazonaws.com"
    forward-addr: 169.254.169.253
    #forward-no-cache: yes
forward-zone:
    name: "."
    forward-addr: 128.196.11.233
    forward-addr: 128.196.11.234
EOFUNBOUND
# Enable and restart the unbound service
# Set custom service dependencies
mkdir -p /etc/systemd/system/unbound.service.d
cat <<EOF1DNS > /etc/systemd/system/unbound.service.d/override.conf
[Unit]
Before=controlm-efs.mount

EOF1DNS
systemctl daemon-reload
systemctl enable unbound.service
systemctl restart unbound.service

# Override DHCP assigned DNS servers used for lookups
# https://aws.amazon.com/premiumsupport/knowledge-center/ec2-static-dns-ubuntu-debian/
cat <<EOFDNS >/etc/dhcp/dhclient.conf
timeout 300;
append domain-search "mosaic.arizona.edu", "uits.arizona.edu", "catnet.arizona.edu", "bluecat.arizona.edu";
supersede domain-name-servers 127.0.0.1;
EOFDNS
systemctl restart network.service

# INSTALL SOPHOS AGENT
mkdir -p /tmp/sophosinstaller
export AGENT_SOPHOS_URL=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/sophos/linux_url" --output text --query Parameter.Value)
echo "Downloading Sophos installer"
# Retry file retrieval until successful
RC=1
SOPHOSSETUP='/tmp/sophosinstaller/SophosSetup.sh'
for i in $(seq 1 5); do
  if [ $RC -eq 0 ]; then
    break
  fi
  /usr/bin/curl "$AGENT_SOPHOS_URL" -o $SOPHOSSETUP
  if [ -s $SOPHOSSETUP ]; then
    /bin/bash $SOPHOSSETUP --version
    RC=$?
  fi
  sleep 1
done
if [ $RC -eq 0 ]; then
  if [ "$ENVIRONMENT" != "dev" ]; then
    SOPHOSLOG="/var/log/cloud-init-sophos.log"
    /bin/bash $SOPHOSSETUP --group=Enterprise >$SOPHOSLOG 2>&1 &
    SOPHOSPID=$!
    echo "Sophos installation proceeding in the background on PID $SOPHOSPID"
    echo "Output redirected to $SOPHOSLOG"
  else
    echo "Dev environment. Skipping Sophos install at build time."
  fi
else
  EXITCODE=$RC
  SNSMSG="${SNSMSG}Failed to download Sophos installer. "
fi

# INSTALL TENABLE/NESSUS AGENT
#mkdir -p /tmp/nessusinstaller
#export AGENT_NESSUS_URL=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/linux_url" --output text --query Parameter.Value)
#export AGENT_NESSUS_HOST=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f1)
#export AGENT_NESSUS_PORT=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/host" --output text --query Parameter.Value | cut -d':' -f2)
#export AGENT_NESSUS_KEY=$(aws --region=${AWS::Region} ssm get-parameter --name "/agents/tenable/nessus/key" --with-decryption --output text --query Parameter.Value)
#aws s3 cp "$AGENT_NESSUS_URL" /tmp/nessusinstaller/
#rpm -vhU /tmp/nessusinstaller/NessusAgent*.rpm
#/opt/nessus_agent/sbin/nessuscli agent link --key=$AGENT_NESSUS_KEY --host=$AGENT_NESSUS_HOST --port=$AGENT_NESSUS_PORT --groups="CloudOps, CloudOps-Linux"
#systemctl enable nessusagent.service
#systemctl start nessusagent.service

# INSTALL AZURE OMS LOG ANALYTICS AGENT
# Get the OMS Agent
yum -y install curl wget
mkdir -p /tmp/omsinstaller
/usr/bin/curl 'https://raw.githubusercontent.com/Microsoft/OMS-Agent-for-Linux/master/installer/scripts/onboard_agent.sh' -o /tmp/omsinstaller/onboard_agent.sh
# Install the OMS Agent
cd /tmp/omsinstaller
export CLOPS_OMS_WORKSPACE=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/workspaceid" --with-decryption --output text --query Parameter.Value)
export CLOPS_OMS_PRIMKEY=$(aws --region=${AWSRegion} ssm get-parameter --name "/agents/omsagent/primarykey" --with-decryption --output text --query Parameter.Value)
/bin/bash /tmp/omsinstaller/onboard_agent.sh -w "$CLOPS_OMS_WORKSPACE" -s "$CLOPS_OMS_PRIMKEY" -d opinsights.azure.com

# Authentication configuration
# Install and configure sssd, kerberos PAM module,
# and oddjob packages to enable AD authentication
yum -y install oddjob-mkhomedir sssd nss_ldap pam_krb5 \
      oddjob openldap-clients realmd samba-common-tools \
      adcli krb5-workstation policycoreutils-python

mv /etc/nsswitch.conf /etc/nsswitch.conf.orig
cat <<EOF1 >/etc/nsswitch.conf
passwd:     files sss
shadow:     files
group:      files sss
sudoers:    files

hosts:      files dns

bootparams: nisplus [NOTFOUND=return] files

ethers:     files
netmasks:   files
networks:   files
protocols:  files
rpc:        files
services:   files

netgroup:   files

publickey:  nisplus

automount:  files
aliases:    files nisplus
EOF1

# Update kerberos logging destination
mkdir /var/log/krb5
chown sssd:sssd /var/log/krb5
chmod 750 /var/log/krb5
sed -i~ 's|/var/log/|/var/log/krb5/|g' /etc/krb5.conf

# Join AD using parameters pulled from SSM
export AD_JOIN_USER=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/user" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_PASSWD=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/password" --with-decryption --output text --query Parameter.Value)
export AD_JOIN_DOMAIN=$(aws --region=${AWSRegion} ssm get-parameter --name "/ad_join/domain" --with-decryption --output text --query Parameter.Value)
echo $AD_JOIN_PASSWD | realm join --user=$AD_JOIN_USER --computer-name=${ServerName} $AD_JOIN_DOMAIN

# Change authentication and homedir designation so it's not fully qualified
sed -i~ 's|use_fully_qualified_names = True|use_fully_qualified_names = False|;s|fallback_homedir = /home/%u@%d|fallback_homedir = /home/%u|' /etc/sssd/sssd.conf

# Set local login permitted groups
mv /etc/security/local.allowed_groups /etc/security/local.allowed_groups.orig
cat <<EOF5 >/etc/security/local.allowed_groups
root
wheel
localusers
is-oc
${GroupName}
EOF5

# Reconfigure PAM
mv /etc/pam.d/system-auth /etc/pam.d/system-auth.orig
cat <<EOF6 >/etc/pam.d/system-auth
auth        required      pam_listfile.so onerr=fail item=group sense=allow file=/etc/security/local.allowed_groups
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        sufficient    pam_sss.so use_first_pass
auth        required      pam_deny.so

account     required      pam_unix.so broken_shadow
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account     [default=bad success=ok user_unknown=ignore] pam_sss.so
account     required      pam_permit.so

password    requisite     pam_cracklib.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha256 shadow nullok try_first_pass use_authtok
password    sufficient    pam_sss.so use_authtok
password    required      pam_deny.so

session     required      pam_oddjob_mkhomedir.so skel=/etc/skel/ umask=0022
session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session     optional      pam_sss.so
EOF6

mv /etc/pam.d/password-auth /etc/pam.d/password-auth.orig
cp /etc/pam.d/system-auth /etc/pam.d/password-auth

systemctl restart sssd.service
systemctl restart nslcd.service
systemctl restart oddjobd.service

# Configure sudoers for the service owning group
cat <<EOF7 > /etc/sudoers.d/99-service-owners
# The provisioning/service owning group has full access
%${GroupName} ALL=(root)   ALL
EOF7
chmod 440 /etc/sudoers.d/99-service-owners

# Configure sudoers for the Control-M service accounts
cat <<EOF7CTM > /etc/sudoers.d/99-controlm
# The Control-M related accounts have access to some privileged commands
Cmnd_Alias CTM_CMDS = /home/controlm/ctm_agent/ctm/scripts/start-ag *, \\
                      /home/controlm/ctm_agent/ctm/scripts/shut-ag *, \\
                      /home/controlm/ctm_agent/ctm/scripts/rc.agent_user *, \\
                      /bin/systemctl start ctmagent.service, \\
                      /bin/systemctl stop ctmagent.service, \\
                      /bin/systemctl status ctmagent.service
controlm ALL=(root) NOPASSWD: CTM_CMDS
EOF7CTM
chmod 440 /etc/sudoers.d/99-controlm

# Configure sshd to permit login from authenticated users in the owning group
sed -i.orig \
    -e 's/^PasswordAuthentication .*$/PasswordAuthentication yes/' \
    -e 's/^GSSAPICleanupCredentials .*$/GSSAPICleanupCredentials yes/' \
    -e '/Subsystem sftp/a \' \
    -e '\nAllowGroups root wheel localusers is-oc '${GroupName} \
    /etc/ssh/sshd_config

systemctl restart sshd.service

# Import ssh public keys for admin users
mkdir -p /tmp/pubkeys
aws s3 sync s3://${S3BucketName}/pubkeys/ /tmp/pubkeys/
for u in ec2-user ctmem controlm; do
  sudo -i -u $u sh -c \
  "if [ ! -d ~/.ssh ]; then \
    mkdir ~/.ssh ; \
    chmod 700 ~/.ssh ; \
  fi ; \
  cat /tmp/pubkeys/* >> ~/.ssh/authorized_keys ; \
  chmod 644 ~/.ssh/authorized_keys"
done
rm -fr /tmp/pubkeys

# Any final automation of copy media and installation of Control-M components

# Install additional dependencies
## git and GNU Debugger
yum -y install git gdb
## GNU Parallel
(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - pi.dk/3) | bash

# Add Control-M DB connection information to config file
echo -en "DBENDPOINT=${DBEndpoint}\nDBPORT=${DBPort}\nDBNAME=${DBName}\n" > /etc/controlm.db
chmod 644 /etc/controlm.db
# Add Control-M file store connection information to config file
echo -en "S3BUCKET=${S3BucketName}\nEFSDNS=${EFSId}.efs.${AWSRegion}.amazonaws.com\n" > /etc/controlm.files
echo -en "SNS_NOTIFICATION_TOPIC=${SNS_NOTIFICATION_TOPIC}\n" >> /etc/controlm.files
if [ "$DRS3BucketName" != "" ]; then
  echo -en "DRS3BUCKET=${DRS3BucketName}\n" >> /etc/controlm.files
fi
chmod 644 /etc/controlm.files

# Placeholder for the EFS and ISO mounts
mkdir -p /controlm/efs
mkdir -p /controlm/iso
yum -y install amazon-efs-utils
. /etc/controlm.files ; echo "$EFSDNS:/     /controlm/efs    efs    defaults,_netdev          0   0" >> /etc/fstab
mount /controlm/efs/

# Ensure that the shared content folder exists on EFS
if [[ ! -d "/controlm/efs/shared/${ServerName}/controlm" || ! -d "/controlm/efs/shared/${ServerName}/ctmem" ]]; then
  mkdir -p /controlm/efs/shared/${ServerName}/{controlm,ctmem}
  chown :controlm /controlm/efs/shared/${ServerName}
  chmod 755 /controlm/efs/shared/${ServerName}
  chown controlm:controlm /controlm/efs/shared/${ServerName}/controlm
  chown ctmem:controlm /controlm/efs/shared/${ServerName}/ctmem
  chmod 755 /controlm/efs/shared/${ServerName}/{controlm,ctmem}
fi

#####
# BEGIN: Populate the installation media in EFS based on the media in S3
#####

# Get environment variables and create target and temp folders
. /etc/controlm.files

# Set initial package path information
declare -A SRC
declare -A DST
SRC[CTM]='/Control-M_9019200/EM/'
SRC[AFT]='/Control-M_9019100/AFT82300/'
SRC[PS8]='/Control-M_9019100/PEOPLESOFT/'
SRC[BIM]='/Control-M_9019100/BIM/'
SRC[FOR]='/Control-M_9019100/Forecast/'
SRC[SLS]='/Control-M_9019100/SelfService/'
SRC[WCM]='/Control-M_9019100/WCM/'
DST[ALL]='/controlm/efs/Control-M_9019200'
DST[OST]='CTM'
DST[AFT]='AFT-V8.2'
DST[PMC]='PS8-V9'
DST[CBM]='BIM'
DST[FOR]='Forecast'
DST[CAG]='SLS'
DST[WCM]='WCM'
TMP_ALL='/tmp/media'

if [ ! -d ${DST[ALL]} ]; then
  mkdir ${DST[ALL]}
fi
if [ ! -d $TMP_ALL ]; then
  mkdir $TMP_ALL
fi

for f in CTM AFT PS8 BIM FOR SLS WCM; do
  echo $S3BUCKET${SRC[$f]}
  aws s3 ls s3://$S3BUCKET${SRC[$f]}
  aws s3 sync s3://$S3BUCKET${SRC[$f]} $TMP_ALL
done

for f in OST AFT PMC CBM FOR CAG WCM; do
  if [ ! -d ${DST[ALL]}/${DST[$f]} ]; then
    mkdir -p ${DST[ALL]}/${DST[$f]}
  fi
  # Check for ISO media file
  FNAME=$(ls $TMP_ALL/DR${f}*.iso | tail -n 1)
  if [ "$FNAME" != "" ]; then
    mount -t iso9660 $FNAME /controlm/iso
    cp -ar /controlm/iso/* ${DST[ALL]}/${DST[$f]}/
    umount /controlm/iso
  fi
  # Check for tar/gzipped media file
  FNAME=$(ls $TMP_ALL/DR${f}*.z | tail -n 1)
  if [ "$FNAME" != "" ]; then
    pushd ${DST[ALL]}/${DST[$f]}/
    tar xzf $FNAME
    popd
  fi
  # Check for BIN patch files
  if [ -f $TMP_ALL/PA${f}*.BIN ]; then
    cp $TMP_ALL/PA${f}*.BIN ${DST[ALL]}/${DST[$f]}/
    chmod +x ${DST[ALL]}/${DST[$f]}/PA${f}*.BIN
  fi
done

# Set folder ownership
if [ -O ${DST[ALL]} ]; then
  chown -R controlm:controlm ${DST[ALL]}
fi

# Clean up temporary package space
rm -rf ${TMP_ALL}

# Symlink to current install media
if [ ! -h /controlm/efs/Control-M_current ]; then
  ln -s ${DST[ALL]} /controlm/efs/Control-M_current
fi

# Pull current silent installation templates
if [ ! -d /controlm/efs/cm-templates/ ]; then
  mkdir -p /controlm/efs/cm-templates
fi
aws s3 sync s3://$S3BUCKET/ctmtemplates /controlm/efs/cm-templates/

#####
# END: Populate the installation media in EFS based on the media in S3
#####

# Conditionally install the CTM Server and EM as secondary
if [ "${CMSecondaryInstallDesired}" == "true" ]; then
  echo "Installing server as Secondary"
  # Install CTM Server here
  # Install EM Server here
fi

# Install the service controls for EM and CTM Servers, and local CTM Agent

cat <<EOF8EM > /etc/systemd/system/ctmem.service
[Unit]
Description=Control-M Enterprise Manager
Wants=network-online.target
After=network-online.target
[Service]
Type=forking
User=ctmem
Environment=PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin:/home/ctmem/bin:/home/ctmem/emuaz/scripts
RemainAfterExit=yes
ExecStart=/home/ctmem/emuaz/scripts/start_em
ExecStop=/home/ctmem/emuaz/scripts/shut_em
[Install]
WantedBy=multi-user.target
EOF8EM
chmod 644 /etc/systemd/system/ctmem.service
/bin/systemctl daemon-reload
/bin/systemctl enable ctmem.service
#/bin/systemctl start ctmem.service

cat <<EOF8CTM > /etc/systemd/system/ctmserver.service
[Unit]
Description=Control-M Server
Wants=network-online.target
After=network-online.target
[Service]
Type=forking
User=controlm
Environment=PATH=/home/controlm/ctm_server/scripts:/home/controlm/ctm_server/health_check/bin:/home/controlm/ctm_server/exe_Linux-x86_64:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin:/home/controlm/ctmuaz/scripts:/usr/etc:/usr/sbin:/home/controlm/ctm_server/oracle/bin:/home/controlm/ctm_agent/ctm/scripts:/home/controlm/ctm_agent/ctm/exe
Environment=LD_LIBRARY_PATH=/home/controlm/ctm_server/oracle/lib:/home/controlm/ctm_server/exe_Linux-x86_64:/home/controlm/ctm_agent/ctm/exe
RemainAfterExit=yes
ExecStart=/home/controlm/ctm_server/scripts/start_ca
ExecStop=/home/controlm/ctm_server/scripts/shut_ca && /home/controlm/ctm_server/scripts/shut_ctm -CA
[Install]
WantedBy=multi-user.target
EOF8CTM
chmod 644 /etc/systemd/system/ctmserver.service
/bin/systemctl daemon-reload
/bin/systemctl enable ctmserver.service
#/bin/systemctl start ctmserver.service

cat <<EOF8AGENT > /etc/systemd/system/ctmagent.service
[Unit]
Description=Control-M Agent
[Service]
Type=forking
RemainAfterExit=yes
ExecStart=/home/controlm/ctm_agent/ctm/scripts/rc.agent_user start
ExecStop=/home/controlm/ctm_agent/ctm/scripts/rc.agent_user stop
[Install]
WantedBy=multi-user.target
EOF8AGENT
chmod 644 /etc/systemd/system/ctmagent.service
/bin/systemctl daemon-reload
/bin/systemctl enable ctmagent.service
#/bin/systemctl start ctmagent.service


# Install backup/DR services
cp -r $WORKINGPATH/efs-backup/backup2efs.sh /usr/local/sbin/.
chmod u+x /usr/local/sbin/backup2efs.sh
cp -r $WORKINGPATH/dr-backup /root/.
if [ ! -d /root/.aws ]; then
  mkdir /root/.aws
fi
mv /root/dr-backup/awsconfig /root/.aws/config
mv /root/dr-backup/s3backup.conf /etc/logrotate.d/s3backup.conf
chmod +x /root/dr-backup/*.sh
ln -s /root/dr-backup/folderlist-server.txt /root/dr-backup/folderlist.txt
ln -s /root/dr-backup/ignorefolderlist-server.txt /root/dr-backup/ignorefolderlist.txt

# Schedule the DR job if this is Prod
#
if [ "$DRS3BucketName" != "" ]; then
  echo "2 23 * * *   . /etc/controlm.files ; /root/dr-backup/s3backup.sh /home \${DRS3BUCKET} /controlm/efs/shared/${ServerName}" | crontab -
fi
# Schedule the local backup job in all environments
#
echo "#15 22 * * *   /usr/local/sbin/backup2efs.sh > /var/log/backup2efs.log 2>&1" | crontab -

# Last line
touch /var/lib/cloud/instance/done-initial.txt

# Debug off
#set +x
