#! /bin/bash

# Initial sync of relevant folders to EFS.
# Do all this as root, as no other account has sufficient access.
# No sign of cron jobs or init scripts to be backed up, so we will only back up the Control-M homedirs.

# Capture all positional parameters.
OPTS="$*"

# Check that we're running as root, exit otherwise.
if [ $(id -u) = 0 ]; then
  echo "Running as root"
else
  echo "Not running as root. Make sure to run this script as root. Exiting."
  exit 1
fi

# Get the short hostname.
NAME=$(hostname -s)
if [ "$NAME" == "" ]; then
  echo "Unable to determine hostname. Exiting."
  exit 1
fi

# Ensure target directory is defined and mounted
BASEDIR="/controlm/efs"
if [ ! -d ${BASEDIR} ] || [[ $(grep -q "${BASEDIR}" /proc/mounts) ]]; then
  echo "Missing $BASEDIR. Exiting."
  exit 1
fi

mkdir -p ${BASEDIR}/snapshots/${NAME}

# Create a lock file before starting sync or complain that it already exists
if [ ! -f ${BASEDIR}/snapshots/${NAME}.lock ]; then
  touch ${BASEDIR}/snapshots/${NAME}.lock
  # Do the data sync with delete on destination folder
  if [ -x /usr/bin/rsync ]; then
    rsync --stats --delete -uavh /home/ctmem /home/controlm ${BASEDIR}/snapshots/${NAME}
  else
    echo "rsync is missing. Exiting."
    exit 1
  fi

  # Initial sync is about 1:20:00 to complete. Subsequent syncs take about 05:00 to complete.

  # Snapshot of installed packages for reference:
  rpm -qa | sort > ${BASEDIR}/snapshots/${NAME}/installed_packages.log

  # Remove lock file
  rm ${BASEDIR}/snapshots/${NAME}.lock
else
  echo "Lock file ${BASEDIR}/snapshots/${NAME}.lock already exists. Exiting."
  exit 1
fi

if [ ! -d ${BASEDIR}/snapshots/${NAME/-b/-a} ]; then
  echo "Missing peer backup folder ${BASEDIR}/snapshots/${NAME/-b/-a}. Exiting."
  exit 1
fi

# Once the backups have been done for both instances, on the -b node,
# run a diff of both trees if specified in the run options
if [[ ${NAME} =~ .*-b ]] && [[ ${OPTS} =~ .*--diff.* ]]; then
  counter=0
  echo -ne "Checking that ${NAME/-b/-a} has completed or waiting for up to 60 min: "
  while [ -f ${BASEDIR}/snapshots/${NAME/-b/-a}.lock ] && [ $counter -lt 60 ]; do
    echo -n "."
    sleep 1m
    ((counter++))
  done
  if [ -f ${BASEDIR}/snapshots/${NAME/-b/-a}.lock ]; then
    echo -ne "\nLock file ${BASEDIR}/snapshots/${NAME/-b/-a}.lock still exists. Exiting.\n"
    exit 1
  fi

  # Generate diff of filesystems
  echo "Comparing filesystems between ${NAME/-[ab]/-a} and ${NAME/-[ab]/-b}"
  diff -q -r ${BASEDIR}/snapshots/${NAME/-[ab]/-a} ${BASEDIR}/snapshots/${NAME/-[ab]/-b} > ${BASEDIR}/snapshots/serverdiff.log
  echo "See ${BASEDIR}/snapshots/serverdiff.log for full details."

  # Checking what's in the log file, filtering out uninteresting files:
  echo "Interesting files that do not match:"
  egrep -v '\.log\b|\.bak\b|\.csv\b|\.[0-9]+\b|^Only in' ${BASEDIR}/snapshots/serverdiff.log
  echo -ne "\nDone\n"
fi

exit 0
