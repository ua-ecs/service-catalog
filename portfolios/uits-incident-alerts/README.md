# UITS Incidents Alerts Stack

The UITS Incidents Alerts Stack is a production-ready blueprint for creating the pre-requisite AWS resources for the UITS Incident Alerts application, then deploying the application via Elastic Beanstalk.

## Goals
* Provide a production-ready blueprint for deploying the entire UITS Incidents Alerts application environment
* Provide an example blueprint that can be used for other moderately-complex Elastic Beanstalk applications that have additional AWS dependencies

## Assumptions
* will be deployed in a VPC consisting of public and private subnets
  * Elastic Beanstalk app will be deployed with ELB in public subnet and EC2 instance(s) in private subnet
  * EC2 instances in private subnet must be able to make outbound connections to necessary ports (e.g., 80, 443) for external API calls
* an S3 bucket has been created; this bucket should contain the following:
  * a ZIP archive of the Elastic Beanstalk application source
  * the public certificate for SAML SP; this should be named `server-cert.pem`
  * the **encrypted** private key for the SAML SP, named `server-pvk.pem.enc`; this should be encrypted using the `utils/genkey.js` utility provided in the [application source](https://bitbucket.org/ua-uits-eas/uits-incident-manager)
  * the **encrypted** Express session secret, named `session-secret.txt.enc`; this should be encrypted using the `utils/genkey.js` utility provided in the [application source](https://bitbucket.org/ua-uits-eas/uits-incident-manager)
* SSL certificate for the ELB has been created
* DNS aliasing for the Elastic Beanstalk-deployed application will be handled externally
* Registration with the UA Shibboleth IdP has been performed in [SIAApps](https://siaapps.uits.arizona.edu/home/?tab=shibbolethtab)

## Additional Technologies/Resources Used
* *Amazon SNS* - used for delivering notifications via email, SMS, and Amazon SQS
* *Amazon SQS* - used to receive SNS messages, which are posted to a Twitter account
* *Amazon CloudWatch Log Stream* - used for logging error messages from application
* *Amazon DynamoDB* - used for application data storage

## Usage

1. If not already done, generate the decryption key and encrypted files (discussed under **Assumptions**) then upload these, along with the application source ZIP archive, to S3 bucket.
2. Modify the environment variables in `stack-env.sh`:
  * `AWS_ACCESS_KEY_ID` - access key for IAM account under which CloudFormation stack will be created
  * `AWS_SECRET_ACCESS_KEY` - secret key for IAM account creating CloudFormation stack will be created
  * `AWS_DEFAULT_REGION` - AWS region in which to create CloudFormation stack
  * `SSL_CERT_PATH` - local filesystem path to SSL server certificate to be used with Elastic Load Balancer
  * `SSL_KEY_PATH` - local filesystem path to SSL server certificate private key
  * `SSL_CERT_CHAIN` - local filesystem path to intermediate/root CA chain that signed SSL certificate; file should start with most immediate signing certificate and end with self-signed root certificate
  * `SERVER_CERT_NAME` - name under which to upload certificate to AWS.
3. Modify the tag values, located in `uits-incident-alerts-tags.json`, which will be passed to the CloudFormation template.
4. Modify the CloudFormation template input parameters in `uits-incident-alerts-params.json`:
  * `S3BucketParam` - bucket for Elastic Beanstalk application ZIP archive
  * `S3FolderParam` - folder prefix for resources stored in `S3BucketParam` (must end in trailing '/')
  * `S3KeyParam` - filename for application ZIP archive (including folder prefix, if any)
  * `DynamoDbTblPfxParam` - string that will be prefixed to all DynamoDB tables; used for scoping permissions
  * `VPCIdParam` - VPC ID into which to create Elastic Beanstalk stack
  * `PrivateSubnetsParam` - comma-separated list of private subnets in VPC; Elastic Beanstalk EC2 instances will be launced in these subnets
  * `PublicSubnetsParam` - comma-separated list of public subnets in VPC; Elastic Beanstalk ELB instances will be launced in these subnets
  * `AdminGroupParam` - name of Grouper group containing application administrators
  * `AlertBaseURLParam` - base URL for constructing links to incident alerts
  * `CloudWatchLogStreamParam` - name of CloudWatch log stream (will be auto-created by application)
  * `DebugParam` - comma-separated list of application "facilities" to debug (debug statements will appear in nodejs log on EC2 instances); valid values are ("http", "api", "scheduler")
  * `DecryptKeyParam` - base64-encoded key used to encrypt/decrypt Shibboleth private key and Express session secret
  * `DomainParam` - FQDN of site (e.g., "incidents.uits.arizona.edu"); needed for PassportJS SAML config
  * `DuoHostParam` - Duo API hostname
  * `DuoIkeyParam` - Duo IKEY (used for admin login when Shibboleth is down)
  * `DuoSkeyParam` - Duo SKEY (used for admin login when Shibboleth is down)
  * `EntityIDParam` - SAML entity ID of SP
  * `GoogleAPIKeyParam` - Google Apps API key that has access to the Google URL Shortener service
  * `LDAPBindDNParam` - DN of account used to connect to EDS LDAP
  * `LDAPBindPwParam` - Password for `LDAPBindDNParam`
  * `LDAPSearchBaseParam` - LDAP search base for EDS
  * `LDAPURLParam` - LDAP URL to connect to EDS (e.g., `ldaps://eds.arizona.edu`)
  * `TwitterAccessTokenKeyParam` - access token key for Twitter account
  * `TwitterAccessTokenSecretParam` - secret for Twitter access token key
  * `TwitterConsumerKeyParam` - consumer key for Twitter account
  * `TwitterConsumerSecretParam` - secret for Twitter consumer key
5. Run the `create-stack.sh` shell script. This script will upload the SSL certificate to the AWS account (if not already present), and kick off the creation of the CloudFormation stack.
6. Upon successful creation of the stack, an output parameter, **ApplicationEndpoint**, will be visible In the AWS CloudFormation console. This is the DNS name to which your CNAME record should point.
