#!/bin/bash

export AWS_ACCESS_KEY_ID=aws_access_key
export AWS_SECRET_ACCESS_KEY=aws_secret_access_key
export AWS_DEFAULT_REGION=us-west-2
export SSL_CERT_PATH=/Users/gary/Downloads/incidents_uits_arizona_edu_cert.cer
export SSL_KEY_PATH=/Users/gary/incidents_uits_arizona_edu_rsa.key
export SSL_CERT_CHAIN=/Users/gary/Downloads/incidents_uits_arizona_edu_interm.cer
export SERVER_CERT_NAME=uits-incidents-cert
