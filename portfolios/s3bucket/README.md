# Basic S3 Bucket

This blueprint creates an S3 Bucket and an IAM account that has full access to it.

## Parameters

* BucketName
* OwnerTag
* OwnerNetidTag
* ProjectNameTag

## Outputs

* BucketName
* AccessKey For New IAM User
* SecretKey For New IAM User


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
