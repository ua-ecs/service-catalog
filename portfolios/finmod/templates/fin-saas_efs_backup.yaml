---
# EFS Backup Plan and Vault CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will build an AWS backup plan and vault to be used for an EFS volume.
# 

AWSTemplateFormatVersion: '2010-09-09'
Description: Backup Plan and Vault Stack

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Application Information
      Parameters:
      - AppSlug
    - Label:
        default: Backup Plan
      Parameters:
      - StartWindowMinutes
      - CompletionWindowMinutes
      - MoveToColdStorageAfterDays
      - DeleteAfterDays
    - Label:
        default: Tags
      Parameters:
      - TagService
      - TagName
      - TagEnvironment
      - TagCreatedBy
      - TagContactNetId
      - TagAccountNumber
      - TagTicketNumber
      - TagResourceFunction
    ParameterLabels:
      AppSlug:
        default: 'Application Slug'

# ### Parameters
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  AppSlug:
    MinLength: '3'
    Type: String
    Default: financials-saas
    Description: Short application slug, ie 'myapp-efs' or 'myapp-nonprod'. Lowercase
      letters, numbers and dashes only
    AllowedPattern: "[a-z0-9-]*"
  StartWindowMinutes:
    Description: "The amount of time in which a backup needs to start. (Relative to hard-coded midnight time in the template.)"
    Type: Number
    Default: 60
  CompletionWindowMinutes:
    Description: The amount of time to wait for the backup to be completed."
    Type: Number
    Default: 300
  MoveToColdStorageAfterDays:
    Description: "The number of days after creation that a recovery point is moved to cold storage."
    Type: Number
    Default: 30
  DeleteAfterDays:
    Description: "The number of days after creation that a recovery point is deleted. Must be greater than MoveToColdStorageAfterDays."
    Type: Number
    Default: 120
  TagService:
    Description: Service name (from the service catalog) that is utilizing this resource
    Type: String
    Default: Financials SaaS
  TagName:
    Description: Descriptive identifier of resource.
    Type: String
    Default: financials-saas-efs-backup
  TagEnvironment:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'. 
    Type: String
    Default: dev
  TagCreatedBy:
    Description: NetID of the user who created this resource
    Type: String
    Default: hlo
  TagContactNetId:
    Description: NetID of the person to contact for information about this resource
    Type: String
    Default: hlo
  TagAccountNumber:
    Description: Financial system account number for the service utilizing this resource
    Type: String
    Default: 1192801
  TagTicketNumber:
    Description: Ticket number that this resource is for
    Type: String
    Default: FINMOD-553
  TagResourceFunction:
    Description: Human-readable description of what function this resource is providing
    Type: String
    Default: Financials SaaS EFS Backup Plan and Vault

# Resources
# ---------
#
# These are all of the actual AWS resources created for this application.
#
Resources:

# #### Backup Vault
#
# Create a backup vault to be used with the AWS Backup service.
  BackupVault:
    Type: AWS::Backup::BackupVault
    Properties: 
      # AccessPolicy: Json
      BackupVaultName: !Sub "${AppSlug}-efs-backup-vault"
# Can revisit LockConfiguration and Notifications later
#    LockConfiguration:
#      LockConfigurationType
#    Notifications: 
#      NotificationObjectType

# #### Backup Plan
#
# Create a backup plan to be used with the AWS Backup service.
# Note that time is UTC.
# See https://aws.amazon.com/premiumsupport/knowledge-center/aws-backup-cloudformation-template/
  BackupPlanForEFS:
    Type: AWS::Backup::BackupPlan
    DependsOn: BackupVault
    Properties: 
      BackupPlan: 
        BackupPlanName: !Sub "${AppSlug}-efs-backup-plan"
        BackupPlanRule: 
        - RuleName: "daily-backup"
          ScheduleExpression: "cron(0 7 * * ? *)" # https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html?icmpid=docs_console_unmapped#CronExpressions
          StartWindowMinutes: !Ref StartWindowMinutes
          CompletionWindowMinutes: !Ref CompletionWindowMinutes
          TargetBackupVault: !Sub "${AppSlug}-efs-backup-vault"
          EnableContinuousBackup: "false"
          Lifecycle:
            MoveToColdStorageAfterDays: !Ref MoveToColdStorageAfterDays
            DeleteAfterDays: !Ref DeleteAfterDays
          RecoveryPointTags: 
            name : !Sub "${AppSlug}-efs-backup-recoverypoint"
            service : !Ref TagService
            environment : !Ref TagEnvironment
            createdby : !Ref TagCreatedBy
            contactnetid : !Ref TagContactNetId
            accountnumber : !Ref TagAccountNumber
            ticketnumber : !Ref TagTicketNumber
            resourcefunction : !Ref TagResourceFunction
      BackupPlanTags:
          name : !Sub "${AppSlug}-efs-backup-plan"
          service : !Ref TagService
          environment : !Ref TagEnvironment
          createdby : !Ref TagCreatedBy
          contactnetid : !Ref TagContactNetId
          accountnumber : !Ref TagAccountNumber
          ticketnumber : !Ref TagTicketNumber
          resourcefunction : !Ref TagResourceFunction

# #### IAM Role
#
# Create an IAM role that has the permission to perform the backup.
  BackupRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - "backup.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - !Sub "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
        - !Sub "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"

# #### Backup Selection
#
# Define what is backed up.
  BackupSelection:
    Type: AWS::Backup::BackupSelection
    Properties: 
      BackupPlanId: !Ref BackupPlanForEFS
      BackupSelection: 
        IamRoleArn: !GetAtt BackupRole.Arn
        SelectionName: !Sub "${AppSlug}-efs-backup-selection"
        ListOfTags: 
          - ConditionType: STRINGEQUALS
            ConditionKey: 'aws:ResourceTag/awsbackup'
            ConditionValue: 'true'

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  VaultName:
    Value: !Ref BackupVault
    Export:
      Name: !Sub "${AppSlug}-efs-backup-vault"

  PlanName:
    Value: !Ref BackupPlanForEFS
    Export:
      Name: !Sub "${AppSlug}-efs-backup-plan"