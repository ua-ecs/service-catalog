##########################################################################
# dr-snapshot-shuffle.sh
# This program will be used to copy snapshots from the us-west-2 regtion
# to the us-west-1 region
#
#  - Find all snapshots in the us-west-2 region without "save" in the snapshot_id
#  - Determine if that snapshot exists in the us-east-1 region
#    - If it exists then
#      - If it's in available state then delete the snapshot from the us-west-2 region
#      - If it's not in avaiable state then do nothing (it's currently being copied)
#    - If it does not exist kick off the copy of the snapshot to us-east-1
#  - Clean up snapshots in the us-east-1 region (target-region)
#    - Describe all snapshots in available status in target-region that do not have "save" in the Snapshot ID
#    - Keep only the latest 7 snapshots for each Database Instance, delete the others
#  - Snapshots with "save" in the Snapshot ID will not be touched
##########################################################################
SOURCE_REGION=us-west-2
TARGET_REGION=us-east-1
TARGET_KF_KMSKEYID="428af4e9-da4d-47b8-a574-df7a9a39457b"

# Frist find all snapshots in the Source Region
# and copy all that do not exist in the Target Region
# Delete them is they are in available status in the Target Region
aws rds describe-db-snapshots \
  --region ${SOURCE_REGION} \
  --output text \
  --snapshot-type manual \
  --query 'DBSnapshots[?Status==`available`].[DBInstanceIdentifier,DBSnapshotIdentifier,SnapshotCreateTime,InstanceCreateTime,DBSnapshotArn,EngineVersion]' \
   | sort -k1,1 -k3,3 -r | while read source_snapshot
do
  source_db_snapshot_id=$(echo $source_snapshot|awk '{print $2}')
  source_db_snapshot_arn=$(echo $source_snapshot|awk '{print $5}')
  source_db_snapshot_version=$(echo $source_snapshot|awk '{print $6}')
  source_db_snapshot_version=${source_db_snapshot_version:0:2}
  #Do not include snapshots with "save" in the Snapshot ID
  echo $source_db_snapshot_id | grep "save" &> /dev/null
  if [ "$?" != 0 ]; then
    #Find the status of the target 
    target_snap_status=$(aws rds describe-db-snapshots \
      --region ${TARGET_REGION} \
      --output text \
      --snapshot-type manual \
      --query 'DBSnapshots[*].[DBSnapshotIdentifier,Status]' | grep ${source_db_snapshot_id} | awk '{print $2}')

    if [ -z ${target_snap_status} ]; then
      target_snap_status="none"
    fi

    if [ "${target_snap_status}" = "available" ]; then
      #If the target status is avaialbe then the snapshot has been copied and the target snapshot can be removed
      OUTPUT=$(aws rds delete-db-snapshot \
        --region ${SOURCE_REGION} \
        --output text \
        --db-snapshot-identifier ${source_db_snapshot_id} 2>&1)
      if [ "$?" != 0 ]; then
        echo "Error encountered when deleting snapshot from ${SOURCE_REGION} region - ${OUTPUT}"
      else
        echo "Removed ${source_db_snapshot_id} from ${SOURCE_REGION} region"
      fi
    elif [ "${target_snap_status}" = "none" ]; then
      #If the target status is none then the snapshot needs to be copied to the Target Region

      #Determin what Option Group that is needed for the copy
      if [ "${source_db_snapshot_version}" = "11" ]; then
        OPTION_GROUP="timezone11-2"
      elif [ "${source_db_snapshot_version}" = "12" ]; then
        OPTION_GROUP="timezone-12-1"
      else 
        OPTION_GROUP="timezone19"
      fi

      OUTPUT=$(aws rds copy-db-snapshot \
        --region ${TARGET_REGION} \
        --source-region ${SOURCE_REGION} \
        --output text \
        --source-db-snapshot-identifier ${source_db_snapshot_arn} \
        --target-db-snapshot-identifier ${source_db_snapshot_id} \
        --copy-tags \
        --option-group-name ${OPTION_GROUP} \
        --kms-key-id ${TARGET_KF_KMSKEYID})
      if [ "$?" != 0 ]; then
        echo "Error encountered when copying snapshot ${source_db_snapshot_id} to ${TARGET_REGION} region - ${OUTPUT}"
      else
        echo "Copy of snapshot ${source_db_snapshot_id} to ${TARGET_REGION} region has begun..."
      fi
    else
      #Otherwise the snapshot is currently being copied so don't need to do anything
      echo "Snapshot ${source_db_snapshot_id} is currently being copied to ${TARGET_REGION} region"
    fi
  fi
done

#Now clean up the Target Snapshots, only 7 snapshots per DB Instance should exist.  Delete the older snapshots
aws rds describe-db-snapshots \
  --region ${TARGET_REGION} \
  --output text \
  --snapshot-type manual \
  --query 'DBSnapshots[?Status==`available`].[DBInstanceIdentifier,DBSnapshotIdentifier,SnapshotCreateTime,InstanceCreateTime,DBSnapshotArn]' \
   | sort -k1,1 -k3,3 -r | while read target_snapshot
do

  #Grab the first field from the rds snapshot output, which is the db instance name
  db_instance_name=`echo $target_snapshot|awk '{print $1}'`

  #Grab the second field from the rds snapshot output, which is the snapshot name
  snapshot_name=`echo $target_snapshot|awk '{print $2}'`

  echo $snapshot_name | grep "save" &> /dev/null
  if [ "$?" != 0 ]; then
    #If this is the first row or the db instance name has changed then reset the counter
    if [ ! "$db_instance_name" == "$prev_db_instance" ]; then
      snapshot_cnt=0
    fi

    #Increment the snapshot counter
    let "snapshot_cnt++"

    #If the snapshot count is greater than 7 
    #and the snapshot is older than the cut date, remove it
    if [ $snapshot_cnt -gt 7 ]; then
      echo "will delete the snapshot $snapshot_name from the ${TARGET_REGION} region"
      delete_output=$(aws rds delete-db-snapshot --db-snapshot-identifier $snapshot_name 2>&1)
      if [ $? -ne 0 ]; then
        echo "Error deleting old manual rds snapshot--$delete_output"
      fi
    fi
    prev_db_instance=$db_instance_name
  fi

done
