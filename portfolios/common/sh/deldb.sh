#
# TITLE......: deldb.sh
# LOCATION...:
# PARAMETER..:
#    /INPUTS.:
#
# USAGE......: deldb.sh [-t Target DB] [-s Take final snapshot]
# OUTPUT.....:
# REQUIRES ..: jq
#
#   jq is a lightweight and flexible command-line JSON processor
#   installed using the following ...
#   wget -qP /usr/local/bin https://stedolan.github.io/jq/download/linux64/jq && chmod +x /usr/local/bin/jq
#
# DESCRIPTION:
#   delete AWS RDS and any snapshots
#
#   http://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_RestoreDBInstanceToPointInTime.html
#
#   aws rds delete-db-instance
#       --db-instance-identifier $TRGSID
#   	--skip-final-snapshot
#

# control output from AWS CLI 
CLIOUT=json
#export AWS_DEFAULT_PROFILE=kuali-nonprod

#*******************************************************************************
# function: describe_db_instance
#
# This function returns information about a provisioned RDS instance
#
# INPUTS:  first argument - instance identifier
#
# RETURNS: 
#
#*******************************************************************************
function describe_db_instance
{
  JSON=`aws rds describe-db-instances --db-instance-identifier $1 --output=$CLIOUT 2>&1` 
  if [ $? != 0 ]; then
    return 1
  fi ;
  return 0
}

#*******************************************************************************
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
  typeset MSG=$1

  echo
  echo "ERROR: ${MSG}"
  echo "aborting . . ."
  echo
  exit 255
}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
  echo "Usage: deldb.sh"
  echo "       [-t target SID]"
  echo "Options:"
  echo "       -t = target database"
  echo "       -s = Optional - if set then final snapshot will be taken"
}

#*******************************************************************************
#
# function: validate_arguments
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
  # -- make sure the target is specified
  if [ "${TRGSID}" = "" ]; then
    print_usage
    error_abort "target database instance must be specified"
  fi

  # name:
  # cannot end with a hyphen
  if [ "${TRGSID: -1}" == "-" ] ||
    # contains no special characters
    ! [[ ${TRGSID:0:1} =~ ^[a-zA-Z]+$ ]] ||
    # does not contain 2 consecutive hyphens
    [[ ${TRGSID} = *"--"* ]] ; then
    error_abort "target name must begin with an alphabetic character, cannot end with a hyphen or contain two consecutive hyphens"
  fi

  # less than 63 in length 
  if [ ${#TRGSID} -ge 63 ] ; then
    error_abort "target name must contain from 1 to 63 alphanumeric characters or hyphens"
  fi

  # Do not remove databases with any variation of prd in it
  echo ${TRGSID}|grep -i prd
  #If this statement returns a 0 error code it means that PRD is contained in the db name
  if [ $? = 0 ]; then
    error_abort "Target Database Passed contains PRD or prd meaning it's a production database"
  fi
}

#*******************************************************************************
#
# function: is_instance_deleted
#
# Deleting an RDS instance and can take longer than 30 mins causing the -
#
# INPUTS: none
#
# RETURNS: none
#
#*******************************************************************************
function is_instance_deleted()
{
  cnt=0
  while [ $cnt -lt 3 ]
  do
    CLI=`aws rds wait db-instance-deleted --db-instance-identifier $TRGSID 2>&1`

    if [ $? = 0 ]; then
      return 0
      break
    fi
    cnt=$(( cnt+1 ))
  done
  return 1
}

#*******************************************************************************
#
# function: get_instance_status
#
# INPUTS:  first argument - JSON block
#
# RETURNS: none
#
#*******************************************************************************
function get_instance_status
{
  DBINSTANCESTATUS=`echo $1 |jq --raw-output .DBInstances[].DBInstanceStatus`
}

#*******************************************************************************
#
# function: parse_msg
#
# INPUTS:  first argument - 
#
# RETURNS: none
#
#*******************************************************************************   
function parse_msg
{
  ERRMSG=`echo "$1"|sed '/^$/d'`
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
TAKE_SNAP="N"

while getopts t:s arguments; do
  case $arguments in
    t) 
      TRGSID=${OPTARG}
      ;;
    s) 
      TAKE_SNAP="Y"
      ;;
   \?)
     print_usage
     echo
     error_abort "option -${OPTARG} is not a valid argument"
     exit 255
  esac
done

validate_arguments

# target instance must exist
describe_db_instance $TRGSID

if [ "$?" = "0" ]; then
  JSONSRC="$JSON"

  if [ "$TAKE_SNAP" = "N" ]; then
    skip_snap="--skip-final-snapshot"
  else
    skip_snap="--no-skip-final-snapshot --final-db-snapshot-identifier ${TRGSID}-final-`date +\"%Y%m%d%H%M%S\"`"
  fi
  
  # delete RDS instance
  CLI=`aws rds delete-db-instance --db-instance-identifier "$TRGSID" ${skip_snap} --output=$CLIOUT 2>&1`

  if [ "$?" != "0" ]; then
    parse_msg "$CLI"
    error_abort "$CLI"
  fi

  if is_instance_deleted; then 
    # -- if a final snapshot was not taken remove all remaining snapshots
    if [ "$TAKE_SNAP" = "N" ]; then
      CLI=`aws rds describe-db-snapshots \
           --db-instance-identifier "$TRGSID" \
           --snapshot-type manual \
           --output=json \
           | jq .DBSnapshots[].DBSnapshotIdentifier --raw-output 2>&1`

      if [ "$?" != "0" ]; then
        parse_msg "$CLI"
        error_abort "$ERRMSG"
      # If $CLI is not empty then remove the snapshots
      elif ! [ -z $CLI ]; then
        for SNAP in `echo $CLI`; do 
          CLI=`aws rds delete-db-snapshot --db-snapshot-identifier $SNAP --output=$CLIOUT 2>&1`
          if [ "$?" != "0" ]; then
            parse_msg "$CLI"
            error_abort "$CLI"
          fi
        done 
      fi
    fi
  fi
else
  parse_msg "$JSON"
  error_abort "$ERRMSG" 
fi
