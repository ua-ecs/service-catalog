#!/bin/bash
#*******************************************************************************
#
# TITLE......: blackout.sh
# PARAMETER..:
#    /INPUTS.:
#              -t Target RDS instance
#              -d delete blackout
#
#              blackout.sh -t peoplesoft-eldev
#              blackout.sh -t peoplesoft-eldev -d 
#
# OUTPUT.....:
# REQUIRES ..: Java JDK 1.7 or higher 
#              EMCLI toolkit
#              MGMT_USER requires select access on a custom view 
#              MGMT_BLOCKED_AGENTS_V
#
# DESCRIPTION:
#
#   The function of this script to place the hostname on which the RDS
#   instance is running under blackout. Placing the host including all
#   of it's targets will prevent alert notifications from being sent
#
#   EMCLI is setup using the -autologin option which automatically 
#   re-establishes the session when a timeout occurs
#
#   export JAVA_HOME=/home/ec2-user/oracle/jdk/jdk1.7.0_80
#   emcli setup -url="https://hostname:port#/em" \
#   -username=username \
#   -password=password \
#   -dir=location \
#   -autologin 
#
#   This also means a password is not required before issuing
#   any EMCLI commands
#
#   emcli add_blackout_reason -name="RDS: Database Refresh"
#
#*******************************************************************************

# emcli requires JAVA 1.7 or greater
export JAVA_HOME=/home/ec2-user/oracle/jdk/jdk1.7.0_80

#*******************************************************************************
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
  typeset MSG=$1

  echo
  echo "ERROR: ${MSG}"
  echo "aborting . . ."
  echo
  exit 255
}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
  echo "Usage: build_ps_env.sh"
  echo "Options:"
  echo "       -t (required) = target RDS instance"
  echo "       -d (optional) = delete blackout"
}

#*******************************************************************************
#
# function: validate_arguments
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
  # -- make sure the target databases are specified
  if [ "${TRGSID}" = "" ]; then
    error_abort "target database instance must be specified"
  fi
}

#*******************************************************************************
#
# function: is_agent_blocked
#
# Management Agent can be reconfigured using target information present in the 
# Management Repository. Resynchronization pushes all targets and related 
# information from the Management Repository to the Management Agent and then 
# unblocks the Agent.
#
# The agent is blocked whenever it is out-of-sync with the repository  
# This means, the blocked agent will not be able to upload any alerts or 
# metric data to the OMS, but it does continue to collect monitoring data
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function is_agent_blocked
{
  BLOCKED=`emcli list -sql="select decode(nvl(count(*),0),1,0,1) blocked \
from sysman.mgmt_blocked_agents_v where target_name='${AGENT}'" -noheader -format="name:csv"`
  return $BLOCKED
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
DELETE_BLACKOUT="N"

while getopts :t:d arguments; do
  case $arguments in
    t)
      TRGSID=${OPTARG}
      ;;
    d)
      DELETE_BLACKOUT="Y"
      ;;
   \?)
     print_usage
     echo
     error_abort "option -${OPTARG} is not a valid argument"
     exit 255
  esac
done

validate_arguments

# locate TRGSID 
CLIOUT=`emcli get_targets -targets="${TRGSID}:%" -noheader 2> /dev/null`

# verify connection to emcli established
if [ $? = 0	]
then

  # found target RDS instance
  if [ -n "$CLIOUT" ]
  then
    #
    # get agent and hostname from emcli list -resource output
    # https://kfprd.csqvaa9kcdo8.us-west-2.rds.amazonaws.com:3872/emd/main/
    #
    AGENT=`emcli list -resource="Targets" -search="TARGET_NAME='${TRGSID}'" \
-noheader -format="name:csv"| awk -F"," '{print $10}' |awk -F "//" '{print $2}'|awk -F '/' '{print $1}'`
    #
    # get hostname
    #
    HOST=`echo $AGENT|awk -F ":" '{print $1}'`

    # located host on which RDS instance is running
    if [ -n "$HOST" ]
    then

      #
      # 16MAR2018
      # the get_blackouts verb is truncating the blackout name which is an issue 
      # with RDS given names
      #
      #BLACKOUT=`emcli get_blackouts | grep ${HOST}|wc -l`
      #

      BLACKOUT=`emcli get_blackouts -target="${HOST}:host" -noheader |grep "RDS:Database Refresh" |wc -l`
      #echo " ....: $BLACKOUT"

      if [ "${DELETE_BLACKOUT}" == "Y" ]; then
        #
        # verify the host is not currently under blackout
        # if the instance is under blackout check there's not an entry in 
        # the mgmt_blocked_agents_v view
        #
        #if [ -n "$BLACKOUT" ]; then
        if [ $BLACKOUT -gt 0 ]; then  
          #
          # if the agent is blocked we need to initiate an agent resync
          #
          if is_agent_blocked ; then
            #echo "resync ${AGENT} ..."
            emcli resyncAgent -agent="${AGENT}"
          fi 
          
          #echo "stopping blackout on ${HOST} ..."
          emcli stop_blackout -name="${HOST}"    
          emcli delete_blackout -name="${HOST}"
        else
          echo "${HOST} is not under blackout"  
        fi
      else
        #if [ -z "$BLACKOUT" ]; then
        if [ $BLACKOUT -gt 0 ]; then
          echo "${HOST} is currently under blackout"
        else

          #
          # creating blackout on ${HOST} ..."
          #
          emcli create_blackout -name="${HOST}" \
            -add_targets="${HOST}:host" \
            -reason="RDS:Database Refresh" \
            -propagate_targets \
            -schedule=duration:-1
        fi
      fi
    fi
  else 
    error_abort "RDS instance (${TRGSID}) instance not found"
  fi                                                 
else
  error_abort "Unable to connect to EM CLI"  
  exit
fi

