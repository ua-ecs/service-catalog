CloudFormation Account Info (VPC) Custom Resource Lambda Function
===========================================================

To reduce the required parameters that need to be entered into a CloudFormation template at deploy time, this Lambda function can be called as a custom resource in CloudFormation templates in order to return relevant account data (but really, just VPC data).

An example resource follows:

```
    Resources:
        AccountProperties:
            Type: Custom::AccountProperties
            Properties:
            ServiceToken: !Sub "arn:${AWS::Partition}:lambda:${AWS::Region}:${AWS::AccountId}:function:fdn-cf-account-info"
```

## VPC Selection

By default, the function will attempt to use the VPC ID value stored in an SSM Parameter Store (string) entry named `/fdn/primaryvpc`. if it exists in the account.

Alternatively, you can provide a VPC ID to the custom resource within your template via a resource property. This example uses a parameter to specify the VPC ID, but it could be hardcoded or come via a mapping as well:

```
Parameters:
  VpcId:
    Description: Select the VPC to deploy the server to. This is a required field but the selection will be ignored in everything but the CCIP account.
    Type: AWS::EC2::VPC::Id

Resources:
  AccountProperties:
    Type: Custom::AccountProperties
    Properties:
      ServiceToken: !Sub "arn:${AWS::Partition}:lambda:${AWS::Region}:${AWS::AccountId}:function:fdn-cf-account-info"
      VPCID: !Ref VpcId

```

## Usage

The custom resource will return any number of data elements (name-value pairs) about VPC resources in the AWS account it is used in. These can then be used to specify values for properties on resources, rather than requiring manual specification of the values at deployment time by using the [!GetAtt](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-getatt.html) CloudFormation function:

```
    VpcId: !GetAtt AccountProperties.vpcid
    SubnetId: !GetAtt AccountProperties.private-subnet-c
    SubnetId: !GetAtt AccountProperties.choose-private-subnet
```

# Primary VPC CIDR only

The following keys can be used, but will only ever return subnets whose CIDR blocks are subsets of the primary CIDR block associated with the VPC. Any additional CIDR blocks associated with a VPC ("extended VPCs") are not supported for these keys. See below for keys that do work across all CIDR blocks within a VPCs.


* `vpcid`
* `vpc-cidr`
* `private-subnet-[a-z]` (where any of [a-z] is the lettered Availability Zone of the subnet)
* `public-subnet-[a-z]` (where any of [a-z] is the lettered Availability Zone of the subnet)
* `private-cidr-[a-z]` (where any of [a-z] is the lettered Availability Zone of the subnet)
* `public-cidr-[a-z]` (where any of [a-z] is the lettered Availability Zone of the subnet)

# All VPC CIDRs

The following keys all "choose" a subnet for you, based on the reported "available IP address" count. In short, given a subnet type (private/public and regular AZ/Local Zone), these keys will return the SubnetId value of the subnet with the most available IP addresses. This works across all CIDR blocks associated with the VPC.

* `choose-private-subnet`
* `choose-public-subnet`
* `choose-localzone-private-subnet` (only if VPC has subnets in a Local Zone!)
* `choose-localzone-public-subnet` (only if VPC has subnets in a Local Zone!)

Attempting to request a key whose value will not exist (e.g., using `private-subnet-c` in a VPC that does not have a subnet in the region's "C" Availability Zone) will result in a error, and thus a failed CloudFormation resource.

## Deployment with Serverless

This Lambda depends on having a specific version of Python available. Make sure that version is your current version before starting the deployment, or it will not be able to package the dependency libraries.

    python --version
    Python 2.7.5
    # If using pyenv to manage your python default version,
    # switch to the correct version.
    pyenv shell 3.11.7

Serverless also needs a couple plugins installed, so make sure those are in place:

    sls plugin install -n serverless-python-requirements
    sls plugin install -n serverless-offline

Load the appropriate credentials for deploying the CFT and Lambda to the target account. Note that because this also creates IAM roles, it will need to be a SuperAdmin credential.

To deploy to the US-West-2 (Oregon) region, which is the default:

    sls deploy

To deploy to another region:

    sls deploy --region us-east-1

If deploying to GovCloud, you also need provide the parition value as a parameter, as GovCloud has its own partition:

    sls deploy --region us-gov-west-1 --param="partition=aws-us-gov"
