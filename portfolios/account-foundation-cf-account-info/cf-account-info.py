import boto3
from botocore.config import Config
import random
import re
# from ipaddress import ip_network
from crhelper import CfnResource

crhelper = CfnResource()

# Create a session using the default profile
session = boto3.session.Session()
# Get the current region name
region = session.region_name
ssm = boto3.client('ssm', region_name=region)
ec2 = boto3.client('ec2', region_name=region)
# Compiled regex pattern for efficiency
pattern_for_regular_az = re.compile(r'^' + region  + '[a-z]$')

def get_ssm_parameter_value(parameter_name, is_secure_parameter=False):
    """Looks up a given SSM Parameter Store entry value"""
    # Create client config
    client_config = Config(
        retries = {
            "max_attempts": 10,
            "mode": "standard"
        }
    )
    # Create an SSM client
    ssm = boto3.client(
        "ssm",
        config=client_config
    )
    # Retrieve the parameter value
    ssm_parameter = ssm.get_parameter(
        Name=parameter_name,
        WithDecryption=is_secure_parameter
    )
    return ssm_parameter['Parameter']['Value']


def get_az_to_use_from_elb(elb_arn):
    """ Get the AZ to use based on the distribution of the ELB's targets """
    ### NOTE: This function ONLY supports ELBs in a region's "A" & "B" Availability Zone
      #       Since this functionlity exists for CCI, it is known and acceptable.
    elbv2 = boto3.client("elbv2", region_name=region)
    # Iterate the ELB's listeners
    elb_listeners = elbv2.describe_listeners(LoadBalancerArn=elb_arn)
    for elb_listener in elb_listeners['Listeners']:
        # Find the listener that forwards using the default action
        if elb_listener['DefaultActions'][0]['Type'] == "forward":
            # Get the target group ARN
            target_group_arn = elb_listener['DefaultActions'][0]['TargetGroupArn']
            # Initialize our availability zone counters
            az_zone_a_count = 0
            az_zone_b_count = 0
            # Iterate the target group's targets
            target_group_targets = elbv2.describe_target_health(TargetGroupArn=target_group_arn)
            for target_group_target in target_group_targets['TargetHealthDescriptions']:
                # Get the target EC2 instance and look up its availability zone
                target_instance_id = target_group_target['Target']['Id']
                # Lookup the instance details to obtain the availability zone
                target_instance_details = ec2.describe_instances(InstanceIds=[target_instance_id])
                target_instance_az = target_instance_details['Reservations'][0]['Instances'][0]['Placement']['AvailabilityZone']
                # Increment the appropriate availability zone counter
                if target_instance_az[-1] == "a":
                    az_zone_a_count += 1
                else:
                    az_zone_b_count += 1
    # Determine which AZ has the least instances
    # If there is a tie, we'll flip a coin to determine which one to use
    if az_zone_a_count < az_zone_b_count:
        return region + "a"
    elif az_zone_a_count > az_zone_b_count:
        return region + "b"
    else:
        return region + "a" if random.randint(0, 1) == 0 else region + "b"

""" Ended up not needing this, but the function may be useful someday
def find_corresponding_vpc_cidr(subnet_id):
    # Given a SubnetId, look up corresponding CIDR associated with VPC based on subnet CIDR
    ec2 = boto3.client('ec2')
    # Get subnet details
    subnet = ec2.describe_subnets(SubnetIds=[subnet_id])['Subnets'][0]
    subnet_cidr = subnet['CidrBlock']
    vpc_id = subnet['VpcId']
    
    # Get VPC details
    vpc = ec2.describe_vpcs(VpcIds=[vpc_id])['Vpcs'][0]
    vpc_cidrs = [association['CidrBlock'] for association in vpc.get('CidrBlockAssociationSet', [])]
    
    # Find which VPC CIDR block encompasses the subnet's CIDR block
    for vpc_cidr in vpc_cidrs:
        if ip_network(subnet_cidr).subnet_of(ip_network(vpc_cidr)):
            return vpc_cidr  # Return the corresponding VPC CIDR block
    return None  # Return None if no encompassing VPC CIDR block is found
"""

def check_subnet_type(subnet_id):
    """ Given a SubnetId, look up its route table to determine if the subnet is private/public """
    ec2 = boto3.client('ec2')

    # Get the association between subnet and route table
    associations = ec2.describe_route_tables(Filters=[{'Name': 'association.subnet-id', 'Values': [subnet_id]}])
    route_tables = associations['RouteTables']
    
    # Check each route in the route table for a route to an IGW
    for rt in route_tables:
        for route in rt['Routes']:
            # Check if the route is to an Internet Gateway
            if 'GatewayId' in route and route['GatewayId'].startswith('igw-'):
                return "public"  # A route to an IGW exists, so the subnet is public
    
    # If no routes to an IGW were found in any associated route table, the subnet is private
    return "private"


def get_subnet_with_most_ips(subnets):
    """ Find the key with the maximum 'available_ips' and return the subnet ID """
    max_ip_subnet = max(subnets, key=lambda k: subnets[k]['availabile_ips'])
    # Return the subnet ID
    return max_ip_subnet


@crhelper.create
@crhelper.update
def get_vpc_info(event, context):
    """Create & update handler, get VPC info and returns it"""

    # Initialize our dicts
    vpc_by_parameter = {}
    private_subnets = {}
    public_subnets =  {}
    local_zone_private_subnets = {}
    local_zone_public_subnets = {}

    try:
        # If a VPC ID has been passed, use it, otherwise, look it up from SSM (and then err if SSM param is missing)
        vpcid = event['ResourceProperties']['VPCID'] if 'VPCID' in event['ResourceProperties'] else get_ssm_parameter_value("/fdn/primaryvpc", True)

        # Get details on our VPC
        vpcResponse = ec2.describe_vpcs(VpcIds=[vpcid])
        vpcData = vpcResponse["Vpcs"][0]
        # Store some VPC values
        vpc_by_parameter['vpcid'] = vpcid
        vpc_by_parameter['vpc-cidr'] = vpcData['CidrBlock']

        # Get the subnets for our VPC
        vpc_subnets = ec2.describe_subnets(
            Filters=[
                {'Name': 'vpc-id', 'Values': [vpcid]},
            ]
        )["Subnets"]
        
        # Iterate the VPC's subnets
        for subnet in vpc_subnets:
            # Look up subnet type (private/public)
            subnet_type = check_subnet_type(subnet['SubnetId'])
            ## Determine the corresponding Availability Zone type (regular, local zone, etc) 
            # We do this by checking if the AZ name is the region name + a lowercase letter
            subnet_az_is_regular_az = pattern_for_regular_az.match(subnet['AvailabilityZone']) is not None
            ## "Regular" AZ/Local Zone check
            if subnet_az_is_regular_az:
                # Private/public subnet type check
                if subnet_type == "private":
                    private_subnets[subnet['SubnetId']] = {
                        'availabile_ips': subnet['AvailableIpAddressCount'],
                        'cidr_block': subnet['CidrBlock']
                    }
                else: # it's public
                    public_subnets[subnet['SubnetId']] = {
                        'availabile_ips': subnet['AvailableIpAddressCount'],
                        'cidr_block': subnet['CidrBlock']
                    }
                ## Add to "VPC by Parameter", a dict of key/value pairs that will (potentially) be returned
                # Build the keys
                subnet_key_name = subnet_type + "-subnet-" + subnet['AvailabilityZone'][-1]
                cidr_key_name = subnet_type + "-cidr-" + subnet['AvailabilityZone'][-1]
                vpc_by_parameter[subnet_key_name] = subnet['SubnetId']
                vpc_by_parameter[cidr_key_name] = subnet['CidrBlock']
            # We assume a "non regular AZ" is a Local Zone.
            else:
                # Private/public subnet type check
                if subnet_type == "private":
                    local_zone_private_subnets[subnet['SubnetId']] = {
                        'availabile_ips': subnet['AvailableIpAddressCount'],
                        'cidr_block': subnet['CidrBlock']
                    }
                else: # it's public
                    local_zone_public_subnets[subnet['SubnetId']] = {
                        'availabile_ips': subnet['AvailableIpAddressCount'],
                        'cidr_block': subnet['CidrBlock']
                    }

        ## Subnet choice
        # "Choose" our subnets, based on having the highest count of available IP addresses
        if private_subnets: vpc_by_parameter['choose-private-subnet'] = get_subnet_with_most_ips(private_subnets)
        if public_subnets: vpc_by_parameter['choose-public-subnet'] = get_subnet_with_most_ips(public_subnets)
        if local_zone_private_subnets: vpc_by_parameter['choose-localzone-private-subnet'] = get_subnet_with_most_ips(local_zone_private_subnets)
        if local_zone_private_subnets: vpc_by_parameter['choose-localzone-public-subnet'] = get_subnet_with_most_ips(local_zone_public_subnets)

        ## ELB subnet "choice" override
        # If an ELB ARN is passed in and we have private subnets
        if 'ElbArn' in event['ResourceProperties'] and private_subnets:
            # Override the private subnet choice based on ELB target group distribution
            ### NOTE: This function ONLY supports ELBs in a region's "A" & "B" Availabity Zone
              #       Since this functionlity exists for CCI, it is known and acceptable.
            chosen_az = get_az_to_use_from_elb(event['ResourceProperties']['ElbArn'])
            if chosen_az[-1] == "a":
                vpc_by_parameter['choose-private-subnet'] = vpc_by_parameter['private-subnet-a'] 
            else:
                vpc_by_parameter['choose-private-subnet'] = vpc_by_parameter['private-subnet-b'] 

        # Shove our own keys into the response
        for vpc_parameter in vpc_by_parameter:
            crhelper.Data[vpc_parameter] = vpc_by_parameter[vpc_parameter]

    except Exception as error:
        error_message = str(error)
        print("ERROR: " + error_message)
        raise ValueError(error_message)


@crhelper.delete
def do_nothing(event, context):
    """Does nothing at all"""
    pass

def lambda_handler(event, context):
    """Main handler/entrypoint"""
    crhelper(event, context)
