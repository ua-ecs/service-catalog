# The AWS Config template has been moved

The AWS Config CloudFormation template is now maintained at [https://bitbucket.org/ua-uits-cloud/organizations-stacksets-aws-config/src/master](https://bitbucket.org/ua-uits-cloud/organizations-stacksets-aws-config/src/master)
