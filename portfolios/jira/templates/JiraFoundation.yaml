---
# JIRA Foundation CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will deploy pieces set up for JIRA

AWSTemplateFormatVersion: '2010-09-09'
Description: JIRA Foundation (EFS, S3, Roles)

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # The name of the bucket.
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.
  VPCID:
    Description: Target VPC
    Type: AWS::EC2::VPC::Id
  PrivSubnetA:
    Description: Private Subnet for Zone A
    Type: AWS::EC2::Subnet::Id
  PrivSubnetB:
    Description: Private Subnet for Zone B
    Type: AWS::EC2::Subnet::Id
  EFSName:
    Type: String
    Description: Name of the EFS Volume

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  ServiceTag:
    Type: String
    Description: Exact name of the Service as defined in the service catalog.
  EnvironmentTag:
    Type: String
    Description: Used to distinguish between development, test, production,etc. environment types.
    AllowedValues: [dev, tst, prd, trn, stg, cfg, sup, rpt]
    Default: dev
  ContactNetidTag:
    Type: String
    Description: Used to identify the netid of the person most familiar with the usage of the resource.
    Default: mhirst
  AccountNumberTag:
    Type: String
    Description: Identifies the financial system account number.
  SubAccountNumberTag:
    Type: String
    Description: Identifies the sub account which is NOT a number.
  TicketNumberTag:
    Type: String
    Description: Used to identify the Jira, Cherwell, or other ticketing system ticket number to 
      link to more information about the need for the resource.

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
    - Label:
        default: Tagging and Cost Management
      Parameters:
      - ServiceTag
      - EnvironmentTag
      - ContactNetidTag
      - AccountNumberTag
      - SubAccountNumberTag
      - TicketNumberTag
    ParameterLabels:
      BucketName:
        default: 'Bucket Name:'   
      

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### S3 Bucket    
  #
  # This deploys the S3 bucket with some tags. The bucket has versioning enabled
  # because this is where backups will go for now.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref "BucketName"
      AccessControl: Private
      VersioningConfiguration:
        Status: Enabled
      Tags:
      - Key: Name
        Value: !Ref BucketName
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag 
      - Key: ticketnumber
        Value: !Ref TicketNumberTag
 
  # #### EFS Shared File System
  #
  # Create an EFS entity to be used as a shared filesystem for the application
  # instances.
  FileSystem:
    Type: AWS::EFS::FileSystem
    Properties:
      FileSystemTags:
      - Key: Name
        Value: !Ref EFSName
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag

  # #### EFS Mount Points
  #
  # EFS Mountpoints must be created for each Availability Zone in the VPC. This is
  # also where you define access controls, as access to EFS is controlled by
  # these security groups.
  #
  # A pair of mount points must be created for each EFS volume.
  EFSMountTargetZoneA:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId: !Ref FileSystem
      SubnetId: !Ref PrivSubnetA
      SecurityGroups:
        - Ref: EFSSecurityGroup
  EFSMountTargetZoneB:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId: !Ref FileSystem
      SubnetId: !Ref PrivSubnetB
      SecurityGroups:
        - Ref: EFSSecurityGroup
  
  # #### EFS Security Group
  #
  # This security group defines what resources are able to access the EFS shared filesystem.
  EFSSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow EFS Ports to EFS Volume
      VpcId: !Ref VPCID
      
      Tags:
      - Key: Name
        Value: !Sub "${EFSName}-efs-sg"
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag 
    
  RDSJiraParameterGroup:
      Type: AWS::RDS::DBParameterGroup
      Properties:
        Description: Jira config Parameter Group
        Family: mysql5.6
        Parameters:
          innodb_log_file_size: 268435456
          max_allowed_packet: 67108864
          time_zone: 'America/Phoenix'
        Tags:
          - Key: Name
            Value: JiraSecurityGroup
          - Key: service
            Value: !Ref ServiceTag
          - Key: environment
            Value: !Ref EnvironmentTag
          - Key: contactnetid
            Value: !Ref ContactNetidTag
          - Key: accountnumber
            Value: !Ref AccountNumberTag
          - Key: subaccountnumber
            Value: !Ref SubAccountNumberTag
          - Key: ticketnumber
            Value: !Ref TicketNumberTag 
  
# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Value: !Ref S3Bucket
    Export:
      Name: !Sub "${AWS::StackName}-jirabucket"
      
  FileSystem:
    Value: !Ref FileSystem
    Export:
      Name: !Sub "${AWS::StackName}-fs-id"
      
  EFSSecurityGroup:
    Value: !Ref EFSSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-efs-sg"
      
  RDSJiraParameterGroup:
    Value: !Ref RDSJiraParameterGroup
    Export: 
      Name: !Sub "${AWS::StackName}-parametergroup"
