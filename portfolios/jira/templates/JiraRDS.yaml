---
# RDS MYSQL CloudFormation Deployment
# -----------------------------------------
#
# This CloudFormation template will deploy an RDS instance using MYSQL
# with it's own security group.

AWSTemplateFormatVersion: '2010-09-09'
Description: Jira RDS Instance

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # The name of the database instance.
  DBName:
    Type: String
    Description: Enter the Identifier for the Database (lowercase, no spaces)


  JiraFoundationStack:
    Type: String
    Description: Name of the jira foundation stack.
    Default: JiraFoundation

  # VPCID is the ID of the VPC where this template will be deployed.
  VPCID:
    Description: Target VPC
    Type: AWS::EC2::VPC::Id

  # The EC2 Subnet IDs for the DB Subnet Group.
  SubnetGroupMembers:
    Description: Subnet Group Members
    Type: List<AWS::EC2::Subnet::Id>

  # Default EC2 Instance Type for Application instances.
  InstanceType:
    Description: EC2 Instance Type
    Type: String
    Default: db.m4.large

  MultiAZ:
    Description: MultiAZ?
    Type: String
    Default: false
    AllowedValues:
    - true
    - false

  # Snapshot identifier for restore. Specify this OR a password, but not both.
  DBSnapshotID:
    Description: Snapshot ID to restore from
    Type: String

  # The password of the database user
  DBPassword:
    Description: DB Password. Specify this OR a snapshot to restore from, not both.
    Type: String
    NoEcho: true

  DBUsername:
    Description: DB Username. Omit if restoring from snapshot.
    Type: String

  DBStorageSize:
    Description: Storage Size in GB. Omit if restoring from snapshot.
    Type: String
    Default: 20

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  ServiceTag:
    Type: String
    Description: Exact name of the Service as defined in the service catalog.
  EnvironmentTag:
    Type: String
    Description: Used to distinguish between development, test, production,etc. environment types.
    AllowedValues: [dev, tst, prd, trn, stg, cfg, sup, rpt]
    Default: dev
  ContactNetidTag:
    Type: String
    Description: Used to identify the netid of the person most familiar with the usage of the resource.
    Default: mhirst
  AccountNumberTag:
    Type: String
    Description: Identifies the financial system account number.
  SubAccountNumberTag:
    Type: String
    Description: Identifies the sub account which is NOT a number.
  TicketNumberTag:
    Type: String
    Description: Used to identify the Jira, Cherwell, or other ticketing system ticket number to
      link to more information about the need for the resource.

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Database Instance Settings
      Parameters:
      - InstanceType
      - DBName
      - DBUsername
      - DBPassword
      - DBStorageSize
      - DBSnapshotID
      - AccountNumberTag
      - SubAccountNumberTag
      - MultiAZ
    - Label:
        default: Network Settings
      Parameters:
      - VPCID
      - SubnetGroupMembers
    - Label:
        default: Foundation Stack
      Parameters:
      - JiraFoundationStack
    - Label:
        default: Tagging and Cost Management
      Parameters:
      - ServiceTag
      - EnvironmentTag
      - ContactNetidTag
      - AccountNumberTag
      - SubAccountNumberTag
      - TicketNumberTag

    ParameterLabels:
      DBName:
        default: 'RDS DB Name'
      JiraFoundationStack:
        default: 'Jira Foundation Stack Name'


# Conditions
# ----------
Conditions:
  HasPassword: !Not [!Equals [!Ref DBPassword, '']]
  HasSnapshot: !Not [!Equals [!Ref DBSnapshotID, '']]


# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### Database Instance
  #
  # This deploys the database RDS instance with some tags.
  DBInstance:
    Type: AWS::RDS::DBInstance
    Properties:
      DBInstanceIdentifier: !Ref DBName
      DBInstanceClass:      !Ref InstanceType
      StorageType:          gp2
      DBSnapshotIdentifier: !If
                          - HasSnapshot
                          - !Ref DBSnapshotID
                          - !Ref AWS::NoValue
      MasterUsername:     !If
                          - HasPassword
                          - !Ref DBUsername
                          - !Ref AWS::NoValue
      MasterUserPassword: !If
                          - HasPassword
                          - !Ref DBPassword
                          - !Ref AWS::NoValue
      AllocatedStorage:   !If
                          - HasPassword
                          - !Ref DBStorageSize
                          - !Ref AWS::NoValue
      DBParameterGroupName:
                            Fn::ImportValue:
                                !Sub "${JiraFoundationStack}-parametergroup"
      AutoMinorVersionUpgrade: true
      BackupRetentionPeriod: '30'
      MultiAZ:              !Ref MultiAZ
      PubliclyAccessible:   false
      Engine:               MySQL
      EngineVersion:        5.6.40
      DBSubnetGroupName:    !Ref DBSubnetGroup
      VPCSecurityGroups:
        - Ref: DBSecurityGroup
      Tags:
      - Key: Name
        Value: !Sub "${DBName}"
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag

  # #### Database Subnet Group
  #
  # Subnet group for the database instance, that has at least two availiablity zones
  DBSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: !Sub "${DBName} Subnet Group"
      SubnetIds: !Ref SubnetGroupMembers
      Tags:
      - Key: Name
        Value: !Sub "${DBName}-subnetgroup"
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag

  # #### Database Security Group
  #
  # Security group for the database instance, that allows MYSQL into the database.
  DBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow MySQL access to DB
      VpcId: !Ref VPCID
      SecurityGroupIngress:
      - CidrIp: "10.220.183.128/26"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "analytics-prd"
      - CidrIp: "10.220.161.64/26"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "analytics-dev"
      - CidrIp: "10.220.161.128/26"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "analytics-dev"
      - CidrIp: "10.220.161.192/27"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "analytics-dev"
      - CidrIp: "10.221.227.0/25"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "Uair AWS Workbench"
      - CidrIp: "10.221.227.128/25"
        IpProtocol: "tcp"
        FromPort: "3306"
        ToPort: "3306"
        Description: "Uair AWS Workbench"        

      Tags:
      - Key: Name
        Value: !Sub "${DBName}-sg"
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetidTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccountnumber
        Value: !Ref SubAccountNumberTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  DBEndpoint:
    Description: The DNS Endpoint for this DB
    Value: !GetAtt DBInstance.Endpoint.Address
  DBPort:
    Description: The DB Port
    Value: !GetAtt DBInstance.Endpoint.Port
  DBSecurityGroup:
    Description: DB security group
    Value: !Ref DBSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-dbsecuritygroup"
