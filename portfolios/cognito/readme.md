# Cognito Deployment

## An easy way to deploy an AppClient, Identity and User Pool that integrate with Shibboleth through SAML.

This project will help you deploy the Cognito infrastructure needed to leverage Shibboleth authentication through your application.

## Steps

### Deploy Cognito User Pool
- Deploy the cognito.yml CloudFormation template. This will create the following resources:
    - AWS Cognito User Pool
    - Cognito App Client
    - Cognito Identity Pool

### Request Shibboleth Integration
- Once the Cognito Pool has been created, look at the outputs from the stack.
    - Make note of the User Pool ID.
    - Make note of the Cognito Domain URL
- Go to the UA IAM Application management console.
    - Login and go to the Shibboleth tag
    - Request new Shibboleth Access
    - Enter your app name
    - Copy the Cognito Domain into the application URLs
    - In the description field, indicate that this is for an AWS Cognito user pool, and provide the User Pool ID.