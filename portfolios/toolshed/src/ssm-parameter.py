"""
This little program will take the contents of a file and put it into the SSM Parameter Store.
This is used to store the SSH Public Key that needs to be placed onto the toolshed server
in order to authenticate with Bitbucket git repositories to perform updates.

The source for this SSH key is in Stache: "Toolshed Checkout Only SSH Key"

You must have the boto3 python package installed first.
"""

import boto3

client = boto3.client('ssm', region_name="us-west-2")

sshKeyText = ""

with open("/home/ec2-user/.ssh/id_rsa") as file:  
  sshKeyText = file.read() 

response = client.put_parameter(
    Name = "/toolshed/bitbucket-ssh-key",
    Description = "Local SSH private key to checkout from git",
    Value = sshKeyText,
    Type = 'SecureString'
)

