# Toolshed

This set of CLoudFormation templates aids in deploying the Toolshed web app hosting service to AWS.

Toolshed is still mostly a "lift and shift" migration to AWS at this point.  These templates simply
deploy the AWS resources needed along with the pre-built AMIs for the hosts.


## Parameters

* 
## Outputs

* 

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
