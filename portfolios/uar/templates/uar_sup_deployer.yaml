---
# EC2 CloudFormation Deployment
# -----------------------------------------
#
# This CloudFormation template will deploy a single EC2 instance with
# its own security group and role.  That role has limited rights to
# access the UAR SUP resources only and only that required for the UAR application
# stack.
#
# This template is meant to be used to create a 'deployer instnace'
# that can be used to run the MET deploy scripts for a production acct
#
AWSTemplateFormatVersion: '2010-09-09'

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  #Cloudformation template name that contains Security Groups
  # HostName to be used in tagging the EC2 instance.
  HostName:
    Type: String
    Description: Enter the name of the host or service, ie 'uar-deployer', etc.
    Default: "uar-sup-deployer"
  KeyName:
    Description: Amazon EC2 Key Pair
    Type: AWS::EC2::KeyPair::KeyName
    Default: "uar-development-keypair"
  AvailabilityZone:
    Description: Availabilty Zone of Selected Subnet
    Type: AWS::EC2::AvailabilityZone::Name
    Default: "us-west-2a"
  HostedZoneName:
    Description: Hosted Zone Name for Route 53 Entry
    Type: String
    Default: "uaccess-aws2.arizona.edu"
  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  ServiceTag:
    Type: String
    Description: Exact name of the Service as defined in the service catalog.
    Default: "Uaccess Research"
  EnvironmentTag:
    Type: String
    Description: Used to distinguish between development, test, production,etc. environment types.
    AllowedValues: [dev, tst, prd, trn, stg, cfg, sup, rpt]
    Default: "sup"
  ContactNetIdTag:
    Type: String
    Description: Used to identify the netid of the person most familiar with the usage of the resource.
    Default: "mhirst"
  AccountNumberTag:
    Type: String
    Description: Identifies the financial system account number.
    Default: "1192622"
  SubAccountTag:
    Description: Identifies the financial system subaccount number
    Type: String
    Default: "Uaccess Research"
  TicketNumberTag:
    Type: String
    Description: Used to identify the Jira, Cherwell, or other ticketing system ticket number to
      link to more information about the need for the resource.
    Default: "UAR-3294"
  CreatedByTag:
    Type: String
    Description: Created by Tag
    Default: "mhirst"

# Resources
# ---------
#
# This is the EC2 instance deployed by the template.
#
Resources:

  # #### EC2 Instance
  #
  # Deploys the EC2 instance with some tags.
  Ec2Instance:
    Type: AWS::EC2::Instance
    Properties:
      ImageId: ami-7172b611
      KeyName: !Ref KeyName
      InstanceType: t2.small
      AvailabilityZone: !Ref AvailabilityZone
      IamInstanceProfile: !Ref InstanceProfile
      NetworkInterfaces:
      -
        AssociatePublicIpAddress: "false"
        DeviceIndex: "0"
        SubnetId: subnet-ab25b0ce
        GroupSet:
          - !Ref InstanceSecurityGroup
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash
            sudo yum update -y
            sudo yum install -y git vim telnet
            sudo pip install awscli --upgrade

            # At this time we are having MET manually checkout the below repositories.
            # If the need arises we can automate the SSH key being added at creation.
            # but the manual checkout is matching KFS so leaving it for now.

            #git clone https://bitbucket.org/ua-ecs/service-catalog.git /home/ec2-user/git/service-catalog
            #git clone git@bitbucket.org:uits-ksd/ksd-db-flowdown.git /home/ec2-user/git/ksd-db-flowdown

            chown -R ec2-user /home/ec2-user/git/service-catalog/
            wget -qP /usr/local/bin https://stedolan.github.io/jq/download/linux64/jq
            chmod +x /usr/local/bin/jq

            aws s3 cp s3://eas-ksd/uar-sup-deployer/uar_sup_shutdown.sh /home/ec2-user/
            aws s3 cp s3://eas-ksd/uar-sup-deployer/uar_sup_deploy.sh /home/ec2-user/

            # this will allow the download scripts to be run.
            chmod +x /home/ec2-user/*

            # start sup M - F 7am and 5pm Sunday Arizona time.
            # NOTE: Below is using UTC time.  Use UTC time when adding/editing.
            (crontab -l 2>/dev/null; echo "0 14 * * 1-5 /home/ec2-user/uar_sup_deploy.sh") | crontab -
            (crontab -l 2>/dev/null; echo "0 00 * * 7  /home/ec2-user/uar_sup_deploy.sh") | crontab -
            # Stop SUP M - F 10PM
            (crontab -l 2>/dev/null; echo "0 5 * * 1-5 /home/ec2-user/uar_sup_shutdown.sh") | crontab -

            wget https://download.oracle.com/otn_software/linux/instantclient/19800/oracle-instantclient19.8-basic-19.8.0.0.0-1.x86_64.rpm
            wget https://download.oracle.com/otn_software/linux/instantclient/19800/oracle-instantclient19.8-sqlplus-19.8.0.0.0-1.x86_64.rpm

            rpm -ivh oracle-instantclient19.8-basic-19.8.0.0.0-1.x86_64.rpm
            rpm -ivh oracle-instantclient19.8-sqlplus-19.8.0.0.0-1.x86_64.rpm

      Tags:
      - Key: Name
        Value: !Ref HostName
      - Key: service
        Value: !Ref ServiceTag
      - Key: environment
        Value: !Ref EnvironmentTag
      - Key: contactnetid
        Value: !Ref ContactNetIdTag
      - Key: accountnumber
        Value: !Ref AccountNumberTag
      - Key: subaccount
        Value: !Ref SubAccountTag
      - Key: ticketnumber
        Value: !Ref TicketNumberTag
      - Key: createdby
        Value: !Ref CreatedByTag

  # #### Instance Security Group
  #
  # Security group for the EC2 instance, that allows you to SSH into the instance
  # locked down to mosaic VPN
  InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: "Allow ssh to deployer"
      VpcId: vpc-8a9428ef
      SecurityGroupIngress:
        - IpProtocol: "tcp"
          FromPort: "22"
          ToPort: "22"
          CidrIp: "10.138.0.0/17"
      Tags:
        - Key: "Name"
          Value: !Sub "${HostName} Security Group"

  # #### Instance Role
  #
  # This is the IAM role that will be applied to the EC2 Instances. Again this policy is
  # pretty broad, so be careful!
  #
  InstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      Policies:
      - PolicyName: deployment-access
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Sid: StmtAllowPassRole
            Effect: Allow
            Action:
            - iam:PassRole
            Resource:
            # Import the ARN of the CloudFormation deployer role for this account.
            - !ImportValue fdn-iam-cloudformation-deployer-role-arn
          - Sid: DeployerAction
            Effect: Allow
            Action:
            - cloudformation:*
            - elasticloadbalancing:*
            - ec2:*
            - kms:*
            - iam:ListAccountAliases
            Resource:
            - "*"
          - Sid: SsmParameterAccess
            Effect: Allow
            Action:
            - "ssm:GetParameter"
            Resource:
            - !Sub "arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/uarsup*"
          - Sid: RDSAccessSup
            Effect: Allow
            Action:
            - rds:*
            Resource:
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:uar-sup-db*"
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:db:kcsup*"
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:og:uar-default-19-option-tz"
          - Sid: RDSAccessPrd
            Effect: Allow
            Action:
            - rds:DescribeDBSnapshots
            - rds:RestoreDBInstanceFromDBSnapshot
            Resource:
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:db:uar-prd-db*",
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:uar-prd-db*",
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:subgrp:uar-rds-private-subnet-group",
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:subgrp:kualiserviceswest-research-support-uarsupenvironment-envdatabasedevicesubnetgroup-ag1haehlk8pi",
            - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:og:oem-agent-19"
            Action:
            - s3:*
            Resource: "arn:aws:s3:::eas-ksd/*"
          - Sid: OpsworksAccess
            Effect: Allow
            Action:
            - opsworks:AssignInstance
            - opsworks:CreateDeployment
            - opsworks:CreateInstance
            - opsworks:GetHostnameSuggestion
            - opsworks:StartInstance
            - opsworks:StopInstance
            - opsworks:StopStack
            - opsworks:StartStack
            - opsworks:UpdateApp
            - opsworks:UpdateInstance
            - opsworks:UpdateStack
            - opsworks:UpdateLayer
            Resource:
            # UAR SUP Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/ef6be100-9a27-40dc-9c9d-f9d5dcd9777f/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/ef6be100-9a27-40dc-9c9d-f9d5dcd9777f/*"
  # #### Instance Profile
  InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - !Ref InstanceRole

  #DNS Record for EC2 Instance
  DeployerDnsRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: !Sub "${HostedZoneName}."
      Name: !Sub "${HostName}.${HostedZoneName}."
      Type: A
      TTL: '900'
      ResourceRecords:
      - !GetAtt Ec2Instance.PrivateIp

# Outputs
# -------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  InstancePrivateIP:
    Description: The Private IP address of the instance
    Value: !GetAtt Ec2Instance.PrivateIp
  InstanceID:
    Description: The Instance ID
    Value: !Ref Ec2Instance
