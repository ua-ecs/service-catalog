---
# UAR Developer Role
# -------------
# 
# This CloudFormation template will deploy an IAM role for UAR DevOps
# staff to access resources in the ua-erp account.
# 

AWSTemplateFormatVersion: '2010-09-09'
Description: 'UAR DevOps User Access'

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:

  # CloudFormation cannot create IAM Identity Providers, so it must be created before
  # this teplate is run.  Pass in the ARN of the created Shibboleth IDP here.
  ShibbolethIDP:
    Description: Name of the SAML Provider
    Default: UA_Shibboleth_IdP
    Type: String

Resources:

  # #### UAR Access Role
  #
  # The UAR Acces role/policy allows those users access to appropriate resources.
  UARAccess:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
        - Effect: Allow
          Principal:
            Federated: !Sub "arn:aws:iam::${AWS::AccountId}:saml-provider/${ShibbolethIDP}"
          Action: sts:AssumeRoleWithSAML
          Condition:
            StringEquals:
              SAML:aud: https://signin.aws.amazon.com/saml
  UARAccessGroup:
    Type: AWS::IAM::Group
    Properties:
      Path: "/"
  # #### The UARAccess Policy
  UARAccessPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        # Basic read info
        - Effect: Allow
          Action: 
            - route53:Get*
            - route53:List*
            - ec2:Describe*
            - elasticloadbalancing:Describe*
            - autoscaling:Describe*
            - rds:Describe*
            - rds:ListTagsForResource
            - s3:List*
            - ecs:ListClusters
            - ecr:DescribeRepositories
          Resource: "*"
        # Allow Docker registry access
        - Effect: Allow
          Action: 
            - ecr:DescribeImages
            - ecr:GetDownloadUrlForLayer
            - ecr:GetRepositoryPolicy
            - ecr:ListImages
          Resource: !Sub "arn:aws:ecr:${AWS::Region}:${AWS::AccountId}:repository/kuali/*"
        # Allow S3 access to the eas-ksd bucket
        - Effect: Allow
          Action:
            - s3:*
          Resource: "arn:aws:s3:::eas-ksd/*"
        # CloudWatch Logs UAR access
        - Effect: Allow
          Action:
            - logs:Describe*
            - logs:List*
            - logs:TestMetricFilter
            - logs:FilterLogEvents
          Resource: "*"
        - Effect: Allow
          Action:
            - logs:Get*
            - logs:List*
          Resource: !Sub "arn:aws:logs:${AWS::Region}:${AWS::AccountId}:kra*"
        # OpsWorks UAR access
        - Effect: Allow
          Action:
            - opsworks:Describe*
            - opsworks:ListTags
            - health:DescribeEventAggregates
            - iam:GetRolePolicy
          Resource: "*"
        - Effect: Allow
          Action:
            - opsworks:AssignInstance
            - opsworks:CreateDeployment
            - opsworks:CreateInstance
            - opsworks:GetHostnameSuggestion
            - opsworks:StartInstance
            - opsworks:StopInstance
            - opsworks:UpdateApp
            - opsworks:UpdateInstance
            - opsworks:UpdateStack
            - opsworks:UpdateLayer
          Resource:
            # UAR DEV Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/f0678810-f41f-4a1a-b0ca-f2ec2f79b83b/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/f0678810-f41f-4a1a-b0ca-f2ec2f79b83b/*"
            # UAR STG Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/bf5228b2-56f4-4086-8e65-eff6b1add3ff/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/bf5228b2-56f4-4086-8e65-eff6b1add3ff/*"
            # UAR SUP Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/ef6be100-9a27-40dc-9c9d-f9d5dcd9777f/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/ef6be100-9a27-40dc-9c9d-f9d5dcd9777f/*"
            # UAR TRN Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/061e172a-f719-434e-b586-6959d4d732e0/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/061e172a-f719-434e-b586-6959d4d732e0/*"
            # UAR TST Environment Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/cf6731ca-8d5b-4baf-b684-d36317968b3a/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/cf6731ca-8d5b-4baf-b684-d36317968b3a/*"
            # Kuali Service Delivery Stack
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/127cf724-921d-4333-8fdd-bffc908c2210/"
            - !Sub "arn:aws:opsworks:${AWS::Region}:${AWS::AccountId}:stack/127cf724-921d-4333-8fdd-bffc908c2210/*"
        # Certificat Manager access
        - Effect: Allow
          Action:
            - acm:DescribeCertificate
            - acm:ListCertificates
            - acm:ListTagsForCertificate
          Resource: "*"
      Roles:
      - Ref: UARAccess
      Groups:
      - Ref: UARAccessGroup

  # #### UARAccess MFA Policy
  #
  # This policy is only attached to the User Group, so will only apply to IAM users who have been
  # added to the group, and not to SAML users logging in via the Role and Shibboleth.
  UARAccessMFAPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        # Allow ALL IAM actions on any resource, with the exceptions of the IAM actions.
        # Also enforces that the user MUST have MultiFactor authentication set up.
        - Effect: Allow
          NotAction: iam:*
          Resource: "*"
          Condition:
            BoolIfExists:
              aws:MultiFactorAuthPresent: 'true'
      Groups:
      - Ref: UARAccessGroup

# Outputs
# -------
#
Outputs:
  UARAccess:
    Value: !Ref UARAccess


