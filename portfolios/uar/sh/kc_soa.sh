#!/bin/bash
#*******************************************************************************
#
# TITLE......: kc_soa.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (KC) (req)
#              -e  Environment (SUP, PRD) (req)
#
# This script will run the following stored procedurs on the pillar and enviornment passed
#  - KRAOWNER.UA_KC_UTIL.POPULATE_PERSON_TABLES
#  - KRAOWNER.UA_KC_UTIL.WITHDRAWN_DESELECT_CHECKBOXES 
# A message will be posted to the SNS topic uar-status-table-results in the kuali prod acct
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="KC"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the value ${pillar_list} (${UPPER_PILLAR})"
fi
LOWER_PILLAR=${UPPER_PILLAR,,}

env_list="PRD SUP"
good_env="N"
for curr_env in $env_list; do
  if [ "${UPPER_ENV}" == "$curr_env" ]; then
    good_env="Y"
  fi
done

if [ $good_env = "N" ]; then
  error_abort "Either a environment wasn't passed or was invalid please use -e with the value (${env_list}) (${UPPER_ENV})"
fi
LOWER_ENV=${UPPER_ENV,,}
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- If this is an error for a RPT, SUP, or PRD then send a message to the VictorOps channel
#if [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ] || [ "${UPPER_ENV}" = "PRD" ]; then
#  USE_THIS_SLACK_URL=${SLACK_URL_VO}
#  SLACK_CHANNEL="ua-errors"
#else
#  USE_THIS_SLACK_URL=${SLACK_URL}
#  SLACK_CHANNEL="met-errors"
#fi

# -- Send a message to the #met-errors channel
#if ! [ -z "${USE_THIS_SLACK_URL}" ]; then
#  SLACK_MSG="Error running build_ps_env_ecs.sh - (${MSG})"
#  JSON="{ \"channel\": \"${SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
#  OUTPUT=$(curl -s -d "payload=${JSON}" "${USE_THIS_SLACK_URL}" 2>&1)
#fi

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: get_db_creds
# This function will get the DB login creds and populate the following variables
# - DB_USER
# - DB_PW
# - DB_NAME
#
# INPUTS:  
#
# RETURNS: 
#
#*******************************************************************************
function get_db_creds
{

cnt=1
while [ $cnt -le 3 ]
do

case $cnt in
  1)
    db_field="connect_user"
    ;;
  2)
    db_field="connect_pw"
    ;;
  3)
    db_field="db_link_name"
    ;;
esac

DB_VALUE=$(sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
select $db_field
from rs_environments
where
    pillar = '${UPPER_PILLAR}'
and environment = '${UPPER_ENV}';
exit;
EOF-SQL
)

case $cnt in
  1)
    DB_USER=$DB_VALUE
    ;;
  2)
    DB_PW=$DB_VALUE
    ;;
  3)
    DB_NAME=$DB_VALUE
    ;;
esac

cnt=$(( cnt+1 ))

done

if [ -z ${DB_USER} ] || [ -z ${DB_PW} ] || [ -z ${DB_NAME} ]; then
  error_abort "Could not find DB Creds in the RS_ENVIRONMENTS table for pillar ${UPPER_PILLAR} and env ${UPPER_ENV}."
fi

}

#*******************************************************************************
#
# function: run_jobs
# This function will use the credentials populated in a pervious step to login 
# to the target database and run the following stored procedures.
#
#  - KRAOWNER.UA_KC_UTIL.POPULATE_PERSON_TABLES
#  - KRAOWNER.UA_KC_UTIL.WITHDRAWN_DESELECT_CHECKBOXES 
#
# INPUTS:  
#
# RETURNS: 
#
#*******************************************************************************
function run_jobs
{

#Run WITHDRAWN_DESELECT_CHECKBOXES
OUT_MSG=$(sqlplus -s ${DB_USER}/${DB_PW}@${DB_NAME} << EOF-SQL
set feed off timing off heading off pagesize 0
var out_msg varchar2;
begin
  kraowner.ua_kc_util.withdrawn_deselect_checkboxes;
  :out_msg := 'C';
exception when others then
  :out_msg := SQLERRM;
end;
/
print out_msg;
exit;
EOF-SQL
)

#If process finished without error then grab the row count from the 
if [ "${OUT_MSG}" = "C" ]; then

ROW_MSG=$(sqlplus -s ${DB_USER}/${DB_PW}@${DB_NAME} << EOF-SQL
set feed off timing off heading off pagesize 0
select message
from kraowner.ua_kc_batch_status b
where job_name = 'withdrawn_deselect_checkboxes'
and date_run = (
  select max(date_run)
  from kraowner.ua_kc_batch_status
  where job_name = b.job_name);
exit;
EOF-SQL
)
  SNS_SUBJECT="KC_Withdrawn_Deselect_Checkboxes_AWS result notification - Successful"
  SNS_MSG="${UPPER_PILLAR}${UPPER_ENV} KC_Withdrawn_Deselect_Checkboxes_AWS stored procedure completed execution.  ${ROW_MSG}"
else
  SNS_SUBJECT="KC_Withdrawn_Deselect_Checkboxes_AWS result notification - ERROR"
  SNS_MSG="${UPPER_PILLAR}${UPPER_ENV} KC_Withdrawn_Deselect_Checkboxes_AWS stored procedure error - ${OUT_MSG}"
fi

OUTPUT=$(aws sns publish --topic-arn arn:aws:sns:us-west-2:740525297805:uar-status-table-results --message "${SNS_MSG}" --subject "${SNS_SUBJECT}")


#Run POPULATE_PERSON_TABLES
OUT_MSG=$(sqlplus -s ${DB_USER}/${DB_PW}@${DB_NAME} << EOF-SQL
set feed off timing off heading off pagesize 0
var out_msg varchar2;
declare
  v_status VARCHAR2(1);
  v_msg    VARCHAR2(32000);
begin
  kraowner.ua_kc_util.populate_person_tables(v_status,v_msg);
  if v_status = 'C' then
    v_msg := v_status;
  end if;
  :out_msg := v_msg;
end;
/
print out_msg;
exit;
EOF-SQL
)

if [ "${OUT_MSG}" = "C" ]; then
  SNS_SUBJECT="POPULATE_PERSON_TABLES result notification - Successful"
  SNS_MSG="${UPPER_PILLAR}${UPPER_ENV} POPULATE_PERSON_TABLES stored procedure completed execution."
else
  SNS_SUBJECT="POPULATE_PERSON_TABLES result notification - ERROR"
  SNS_MSG="${UPPER_PILLAR}${UPPER_ENV} POPULATE_PERSON_TABLES stored procedure error - ${OUT_MSG}"
fi

OUTPUT=$(aws sns publish --topic-arn arn:aws:sns:us-west-2:740525297805:uar-status-table-results --message "${SNS_MSG}" --subject "${SNS_SUBJECT}")

}


#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************

# -- parse command-line options
while getopts :p:e: arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    e)
       UPPER_ENV=${OPTARG^^}
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
  esac
done

# validate input parameters
validate_arguments

#Get DB login Credentials 
get_db_creds

#Run Jobs and post to SNS topic
run_jobs
