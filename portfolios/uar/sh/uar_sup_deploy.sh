#!/bin/bash
#*************************************************
#
# TITLE: uar_sup_deploy.sh
# Purpose:
# Create a new SUP RDS instance from the daily production snapshot.
#
# After the database has been created, start the SUP opsworks intances
#
# Technically we could hard code everything as UAR should NEVER change....
# But you never know, so I have added some functions to get the instance IDs.
#*************************************************

export AWS_DEFAULT_REGION=us-west-2

# setting up logging so if something breaks, it can be reviewed.
# everything 'run' below should go to opsworks_shutdown.log

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>uar_sup_deploy.log 2>&1

#*************************************************
# UAR SUP Cldouformation Stack Name.  This 'should' never change
# we need this to get the opsworks stack ID associated with it.
# While the Opsworks ID should not change either, I am automating that
# further down just in case it does change for any reason.
#*************************************************
# ua-erp account CF stack name
CF_STACK_NAME="KualiServicesWest-Research-Support-UarSupEnvironment"

printf "\n---- The cloudformation Stack name is: $CF_STACK_NAME ------\n"

#*******************************************************************************
# function: get_opsworks_instance_ids
#
# This function will get all of instance IDs associated with a specific
# OpsWorks stack.  Does not matter if they add more manually.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function get_opsworks_instance_ids
{
  # Even though this is not passed in,
  # just a quick check to make sure we are not shutting down something else

  if [ $CF_STACK_NAME == "KualiServicesWest-Research-Support-UarSupEnvironment" ]; then
    # aws cli to get an opsworks stack id from the CF stack.
    OPSWORKS_STACK_ID=`aws cloudformation describe-stack-resources --region us-west-2 --stack-name $CF_STACK_NAME --output text --query 'StackResources[?ResourceType==\`AWS::OpsWorks::Stack\`].PhysicalResourceId'`
    echo "getting the OPSWORKS_STACK_ID  for the proto(stack id) returns: $OPSWORKS_STACK_ID"
  else
    echo "This can only be run on SUP"
    exit 1 # exit with General/Miscellaneous errors
  fi
}

#*******************************************************************************
#
# function: get_prd_snapshot
#
# Get the snapshot that is created from uar-prd-db that is named like this:
# uar-prd-db-dr-snapshot-3bf5caa1-1b4d-41c3-b17b-9cf798f5b324
# The number on the end is a UUID number and is random, which is why we have
# to query the snapshot name like below.
#
# We are using the 'dr' snapshot because that is what ua-erp jenkins was using
# and we are sticking with that.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function get_prd_snapshot {

  DB_INSTANCE_FOR_SNAPSHOT="uar-prd-db"

  SNAPSHOT_FROM=$(aws rds describe-db-snapshots --region=us-west-2 --db-instance-identifier $DB_INSTANCE_FOR_SNAPSHOT --output text --query 'DBSnapshots[?Status==`available`].[DBInstanceIdentifier,DBSnapshotIdentifier,SnapshotCreateTime]' 2>&1 | awk '/uar-prd-db-dr-snapshot-/ {print $2}')

  if [ ! -z "$SNAPSHOT_FROM" ]; then
    printf "\n----- $SNAPSHOT_FROM ----\n"
  else
    printf "\n---- NO SNAPSHOT FOUND -----\n"
    exit 1
  fi
}

#*******************************************************************************
#
# function: refresh_database
#
# Refresh the SUP database from the prd snapshot using the snapshot found
# in the get_prd_snapshot function
# The DB will be restored from a PRD snapshot when the env is started.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function refresh_database
{
  UAR_DB_NAME="kcsup"

  UAR_DB_INSTANCE_IDENTIFIER="uar-sup-db"

  printf "\n----- SNAPSHOT USED $SNAPSHOT_FROM -------\n"

  #delete existing database (if it exists)
  aws rds delete-db-instance --db-instance-identifier $UAR_DB_NAME --skip-final-snapshot
  aws rds wait db-instance-deleted --db-instance-identifier $UAR_DB_NAME

  # Now lets create a new SUP DB
  aws rds restore-db-instance-from-db-snapshot --availability-zone us-west-2a --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER --db-snapshot-identifier $SNAPSHOT_FROM \
    --db-instance-class db.m5.large \
    --db-subnet-group-name kualiserviceswest-research-support-uarsupenvironment-envdatabasedevicesubnetgroup-ag1haehlk8pi \
    --no-multi-az \
    --no-publicly-accessible \
    --no-auto-minor-version-upgrade \
    --license-model bring-your-own-license \
    --db-name $UAR_DB_NAME \
    --storage-type gp2 \
    --engine oracle-ee \
    --option-group-name oem-agent-19 \
    --copy-tags-to-snapshot \
    --tags \
        Key=service,Value="UAccess Research" \
        Key=Name,Value="$UAR_DB_NAME RDS Instance" \
        Key=environment,Value=dev \
        Key=createdby,Value="automation" \
        Key=contactnetid,Value="mhirst" \
        Key=accountnumber,Value="1192622" \
        Key=subaccount,Value="12AWS" \
        Key=ticketnumber,Value=UAR-3294 \
        Key=resourcefunction,Value="$UAR_DB_NAME RDS instance for use with UAR SUP Environment" \
        Key=resetdaily,Value="false" \
        Key=restorefromdatabase,Value="$SNAPSHOT_FROM"

    aws rds wait db-instance-available --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER \
         || aws rds wait db-instance-available --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER
      #Wait a second time if the first one fails, since it might have just taken too long and given up

      #modify security group and backup retention
    aws rds modify-db-instance --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER \
        --vpc-security-group-ids "sg-182d137d" "sg-ccb2b1a9" \
        --backup-retention-period 0 \
        --apply-immediately

    #arbitrarily wait a few moments for modification to start happening so 'wait' will actually wait
    sleep 30s

    aws rds wait db-instance-available --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER \
         || aws rds wait db-instance-available --db-instance-identifier $UAR_DB_INSTANCE_IDENTIFIER
}

#*******************************************************************************
#
# function: run_flowdown
#
# Run flowdown against the newly created/refreshed DB
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function run_flowdown
{
  # get credentials needed
  printf "\n------- Getting DB USERNAME ------\n"
  DB_USERNAME=`aws ssm get-parameter --name /uarsup/AppDBUsername --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n------- Getting DB PASSWORD ------\n"
  DB_PASSWORD=`aws ssm get-parameter --name /uarsup/AppDBPassword --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n----- Getting DB HOST $UAR_DB_HOST -------\n"
  UAR_DB_HOST=`aws ssm get-parameter --name /uarsup/UarDbHost --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n----- Getting UAR_DB_SERVICE Name -------\n"
  UAR_DB_SERVICE=`aws ssm get-parameter --name /uarsup/UarDbServiceName --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n------- AppDBMasterUsername ------\n"
  DB_MASTER_USER=`aws ssm get-parameter --name /uarsup/AppDBMasterUsername --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n------- Getting AppDBMasterPwNP ------\n"
  DB_MASTER_PASSWD=`aws ssm get-parameter --name /uarsup/AppDBMasterPw --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n------- Getting AppDIUserPW ------\n"
  DI_USER_PASSWD=`aws ssm get-parameter --name /uarsup/AppDIUserPW --with-decryption --output text --query 'Parameter.[Value]'`

  printf "\n------- Getting UAR_NP_ADMIN ------\n"
  UAR_NP_ADMIN_PW=`aws ssm get-parameter --name /uarsup/AppUarNPAdmin --with-decryption --output text --query 'Parameter.[Value]'`


  #--------------------------------
  # The SQL bellow will run the 'special' stuff that
  # the ksd-db-manager used to run for sup.
  # -------------------------------
  printf "\n------ RUNNING ALTER KRAOWNER -------\n"
  SQL_ALTER_KRAOWNER=$(sqlplus -s "$DB_MASTER_USER"/"$DB_MASTER_PASSWD"@"$UAR_DB_HOST"/"$UAR_DB_SERVICE" << EOF
  SET DEFINE OFF;
  ALTER USER kraowner IDENTIFIED BY "${DB_PASSWORD}";
  commit;
  EXIT
EOF
  )
  echo $SQL_ALTER_KRAOWNER

  printf "\n------ CREATING DI_USER ---------------\n"
  SQL_CREATE_DI_USER=$(sqlplus -s "$DB_MASTER_USER"/"$DB_MASTER_PASSWD"@"$UAR_DB_HOST"/"$UAR_DB_SERVICE" << EOF
  SET DEFINE OFF;
  GRANT CREATE SESSION TO DI_USER IDENTIFIED BY "${DI_USER_PASSWD}";
  COMMIT;
  EXIT
EOF
  )
  echo $SQL_CREATE_DI_USER

  printf "\n------ CREATING UAR_NP_ADMIN ---------------\n"
  SQL_CREATE_UAR_NP_ADMIN_USER=$(sqlplus -s "$DB_MASTER_USER"/"$DB_MASTER_PASSWD"@"$UAR_DB_HOST"/"$UAR_DB_SERVICE" << EOF
  SET DEFINE OFF;
  GRANT CONNECT, DBA TO UAR_NP_ADMIN IDENTIFIED BY "${UAR_NP_ADMIN_PW}";
  COMMIT;
  EXIT
EOF
  )
  echo $SQL_CREATE_UAR_NP_ADMIN_USER

  FLOWDOWN_FILE_KC="/home/ec2-user/repos/ksd-db-flowdown/scripts/KC-5.2.1-FlowdownScript.sql"
  FLOWDOWN_FILE_GRANT="/home/ec2-user/repos/ksd-db-flowdown/scripts/grant_crud.sql"

  printf "\n------ runing flowdown for $FLOWDOWN_FILE_KC--------------\n"
    SQL_FLOWDOWN_KC=$(sqlplus -s "$DB_MASTER_USER"/"$DB_MASTER_PASSWD"@"$UAR_DB_HOST"/"$UAR_DB_SERVICE" << EOF
      --@git/ksd-db-flowdown/scripts/KC-5.2.1-FlowdownScript.sql
      @${FLOWDOWN_FILE_KC}
      exit
  EOF
    )
    echo $SQL_FLOWDOWN_KC

    printf "\n------ runing flowdown for $FLOWDOWN_FILE_GRANT--------------\n"
    SQL_FLOWDOWN_GRANT=$(sqlplus -s "$DB_MASTER_USER"/"$DB_MASTER_PASSWD"@"$UAR_DB_HOST"/"$UAR_DB_SERVICE" << EOF
      --@git/ksd-db-flowdown/scripts/grant_crud.sql
      @${FLOWDOWN_FILE_GRANT}
      exit
EOF
  )
  echo $SQL_FLOWDOWN_GRANT

  printf "\n------ flowdown should be done --------------\n"

  # Log file that will be uploaded to the eas-ksd/uar-sup-deployer S3 bucket.
  # We have the ability to download and review those if needed.
  log_upload_time=$(date +"%m-%d-%y-%T")
  aws s3 cp uar_sup_deploy.log s3://eas-ksd/uar-sup-deployer/uar_sup_deploy-$log_upload_time.log
}

#*******************************************************************************
#
# function: start_opsworks_instance
#
# This function will start all ec2 instances associated with this OpsWorks stack
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function start_opsworks_instance
{
  printf "\n---- This SHOULD start all EC2 instances associated with $OPSWORKS_STACK_ID --------\n"
  aws opsworks --region us-east-1 start-stack --stack-id $OPSWORKS_STACK_ID
}

# Start by finding the daily prd snapshot name
get_prd_snapshot

# on to refreshing the SUP db.
refresh_database

# run DB flowdown
run_flowdown

# get the instnace ids so we can start any EC2 instances
get_opsworks_instance_ids

# Now start the instances.
start_opsworks_instance
