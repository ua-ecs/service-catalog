#!/bin/bash
#*************************************************
#
# TITLE: uar_sup_shutdown.sh
# Purpose:
# Shutdown ONLY the UAR SUP Opsworks Instance(s)
#
# DELETE the SUP database without a final snapshot - this is because it will be refreshed from prd.
#
# SUP DB and App Node shutdowns are separate because there are BI jobs that run against the SUP database at night.
#
# Technically we could hard code everything as UAR should NEVER change....
# But you never know, so I have added some functions to get the instance IDs.
#*************************************************

export AWS_DEFAULT_REGION=us-west-2

# setting up logging so if something breaks, it can be reviewed.
# everything 'run' below should go to opsworks_shutdown.log
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>uar_sup_shutdown.log 2>&1

#*************************************************
# UAR SUP Cldouformation Stack Name.  This should never change
# we need this to get the opsworks stack ID associated with it.
# while the Opsworks ID should not change either, I am automating that
# further down
#*************************************************

# ua-erp acct CF stack name
CF_STACK_NAME="KualiServicesWest-Research-Support-UarSupEnvironment"

printf "\n---- The cloudformation Stack name is: $CF_STACK_NAME ------\n"

#*******************************************************************************
#
# function: get_opsworks_instance_ids
#
# This function will get all of the instance IDs associated with a specific
# OpsWorks stack.  Does not matter if more were added manually.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function get_opsworks_instance_ids
{
  # Just a quick check to make sure we are not shutting down something else
  if [ $CF_STACK_NAME == "KualiServicesWest-Research-Support-UarSupEnvironment" ]; then
    # aws cli to get an opsworks stack id from the CF stack.
    OPSWORKS_STACK_ID=`aws cloudformation describe-stack-resources --region us-west-2 --stack-name $CF_STACK_NAME --output text --query 'StackResources[?ResourceType==\`AWS::OpsWorks::Stack\`].PhysicalResourceId'`
    echo "getting the OPSWORKS_STACK_ID  for the proto(stack id) returns: $OPSWORKS_STACK_ID"

    # Now call the stop_opsworks_instance function
    printf "\n------ CALLING STOP OPSWORKS INSTANCE ------\n"
    stop_opsworks_instance
  else
    echo "This can only be run on SUP"
    exit 1 # exit with General/Miscellaneous errors
  fi
}

#*******************************************************************************
#
# function: stop_opsworks_instance
#
# This function will stop all ec2 instances associated with this OpsWorks stack
# It does not matter if an ec2 was added manually or not.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function stop_opsworks_instance()
{
  printf "\n---- This SHOULD STOP all EC2 instances associated with $OPSWORKS_STACK_ID --------\n"
  aws opsworks --region us-east-1 stop-stack --stack-id $OPSWORKS_STACK_ID
}

#*******************************************************************************
#
# function: delete_database
#
# Deletes the UAR SUP database WITHOUT a final snapshot.
# The DB will be restored from a PRD snapshot when the env is started.
# Reason: Because we are matching how ua-erp Jenkins handled UAR SUP
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function delete_database
{
  # while this could be parameterized, it is meant ONLY for SUP, so hardcode for now.
  UAR_DB_NAME="uar-sup-db"

  # see if the DB exists.
  DB_EXISTS=$(aws rds describe-db-instances --db-instance-identifier $UAR_DB_NAME | grep -q "arn:aws:rds" &> /dev/null)

  # -- if the grep in the DB_EXISTS finds the arn string it will return a 0, otherwise it will return a 1
  if [ $? == 0 ]; then
    aws rds delete-db-instance --db-instance-identifier $UAR_DB_NAME --skip-final-snapshot

    # We could do an aws rds wait for the instance to be deleted, but there is no need as
    # this will happen at night and nothing else is dependent on it being deleted here.
    # keeping below command if we want it later
    # aws rds wait db-instance-deleted --db-instance-identifier $UAR_DB_NAME
  else
    printf "\n----- $UAR_DB_NAME does not exist, skipping delete. -----\n"
  fi
}

# kick this off by getting instance ids.  These are used to stop the instances.
get_opsworks_instance_ids

# Delete the database
# Reason: Because we are matching how ua-erp jenkins handled UAR SUP.
delete_database
