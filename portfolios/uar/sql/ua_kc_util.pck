CREATE OR REPLACE PACKAGE KRAOWNER.UA_KC_UTIL AS 
/********************************************************************************
  Package Dependencies
  Packages
  Tables
  - KRAOWNER.UAR_USER_PS_UA_DPT_UAR_KCP This is s synonym that points to 
    SYSADM.PS_UA_DPT_UAR_KCP@EPPRDGEN

  - New Table UA_KC_BATCH_STATUS

    CREATE TABLE KRAOWNER.UA_KC_BATCH_STATUS (
     JOB_NAME VARCHAR2(100)
    ,DATE_RUN DATE
    ,MESSAGE  VARCHAR2(100))
    TABLESPACE KUL01;
*********************************************************************************/

/********************************************************************************
  This procedure will populate the following table.
  - KRAOWNER.PERSON_EXT_T
  - KRAOWNER.PERSON_APPOINTMENT
  - KRAWONER.JOB_CODE

  v_out_status - This out variable will return C - Complete, E - Error
  v_out_message - This will provide a message to accompany the v_out_status

********************************************************************************/
PROCEDURE populate_person_tables(v_out_status OUT VARCHAR2
                                ,v_out_message OUT VARCHAR2);


/********************************************************************************
  This procedure is to replace the following job currently run from SOA

  Replace SOA job kc_withdrawn_deselect_checkboxes_aws

  -- Run a query
  -- If it returns more than 0 rows then run a delete statement
  -- send an email
  -- schedule to run M - F 6am
********************************************************************************/
PROCEDURE withdrawn_deselect_checkboxes;

END;
/

sho error



CREATE OR REPLACE PACKAGE BODY KRAOWNER.UA_KC_UTIL AS 
/********************************************************************************
  This procedure is to replace the following job currently run from SOA

  Replace SOA job kc_withdrawn_deselect_checkboxes_aws-MEM-19447

  -- Run a query
  -- If it returns more than 0 rows then run a delete statement
  -- send an email
  -- schedule to run M - F 6am
********************************************************************************/
PROCEDURE withdrawn_deselect_checkboxes IS

  CURSOR check_cnt IS
    SELECT count(1)
    FROM   kraowner.proto_amend_renew_modules parm 
    WHERE  
        EXISTS (
          SELECT NULL 
          FROM   
           kraowner.protocol p
          ,kraowner.proto_amend_renewal par
          ,kraowner.protocol_status ps
          ,kraowner.protocol_modules pm 
          WHERE  
              p.protocol_id = par.protocol_id 
          AND parm.proto_amend_renewal_id = par.proto_amend_renewal_id 
          AND p.protocol_status_code = ps.protocol_status_code 
          AND pm.protocol_module_code = parm.protocol_module_code 
          AND p.protocol_status_code = '304' 
          AND parm.protocol_module_code NOT IN ( '008', '023' ));

  v_count   NUMBER;
  v_to      VARCHAR2(1000);
  v_subject VARCHAR2(1000);
  v_message VARCHAR2(1000);

BEGIN

  OPEN check_cnt;
    FETCH check_cnt INTO v_count;
  CLOSE check_cnt;


  IF v_count > 0 THEN
    DELETE FROM kraowner.proto_amend_renew_modules parm 
    WHERE  EXISTS (SELECT par.proto_amend_ren_number, 
                      ps.description, 
                      parm.protocol_module_code, 
                      pm.description 
               FROM   kraowner.protocol p, 
                      kraowner.proto_amend_renewal par, 
                      kraowner.protocol_status ps, 
                      kraowner.protocol_modules pm 
               WHERE  p.protocol_id = par.protocol_id 
                      AND parm.proto_amend_renewal_id = 
                          par.proto_amend_renewal_id 
                      AND p.protocol_status_code = ps.protocol_status_code 
                      AND pm.protocol_module_code = parm.protocol_module_code 
                      AND p.protocol_status_code = '304' 
                      AND parm.protocol_module_code NOT IN ( '008', '023' ));
  END IF;

  v_message := 'Number of rows deleted: '||v_count;

  INSERT INTO KRAOWNER.UA_KC_BATCH_STATUS (
     JOB_NAME
    ,DATE_RUN 
    ,MESSAGE)
  VALUES(
     'withdrawn_deselect_checkboxes'
    ,SYSDATE
    ,v_message);

  COMMIT;

  --Send Email here once KC is on 12.1 (right now on 11.2)

  --From Default 
  --To   kuali_coeus@list.arizona.edu,ua_met_app@tbginc.com
  --Subject	KC_Withdrawn_Deselect_Checkboxes_AWS result notification 
  --Body	Environment: KCPRD 
  --Execution Status: Success 
  --Status Details: Number of rows updated: 6 

EXCEPTION WHEN OTHERS THEN
  --Send an email to MET
  null;

END withdrawn_deselect_checkboxes;


/********************************************************************************
  This procedure will populate the following table.

  - KRAOWNER.PERSON_EXT_T
  - KRAOWNER.PERSON_APPOINTMENT
  - KRAWONER.JOB_CODE

  v_out_status - This out variable will return C - Complete, E - Error
  v_out_message - This will provide a message to accompany the v_out_status

********************************************************************************/
PROCEDURE populate_person_tables(v_out_status OUT VARCHAR2
                                ,v_out_message OUT VARCHAR2) IS

  CURSOR check_count IS 
    SELECT COUNT(1)
    FROM   UAR_USER_PS_UA_DPT_UAR_KCP;

  CURSOR check_temp_table (in_owner      IN VARCHAR2 
                          ,in_temp_table IN VARCHAR2) IS
    SELECT COUNT(1)
    FROM   ALL_TABLES
    WHERE  
        OWNER      = in_owner
    AND TABLE_NAME = in_temp_table;

  v_sql        VARCHAR2(4000);
  v_owner      VARCHAR2(30) := 'KRAOWNER';
  v_temp_table VARCHAR2(30) := 'ZDT_PA_PREP';
  v_min_count  NUMBER       := 15000;
  v_count      NUMBER;

  v_to      VARCHAR2(1000);
  v_subject VARCHAR2(1000);
  v_message VARCHAR2(1000);

BEGIN
  -- Set outgoing status to C - Complete
  v_out_status := 'C';

  -- Check to make sure there are at least 15,000 rows in SYSADM.PS_UA_DPT_UAR_KCP@EPPRDGEN
  BEGIN
    OPEN check_count;
      FETCH check_count INTO v_count;
      IF check_count%NOTFOUND THEN
        v_count := 0;
      END IF;
    CLOSE check_count;
  EXCEPTION WHEN OTHERS THEN
     RAISE_APPLICATION_ERROR(-20111,SUBSTR('Could not execute get count on SYSADM.PS_UA_DPT_UAR_KCP@EPPRDGEN-SQLERRM='||SQLERRM,1,2000));
  END;

  IF v_count < v_min_count THEN
    v_out_message := 'The minium number of records in the driver table is '||v_min_count||' '||
                            'there are only '||v_count||' records so processing will stop';
    RETURN;
  END IF;
  
  -- Check to see if TEMP table already exists.  If it does then DROP it.
  OPEN check_temp_table(v_owner,v_temp_table);
    FETCH check_temp_table INTO v_count;
    IF check_temp_table%NOTFOUND THEN
      v_count := 0;
    END IF;
  CLOSE check_temp_table;

  IF v_count > 0 THEN
    v_sql := 'DROP TABLE '||v_owner||'.'||v_temp_table;
    BEGIN
      EXECUTE IMMEDIATE v_sql;
    EXCEPTION WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20101,SUBSTR('Could not execute SQL-'||v_sql||' SQLERRM='||SQLERRM,1,2000));
    END;
  END IF;

  -- Load temporary table in KC to avoid BI dependance
  v_sql := 'CREATE TABLE KRAOWNER.ZDT_PA_PREP as ( '||
           'SELECT DISTINCT '||
           'UAID,'||
           'DEPT_ID,'||
           'JOB_ENTRY_DT,'||
           'CLASS_INDC,'||
           'UA_TITLE_PRIM_LD,'||
           'UA_TITLE_CONCAT,'||
           'JOBCODE,'||
           'JOBCODE_LD,'||
           'TOTAL_ANNUAL_RT,'||
           'CITZ_STAT_LD '|| 
           'FROM UAR_USER_PS_UA_DPT_UAR_KCP)';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20102,SUBSTR('Could not execute SQL-'||v_sql||' SQLERRM='||SQLERRM,1,2000));
  END;
 
  -- Clear tables
  BEGIN
    --v_sql := 'TRUNCATE TABLE KRAOWNER.PERSON_EXT_T';
    v_sql := 'DELETE FROM KRAOWNER.PERSON_EXT_T';
    EXECUTE IMMEDIATE v_sql;

    --v_sql := 'TRUNCATE TABLE KRAOWNER.PERSON_APPOINTMENT';
    v_sql := 'DELETE FROM KRAOWNER.PERSON_APPOINTMENT';
    EXECUTE IMMEDIATE v_sql;

    --v_sql := 'TRUNCATE TABLE KRAOWNER.JOB_CODE';
    v_sql := 'DELETE FROM KRAOWNER.JOB_CODE';
    EXECUTE IMMEDIATE v_sql;

  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20103,SUBSTR('Could not execute SQL-'||v_sql||' SQLERRM='||SQLERRM,1,2000));
  END;


  -- Load job code into KRAOWNER.JOB_CODE
  v_sql := 'INSERT INTO KRAOWNER.JOB_CODE ( '||
           'JOB_CODE,'||
           'JOB_TITLE,'||
           'UPDATE_TIMESTAMP,'||
           'UPDATE_USER,'||
           'VER_NBR,'||
           'OBJ_ID) '||
           'SELECT DISTINCT '||
           'JOBCODE as JOB_CODE,'||
           'SUBSTR(JOBCODE_LD,1,50) as JOB_TITLE,'||
           'sysdate as UPDATE_TIMESTAMP,'||
           '''KRABI'' AS UPDATE_USER,'||
           '1 AS VER_NBR,'||
           '1 AS OBJ_ID '||
           'FROM '||v_owner||'.'||v_temp_table;

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20104,SUBSTR('Could not execute SQL-'||v_sql||' SQLERRM='||SQLERRM,1,2000));
  END;

  BEGIN
    UPDATE KRAOWNER.JOB_CODE SET OBJ_ID = SYS_GUID ();
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20105,SUBSTR('Could not execute SQL-UPDATE KRAOWNER.JOB_CODE... SQLERRM='||SQLERRM,1,2000));
  END;


  -- Load Personnel data into KRAOWNER.PERSON_APPOINTMENT
  v_sql := 'INSERT INTO KRAOWNER.PERSON_APPOINTMENT ( '||
           'APPOINTMENT_ID,'|| 
           'PERSON_ID,'|| 
           'UNIT_NUMBER,'||
           'APPOINTMENT_START_DATE,'|| 
           'APPOINTMENT_TYPE_CODE,'|| 
           'JOB_TITLE, '||
           'PREFERED_JOB_TITLE,'||
           'JOB_CODE, '||
           'SALARY, '||
           'UPDATE_TIMESTAMP, '||
           'UPDATE_USER, '||
           'VER_NBR, '||
           'OBJ_ID) '||
           'SELECT '|| 
           'SEQ_PERSON_APPOINTMENT.NEXTVAL,'||
           'TRIM(P.UAID),'||
           'P.DEPT_ID,'||
           'P.JOB_ENTRY_DT,'||
           'CASE '||
             'WHEN P.CLASS_INDC = ''0'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''1'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''2'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''3'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''4'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''5'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''6'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''7'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''8'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''9'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''A'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''D'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''E'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''F'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''G'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''H'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''I'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''J'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''K'' THEN ''2'' '||
             'WHEN P.CLASS_INDC = ''L'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''M'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''N'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''P'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''Q'' THEN ''1'' '||
             'WHEN P.CLASS_INDC = ''R'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''U'' THEN ''3'' '||
             'WHEN P.CLASS_INDC = ''V'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''W'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''X'' THEN ''6'' '||
             'WHEN P.CLASS_INDC = ''Z'' THEN ''6'' '||
             'ELSE ''999'' '||
            'END,'||
            'P.UA_TITLE_PRIM_LD,'||
            'P.UA_TITLE_CONCAT,'||
            'P.JOBCODE,'||
            'P.TOTAL_ANNUAL_RT,'||
            'SYSDATE,'||
            '''KRABI'','||
            '1,'||
            'SYS_GUID () '||
            'FROM '||v_owner||'.'||v_temp_table||' P '||
            'WHERE TRIM(UAID) IS NOT NULL AND UAID <> ''-''';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20106,SUBSTR('Could not execute SQL-'||SUBSTR(v_sql,1,200)||'... SQLERRM='||SQLERRM,1,2000));
  END;


  -- Load Citizenship data into KRAOWNER.PERSON_EXT_T
  v_sql := 'INSERT INTO KRAOWNER.PERSON_EXT_T ('||
           'PERSON_ID,'|| 
           'CITIZENSHIP_TYPE_CODE,'||
           'UPDATE_TIMESTAMP,'||
           'UPDATE_USER,'||
           'VER_NBR,'|| 
           'OBJ_ID) '||
           'SELECT '||
           'TRIM(UAID),'||
           'CASE '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Citizen'' THEN 1 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Naturalized'' THEN 1 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Permanent Resident'' THEN 2 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Alien Permanent'' THEN 2 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Temporary Visa'' THEN 3 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Employment Visa'' THEN 3 '||
             'WHEN TRIM(CITZ_STAT_LD) = ''Canadian Citizen'' THEN 3 '||
             'ELSE 1 '||
           'END,'||
           'SYSDATE,'||
           '''KRABI'','||
           '1,'||
           'SYS_GUID() '||
           'FROM '||v_owner||'.'||v_temp_table||' '||
           'WHERE TRIM(UAID) IS NOT NULL AND UAID <> ''-''';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20107,SUBSTR('Could not execute SQL-'||SUBSTR(v_sql,1,200)||'... SQLERRM='||SQLERRM,1,2000));
  END;


  -- Loading object code for summer salaries
  BEGIN
    INSERT INTO KRAOWNER.JOB_CODE (
     JOB_CODE
    , JOB_TITLE
    , UPDATE_TIMESTAMP
    , UPDATE_USER
    , VER_NBR
    , OBJ_ID)
    VALUES(
      'KCSUMM'
    , 'Academic Appointment Summer'
    , SYSDATE
    , 'KRABI'
    , 1
    , sys_guid());
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20108,SUBSTR('Could not execute SQL-Loading object code for summer... SQLERRM='||SQLERRM,1,2000));
  END;


  -- Load calculated summer salaries for Academic Appointment staff with summer object code
  BEGIN
    INSERT INTO KRAOWNER.PERSON_APPOINTMENT (
      APPOINTMENT_ID
    , PERSON_ID
    , UNIT_NUMBER
    , APPOINTMENT_START_DATE
    , APPOINTMENT_TYPE_CODE
    , JOB_TITLE
    , PREFERED_JOB_TITLE
    , JOB_CODE
    , SALARY
    , UPDATE_TIMESTAMP
    , UPDATE_USER
    , VER_NBR
    , OBJ_ID)
    SELECT 
      SEQ_PERSON_APPOINTMENT.NEXTVAL
    , PERSON_ID
    , UNIT_NUMBER
    , APPOINTMENT_START_DATE
    , '2'
    , JOB_TITLE
    , PREFERED_JOB_TITLE
    , 'KCSUMM'
    , ROUND((SALARY * 0.00072 * 464),2)
    , SYSDATE
    , 'KRABI'
    , 1
    , SYS_GUID ()
    FROM KRAOWNER.PERSON_APPOINTMENT
    WHERE appointment_type_code = '3';
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20109,SUBSTR('Could not execute SQL-Load calculated summar salaries ... SQLERRM='||SQLERRM,1,2000));
  END;

  --Drop Temp Table
  v_sql := 'DROP TABLE '||v_owner||'.'||v_temp_table;
  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20110,SUBSTR('Could not execute SQL-'||v_sql||' SQLERRM='||SQLERRM,1,2000));
  END;

  v_message := 'Number of rows deleted: '||v_count;

  INSERT INTO KRAOWNER.UA_KC_BATCH_STATUS (
     JOB_NAME
    ,DATE_RUN 
    ,MESSAGE)
  VALUES(
     'populate_person_tables'
    ,SYSDATE
    ,'Successful Run');

  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    v_out_status  := 'E';
    v_out_message := 'UA_KC_UTIL.POPULATE_PERSON_TABLES Error - '||SQLERRM;
     
END populate_person_tables;

END ua_kc_util;
/
sho error

--GRANT EXECUTE ON KRAOWNER.UA_KC_UTIL TO SOA_USER
--/
