# University of Arizona CIO / UITS AWS Account Foundations

CIO managed AWS accounts should all be set up in a standard way, and should adhere to
security and tagging policies that have been established. These templates are modeled
after the [AWS NIST 800-53 Quickstart][1].

These policies will be supported by a set of Foundation templates that are deployed to CIO
AWS accounts.

These Foundation templates are delivered through a git repository hosted in BitBucket:
[https://bitbucket.org/ua-ecs/service-catalog/src][2]

[1]: http://docs.aws.amazon.com/quickstart/latest/accelerator-nist/overview.html
[2]: https://bitbucket.org/ua-ecs/service-catalog/src

These Foundation templates provide the following resources and controls.

## Alarms

Several alarms are created to watch for activity from CloudTrail and CloudWatch Logs.
These notifications are delivered to the email address of the main account by default.


| Alarm                  |  Prod    | Non-Prod |
| -----------------------| :------: | :------: |
| Network ACL Changes    |     X    |    X     |
| Security Group Changes |     X    |          |
| Root Account Activity  |     X    |    X     | 
| Unauthorized Access    |     X    |    X     | 
| IAM Policy Changes     |     X    |          |
| Access Key Creation    |     X    |          |
| CloudTrail Changes     |     X    |          |



## Federated Roles

The fdn-iam template creates several roles which can be accessed via account federation 
through Shibboleth.

> All access by people should be driven through a Federated role.  
> Limit IAM user accounts to application and service account types.

|IAM Role         |Role Abilities |Production Members |Non-Prouction Members  |
|---              |---            |---                |---                    |
|SysAdmin         |Access to All AWS features except for IAM and KMS|Production Deployment & Update Staff|Deployment & Update Staff / ECSApplication / Team Members|
|IAMAdmin         |Access to all IAM features|Production Deployment & Update Staff|Deployment & Update Staff / ECS|
|InstanceOps      |All EC2, ELB, CloudWatch, and Auto-scaling features|Operations Staff|Operations StaffApplication / Team Members|
|ReadOnly         |Read-only access to a broad set of services. See the complete list here.|Operations Staff|Operations StaffApplication / Team Members|
|BillingReadOnly  |Read-only access to billing information. |Financial Team|Financial Team|
|BillingPurchasing|Buy reserved instances. |Financial Team|Financial Team|
|NetworkSecurity  |Undefined  |NetSecOps Staff  |NetSecOps Staff|
|DatabaseAdmin    |Undefined  |DBAs / Burgundy Group  |DBAs / Burgundy Group|


## OpsWorks Service Role

The template creates an OpsWorks service role that can be referenced anytime you need to
create an OpsWorks stack. This way the service role is not tied to your stack, and there
is no risk of accidentally deleting the service role before the rest of the OpsWorks stack
has been removed.  This role will be exported in us-west-2 and available for import in
CloudFormation templates:

> The exact export name may vary if you customized the foundation templates somehow, or
> deployed the stack names with a non-standard name. The foundation IAM stack should be
> named "fdn-ima" by default, leading to the following import statement functioning. Check
> your CloudFormation Exports list to be certain.

## CloudFormation Admin Deployer Role

Because many CloudFormation templates create or manipulate IAM resources, it is not
possible to deploy those templates when logging in through Shibboleth Federation in the
console. However CloudFormation now supports deployment roles, meaning that the IAM
permissions used for deploying a stack will be taking from the designated role, and not
the logged in console user.  So this CloudFormation Deployer role has all AWS account
permissions allowed to it, allowing you to assign this role to a given stack deployment,
and thus being able to deploy those templates.  This makes it much less likely for a
console user to perform accidental high level actions, as those actions would first have
to be codified in a CloudFormation template, and the run with this role.

You can choose this role from the popup on the Options screen when deploying a
CloudFormation template.

![CloudFormation Deployer Role selection during deployment](https://bytebucket.org/ua-ecs/service-catalog/raw/d0058da3e8c0a871c9c43c8f5133f7c5cbe57326/portfolios/account-foundation/images/cf-admin-deployer.png)

You can also include it on the command line tools by specifying the *-role-arn* option.

    aws cloudformation create-stack \
        --region us-west-2 \
        --stackname "my-great-stack" \
        --capabilities "CAPABILITY_IAM" \
        --template-body "file://cf-template.yaml \
        --parameters "file://params.json" \
        --on-failure "DO_NOTHING" \
        --role-arn "arn:aws:iam::XXXXXXXXXXXX:role/fdn-iam-CloudFormationAdminDeployerRole-12AJKX5827CDR

> Your Role ARN will be different for each account.


## Cloudability User
An IAM user with the [required permissions][1] for Cloudability to retrieve detailed service
information from this account is created. 

[1]: http://ua-ecs.bitbucket.io/service-catalog/portfolios/doc/foundation-user-roles.html#cloudability-user-and-policy

> IAM Access Keys are NOT created during the template deployment for security reasons. If
> you want to connect this account to Cloudabilty you will need to (As the IAMAdmin)
> generate Access Keys for the Cloudabilty user.


## Lambda Functions

Several Lambda functions are created during the foundation setup to help out and to enforce policy.


### Sleep Delay Lambda Function

Currently when deploying Lambda functions via CloudFormation, it is possible (~50%) for
the deployment to fail because the Execution Role for the function isn't somehow fully
created by the time the Lambda function gets created. Unfortunately the only workaround so
far is to simply wait some time between creating the Execution Role, and creating the
Lambda function.  Since CloudFormation supports custom resources that can be tied to
existing Lambda functions, this function servers to simply sleep 30 seconds before
completing.  You can then chain your resources together in CloudFormation using the
DependsOn: option. This template exports foundation-SleepDelayFunction-arn for use in
CloudFormation ImportValue statements.

    SleepDelay:
      Type: Custom::SleepDelay
      DependsOn:
      - LambdaExecutionRole
      Properties:
        ServiceToken: !ImportValue foundation-SleepDelayFunction-arn


### OpsWorks Tagging Lambda Function

The AWS OpsWorks service currently does not support tagging of EC2 instances that are
created from within the OpsWorks service. This makes billing and resource tracking very
difficult.  This lambda function listens for CloudWatch EC2 CreateInstance events, will
pull tag data from its parent OpsWorks stack and apply those tags to the instance.  This
is accomplished by setting custom JSON on the OpsWorks stack in the following format:

    {
      "opsworks_tags": {
        "instances": {
          "owner": "Your Name",
          "projectname": "Some Project",
          "netid":"fischerm"
        }
      }
    }

This can be specified in a CloudFormation template also:

    EnvStack:
      Type: AWS::OpsWorks::Stack
      Properties:
        Name: !Sub "${EnvAppName} ${EnvSlug}"
        VpcId: !Ref VPCID
        ...
        CustomJson:
          opsworks_tags:
            instances:
              owner: !Ref TagOwner
              netid: !Ref TagNetid
              projectname: !Ref TagProjectName

### CreatedBy Lambda Function

This Lambda function listens for CloudWatch EC2 RunInstance events and adds a "createdby"
tag to all instances with whatever principal initiated the create event.  If this is a
user on the Console, then the username is put in there. If it is CloudFormation or
OpsWorks, then the service is put in there.

Currently this only works for EC2 but we have plans to extend this to RDS, CloudFormation,
and S3 buckets.

Tag values will look something like:

* user/windhamg
* assumed-role/uapilots-shib-powerusers/cantuj@arizona.edu
* assumed-role/aws-opsworks-service-role/OpsWorks
* assumed-role/cloudformation-deployer-admin-role/AWSCloudFormation


### Alarm Logs Lambda Function
This Lambda function enriches the alarm notifications that are generated from the
fdn-logging layer. The CloudWatch Alarms established in that template send their alarm
notifications to a Security Lambda SNS topic.  This Lambda function is subscribed to that
Topic, and fires whenever an alarm is triggered.

The function examines the alarm that was sent, and uses information in the alarm to look
up specific log entries in the CloudTrail log group in CloudWatch. The Lambda function
then formats a new message with details from the log entries and sends it to the Security
Alarm SNS topic, which is subscribed to by the account owner email address.


### EC2 Route53 Lambda Function

This Lambda function listens for CloudWatch EC2 RunInstance events, and looks at the tags
on the just started EC2 instance.  If it finds Route53 related tags, this function will
create appropriate Route53 records for that EC2 instance.

> A matching Route53 Hosted Zone is required for this function to work.
>
> Currently this function only creates Route53 entries. Manual cleanup of unneeded entries is required.

|Tag Name|Action|
|--- |--- |
|privateroute53record|Create a Route53 A record with the Tag's Value pointed at the Private IP address of this EC2 Instance|
|publicroute53record|Create a Route53 A record with the Tag's Value pointed at the Public IP address of this EC2 Instance|
|opsworks:instance|Pre-pend the Tag's Value to any records created above|


### Zabbix SSM Parameter Store Values

In order to facilitate the use of Zabbix on EC2 hosts in AWS, certain configuration values
need to be stored in the account. The Foundation templates deploy the following parameters
in the SSM Parameter Store

* ZabbixAPIHostname
* ZabbixProxyHostname
* ZabbixAPIUsername
* ZabbixAPIPassword



# Setup & Installation

## Ansible Deployer

This project is deployed via Ansible, and so needs to have a host with ansible installed on it, 
and also with permissions to do all the account setup functions.  There is an accompanying project
called 'ansible-deployer' which can be used to run this project from.  See that project for 
setup details.

## Lambda Source Staging
The source code for the Lambda functions must be placed into an existing S3 bucket which is 
publicly accessible. The path and bucketname must be passed in to the main new account playbook
script when its run.

Initially, and when there are code updates to the lambda functions, you must run the 'staging' 
playbook which will package up the lambda functions and put them into the S3 distribution bucket.

    $ ansible-playbook stage-files.yaml

You will be asked for an AWS Access / Secret Key pair which will need to have PutObject permissions
on the S3 bucket/path to hold the contents of the 'src' directory.

## New Account Deployment

When setting up a new AWS account, run the following:

    $ ansible-playbook -v new-account-playbook.yaml

This will kick off the ansible playbook and deploy all the resources required for a new AWS account.



## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
