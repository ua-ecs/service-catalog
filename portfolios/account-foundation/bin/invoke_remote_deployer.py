"""
This program triggers the Remote Deployer Lambda function in the ua-erp account
to go out and run an account foundation update in the target accounts.

This program can be used to trigger multiple concurrent updates.

You must have active API Keys for the ua-erp account, with sufficient permissions
to trigger the remote deployer lambda function.
"""
import sys
import boto3
import json

client = boto3.client('lambda')

if len(sys.argv) == 1:
    print("Usage: python3 invoke_remote_deployer.py <nonprod|prod|[acct1,acct2,acct3]>")
    sys.exit()

account_arg = sys.argv[1]
if account_arg == "nonprod":
    # get all the nonprod accounts
    a = 1
elif account_arg == "prod":
    # get all the prod accounts
    a = 2
else:
    # Turn the comma separated string of account IDs into a propper list
    account_num_list = account_arg.split(",")

for account_id in account_num_list:
    lambda_event = {
        "TargetAccountId": account_id,
        "PerformVersionCheck": True
    }

    event_string = json.dumps(lambda_event)
    response = client.invoke(
        FunctionName='fdn-remote-deployer-lambda',
        InvocationType='Event',
        LogType='Tail',
        Payload=event_string.encode("utf-8")
    )
    print(response)
