#!/bin/bash

/usr/local/bin/ansible-playbook new-account-playbook.yaml -vvv  \
    --extra-vars "alarmEmail='${AlertEmail}'                    \
                  accountType='${AccountType}'                  \
                  ZabbixAPIHostname='${ZabbixAPIHostname}'      \
                  ZabbixProxyHostname='${ZabbixProxyHostname}'  \
                  ZabbixAPIUsername='${ZabbixAPIUsername}'      \
                  ZabbixAPIPassword='${ZabbixAPIPassword}'      \
                  serviceTag='${ServiceTag}'                    \
                  environmentTag='${EnvironmentTag}'            \
                  contactNetidTag='${ContactNetidTag}'          \
                  accountNumberTag='${AccountNumberTag}'        \
                  ticketNumberTag='${TicketNumberTag}'"         \
    >> /tmp/foundation-ansible.log 2>&1
