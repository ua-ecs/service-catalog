# Cost and Usage Report - Athena integration support

This CloudFormation template was generated by AWS when enabling the Athena integration on the Cost and Usage report. It has been modified to handle tagging on those resources which can be tagged.

Note that the template is specific to the CUR report it was generated from and should not be used otherwise.
