# U of A VPC Templates

These templates will create a basic two availability zone VPC. There are two flavors,
one with, and one without a VPC connection back to campus.

## Pre-Requisites

Before using this template, you should already have an AWS account created, along with
IAM administrator rights in the account.

You should have some familiarity with running CloudFormation templates. See the 
[AWS CloudFormation][cloudform] documentation to get started, or come to the
[AWS User Group][] meetings some time.

[AWS User Group]: https://confluence.arizona.edu/display/UIPR/AWS
[cloudform]: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html


### VPN based VPC Pre-Requisites:

If you are creating a VPC with a VPN connection back to campus, you will need to contact
Network Operations first in order to be assigned a section of the private 10 net space used
on campus.  [Contact NetOps by filling out a ticket][netops] and request the following:

> _Hello, my name is {...} and I'd like to request a VPN connection to an AWS VPC for 
> {your department/college/unit}. Please allocate a CIDR block for me from the campus 
> 10 net space, and let me know what BGP IP addresses to use for setting up my VPC.  
> I'll respond with the VPN configurations once they're set up.  Thanks!_

They'll send you back something like:

    VPC CIDR Block: 10.221.10.0/20
    
    ASR BGP ASN 1: 65524
    ASR IP 1: 206.207.227.86
    
    ASR BGP ASN 2: 65525
    ASR IP 2: 206.207.227.102

You'll need this information to plug into the CloudFormation template when you deploy it.

[netops]: http://uits.arizona.edu/get-support

## Subnet Allocation

If you're deploying a non-VPN based VPC, then feel free to pick whatever 10 net space you'd like
for your VPC.  See the [AWS documentation for VPC sizing][vpcsizing] to get started. If you're
deploying a VPN based VPC, and you've received your IP CIDR block back from NetOps already, then
you'll use that for your VPC's CIDR block.

[vpcsizing]: http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html

Once you have your IP space for your whole VPC in hand, you'll need to decide how you're going
to split it up among the availability zones and subnets.

Both templates build out four subnets, two in each availability zones.  There are a pair of 
public subnets, and a pair of private subnets.

### Public Subnets

The two public subnets are public because they have a route table assigned to them that allows
incoming traffic from the public internet to reach them. These subnets are typically smaller, 
since its best practice to keep application and database services in the private subnets. Usually
you will only have load balancers, or maybe a NAT device of bastion host in the public subnets.

### Private Subnets

The private subnets are not reachable from the public internet.  In the case of a non-VPN based
VPC, the private subnets are only accessible from within the VPC, to resources in either the 
private or public subnets.  This allows for a public load balancer to communicate to a back end
application node, or for you to connect to a bastion host in the public subnet, then hop to a 
private resource from there.

In the case of a VPN based VPC, the private subnet is also available from certain on-campus 
resources, and also resources in the private subnets can talk securely back to restricted
on-campus resources.

### Example Subnets

Say you're given the following CIDR block from NetOps:

    10.221.10.0/20

A 'slash 20' CIDR block has 4096 IP addresses available in it.  Lets say we want to have two
smaller public subnets, and two larger private subnets.

    Public Subnet A:    10.221.10.0/23       512 Addresses
    Public Subnet B:    10.221.12.0/23       512 Addresses
    Private Subnet A:   10.221.14.0/22       1024 Addresses
    Private Subnet B:   10.221.18.0/22       1024 Addresses

This leaves an additional 1024 addresses unassigned, which you could use in the future if you
needed greater network separation for some application.

Again, this is _only an example_ and should not be used as-is.  Come to the [AWS User Group][]
meetings some time to discuss additional scenarios.

[AWS User Group]: https://confluence.arizona.edu/display/UIPR/AWS

## Template Deployment

Once you've gathered the above information, you're ready to deploy the template.

Download the template you want from the site here and save it somewhere on your local PC.

Go to the CloudFormation service in your AWS account and click the "Create Stack" button.
Then choose "Upload a template to Amazon S3" and upload the .json template file. Click the
"Next" button.

Enter a Stack Name for this deployment. This is a purely descriptive name that will be used
to describe this particular CloudFormation deployment. You may end up creating more than
one VPC in your account, so name this deployment something appropriate.

Fill out the rest of the fields with the information gathered above, then click "Next".

Add any tags you'd like.

Review your template, and click "Create".

After that, you can watch the progress in the _Events_ tab of your deployment.

### Non-VPC Deployment

Once the stack has deployed, you're done! Go to the VPC section of the AWS console, and 
look around at the pieces created (VPC, subnets, etc). Note that this VPC tempalte
does not create any compute resources, so you may need to [deploy a NAT box][nat] (for resources 
in your private subnet to access the public internet) and a [bastion host][] (so you could 
SSH in to resources in the private subnets).

Once the stack has finished deploying, you will need to gather some additional information
to send back to NetOps to complete the VPN setup.

[nat]: http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_NAT_Instance.html
[bastion host]: http://blogs.aws.amazon.com/security/post/Tx3N8GFK85UN1G6/Securely-connect-to-Linux-instances-running-in-a-private-Amazon-VPC

### VPC Deployment

Once the stack has been deployed, you need to get some information and send it back to NetOps in 
order to finalize the VPN connection.

1. Go to the VPC service in the Amazon Console
2. Go to "VPN Connections" in the left side menu
3. Select each of the VPN Connections in turn, and click the "Download Configuration" button
4. Choose "Cisco Systems, Inc" from the Vendor popup. Leave the other menu options as default
5. Click the "Yes, Download" button, and save the resulting file somewhere. Each of the VPN
   connections is associated with either ASR1 or ASR2. Add that "ASR1" or "2" to the files that
   you download, that makes it easier for NetOps to finish their work. 
6. Send these two files to NetOps in a secure manner (encrypted email, or [Stache][]).

[Stache]: https://stache.arizona.edu/

Once NetOps has the configuration files, they will contact you once the configurations are finished.
Check the "State" value of the VPN Connections and they should say "Available" once everything
is working.

Once the VPN is in place, you should be able to connect directly to resources in the private subnets
in your VPC directly using that resource's private IP address (ie 10.221.10.15).

## Further References

* <http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html>
* <http://docs.aws.amazon.com/AmazonVPC/latest/GettingStartedGuide/ExerciseOverview.html>

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

<uits-ecs@list.arizona.edu>
