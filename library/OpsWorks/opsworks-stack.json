{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "ECS OpsWorks Stack",
  "Metadata" : {
    "AWS::CloudFormation::Interface" : {
      "ParameterGroups" : [
        {
          "Label" : {"default": "Application Information"},
          "Parameters" : [
            "EnvAppName",
            "AppSlug",
            "EnvSlug"
          ]
        },
        {
          "Label" : {"default": "Load Balancer Settings"},
          "Parameters" : [
            "SSLCertName",
            "LBSubnetA",
            "LBSubnetB"
          ]
        },
        {
          "Label" : {"default": "Instance Settings"},
          "Parameters" : [
            "InstanceType",
            "KeyName",
            "VPCID",
            "AppSubnetA",
            "AppSubnetB",
            "CustomCookbooksSource"
          ]
        },
        {
          "Label" : {"default": "Tags"},
          "Parameters" : [
            "TagOwner",
            "TagNetid",
            "TagProjectName"
          ]
        }
      ],
      "ParameterLabels" : {
        "EnvAppName":       {"default": "Application Name:"},
        "AppSlug":          {"default": "Application Slug:"},
        "EnvSlug":          {"default": "Environment Slug:"},
        "LBSubnetA":        {"default": "Load Balancer Subnet A:"},
        "LBSubnetB":        {"default": "Load Balancer Subnet B:"},
        "AppSubnetA":       {"default": "App Instance Subnet A:"},
        "AppSubnetB":       {"default": "App Instance Subnet B:"}
      }
    }
  },
  "Parameters": {
    "EnvAppName": {
      "MinLength": "3",
      "Type": "String",
      "Description": "Full Application name, ie 'Kuali Financials'"
    },
    "EnvSlug": {
      "MinLength": "2",
      "Type": "String",
      "Description": "Short environment slug, ie 'dev'. Lowercase letters, numbers and dashes only",
      "AllowedPattern" : "[a-z0-9]*"
    },
    "AppSlug": {
      "MinLength": "3",
      "Type": "String",
      "Description": "Short application slug, ie 'kfs'. Lowercase letters, numbers and dashes only",
      "AllowedPattern" : "[a-z0-9-]*"
    },
    

    "VPCID" : {
      "Description" : "Target VPC",
      "Type" : "AWS::EC2::VPC::Id"
    },
    "AppSubnetA" : {
      "Description" : "Application Subnet for Zone A",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "AppSubnetB" : {
      "Description" : "Application Subnet for Zone B",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    

    "SSLCertName" : {
      "Description" : "SSL Certificate Name as uploaded",
      "Type" : "String"
    },
    "LBSubnetA" : {
      "Description" : "Load Balancer Subnet for Zone A",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "LBSubnetB" : {
      "Description" : "Load Balancer Subnet for Zone B",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    
    "KeyName" : {
      "Description" : "Amazon EC2 Key Pair",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "CustomCookbooksSource": {
      "Description" : "URL to S3 cookbooks, ie 'https://s3.amazonaws.com/edu.arizona.pilots.eas/cookbooks.tar.gz'",
      "Type": "String"
    },
    "InstanceType" : {
      "Description" : "EC2 Instance Type",
      "Type" : "String",
      "Default" : "t2.micro",
      "AllowedValues" : ["t2.micro", "m3.small", "m3.medium"]
    },
   
    "TagOwner" : {
      "Description" : "Name of the Owner of this resource (Full Name)",
      "Type" : "String"
    },
    "TagNetid" : {
      "Description" : "UA NetID of responsible person",
      "Type" : "String"
    },
    "TagProjectName" : {
      "Description" : "Name of the Project this is for, or JIRA ticket for more info",
      "Type" : "String"
    }
    
  },
  "Resources": {
    "EnvServiceRole": {
       "Type": "AWS::IAM::Role",
       "Properties": {
          "AssumeRolePolicyDocument": {
             "Version" : "2012-10-17",
             "Statement": [ {
                "Effect": "Allow",
                "Principal": {
                  "Service": [ "opsworks.amazonaws.com" ]
                },
                "Action": [ "sts:AssumeRole" ]
             } ]
          },
          "Path": "/",
          "Policies": [ 
            {
              "PolicyName": "opsworks-service",
              "PolicyDocument": {
                "Statement": [
                {
                    "Action": [
                        "ec2:*",
                        "iam:PassRole",
                        "cloudwatch:GetMetricStatistics",
                        "elasticloadbalancing:*",
                        "rds:*"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "*"
                    ]
                }
                ]
              }
            }
          ]
       }
    },
    "EnvInstanceRole": {
       "Type": "AWS::IAM::Role",
       "Properties": {
          "AssumeRolePolicyDocument": {
             "Version" : "2012-10-17",
             "Statement": [ {
                "Effect": "Allow",
                "Principal": {
                   "Service": [ "ec2.amazonaws.com" ]
                },
                "Action": [ "sts:AssumeRole" ]
             } ]
          },
          "Path": "/",
          "Policies": [ ]
       }
    },
    "EnvInstanceProfile": {
       "Type": "AWS::IAM::InstanceProfile",
       "Properties": {
          "Path": "/",
          "Roles": [ { "Ref": "EnvInstanceRole"} ]
       }
    },
    "EnvStack": {
      "Type": "AWS::OpsWorks::Stack",
      "Properties": {
        "Name": { "Fn::Join": [" ", [{ "Ref": "EnvAppName" }, { "Ref": "EnvSlug" }] ] },
        "ConfigurationManager": {
          "Name": "Chef",
          "Version": "12"
        },
        "CustomCookbooksSource": {
          "Type": "s3",
          "Url": { "Ref": "CustomCookbooksSource" }
        },
        "ServiceRoleArn": {"Fn::GetAtt" : ["EnvServiceRole", "Arn"] },
        "DefaultInstanceProfileArn": {"Fn::GetAtt" : ["EnvInstanceProfile", "Arn"] },
        "DefaultOs": "Amazon Linux 2016.03",
        "DefaultSshKeyName": { "Ref": "KeyName" },
        "DefaultRootDeviceType": "ebs",
        "DefaultSubnetId": { "Ref": "AppSubnetA" },
        "HostnameTheme": "Layer_Dependent",
        "UseCustomCookbooks": "true",
        "VpcId": { "Ref": "VPCID" }
      }
    },
    "EnvApplicationLayerLoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme": "internet-facing",
        "ConnectionSettings": { "IdleTimeout": "300" },
        "HealthCheck": {
          "HealthyThreshold": "5",
          "Interval": "60",
          "Target": "HTTP:80/__IN_SERVICE__",
          "Timeout": "10",
          "UnhealthyThreshold": "3"
        },
        "LBCookieStickinessPolicy": [
          {
            "CookieExpirationPeriod": "28800",
            "PolicyName": "DefaultSessionTimeout"
          }
        ],
        "Listeners": [
          {
            "LoadBalancerPort": "443",
            "Protocol": "HTTPS",
            "InstancePort": "80",
            "InstanceProtocol": "HTTP",
            "PolicyNames": [ "DefaultSessionTimeout" ],
            "SSLCertificateId": {
              "Fn::Join": [ "", 
                [ "arn:aws:iam::", { "Ref": "AWS::AccountId" }, ":server-certificate/", { "Ref": "SSLCertName" } ] 
              ]
            }
          }
        ],
        "LoadBalancerName": { 
          "Fn::Join": [ "-", [ { "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "app-lb" ] ]
        },
        "SecurityGroups": [ { "Ref": "LoadBalancerSecurityGroup" } ],
        "Subnets": [
          { "Ref": "LBSubnetA" },
          { "Ref": "LBSubnetB" }
        ],
        "Tags" :  [
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "-lb" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
               ]
      }
    },
    "InstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Allow Load Balancer and SSH to client host",
        "VpcId" : {"Ref" : "VPCID"},
        "SecurityGroupIngress" : [
            {
              "IpProtocol" : "tcp",
              "FromPort" : "22",
              "ToPort" : "22",
              "CidrIp" : "0.0.0.0/0"
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "80",
              "ToPort" : "80",
              "SourceSecurityGroupId" :  {"Ref" : "LoadBalancerSecurityGroup"}
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "8080",
              "ToPort" : "8080",
              "SourceSecurityGroupId" :  {"Ref" : "LoadBalancerSecurityGroup"}
            }
            ],
        "Tags" :  [
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "instance-sg" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
               ]
      }
    },
    "LoadBalancerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Allow web traffic to the Load Balancer",
        "VpcId" : {"Ref" : "VPCID"},
        "SecurityGroupIngress" : [
            { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0" },
            { "IpProtocol" : "tcp", "FromPort" : "8080", "ToPort" : "8080", "CidrIp" : "0.0.0.0/0" },
            { "IpProtocol" : "tcp", "FromPort" : "443", "ToPort" : "443", "CidrIp" : "0.0.0.0/0" }
            ],
        "Tags" :  [ 
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "load-balancer-sg" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
              ]
      }
    },
    "DBSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Allow DB traffic from Application Instances",
        "VpcId" : {"Ref" : "VPCID"},
        "SecurityGroupIngress" : [
            { "IpProtocol" : "tcp", "FromPort" : "1521", "ToPort" : "1521",
              "SourceSecurityGroupId" :  {"Ref" : "InstanceSecurityGroup"} }
            ],
        "Tags" :  [ 
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "database-sg" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
              ]
      }
    }

  },
  "Outputs": {
    "OPWStackId":                   { "Value": { "Ref": "EnvStack" } },
    "OPWInstanceRole":              { "Value": { "Ref": "EnvInstanceRole" } },
    "OPWInstanceSecurityGroup":     { "Value": { "Ref": "InstanceSecurityGroup" } },
    "OPWDBSecurityGroup":           { "Value": { "Fn::GetAtt": [ "DBSecurityGroup", "GroupId" ] } },
    "OPWLoadBalancerSecurityGroup": { "Value": { "Ref": "LoadBalancerSecurityGroup" } },
    "OPWLoadBalancerId":            { "Value": { "Ref": "EnvApplicationLayerLoadBalancer" } },
    "OPWLoadBalancerDNS":           { "Value": {"Fn::GetAtt" : ["EnvApplicationLayerLoadBalancer", "DNSName"] } }
  }
}
